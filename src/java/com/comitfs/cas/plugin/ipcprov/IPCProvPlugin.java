package com.comitfs.cas.plugin.ipcprov;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.webapp.WebAppContext;
import org.jivesoftware.openfire.cluster.ClusterManager;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.util.JiveGlobals;
import org.jivesoftware.util.StringUtils;

import com.comitfs.cas.plugin.ipcprov.core.ReloadUsersScheduler;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.dataservice.LoadUsersForAllSitesThread;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvConstants;


public class IPCProvPlugin implements Plugin {
	private final Logger logger = Logger.getLogger(IPCProvPlugin.class);
	public static final String PLUGIN_NAME = "ipcprov";
	public static final String IPC_PLUGIN_NAME = "ipc";
	private File pluginDirectory = null;
	private static final String CUSTOM_AUTH_FILTER_PROPERTY_NAME = "plugin.ipcprov.customAuthFilter";
	public static final String SERVICE_LOGGING_ENABLED = "plugin.ipcprov.serviceLoggingEnabled";
	ExecutorService service = Executors.newFixedThreadPool(1);

	/** The secret. */
	private String secret;

	/** The allowed i ps. */
	private Collection<String> allowedIPs;

	/** The enabled. */
	private boolean enabled;

	public boolean isServiceLoggingEnabled() {
		return serviceLoggingEnabled;
	}

	public void setServiceLoggingEnabled(boolean serviceLoggingEnabled) {
		JiveGlobals.setProperty(SERVICE_LOGGING_ENABLED, Boolean.toString(serviceLoggingEnabled));
		this.serviceLoggingEnabled = serviceLoggingEnabled;
	}

	private boolean serviceLoggingEnabled;

	/** The http auth. */
	private String httpAuth;

	/** The custom authentication filter */
	private String customAuthFilterClassName;

	@Override
	public void initializePlugin(PluginManager pluginManager, File file) {
		logger.info("Initializing IPC IPCProvPlugin ....");
		this.pluginDirectory = file;
		secret = JiveGlobals.getProperty("plugin.ipcprov.secret", "");
		// If no secret key has been assigned, assign a random one.
		if ("".equals(secret)) {
			secret = StringUtils.randomString(16);
			setSecret(secret);
		}

		// See if Custom authentication filter has been defined
		customAuthFilterClassName = JiveGlobals.getProperty("plugin.ipcprov.customAuthFilter", "");

		// See if the service is enabled or not.
		enabled = JiveGlobals.getBooleanProperty("plugin.ipcprov.enabled", true);

		// See if the HTTP Basic Auth is enabled or not.
		httpAuth = JiveGlobals.getProperty("plugin.ipcprov.httpAuth", "basic");

		// Get the list of IP addresses that can use this service. An empty list
		// means that this filter is disabled.
		allowedIPs = StringUtils.stringToCollection(JiveGlobals.getProperty("plugin.ipcprov.allowedIPs", ""));

		setServiceLoggingEnabled(JiveGlobals.getBooleanProperty(SERVICE_LOGGING_ENABLED, false));
		// Listen to system property events
		createWebAppContext();
		logger.debug("cluster status .... " + ClusterManager.isClusteringEnabled() +" is senior "+ClusterManager.isSeniorClusterMember());
		
		boolean reloadScheduler = JiveGlobals.getBooleanProperty("plugin.ipcprov.enabled.reload.preferences", true);
		boolean loadPreferences = JiveGlobals.getBooleanProperty("plugin.ipcprov.enabled.preferences", true);
		if(loadPreferences) {
			new Thread(() -> {
				//Scheduler to trigger reload of user preferences
				if(reloadScheduler)
				{
					ReloadUsersScheduler reloadUsersScheduler = new ReloadUsersScheduler();
					reloadUsersScheduler.start();
				}
				if(ClusterManager.isSeniorClusterMemberOrNotClustered()) {
					logger.info("[IPCProvPlugin] Loading user preferences ....");
					JiveGlobals.setProperty("plugin.ipcprov.loading.preferences.done", String.valueOf(false));
					service.execute(new LoadUsersForAllSitesThread());
				}else {
					boolean loadingPreferencesDone = JiveGlobals.getBooleanProperty("plugin.ipcprov.loading.preferences.done", false);
					logger.info("[IPCProvPlugin] Loading user preferences in junior ...."+loadingPreferencesDone);
					while(!loadingPreferencesDone)
					{
						logger.info("[IPCProvPlugin] waiting for senior user preferences loading to complete ....");
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							logger.error("Error while waiting ", e);
						}
						loadingPreferencesDone = JiveGlobals.getBooleanProperty("plugin.ipcprov.loading.preferences.done", false);
						
					}
					IPCDataService.getInstance().updateLocalMap();
					JiveGlobals.setProperty("plugin.ipcprov.loading.preferences.done", String.valueOf(false));
				}
			}).start();
		}

	}

	@Override
	public void destroyPlugin() {

	}

	private void createWebAppContext() {
		try {
			logger.info("[IPCProvPlugin] createWebAppContext - Creating web service ");

			ServletContextHandler contexts = new WebAppContext(null, pluginDirectory.getPath(), "/ipcprov");
			contexts.setClassLoader(this.getClass().getClassLoader());

			try {
				contexts.setWelcomeFiles(new String[] { "index.html" });
			} catch (Exception e) {
				logger.info("Error while initializing component for IPCProv Plugin", e);
			}
		} catch (Exception e) {
			logger.error("Error creating webapp context", e);
		}
	}

	/**
	 * Returns the secret key that only valid requests should know.
	 *
	 * @return the secret key.
	 */
	public String getSecret() {
		return secret;
	}

	/**
	 * Sets the secret key that grants permission to use the userservice.
	 *
	 * @param secret
	 *            the secret key.
	 */
	public void setSecret(String secret) {
		JiveGlobals.setProperty("plugin.ipcprov.secret", secret);
		this.secret = secret;
	}

	/**
	 * Gets the allowed i ps.
	 *
	 * @return the allowed i ps
	 */
	public Collection<String> getAllowedIPs() {
		return allowedIPs;
	}

	/**
	 * Sets the allowed i ps.
	 *
	 * @param allowedIPs the new allowed i ps
	 */
	public void setAllowedIPs(Collection<String> allowedIPs) {
		JiveGlobals.setProperty("plugin.ipcprov.allowedIPs", StringUtils.collectionToString(allowedIPs));
		this.allowedIPs = allowedIPs;
	}

	/**
	 * Returns true if the user service is enabled. If not enabled, it will not
	 * accept requests to create new accounts.
	 *
	 * @return true if the user service is enabled.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Enables or disables the user service. If not enabled, it will not accept
	 * requests to create new accounts.
	 *
	 * @param enabled
	 *            true if the user service should be enabled.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		JiveGlobals.setProperty("plugin.ipcprov.enabled", enabled ? "true" : "false");
	}

	/**
	 * Gets the http authentication mechanism.
	 *
	 * @return the http authentication mechanism
	 */
	public String getHttpAuth() {
		return httpAuth;
	}

	/**
	 * Sets the http auth.
	 *
	 * @param httpAuth the new http auth
	 */
	public void setHttpAuth(String httpAuth) {
		this.httpAuth = httpAuth;
		JiveGlobals.setProperty("plugin.ipcprov.httpAuth", httpAuth);
	}

	/* (non-Javadoc)
	 * @see org.jivesoftware.util.PropertyEventListener#propertySet(java.lang.String, java.util.Map)
	 */
	public void propertySet(String property, Map<String, Object> params) {
		if (property.equals("plugin.ipcprov.secret")) {
			this.secret = (String) params.get("value");
		} else if (property.equals("plugin.ipcprov.enabled")) {
			this.enabled = Boolean.parseBoolean((String) params.get("value"));
		} else if (property.equals("plugin.ipcprov.allowedIPs")) {
			this.allowedIPs = StringUtils.stringToCollection((String) params.get("value"));
		} else if (property.equals("plugin.ipcprov.httpAuth")) {
			this.httpAuth = (String) params.get("value");
		} else if(property.equals(CUSTOM_AUTH_FILTER_PROPERTY_NAME)) {
			this.customAuthFilterClassName = (String) params.get("value");
		}
	}

	/* (non-Javadoc)
	 * @see org.jivesoftware.util.PropertyEventListener#propertyDeleted(java.lang.String, java.util.Map)
	 */
	public void propertyDeleted(String property, Map<String, Object> params) {
		if (property.equals("plugin.ipcprov.secret")) {
			this.secret = "";
		} else if (property.equals("plugin.ipcprov.enabled")) {
			this.enabled = false;
		} else if (property.equals("plugin.ipcprov.allowedIPs")) {
			this.allowedIPs = Collections.emptyList();
		} else if (property.equals("plugin.ipcprov.httpAuth")) {
			this.httpAuth = "basic";
		} else if(property.equals(CUSTOM_AUTH_FILTER_PROPERTY_NAME)) {
			this.customAuthFilterClassName = null;
		}
	}
//	private void initializeLogger() {
//		try {
//			String pattern = JiveGlobals.getProperty(IPCProvConstants.LOG_PATTERN, DEFAULT_CONVERSION_PATTERN);
//			String logLevel = JiveGlobals.getProperty(IPCProvConstants.LOG_LEVEL, DEFAULT_LOG_LEVEL);
//			boolean logAppend = JiveGlobals.getBooleanProperty(IPCProvConstants.LOG_APPEND, IPCPROV_LOG_APPEND);
//			String maxFileSize = JiveGlobals.getProperty(IPCProvConstants.LOG_MAX_FILE_SIZE, IPCPROV_LOG_MAX_FILE_SIZE);
//			String maxBackupIndex = JiveGlobals.getProperty(IPCProvConstants.LOG_MAX_BACKUP_FILES, IPCPROv_LOG_MAX_BACKUP_INDEX);
//
//			if ("ALL".equals(logLevel) || "DEBUG".equals(logLevel)) {
//				pattern = JiveGlobals.getProperty(IPCProvConstants.LOG_PATTERN, DEFAULT_DEBUG_CONVERSION_PATTERN);
//			}
//			RollingFileAppender appender = new RollingFileAppender(new PatternLayout(pattern), org.jivesoftware.util.Log.getLogDirectory() + IPCProvPlugin.PLUGIN_NAME + ".log", logAppend);
//			appender.setMaxFileSize(maxFileSize);
//			appender.setMaxBackupIndex(Integer.parseInt(maxBackupIndex));
//
//			logger = Logger.getLogger("com.comitfs.cas.plugin.ipc.prov");
//			logger.setLevel(Level.toLevel(logLevel, Level.toLevel(DEFAULT_LOG_LEVEL)));
//			logger.setAdditivity(false);
//			logger.addAppender(appender);
//		} catch (Exception e) {
//			logger.error("Can not initialize IPC logger ", e);
//		}
//	}
}
