package com.comitfs.cas.plugin.ipcprov.session;

import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.net.NoRouteToHostException;

public class SessionManager {
    private static final String SERVER_INTNL_ERR = "SERVER_INTERNAL_ERROR";
    private static final String AUTHENTICATION_ERROR = "AUTHENTICATION_ERROR";
    private final Logger log = Logger.getLogger(SessionManager.class);
    private final IPCSite ipcSite;
    private final String forLog;
    private WebAPIUser webAPIUser;
    private AccountManagementUser accountManagementUser;
    private String ipcAuthToken = null;

    private BWPoolConnection bwConnection;

    private boolean isServiceUnavailable;

    public SessionManager(WebAPIUser webAPIUser, IPCSite ipcSite, BWPoolConnection bwConn) {
        this.webAPIUser = webAPIUser;
        this.ipcSite = ipcSite;
        this.bwConnection = bwConn;
        this.forLog = String.format("[%s][%s] ", ipcSite.getName(), bwConnection.getHost());
        createSession(webAPIUser);
    }

    public SessionManager(AccountManagementUser accountManagementUser, IPCSite ipcSite) {
        this.accountManagementUser = accountManagementUser;
        this.ipcSite = ipcSite;
        this.forLog = String.format("[%s] ", ipcSite.getName());
        createSessionForAcctMgmt(accountManagementUser);
    }

    private void createSession(WebAPIUser user) {
        log.info(forLog + "##### Creating WebAPI session for user " + user.getUserId() + " Zone: " + bwConnection.getHost() + " Port is " + bwConnection.getPort());
        try {
            String resourceUrl = IPCUtil.formURI(bwConnection, IPCRESTAPI.SESSION_API.toString(), null, ipcSite.isSecure());

            log.debug(forLog + "CreateSession uri: " + resourceUrl);

            final String httpAuthorization = IPCUtil.generateHttpAuthorization(user.getUserId(), user.getPassword());

            CreateSession createSession = new CreateSession();

            SessionInformation sessionInfo = new SessionInformation();
            sessionInfo.setClientType(ClientTypes.CTI);
            createSession.setSessionInfo(sessionInfo);

            Response response = IPCProvHttpClient.invokeHTTPPost(resourceUrl, httpAuthorization, null, ipcSite.getApiVersion(), createSession, ipcSite.isSecure());
            log.debug(forLog + "create session response: " + response);

            Object respEntity = null;

            String apiRes = "";
            if (response != null && !isServiceUnavailable(response)) {
                apiRes = response.readEntity(String.class);
                JAXBContext jaxbContext = UtilHelper.getJAXBContext();
                respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));
            }

            if (respEntity == null) {
                return;
            }


            if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error(forLog + "CreateSession error response for Site Name: " + ipcSite.getName() + " Zone: " + bwConnection.getHost() + " : " + formattedResponseXML);
            }

            if (respEntity instanceof CreateSessionResponse) {

                CreateSessionResponse createSessionResp = (CreateSessionResponse) respEntity;

                String createSessionRespString = UtilHelper.marshalJAXBObjectForResponse(createSessionResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(createSessionRespString);
                log.debug(forLog + "Create session response for Site Name : " + " Zone: " + bwConnection.getHost() + " is:" + formattedResponseXML);

                ReasonCodes reasonCode = createSessionResp.getReason().getReasonCode();
                String reasonDescription = createSessionResp.getReason().getReasonDescription();
                if (reasonCode.equals(SERVER_INTNL_ERR)) {
                    log.debug(forLog + "Create session failed for zone: " + bwConnection.getHost() + " Reason: " + reasonCode + "/" + reasonDescription);
                    return;
                }

                if (reasonCode.equals(AUTHENTICATION_ERROR)) {
                    log.debug(forLog + "#Create session failed for zone: " + bwConnection.getHost() + " Reason: " + reasonCode + "/" + reasonDescription);
                    return;
                }

                ipcAuthToken = createSessionResp.getAuthorizationInfo().getAuthenticationToken();
                log.debug(forLog + String.format("user: %s ipcAuthenticationToken: %s", user.getUserId(), ipcAuthToken));

            }
        } catch (NoRouteToHostException noRouteEx) {
            log.error(forLog + "###BW ZONE DOWN Create Session Failed for Zone: " + bwConnection.getHost() + " - Exception Encountered - ", noRouteEx);
        } catch (Exception ex) {
            log.error(forLog + "Create Session Failed for Zone: " + bwConnection.getHost() + " - Exception Encountered", ex);
        }
    }

    public String getIpcProvAuthToken() {
        return ipcAuthToken;
    }

    private void createSessionForAcctMgmt(AccountManagementUser user) {
        log.debug(forLog + "Creating session for acct mgmt for user " + user.getUserId());
        try {
            //TODO shd acct mgt session be created specific to where the user belongs?(on whom provisioning is being invoked)
            String resourceUrl = IPCUtil.formAPIPURL(ipcSite, IPCRESTAPI.SESSION_API.toString(), ipcSite.isSecure());

            final String httpAuthorization = IPCUtil.generateHttpAuthorization(user.getUserId(), user.getPassword());

            log.debug(forLog + "resourceUrl for creating session for end-user acct mgt: " + resourceUrl);

            CreateSession createSession = new CreateSession();
            SessionInformation sessionInfo = new SessionInformation();
            sessionInfo.setClientType(ClientTypes.ENDUSERACCTMGMT);
            createSession.setSessionInfo(sessionInfo);

            Response response = IPCProvHttpClient.invokeHTTPPost(resourceUrl, httpAuthorization,
                    null, ipcSite.getApiVersion(), createSession, ipcSite.isSecure());
            log.debug(forLog + "create end-user acct mgt session response: " + response);
            String apiRes = response.readEntity(String.class);
            final JAXBContext jaxbContext = UtilHelper.getJAXBContext();

            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error(forLog + "Response Entity for creating Account Management session is null ");
            }

            if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error(forLog + "Create Session error response for Account Management: " + formattedResponseXML);
            } else if (respEntity instanceof CreateSessionResponse) {
                CreateSessionResponse createSessionResp = (CreateSessionResponse) respEntity;
                String createSessionRespString = UtilHelper.marshalJAXBObjectForResponse(createSessionResp);

                log.debug(forLog + "create session for end-user acct mgt response: " + createSessionRespString);
                ipcAuthToken = createSessionResp.getAuthorizationInfo().getAuthenticationToken();
            }
        } catch (Exception ex) {
            log.error(forLog + "Account Management Create Session Failed ", ex);
        }
    }

    public String deleteSession(String httpAuth, String ipcAuthToken, String apiVersion) {
        log.debug(forLog + "delete Acct mgmt session ipcAuthToken:  " + ipcAuthToken + " for Site: " + ipcSite.getName());
        try {

            String resourceUrl = IPCUtil.formAPIPURL(ipcSite, IPCRESTAPI.SESSION_API.toString(), ipcSite.isSecure());

            log.debug(forLog + " resourceUrl for delete session : " + resourceUrl);

            Response response = IPCProvHttpClient.invokeHTTPDelete(resourceUrl, httpAuth, ipcAuthToken, apiVersion, ipcSite.isSecure());
            String apiRes = response.readEntity(String.class);
            final JAXBContext jaxbContext = UtilHelper.getJAXBContext();

            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error(forLog + "Response Entity for deleting session is null for Site: " + ipcSite.getName());
            }

            if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error(forLog + "DeleteSession error response for Site: " + ipcSite.getName() + formattedResponseXML);
            } else if (respEntity instanceof DeleteSessionResponse) {
                DeleteSessionResponse deleteSessionResp = (DeleteSessionResponse) respEntity;
                String deleteSessionRespString = UtilHelper.marshalJAXBObjectForResponse(deleteSessionResp);
                log.debug(forLog + "DeleteSession  response for Site: " + ipcSite.getName() + deleteSessionRespString);
            }
        } catch (Exception e) {
            log.error(forLog + "Delete Session Failed for Site: " + ipcSite.getName() + " - Exception Encountered - ", e);
        }
        return null;
    }

    private String deleteSessionForWebApiUser(String httpAuth, String ipcAuthToken, String apiVersion) {
        try {
            String resourceUrl = IPCUtil.formURI(this.bwConnection, IPCRESTAPI.SESSION_API.toString(), null, ipcSite.isSecure());

            log.debug(forLog + "resourceUrl for delete session : " + resourceUrl);

            Response response = IPCProvHttpClient.invokeHTTPDelete(resourceUrl, httpAuth, ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = response.readEntity(String.class);
            final JAXBContext jaxbContext = UtilHelper.getJAXBContext();

            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error(forLog + "Response Entity for delete session is null for Site: " + ipcSite.getName());
            }

            if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error(forLog + "DeleteSession error response: " + formattedResponseXML);
            } else if (respEntity instanceof DeleteSessionResponse) {
                DeleteSessionResponse deleteSessionResp = (DeleteSessionResponse) respEntity;
                String deleteSessionRespString = UtilHelper.marshalJAXBObjectForResponse(deleteSessionResp);
                log.debug(forLog + "delete session response: " + deleteSessionRespString);
            }
        } catch (Exception e) {
            log.error(forLog + "Delete Session Failed - Exception Encountered", e);
        }
        return null;
    }

    private boolean isServiceUnavailable(Response response) {
        try {
            if (response != null) {
                isServiceUnavailable = response.getStatus() == 503;
            }
        } catch (Exception e) {
            log.error("Error checking clientresp status on createsession request: " + e.getMessage());
        }
        return isServiceUnavailable;
    }
}
