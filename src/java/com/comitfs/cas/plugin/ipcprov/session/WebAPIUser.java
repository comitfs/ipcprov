package com.comitfs.cas.plugin.ipcprov.session;

public class WebAPIUser implements SessionUser {

    private final String password;
    private final String userId;

    public WebAPIUser(String userId, String password) {
        this.userId = userId;
        this.password = password;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public String getPassword() {
        return password;
    }
}
