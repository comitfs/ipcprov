package com.comitfs.cas.plugin.ipcprov.session;


public class AccountManagementUser implements SessionUser {

    private final String password;
    private final String userId;

    public AccountManagementUser(String userId, String password) {
        this.password = password;
        this.userId = userId;
    }

    @Override
    public String getUserId() {
        return userId;
    }


    public String getPassword() {
        return password;
    }
}
