package com.comitfs.cas.plugin.ipcprov.session;

public interface SessionUser {
    String getUserId();
}
