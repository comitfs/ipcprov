package com.comitfs.cas.plugin.ipcprov.util;

import com.comitfs.cas.ipc.bw.jaxb.HandsetSideTypes;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import org.apache.log4j.Logger;
import org.jivesoftware.util.Base64;
import org.jivesoftware.util.JiveGlobals;

import javax.net.ssl.*;
import java.security.cert.CertificateException;

public class IPCUtil {

    private static final Logger LOG = Logger.getLogger(IPCUtil.class);

    public static String generateHttpAuthorization(String userName, String password) {
        final String authFormat = userName + ":" + password;
        final String authStringEnc = "Basic " + Base64.encodeBytes(authFormat.getBytes());
        return authStringEnc.trim();
    }

    //create acct mgt session in default zone
    public static String createURI(IPCSite ipcSite, String path) {
        BWPoolConnection bwZone = new BWZoneSelector(ipcSite).getActiveBWZone();
        return "http://" + bwZone.getHost() + ":" + bwZone.getPort() + path;
    }

    //if site level port setting is 80, this fails. For some Acct mgt invocations, its always secure
    public static String createSecureURI(IPCSite ipcSite, String path) {
        BWPoolConnection defaultZone = new BWZoneSelector(ipcSite).getActiveBWZone();
        return "https://" + defaultZone.getHost() + ":" + defaultZone.getPort() + path;
    }

    public static String createURI(BWPoolConnection bwPoolConnection, String path, String reqParams) {
        return "http://" + bwPoolConnection.getHost() + ":" + bwPoolConnection.getPort() + path + (reqParams != null ? reqParams : "");
    }

    public static String createSecureURI(BWPoolConnection bwPoolConnection, String path, String reqParams) {
        return "https://" + bwPoolConnection.getHost() + ":" + bwPoolConnection.getPort() + path + (reqParams != null ? reqParams : "");
    }

    public static HostnameVerifier getHostNameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        };
    }

    public static String formURI(BWPoolConnection bwPoolConnection, String path, String params, boolean isSecure) throws Exception {
        try {
            if (isSecure) {
                return createSecureURI(bwPoolConnection, path, params);
            }
            return createURI(bwPoolConnection, path, params);

        } catch (Exception e) {
            LOG.error("Failed creating Uri", e);
            return null;
        }
    }

    public static String formAPIPURL(IPCSite ipcSite, String path, boolean isSecure) throws Exception {
        try {
            if (isSecure) {
                return createSecureURI(ipcSite, path);
            }
            return createURI(ipcSite, path);

        } catch (Exception e) {
            LOG.error("Failed creating Uri", e);
            return null;
        }
    }

    public static SSLContext getSSLContext() {
        try {
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(
                                final java.security.cert.X509Certificate[] arg0, final String arg1)
                                throws CertificateException {
                            // do nothing and blindly accept the certificate
                        }

                        public void checkServerTrusted(
                                final java.security.cert.X509Certificate[] arg0, final String arg1)
                                throws CertificateException {
                            // do nothing and blindly accept the server
                        }
                    }
            };

            SSLContext sslcontext = SSLContext.getInstance("TLSv1.2");
            sslcontext.init(null, trustAllCerts, new java.security.SecureRandom());
            return sslcontext;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static float calculatePagesToRequest(Integer availableRecords) {
        float pages = 0;
        try {
            int maxRecordsPerPage = JiveGlobals.getIntProperty(IPCProvConstants.MAX_RECORDS_IN_RESPONSE, 400);
            if (availableRecords > maxRecordsPerPage) {
                pages = (float) availableRecords / maxRecordsPerPage;
            }
        } catch (Exception e) {
            LOG.error("Error getting calculated number pages to request: ", e);
        }
        return pages;
    }

    public static float calculatePagesToRequestPerCount(Integer availableRecords, Integer count) {
        float pages = 0;
        try {
            if (availableRecords > count) {
                pages = availableRecords / count;
            }
        } catch (Exception e) {
            LOG.error("Error getting calculated number pages to request: ", e);
        }
        return pages;
    }
}
