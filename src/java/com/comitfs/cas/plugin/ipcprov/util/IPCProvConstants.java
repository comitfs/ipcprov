package com.comitfs.cas.plugin.ipcprov.util;

public interface IPCProvConstants {
    String IPC_PLUGIN_HTTP_CONN_TIMEOUT = "plugin.cas.ipc.http.connection.timeout";

    String IPC_PLUGIN_HTTP_READ_TIMEOUT = "plugin.cas.ipc.http.read.timeout";

    String PLUGIN_IPC_LOG_VERBOSE = "plugin.cas.ipc%s.log.verbose";

    String MAX_RECORDS_IN_RESPONSE = "plugin.cas.ipc.max.records.in.response";

    String IPC_PLUGIN_SITE_CONFIG_LOCATION = "plugin.cas.ipc.site.config.location";

    String MSSEC_SCV_NS = "plugin.cas.ipc%s.mssec.scv.namespace";

    String MSSEC_SCV_WEBAPI_PASSWORD_KEY = "plugin.cas.ipc%s.mssec.scv.webapi.password.key";

    String MSSEC_SCV_ACCMGMT_PASSWORD_KEY = "plugin.cas.ipc%s.mssec.scv.accmgmt.password.key";

}
