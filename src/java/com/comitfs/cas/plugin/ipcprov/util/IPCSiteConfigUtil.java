package com.comitfs.cas.plugin.ipcprov.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;

import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IPCSiteConfigUtil {

    public static final String IPC_PLUGIN_SITE_CONFIG_JSON = "ipc-site-config.json";
    private static final Logger LOG = Logger.getLogger(IPCSiteConfigUtil.class);

    public static List<IPCSite> connectionsFromJsonConfig() {
        List<IPCSite> sites = new ArrayList<IPCSite>();
        try {
            String configFilePath = getConfigFile();
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(new File(configFilePath + IPC_PLUGIN_SITE_CONFIG_JSON));
            } catch (FileNotFoundException e) {
                LOG.warn("IPC Site connection configuration file not found at location: " + configFilePath);
                createDefaultConfigFile();
                fis = new FileInputStream(new File(configFilePath + IPC_PLUGIN_SITE_CONFIG_JSON));
            }
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
            IPCSiteConfigUtilBean siteBeans = mapper.readValue(fis, IPCSiteConfigUtilBean.class);
            if (siteBeans != null && siteBeans.getIpcSites() != null) {
                sites.addAll(siteBeans.getIpcSites());
            }
        } catch (Throwable e) {
            LOG.error("Error creating IPC config bean from config file ", e);
        }
        return sites;
    }

    private static String getConfigFile() {
        String path;
        try {
            String defaultPath = defaultPath();
            path = JiveGlobals.getProperty(IPCProvConstants.IPC_PLUGIN_SITE_CONFIG_LOCATION, defaultPath);
            if (!path.endsWith(File.separator)) {
                path = path + File.separator;
            }
        } catch (Exception e) {
            LOG.error("Error in get Config File: ", e);
            return null;
        }
        return path;
    }

    public static String defaultPath() {
        return JiveGlobals.getHomeDirectory() + File.separator
                + "conf" + File.separator
                + "plugins" + File.separator
                + "ipc" + File.separator;
    }

    private static void createDefaultConfigFile() {
        try {
            LOG.debug("Creating default configuration file for IPC Site config..");
            toConfigJson(new ArrayList<IPCSite>());
            File jsonFile = new File(getConfigFile() + IPC_PLUGIN_SITE_CONFIG_JSON);
            if (null != jsonFile.getParentFile()) {
                jsonFile.getParentFile().mkdirs();
            }
            jsonFile.createNewFile();
        } catch (Exception e) {
            LOG.error("Unable to create default IPC connection configuration file ", e);
        }
    }

    public static void toConfigJson(List<IPCSite> ipcSites) {
        try {
            IPCSiteConfigUtilBean ipcSiteConfigUntilBean = new IPCSiteConfigUtilBean();
            ipcSiteConfigUntilBean.setIpcSites(ipcSites);
            ObjectMapper mapper = new ObjectMapper();

            File jsonFile = new File(getConfigFile() + IPC_PLUGIN_SITE_CONFIG_JSON);
            if (null != jsonFile.getParentFile()) {
                jsonFile.getParentFile().mkdirs();
            }
            jsonFile.createNewFile();
            mapper.writerWithDefaultPrettyPrinter().writeValue(jsonFile, ipcSiteConfigUntilBean);
        } catch (Exception e) {
            LOG.error("Error updating IPC Site json config ", e);
        }
    }

  
}
