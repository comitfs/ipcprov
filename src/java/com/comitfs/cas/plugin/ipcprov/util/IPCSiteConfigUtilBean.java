package com.comitfs.cas.plugin.ipcprov.util;


import java.util.List;

import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.fasterxml.jackson.annotation.JsonProperty;

public class IPCSiteConfigUtilBean {

    @JsonProperty("ipcSites")
    private List<IPCSite> ipcSites;

    public List<IPCSite> getIpcSites() {
        return ipcSites;
    }

    public void setIpcSites(List<IPCSite> ipcSites) {
        this.ipcSites = ipcSites;
    }
}
