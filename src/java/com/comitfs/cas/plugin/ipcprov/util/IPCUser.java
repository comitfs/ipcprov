package com.comitfs.cas.plugin.ipcprov.util;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.NoArgsConstructor;


@NoArgsConstructor
@XmlRootElement(name="ipcusers")
@JsonIgnoreProperties
public class IPCUser implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  String userId;
    @JsonIgnore
    private Integer ipcId;
    @JsonIgnore
    private String userName;
    @JsonIgnore
    private String deviceIpAdd;
    private String deviceModel;
    //@JsonIgnore
    private String personalExtn;
    @JsonIgnore
    private Integer deviceId;
    @JsonIgnore
    private String zoneId;
    @JsonIgnore
    private Integer userCDIId;
    
    private Boolean turretRecordingEnabled;
    private Integer turretrecordMixProfileId = 0;
    private String turretrecordMixProfileName;
    private String turretRecordingProtocol;
    
    private Boolean pulseRecordingEnabled;
    private Integer pulserecordMixProfileId = 0;
    private String pulserecordMixProfileName;
    private String pulseRecordingProtocol;
    
    private Boolean iqMaxTouchRecordingEnabled;
    private Integer iqMaxTouchrecordMixProfileId = 0;
    private String iqMaxTouchrecordMixProfileName;
    private String iqMaxTouchRecordingProtocol;
    
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@JsonIgnore
	public Integer getIPCId() {
		return ipcId;
	}
	@JsonIgnore
	public void setIPCId(Integer iPCId) {
		ipcId = iPCId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getUserCDIId() {
		return userCDIId;
	}
	public void setUserCDIId(Integer userCDIId) {
		this.userCDIId = userCDIId;
	}
	public Boolean getTurretRecordingEnabled() {
		return turretRecordingEnabled;
	}
	public void setTurretRecordingEnabled(Boolean turretRecordingEnabled) {
		this.turretRecordingEnabled = turretRecordingEnabled;
	}
	public Boolean getPulseRecordingEnabled() {
		return pulseRecordingEnabled;
	}
	public void setPulseRecordingEnabled(Boolean pulseRecordingEnabled) {
		this.pulseRecordingEnabled = pulseRecordingEnabled;
	}
	
	public String getDeviceIpAdd() {
		return deviceIpAdd;
	}
	public void setDeviceIpAdd(String deviceIpAdd) {
		this.deviceIpAdd = deviceIpAdd;
	}
	public Integer getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getPersonalExtn() {
		return personalExtn;
	}
	public void setPersonalExtn(String personalExtn) {
		this.personalExtn = personalExtn;
	}
	public String getDeviceModel() {
		return deviceModel;
	}
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	public Integer getTurretrecordMixProfileId() {
		return turretrecordMixProfileId;
	}
	public void setTurretrecordMixProfileId(Integer turretrecordMixProfileId) {
		this.turretrecordMixProfileId = turretrecordMixProfileId;
	}
	public String getTurretrecordMixProfileName() {
		return turretrecordMixProfileName;
	}
	public void setTurretrecordMixProfileName(String turretrecordMixProfileName) {
		this.turretrecordMixProfileName = turretrecordMixProfileName;
	}
	public Integer getPulserecordMixProfileId() {
		return pulserecordMixProfileId;
	}
	public void setPulserecordMixProfileId(Integer pulserecordMixProfileId) {
		this.pulserecordMixProfileId = pulserecordMixProfileId;
	}
	public String getPulserecordMixProfileName() {
		return pulserecordMixProfileName;
	}
	public void setPulserecordMixProfileName(String pulserecordMixProfileName) {
		this.pulserecordMixProfileName = pulserecordMixProfileName;
	}

	public Boolean getIqMaxTouchRecordingEnabled() {
		return iqMaxTouchRecordingEnabled;
	}
	public void setIqMaxTouchRecordingEnabled(Boolean iqMaxTouchRecordingEnabled) {
		this.iqMaxTouchRecordingEnabled = iqMaxTouchRecordingEnabled;
	}
	public Integer getIqMaxTouchrecordMixProfileId() {
		return iqMaxTouchrecordMixProfileId;
	}
	public void setIqMaxTouchrecordMixProfileId(Integer iqMaxTouchrecordMixProfileId) {
		this.iqMaxTouchrecordMixProfileId = iqMaxTouchrecordMixProfileId;
	}
	public String getIqMaxTouchrecordMixProfileName() {
		return iqMaxTouchrecordMixProfileName;
	}
	public void setIqMaxTouchrecordMixProfileName(String iqMaxTouchrecordMixProfileName) {
		this.iqMaxTouchrecordMixProfileName = iqMaxTouchrecordMixProfileName;
	}
	public String getPulseRecordingProtocol() {
		return pulseRecordingProtocol;
	}
	public void setPulseRecordingProtocol(String pulseRecordingProtocol) {
		this.pulseRecordingProtocol = pulseRecordingProtocol;
	}
	public String getIqMaxTouchRecordingProtocol() {
		return iqMaxTouchRecordingProtocol;
	}
	public void setIqMaxTouchRecordingProtocol(String iqMaxTouchRecordingProtocol) {
		this.iqMaxTouchRecordingProtocol = iqMaxTouchRecordingProtocol;
	}
	public String getTurretRecordingProtocol() {
		return turretRecordingProtocol;
	}
	public void setTurretRecordingProtocol(String turretRecordingProtocol) {
		this.turretRecordingProtocol = turretRecordingProtocol;
	}
	
	
    
    
}