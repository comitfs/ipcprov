package com.comitfs.cas.plugin.ipcprov.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IPCUserDeviceConfig {
	private static final Logger LOG = Logger.getLogger(IPCUserDeviceConfig.class);
	public static IPCUserDeviceConfig ipcUserDeviceConfig = new IPCUserDeviceConfig();
	
	private  IPCUserDeviceConfig() {
		super();
	}
	public static IPCUserDeviceConfig  getInstance() {
		return ipcUserDeviceConfig;
	}
	

    public void ipcUsersFromJsonConfig(String filePath) {
        List<IPCUser> users = IPCDataService.getInstance().getIpcImportUsers();
        try {
            FileInputStream fis = null;
            File userConfigFile = new File(filePath);
            if(userConfigFile.exists() && userConfigFile.canRead()) {
            	fis = new FileInputStream(new File(filePath));
            	ObjectMapper mapper = new ObjectMapper();
            	mapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
            	IPCUserConfig usersBean = mapper.readValue(fis, IPCUserConfig.class);
            	if (usersBean != null && usersBean.getIpcUsers() != null) {
            		users.addAll(usersBean.getIpcUsers());
            	}
            	
            }
        } catch (Throwable e) {
            LOG.error("Error creating IPC user bean from config file ", e);
        }
    }

}
