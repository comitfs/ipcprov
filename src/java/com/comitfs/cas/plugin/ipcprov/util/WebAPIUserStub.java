package com.comitfs.cas.plugin.ipcprov.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;

import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvConstants;

import msjava.base.scv.SecureCredentialsVault;

public class WebAPIUserStub {
    private final String forLog;
    private final Logger log = Logger.getLogger(WebAPIUserStub.class);
    private final IPCSite ipcsite;

    public WebAPIUserStub(IPCSite ipcsite) {
        this.ipcsite = ipcsite;
        this.forLog = String.format("[%s] ", ipcsite.getName());
    }

    public WebAPIUser getCTIWebAPIUser() throws Exception {
        long siteId = ipcsite.getId();

        String apiUser = ipcsite.getApiUser();
        String apiUserPwd = null;
        String msSecSCVNS = JiveGlobals.getProperty(String.format(IPCProvConstants.MSSEC_SCV_NS, siteId));
        if (StringUtils.isNotBlank(msSecSCVNS)) {
            log.info(forLog + "Creating WebAPI session using MS SCV");
            String scvWebAPIPwdKey = JiveGlobals.getProperty(String.format(IPCProvConstants.MSSEC_SCV_WEBAPI_PASSWORD_KEY, siteId));
            if (StringUtils.isBlank(scvWebAPIPwdKey)) {
                throw new Exception("Site configured with Morgan Stanley SCV mechanism without password key");
            }
            SecureCredentialsVault scv = new SecureCredentialsVault();
            char[] pwdChars = scv.getTextCred(msSecSCVNS, scvWebAPIPwdKey);
            apiUserPwd = String.valueOf(pwdChars);
        } else {
            log.info(forLog + "Creating WebAPI session with Site configured ");
            apiUserPwd = ipcsite.getPassword();
        }
        return new WebAPIUser(apiUser, apiUserPwd);
    }
}
