package com.comitfs.cas.plugin.ipcprov.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.jivesoftware.util.JiveGlobals;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

public class IPCProvHttpClient {

    public static final int connectionTimeout;
    public static final int readTimeout;
    private static final Logger LOG = Logger.getLogger(IPCProvHttpClient.class);
    private static final String IPC_VERSION_HEADER = "X-IPCBWAPIVersion";
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String XIPC_AUTHTOKEN_HEADER = "X-IPCAuthToken";

    static {
        connectionTimeout = JiveGlobals.getIntProperty(IPCProvConstants.IPC_PLUGIN_HTTP_CONN_TIMEOUT, 100000);
        readTimeout = JiveGlobals.getIntProperty(IPCProvConstants.IPC_PLUGIN_HTTP_READ_TIMEOUT, 100000);
    }

    public IPCProvHttpClient() {
    }

    private static Client getHttpClient() {
        JerseyClient client = JerseyClientBuilder.createClient();
        client.property(ClientProperties.CONNECT_TIMEOUT, connectionTimeout);
        client.property(ClientProperties.READ_TIMEOUT, readTimeout);
        return client;
    }

    private static Client getHttpsClient() {
        ClientBuilder builder = JerseyClientBuilder.newBuilder()
                .hostnameVerifier(IPCUtil.getHostNameVerifier())
                .sslContext(IPCUtil.getSSLContext())
                .connectTimeout(connectionTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS);
        Client client = builder.build();
        return client;
    }

    public static Response httpPost(String url, String httpAutorization, String ipcAuthToken, String apiVer, Object payload) throws Exception {
        try {
            String header = AUTHORIZATION_HEADER;
            String headerVal = httpAutorization;
            if (ipcAuthToken != null) {
                header = XIPC_AUTHTOKEN_HEADER;
                headerVal = ipcAuthToken;
            }
            String body = UtilHelper.marshalJAXBObjectForRequest(payload);
            Client client = getHttpClient();
            Response response = client.target(url)
                    .request(MediaType.APPLICATION_XML)
                    .header(IPC_VERSION_HEADER, apiVer)
                    .header(header, headerVal)
                    .post(Entity.entity(body, MediaType.APPLICATION_XML_TYPE));
            return response;
        } catch (Throwable t) {
            if (t instanceof ProcessingException && t.getCause() instanceof SocketTimeoutException) {
                LOG.error("Connect time out / Read time out exception in HTTP post request: " + url + " - " + t.getMessage(), t);
            } else {
                LOG.error("Failed invoking HTTP Post request" + url + " - ", t);
            }
            return null;
        }
    }

    public static Response httpsPost(String url, String httpAuthorization, String ipcAuthToken, String apiVer, Object payload) throws Exception {
        try {
            String header = AUTHORIZATION_HEADER;
            String headerVal = httpAuthorization;
            if (ipcAuthToken != null) {
                header = XIPC_AUTHTOKEN_HEADER;
                headerVal = ipcAuthToken;
            }

            Client client = getHttpsClient();
            String body = UtilHelper.marshalJAXBObjectForRequest(payload);
            Response response = client.target(url)
                    .request(MediaType.APPLICATION_XML)
                    .header(IPC_VERSION_HEADER, apiVer)
                    .header(header, headerVal)
                    .post(Entity.entity(body, MediaType.APPLICATION_XML_TYPE));
            return response;
        } catch (Throwable t) {
            if (t instanceof ProcessingException && t.getCause() instanceof SocketTimeoutException) {
                LOG.error("Connect time out / Read time out exception in HTTPS POST request: " + url + " - " + t.getMessage(), t);
            } else {
                LOG.error("Failed invoking HTTPS Post request" + url + " - ", t);
            }
            return null;
        }
    }

    public static Response httpPut(String url, String httpAuthorization, String ipcAuthToken, String apiVer, Object payload) {
        try {
            Response response;
            Client client = getHttpClient();
            Invocation.Builder builder = client.target(url).request(MediaType.APPLICATION_XML).header(IPC_VERSION_HEADER, apiVer);

            if (StringUtils.isNotBlank(httpAuthorization)) {
                builder.header(AUTHORIZATION_HEADER, httpAuthorization);
            }

            if (StringUtils.isNotBlank(ipcAuthToken)) {
                builder.header(XIPC_AUTHTOKEN_HEADER, ipcAuthToken);
            }

            if (null != payload) {
                String body = UtilHelper.marshalJAXBObjectForRequest(payload);
                response = builder.put(Entity.entity(body, MediaType.APPLICATION_XML_TYPE));
            } else {
                response = builder.put(Entity.text(""));
            }
            return response;
        } catch (Throwable t) {
            if (t instanceof ProcessingException && t.getCause() instanceof SocketTimeoutException) {
                LOG.error("Connect time out / Read time out exception in HTTP PUT request: " + url + " - " + t.getMessage(), t);
            } else {
                LOG.error("Failed invoking HTTP Put request:" + url + " - ", t);
            }
            return null;
        }
    }

    public static Response httpsPut(String url, String httpAuth, String ipcAuthToken, String apiVer, Object payload) throws Exception {
        try {
            Client client = getHttpsClient();
            Invocation.Builder builder = client.target(url).request(MediaType.APPLICATION_XML).header(IPC_VERSION_HEADER, apiVer);
            if (StringUtils.isNotBlank(httpAuth)) {
                builder.header(AUTHORIZATION_HEADER, httpAuth);
            }

            if (StringUtils.isNotBlank(ipcAuthToken)) {
                builder.header(XIPC_AUTHTOKEN_HEADER, ipcAuthToken);
            }

            Response response = null;

            if (null != payload) {
                String body = UtilHelper.marshalJAXBObjectForRequest(payload);
                response = builder.put(Entity.entity(body, MediaType.APPLICATION_XML_TYPE));
            } else {
                response = builder.put(Entity.text(""));
            }
            return response;
        } catch (Throwable t) {
            if (t instanceof ProcessingException && t.getCause() instanceof SocketTimeoutException) {
                LOG.error("Connect time out / Read time out exception in HTTPS PUT request: " + url + " - " + t.getMessage(), t);
            } else {
                LOG.error("Failed invoking HTTPS Put request:" + url + " - ", t);
            }
            return null;
        }
    }

    public static Response httpGet(String url, String ipcAuthToken, String apiVersion) {
        try {
            return getHttpClient().target(url).request(MediaType.APPLICATION_XML).header(XIPC_AUTHTOKEN_HEADER, ipcAuthToken)
                    .header(IPC_VERSION_HEADER, apiVersion).get();
        } catch (Throwable t) {
            if (t instanceof ProcessingException && t.getCause() instanceof SocketTimeoutException) {
                LOG.error("Connect time out / Read time out exception in HTTP GET request: " + url + " - " + t.getMessage(), t);
            } else {
                LOG.error("Failed invoking HTTP GET request:" + url + " - ", t);
            }
            return null;
        }
    }

    public static Response httpsGet(String url, String ipcAuthToken, String apiVersion) {
        try {
            return getHttpsClient().target(url).request(MediaType.APPLICATION_XML)
                    .header(XIPC_AUTHTOKEN_HEADER, ipcAuthToken).header(IPC_VERSION_HEADER, apiVersion).get();
        } catch (Throwable t) {
            if (t instanceof ProcessingException && t.getCause() instanceof SocketTimeoutException) {
                LOG.error("Connect time out / Read time out exception in HTTPS GET request: " + url + " - " + t.getMessage(), t);
            } else {
                LOG.error("Failed invoking HTTPS GET request:" + url + " - ", t);
            }
            return null;
        }
    }

    public static Response httpDelete(String url, String httpAuthorization, String ipcAuthToken, String apiVersion) {
        try {
            Response response = null;
            Invocation.Builder builder = getHttpClient().target(url).request(MediaType.APPLICATION_XML).header(IPC_VERSION_HEADER, apiVersion);
            if (null == httpAuthorization) {
                response = builder.header(XIPC_AUTHTOKEN_HEADER, ipcAuthToken).delete();
            } else {
                response = builder.header(AUTHORIZATION_HEADER, httpAuthorization)
                        .header(XIPC_AUTHTOKEN_HEADER, ipcAuthToken).delete();
            }
            return response;
        } catch (Throwable t) {
            if (t instanceof ProcessingException && t.getCause() instanceof SocketTimeoutException) {
                LOG.error("Connect time out / Read time out exception in HTTP DELETE request: " + url + " - " + t.getMessage(), t);
            } else {
                LOG.error("Failed invoking HTTP DELETE request:" + url + " - ", t);
            }
            return null;
        }
    }

    public static Response httpsDelete(String url, String httpAuthorization, String ipcAuthToken, String apiVersion) {
        try {
            Invocation.Builder builder = getHttpsClient().target(url).request(MediaType.APPLICATION_XML)
                    .header(IPC_VERSION_HEADER, apiVersion).header(XIPC_AUTHTOKEN_HEADER, ipcAuthToken);

            if (null != httpAuthorization) {
                builder.header(AUTHORIZATION_HEADER, httpAuthorization);
            }
            return builder.delete();
        } catch (Throwable t) {
            if (t instanceof ProcessingException && t.getCause() instanceof SocketTimeoutException) {
                LOG.error("Connect time out / Read time out exception in HTTPS DELETE request: " + url + " - " + t.getMessage(), t);
            } else {
                LOG.error("Failed invoking HTTPS DELETE request:" + url + " - ", t);
            }
            return null;
        }
    }

    public static Response invokeHTTPGET(String resourceUrl, String ipcAuthToken, String apiVersion, boolean isSecure) {
        try {
            if (isSecure) {
                return httpsGet(resourceUrl, ipcAuthToken, apiVersion);
            }
            return httpGet(resourceUrl, ipcAuthToken, apiVersion);
        } catch (Exception e) {
            LOG.error("Failed invoking HTTP request: ", e);
        }
        return null;
    }

    public static Response invokeHTTPPost(String resourceUrl, String httpAutorization, String ipcAuthToken, String apiVersion, Object payload, boolean isSecure) throws Exception {
        try {
            if (isSecure) {
                return httpsPost(resourceUrl, httpAutorization, ipcAuthToken, apiVersion, payload);
            }
            return httpPost(resourceUrl, httpAutorization, ipcAuthToken, apiVersion, payload);

        } catch (Exception e) {
            LOG.error("Failed invoking HTTP request: ", e);
        }
        return null;
    }

    public static Response invokeHTTPPut(String resourceUrl, String httpAutorization, String ipcAuthToken, String apiVersion, Object payload, boolean isSecure) throws Exception {
        try {
            if (isSecure) {
                return httpsPut(resourceUrl, httpAutorization, ipcAuthToken, apiVersion, payload);
            }
            return httpPut(resourceUrl, httpAutorization, ipcAuthToken, apiVersion, payload);

        } catch (Exception e) {
            LOG.error("Failed invoking HTTP request: ", e);
        }
        return null;
    }

    public static Response invokeHTTPDelete(String resourceUrl, String httpAuthorization, String ipcAuthToken, String apiVersion, boolean isSecure) {
        try {
            if (isSecure) {
                return httpsDelete(resourceUrl, httpAuthorization, ipcAuthToken, apiVersion);
            }
            return httpDelete(resourceUrl, httpAuthorization, ipcAuthToken, apiVersion);

        } catch (Exception e) {
            LOG.error("Failed invoking HTTP request: ", e);
        }
        return null;
    }
}