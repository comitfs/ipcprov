package com.comitfs.cas.plugin.ipcprov.util;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IPCUserConfig {
	 @JsonProperty("ipcUsers")
	private List<IPCUser> ipcUsers = new ArrayList<IPCUser>();

	public List<IPCUser> getIpcUsers() {
		return ipcUsers;
	}

	public void setIpcUsers(List<IPCUser> ipcUsers) {
		this.ipcUsers = ipcUsers;
	}
	 
}
