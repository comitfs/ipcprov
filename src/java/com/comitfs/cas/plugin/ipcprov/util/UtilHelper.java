package com.comitfs.cas.plugin.ipcprov.util;

import com.comitfs.cas.ipc.bw.jaxb.ObjectFactory;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;

public class UtilHelper {
    private static final Logger LOG = Logger.getLogger(UtilHelper.class.getName());
    private static JAXBContext jCtx = null;

    public static String marshalJAXBObjectForRequest(Object obj) throws JAXBException {
        Marshaller marshaller = getJAXBContext().createMarshaller();
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(obj, stringWriter);
        return stringWriter.toString();
    }

    public static String marshalJAXBObjectForResponse(Object obj) throws JAXBException {
        Marshaller marshaller = getJAXBContext().createMarshaller();
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(obj, stringWriter);
        return stringWriter.toString();
    }

    public static String getFormattedXMLString(String xmlString) {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            StreamResult result = new StreamResult(new StringWriter());
            StreamSource source = new StreamSource(new StringReader(xmlString));
            transformer.transform(source, result);
            return result.getWriter().toString();
        } catch (TransformerException ex) {
            LOG.error("Encountered TransformerException while formatting XML - will return initial unformatted XML string");
            LOG.error(ex.getMessage());
            return xmlString;
        }
    }

    public static JAXBContext getJAXBContext() {
        try {
            if (null == jCtx) {
                LOG.info("JAXBContext was null so getting a new instance now");
                ClassLoader classLoader = ObjectFactory.class.getClassLoader();
                jCtx = JAXBContext.newInstance("com.comitfs.cas.ipc.bw.jaxb", classLoader);
            }
        } catch (JAXBException ex) {
            LOG.error("Exception encountered getting JAXBContext - ", ex);
            return null;
        }
        return jCtx;
    }
}
