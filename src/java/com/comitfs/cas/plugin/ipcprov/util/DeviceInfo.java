package com.comitfs.cas.plugin.ipcprov.util;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class DeviceInfo {
	private Integer deviceId;
	private String deviceModel;
	private String deviceIpAddr;
}
