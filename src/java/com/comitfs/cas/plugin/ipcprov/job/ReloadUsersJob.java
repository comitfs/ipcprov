package com.comitfs.cas.plugin.ipcprov.job;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.jivesoftware.openfire.cluster.ClusterManager;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.comitfs.cas.plugin.ipcprov.dataservice.LoadUsersForAllSitesThread;

public class ReloadUsersJob implements Job{


	private final Logger log = Logger.getLogger(getClass().getName());
	ExecutorService service = Executors.newFixedThreadPool(1);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		try {
			
				if(ClusterManager.isSeniorClusterMemberOrNotClustered()) {
					log.info("[ReloadUsersJob] Loading user preferences ....");
					service.execute(new LoadUsersForAllSitesThread());
				}


		} catch (Throwable e) {
			log.error("[Users Reload ] job failed", e);
		}
	}

	


}
