package com.comitfs.cas.plugin.ipcprov.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jivesoftware.database.JiveID;

import java.util.List;

@JiveID(200)
//@JsonIgnoreProperties({"pbxUseIDD","bwConnectionList"})
@JsonIgnoreProperties({"pbxUseIDD"})
public class IPCSite {
    private boolean enabled;

    @JsonProperty("id")
    private long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("host")
    private String host;

    @JsonProperty("port")
    private int port;

    @JsonProperty("secure")
    private boolean secure;

    @JsonProperty("apiUser")
    private String apiUser;

    @JsonProperty("password")
    private String password;

    @JsonProperty("apiVersion")
    private String apiVersion;

    @JsonProperty("acctMgmtUser")
    private String acctMgmtUser;

    @JsonProperty("acctMgmtPwd")
    private String acctMgmtPwd;

    @JsonProperty
    private boolean mediaAPI;

    @JsonProperty("pbxCountry")
    private String pbxCountry;

    @JsonProperty("pbxUseIDD")
    private boolean pbxUseIDD;

    @JsonProperty("pbxCode")
    private int pbxCode;

    @JsonProperty("pbxTrunkCode")
    private int pbxTrunkCode;

    @JsonProperty("pbxExtnLength")
    private int pbxExtnLength;

    @JsonProperty("pbxRoutingPrefixes")
    private String pbxRoutingPrefixes;

    @JsonProperty("refreshTime")
    private String refreshTime;

    //TODO unigyinstance need to be mapped to respective bwConnList
    @JsonProperty("bwConnectionList")
    private List<BWPoolConnection> bwConnectionList;
    
    @JsonProperty("location")
    private String location;

    public IPCSite() {
        //It is required for marshalling/un-marshalling by JSON
    }

    public IPCSite(String name, boolean enabled, String host, int port, String apiUser, String password, String apiVersion,
                   String refreshTime, int pbxCode, int pbxTrunkCode, String pbxCountry, int pbxExtnLength, String pbxRoutingPrefixes, boolean pbxUseIDD, String acctMgtUser, String acctMgtPwd, boolean mediaAPI) {
        this.name = name;
        this.enabled = enabled;
        this.host = host;
        this.port = port;
        this.apiUser = apiUser;
        this.password = password;
        this.apiVersion = apiVersion;
        this.refreshTime = refreshTime;
        this.pbxCode = pbxCode;
        this.pbxTrunkCode = pbxTrunkCode;
        this.pbxCountry = pbxCountry;
        this.pbxExtnLength = pbxExtnLength;
        this.pbxRoutingPrefixes = pbxRoutingPrefixes;
        this.pbxUseIDD = pbxUseIDD;
        this.acctMgmtUser = acctMgtUser;
        this.acctMgmtPwd = acctMgtPwd;
        this.mediaAPI = mediaAPI;
    }

    public IPCSite(String name, boolean enabled, String host, int port, String apiUser, String password, String apiVersion,
                   String refreshTime, int pbxCode, int pbxTrunkCode, String pbxCountry, int pbxExtnLength, String pbxRoutingPrefixes, boolean pbxUseIDD, String acctMgtUser, String acctMgtPwd, boolean secure, List<BWPoolConnection> bwConnectionList, boolean mediaAPI, String locationBasedCTIMonitor) {
        this.name = name;
        this.enabled = enabled;
        this.host = host;
        this.port = port;
        this.secure = secure;
        this.apiUser = apiUser;
        this.password = password;
        this.apiVersion = apiVersion;
        this.refreshTime = refreshTime;
        this.pbxCode = pbxCode;
        this.pbxTrunkCode = pbxTrunkCode;
        this.pbxCountry = pbxCountry;
        this.pbxExtnLength = pbxExtnLength;
        this.pbxRoutingPrefixes = pbxRoutingPrefixes;
        this.pbxUseIDD = pbxUseIDD;
        this.acctMgmtUser = acctMgtUser;
        this.acctMgmtPwd = acctMgtPwd;
        this.bwConnectionList = bwConnectionList;
        this.mediaAPI = mediaAPI;
        this.location = location;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getApiUser() {
        return apiUser;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public String getRefreshTime() {
        return null != refreshTime ? refreshTime.trim() : null;
    }

    public String getHost() {
        return host;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public int getPbxCode() {
        return pbxCode;
    }

    public int getPbxTrunkCode() {
        return pbxTrunkCode;
    }

    public String getPbxCountry() {
        return pbxCountry;
    }

    public int getPbxExtnLength() {
        return pbxExtnLength;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public int getPort() {
        return port;
    }

    public String getPbxRoutingPrefixes() {
        return pbxRoutingPrefixes;
    }

    public boolean canUseIDD() {
        return pbxUseIDD;
    }

    public String getAcctMgmtUser() {
        return acctMgmtUser;
    }

    public void setAcctMgmtUser(String acctMgmtUser) {
        this.acctMgmtUser = acctMgmtUser;
    }

    public String getAcctMgmtPwd() {
        return acctMgmtPwd;
    }

    public void setAcctMgmtPwd(String acctMgmtPwd) {
        this.acctMgmtPwd = acctMgmtPwd;
    }

    public synchronized List<BWPoolConnection> getBwConnectionList() {
        return bwConnectionList;
    }

    public void setBwConnectionList(List<BWPoolConnection> bwConnectionList) {
        this.bwConnectionList = bwConnectionList;
    }

    public boolean isSecure() {
        return secure;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    public boolean isMediaAPI() {
        return mediaAPI;
    }

    public void setMediaAPI(boolean mediaAPI) {
        this.mediaAPI = mediaAPI;
    }

    public void setRefreshTime(String refreshTime) {
        this.refreshTime = refreshTime;
    }

    public void setApiUser(String apiUser) {
        this.apiUser = apiUser;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public void setPbxCountry(String pbxCountry) {
        this.pbxCountry = pbxCountry;
    }

    public void setPbxUseIDD(boolean pbxUseIDD) {
        this.pbxUseIDD = pbxUseIDD;
    }

    public void setPbxCode(int pbxCode) {
        this.pbxCode = pbxCode;
    }

    public void setPbxTrunkCode(int pbxTrunkCode) {
        this.pbxTrunkCode = pbxTrunkCode;
    }

    public void setPbxExtnLength(int pbxExtnLength) {
        this.pbxExtnLength = pbxExtnLength;
    }

    public void setPbxRoutingPrefixes(String pbxRoutingPrefixes) {
        this.pbxRoutingPrefixes = pbxRoutingPrefixes;
    }

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	
    
    
}
