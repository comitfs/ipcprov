package com.comitfs.cas.plugin.ipcprov.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BWZoneSelector {
    private final Logger log = LogManager.getLogger(BWZoneSelector.class);
    private final IPCSite site;

    public BWZoneSelector(IPCSite site) {
        this.site = site;
    }

    public BWPoolConnection getActiveBWZone() {
        try {
            return site.getBwConnectionList().stream().filter(bwZone -> bwZone.isEnabled()).findFirst().orElse(null);
        } catch (Exception e) {
            log.error("error getting active BW zone from site: " + site.getName(), e);
        }
        return null;
    }
}
