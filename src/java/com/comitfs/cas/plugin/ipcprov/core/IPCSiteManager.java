package com.comitfs.cas.plugin.ipcprov.core;

import com.comitfs.cas.plugin.ipcprov.util.IPCSiteConfigUtil;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.util.JiveGlobals;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class IPCSiteManager {

    public static IPCSiteManager ipcSiteManager = new IPCSiteManager();
    private Map<Long, IPCSite> ipcSiteMap = null;
    private List<IPCSite> ipcSites = null;

    private IPCSiteManager() {
        super();
    }

    public static IPCSiteManager getInstance() {
        return ipcSiteManager;
    }

    public void loadIPCSites() {
        ipcSites = IPCSiteConfigUtil.connectionsFromJsonConfig();
        ipcSiteMap = ipcSites.stream().collect(Collectors.toMap(IPCSite::getId, Function.identity()));
    }

    public Map<Long, IPCSite> getIpcSiteMap() {
        loadIPCSites();
        return ipcSiteMap;
    }

    public List<IPCSite> getIpcSites() {
        loadIPCSites();
        return ipcSites;
    }

    public String getDomain() {
        String hostName = XMPPServer.getInstance().getServerInfo().getHostname();
        return JiveGlobals.getProperty("xmpp.domain", hostName);
    }


}
