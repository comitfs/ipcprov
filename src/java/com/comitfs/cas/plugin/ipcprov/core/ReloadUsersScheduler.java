package com.comitfs.cas.plugin.ipcprov.core;

import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import com.comitfs.cas.plugin.ipcprov.job.ReloadUsersJob;

import java.util.UUID;


public class ReloadUsersScheduler {
    private final Logger log = Logger.getLogger(getClass().getName());
    private String userReloadPoll = "0 0 1 * * ?";

    public ReloadUsersScheduler() {
        userReloadPoll = JiveGlobals.getProperty("plugin.ipcprov.users.load.interval", this.userReloadPoll);
    }

    public void start() {
        try {
            JobDetail processDataJob = JobBuilder.newJob(ReloadUsersJob.class)
                    .withIdentity(UUID.randomUUID().toString())
                    .build();
            SchedulerFactory schFactory = new StdSchedulerFactory();
            Scheduler sch = schFactory.getScheduler();
            sch.start();
            CronTrigger metadata_trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity(UUID.randomUUID().toString(), "group2")
                    .withSchedule(CronScheduleBuilder.cronSchedule(userReloadPoll))
                    .build();
            metadata_trigger.getJobDataMap().put("reloadUsersJob", this);
            sch.scheduleJob(processDataJob, metadata_trigger);
            log.debug("[IPC PROV ] Started scheduler for job");

        } catch (SchedulerException e) {
            log.error("[ IPC PROV] scheduler error while scheduling adaptor job", e);
            e.printStackTrace();
        } catch (Exception e) {
            log.error("[IPC AHC ] scheduler error while scheduling adaptor job", e);
        }

    }

}
