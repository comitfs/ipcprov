package com.comitfs.cas.plugin.ipcprov.core;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;

import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvConstants;

import msjava.base.scv.SecureCredentialsVault;

public class EndUserAcctMgmtUserStub {
    private final String forLog;
    private final Logger log = Logger.getLogger(EndUserAcctMgmtUserStub.class);
    private final IPCSite ipcSite;

    public EndUserAcctMgmtUserStub(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.forLog = String.format("[%s]", ipcSite.getName());
    }

    public AccountManagementUser getAcctMgmtUser() throws Exception {
        log.info(forLog + " creating EndUser Account management WebAPI user");
        long siteId = ipcSite.getId();
        String acctMgmtUser = ipcSite.getAcctMgmtUser();
        String acctMgmtUserPwd = null;

        String msSecSCVNS = JiveGlobals.getProperty(String.format(IPCProvConstants.MSSEC_SCV_NS, siteId));
        if (StringUtils.isNotBlank(msSecSCVNS)) {
            log.info(forLog + "Creating EndUser Account management WebAPI user session using MS SCV");
            String msSCVAcctPwdKey = JiveGlobals.getProperty(String.format(IPCProvConstants.MSSEC_SCV_ACCMGMT_PASSWORD_KEY, siteId));
            if (StringUtils.isBlank(msSCVAcctPwdKey)) {
                throw new Exception("Site configured with Morgan Stanley SCV mechanism without password key");
            }

            SecureCredentialsVault scv = new SecureCredentialsVault();
            char[] pwdChars = scv.getTextCred(msSecSCVNS, msSCVAcctPwdKey);
            acctMgmtUserPwd = String.valueOf(pwdChars);
        } else {
            log.info(forLog + " Creating WebAPI session with Site configured ");
            acctMgmtUserPwd = ipcSite.getAcctMgmtPwd();
        }
        return new AccountManagementUser(acctMgmtUser, acctMgmtUserPwd);
    }
}
