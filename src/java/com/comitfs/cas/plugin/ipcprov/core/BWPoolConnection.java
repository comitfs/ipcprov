package com.comitfs.cas.plugin.ipcprov.core;


public class BWPoolConnection {
    private int port;
    private int parentInstanceId = 0;
    private String host;
    private String bwZoneName;
    private String parentInstanceName;
    private boolean enabled;
    private int sessionRefreshTime;

    public BWPoolConnection() {
        //It is required for marshalling by JSON
    }

    public String getBwZoneName() {
        return bwZoneName;
    }

    public void setBwZoneName(String bwZoneName) {
        this.bwZoneName = bwZoneName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getParentInstanceId() {
        return parentInstanceId;
    }

    public void setParentInstanceId(int parentInstanceId) {
        this.parentInstanceId = parentInstanceId;
    }

    public String getParentInstanceName() {
        return parentInstanceName;
    }

    public void setParentInstanceName(String parentInstanceName) {
        this.parentInstanceName = parentInstanceName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getSessionRefreshTime() {
        return sessionRefreshTime;
    }

    public void setSessionRefreshTime(int sessionRefreshTime) {
        this.sessionRefreshTime = sessionRefreshTime;
    }

}
