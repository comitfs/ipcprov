package com.comitfs.cas.plugin.ipcprov.dataservice;

import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.LineInfoBean;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.LineInfoRestBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;


public class LineResourceInformation {
    private final Logger LOG = Logger.getLogger(LineResourceInformation.class);
    private final List<ResourceAOR> resourceAORList;
    private final IPCSite ipcSite;
    private final List<LineInfoBean> lineInfoBeanList;
    private int pageCounter = 1;
    private int pagesToRequest = 1;

    public LineResourceInformation(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.resourceAORList = new ArrayList<ResourceAOR>();
        lineInfoBeanList = new ArrayList<LineInfoBean>();
    }

    public LineInfoRestBean fetchLineInformation(int pageNumber, String lineType) {

        LOG.debug("Fetch Line info  handler");

        String apiVersion = ipcSite.getApiVersion();

        //for Response
        LineInfoRestBean lineInfoRestBean = new LineInfoRestBean();


        try {
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);

            String ipcAuthToken = sessionManager.getIpcProvAuthToken();
            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.RESOURCEAOR_INFO.toString(), null, ipcSite.isSecure());

            if (!StringUtils.isEmpty(lineType)) {
                resourceUrl = resourceUrl + "&type=" + lineType;
            }
            if (pageNumber > 0) {
                resourceUrl = resourceUrl + "&pagenum=" + pageNumber;
            }
            LOG.debug("resource url "+resourceUrl);
            Response lineInfoResponse = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = lineInfoResponse.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object lineInfoRespEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            LOG.debug("### response in FetchLineInformation handler: " + lineInfoResponse);

            if (lineInfoRespEntity == null) {
                LOG.error("Response Entity for FetchLineInformation  is null.");
                return null;
            } else if (lineInfoRespEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) lineInfoRespEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error("FetchLineInformation error response " + formattedResponseXML);

                ResponseDataBean reasonData = new ResponseDataBean();
                reasonData.setReasonCode(errorResp.getReason().getReasonCode().value());
                reasonData.setReasonDescription(errorResp.getReason().getReasonDescription());
                lineInfoRestBean.setReasonData(reasonData);
                return lineInfoRestBean;

            } else if (lineInfoRespEntity instanceof ResourceAORResponse) {
                LOG.debug("respEntity instanceof UserResponse in FetchUserInformation");
                ResourceAORResponse lineInfoResp = (ResourceAORResponse) lineInfoRespEntity;

                ResourceAORList resourceAORList = lineInfoResp.getResourceAORList();
                addToList(resourceAORList);

                if (pageCounter == 1) {
                    pagesToRequest = lineInfoResp.getNumberOfPagesAvailable();
                }

                if (pageCounter == pagesToRequest) {
                    generateLineInfoBean();
                } else {
                    return fetchLineInformation(++pageCounter, lineType);
                }

                ReasonData reasonData = lineInfoResp.getReason();
                if (reasonData != null) {
                    ResponseDataBean rdBean = new ResponseDataBean();
                    rdBean.setReasonCode(reasonData.getReasonCode().value());
                    rdBean.setReasonDescription(reasonData.getReasonDescription());
                    lineInfoRestBean.setReasonData(rdBean);
                }
                lineInfoRestBean.setLineResourceInformation(lineInfoBeanList);
                return lineInfoRestBean;
            }
        } catch (Exception e) {
            LOG.error("Error in FetchLineResourceInformation: ", e);
            return null;
        }
        return null;
    }

    private void generateLineInfoBean() {
        if (resourceAORList != null) {
            if (resourceAORList != null && resourceAORList.size() > 0) {

                for (ResourceAOR resourceAOR : resourceAORList) {
                    LineInfoBean lineInfo = new LineInfoBean();

                    lineInfo.setLineid(String.valueOf(resourceAOR.getId()));
                    lineInfo.setResourceAOR(resourceAOR.getResourceAOR());
                    JAXBElement<ResourceAORType> resourceAORType = resourceAOR.getType();
                    lineInfo.setType(resourceAORType.getValue().value());
                    if (resourceAORType.getValue() == ResourceAORType.PRIVATE) {
                        if (resourceAOR.isSignaling()) {
                            lineInfo.setPrivateLineType("MRD");
                        } else {
                            lineInfo.setPrivateLineType("ARD");
                        }
                    }

                    lineInfo.setLastModified(String.valueOf(resourceAOR.getLastModified()));
                    lineInfo.setRingNoAnswerTime(String.valueOf(resourceAOR.getRNATime()));
                    lineInfo.setAutomaticEstablishment(String.valueOf(resourceAOR.isAutomaticEstablishment()));
                    lineInfo.setBusyAppearanceThreshold(String.valueOf(resourceAOR.getBusyAppearanceThreshold()));
                    lineInfo.setDescription(resourceAOR.getDescription());
                    lineInfo.setDescriptor(resourceAOR.getDescriptor().getValue());
                    lineInfo.setDisplayStatusWhenOnSpkr(String.valueOf(resourceAOR.isDisplayStatusWhenOnSpkr()));
                    ResourceAORDiversionReason diversionReason = resourceAOR.getDiversionReason();
                    lineInfo.setDiversionReason(diversionReason.value());
                    lineInfo.setEquipped(String.valueOf(resourceAOR.isEquipped()));
                    lineInfo.setFallBackToDynamicRouting(String.valueOf(resourceAOR.isFallBackToDynamicRouting()));
                    ResourceAORForkingType forkingType = resourceAOR.getForkingType();
                    lineInfo.setForkingType(forkingType.value());
                    ResourceAORHandsetMute handsetMute = resourceAOR.getHandsetMute();
                    lineInfo.setHandsetMute(handsetMute.value());
                    lineInfo.setHoot(String.valueOf(resourceAOR.isHoot()));
                    lineInfo.setInitiateCallOnSeize(String.valueOf(resourceAOR.isInitiateCallOnSeize()));
                    lineInfo.setUnigyInstanceId(String.valueOf(resourceAOR.getInstanceId()));
                    lineInfo.setLineIsRecorded(String.valueOf(resourceAOR.isLineIsRecorded()));
                    lineInfo.setMaxAppearances(String.valueOf(resourceAOR.getMaxAppearances()));
                    lineInfo.setMediaBridgeRequired(String.valueOf(resourceAOR.isMediaBridgeRequired()));
                    ResourceAORMicMute micMute = resourceAOR.getMicMute();
                    lineInfo.setMicMute(micMute.value());
                    lineInfo.setMinimalSignalRingTime(String.valueOf(resourceAOR.getMinimalSignalRingTime()));
                    lineInfo.setMultiCDItoSingleCDIDelay(String.valueOf(resourceAOR.getMultiCDItoSingleCDIDelay()));
                    lineInfo.setoOPSResourceId(String.valueOf(resourceAOR.getOOPSResourceId()));
                    lineInfo.setParentEnterpriseId(String.valueOf(resourceAOR.getParentEnterpriseId()));
                    lineInfo.setPublishedUserInfoBase(resourceAOR.getPublishedUserInfoBase());
                    lineInfo.setCliNameOverride(resourceAOR.getCLINameOverride().getValue());
                    lineInfo.setCliNumberOverride(resourceAOR.getCLINumberOverride().getValue());
                    lineInfo.setDivertOnImmorBusytoExtension(resourceAOR.getDivertOnImmorBusytoExtension().getValue());
                    lineInfo.setDivertOnRNAtoExtension(resourceAOR.getDivertOnRNAtoExtension().getValue());

                    lineInfo.setSignaling(String.valueOf(resourceAOR.isSignaling()));
                    ResourceAORSource source = resourceAOR.getSource();
                    lineInfo.setSource(source.value());
                    lineInfo.setUiName(resourceAOR.getUiName());

                    lineInfoBeanList.add(lineInfo);
                }
            }
        }
    }

    private void addToList(ResourceAORList resAORList) {
        this.resourceAORList.addAll(resAORList.getResourceAOR());
    }
}
