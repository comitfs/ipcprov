package com.comitfs.cas.plugin.ipcprov.dataservice;

import com.comitfs.cas.ipc.bw.jaxb.UserPreferencesType;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.DeviceInventory;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.DeviceInfo;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import org.apache.log4j.Logger;

import java.util.concurrent.Callable;

public class LoadUserPreferenceThread implements Callable<Boolean> {
    Logger log = Logger.getLogger(this.getClass());
    String userId;
    Long siteId;
    String authToken;
    IPCSite ipcSite;


    public LoadUserPreferenceThread(String userId, Long siteId, SessionManager sessionManager) {
        this.userId = userId;
        this.siteId = siteId;
        ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
        this.authToken = sessionManager.getIpcProvAuthToken();
    }

    public void getUserPreference() {

        //Fecth User Preferecne for each User.
        UserInformation userInformation = new UserInformation(ipcSite, authToken);
        UserPreferencesType userPreferencesType = userInformation.fetchUserPreferences(userId);
        LogonSessionInfoHelper logonSessionInfoHelper = new LogonSessionInfoHelper(ipcSite);
        Integer deviceId = logonSessionInfoHelper.updateUserWithLogonSession(userId, userPreferencesType.getId(), authToken);
        DeviceInfo deviceInfo = null;
        if (deviceId != null && deviceId != 0) {
            DeviceInventory deviceInventory = new DeviceInventory(ipcSite);
            deviceInfo = deviceInventory.getDeviceModel(deviceId, authToken);
        }
        IPCUser ipcUser = IPCDataService.getInstance().getIpcUser(userId, userPreferencesType, deviceInfo,ipcSite.getId());
        log.debug("Ipc User generated for user " + userId + " " + ipcUser);
        //IPCDataService.getInstance().addIpcUserToMap(ipcUser, ipcSite.getId(), userId);
        IPCDataService.getInstance().getIpcUserSiteMap(siteId).add(ipcUser);


    }


    @Override
    public Boolean call() throws Exception {
        Boolean result = Boolean.TRUE;
        try {
            this.getUserPreference();
        } catch (Throwable e) {
            log.error("Exception while loading user preference of user: " + userId, e);
            result = Boolean.FALSE;
        }
        return result;
    }

}
