package com.comitfs.cas.plugin.ipcprov.dataservice;

import java.util.List;
import java.util.Map;

public class LoadUsersForAllSitesThread implements Runnable {

    @Override
    public void run() {
        Map<Long, List<String>> siteUsers = IPCDataService.getInstance().getEnterpriseContactsForAllSites();
        IPCDataService.getInstance().getUserPreferences(siteUsers);
    }
}
