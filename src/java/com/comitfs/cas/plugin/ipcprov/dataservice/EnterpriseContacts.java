package com.comitfs.cas.plugin.ipcprov.dataservice;

import com.comitfs.cas.ipc.bw.jaxb.EnterpriseContact;
import com.comitfs.cas.ipc.bw.jaxb.EnterpriseContactList;
import com.comitfs.cas.ipc.bw.jaxb.EnterpriseContactResponse;
import com.comitfs.cas.ipc.bw.jaxb.StandardErrorResponse;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;
import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import static com.comitfs.cas.plugin.ipcprov.util.IPCProvConstants.PLUGIN_IPC_LOG_VERBOSE;

public class EnterpriseContacts {

    private final IPCSite ipcSite;
    private final List<EnterpriseContact> enterpriseContactList;
    private final Logger LOG = Logger.getLogger(EnterpriseContacts.class);
    private final String forLog;
    private final List<String> entrContactsIdList;
    private long numPagesInIPC = 1;

    public EnterpriseContacts(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.enterpriseContactList = new ArrayList<EnterpriseContact>();
        this.entrContactsIdList = new ArrayList<String>();
        this.forLog = String.format("[%s] ", ipcSite.getName());
    }

    public List<String> fetchEnterpriseContacts(int pageNumber) {
        try {
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String resourceUrl = IPCUtil.formURI(bwPoolConnection,
                    IPCRESTAPI.ENTERPRISE_CONTACTS.toString(), String.valueOf(pageNumber), ipcSite.isSecure());

            LOG.debug(forLog + "uri for enterprise contacts: " + resourceUrl);
            String apiVersion = ipcSite.getApiVersion();

            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            long siteId = ipcSite.getId();
            boolean verbose = JiveGlobals.getBooleanProperty(String.format(PLUGIN_IPC_LOG_VERBOSE, siteId), false);

            if (response == null) {
                LOG.debug(forLog + "Enterprise contacts response is null");
                return null;
            }

            String apiRes = response.readEntity(String.class);
            if (verbose) {
                LOG.debug(forLog + "Response of Enterprise Contacts: " + apiRes);
            }

            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));


            if (respEntity == null) {
                LOG.error(forLog + "Response Entity for fetchEnterpriseContacts  is null");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error(forLog + "fetchEnterpriseContacts error response " + formattedResponseXML);
            } else if (respEntity instanceof EnterpriseContactResponse) {
                EnterpriseContactResponse enterpriseContactResp = (EnterpriseContactResponse) respEntity;
                EnterpriseContactList enterpriseContacts = enterpriseContactResp.getEnterpriseContactList();
                if (enterpriseContacts != null) {
                    addToList(enterpriseContacts);
                }

                if (pageNumber == 1) {
                    float numPages = IPCUtil.calculatePagesToRequest(enterpriseContactResp.getNumberOfRecsAvailable());
                    if (numPages > 0) {
                        numPagesInIPC = (long) Math.ceil(numPages);
                    }
                }
                LOG.debug(forLog + "Enterprise contact pages : " + numPagesInIPC);
                if (pageNumber == numPagesInIPC) {
                    retrieveContactIDs();
                    return entrContactsIdList;
                } else {
                    return fetchEnterpriseContacts(++pageNumber);
                }
            }
        } catch (Exception e) {
            LOG.error(forLog + "Error fetching enterprise contacts: ", e);
        }
        return entrContactsIdList;
    }

    private void retrieveContactIDs() {
        try {
            for (EnterpriseContact enterpriseContact : enterpriseContactList) {
                String presenceUriValue = enterpriseContact.getPresenceURI().getValue();
                if (null == presenceUriValue) {
                    continue;
                }

                String loginName = getLoginNameFromPresenceUri(presenceUriValue);
                if (null == loginName) {
                    continue;
                }

                if (!entrContactsIdList.contains(loginName)) {
                    entrContactsIdList.add(loginName);
                }
            }
        } catch (Exception e) {
            LOG.error(forLog + "Error fetching enterprise contacts: ", e);
        }
    }

    private String getLoginNameFromPresenceUri(String presenceUriValue) {
        try {
            return presenceUriValue.substring(0, presenceUriValue.indexOf("@"));
        } catch (Exception e) {
            LOG.error(forLog + "Error getting username from presence uri: ", e);
        }
        return null;
    }

    private void addToList(EnterpriseContactList enterpriseContacts) {
        try {
            enterpriseContactList.addAll(enterpriseContacts.getEnterpriseContact());
        } catch (Exception e) {
            LOG.error(forLog + "Error adding enterprise contacts: ", e);
        }
    }
}
