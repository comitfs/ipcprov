package com.comitfs.cas.plugin.ipcprov.dataservice;


import java.io.StringReader;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;

import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;

import com.comitfs.cas.ipc.bw.jaxb.ButtonEx;
import com.comitfs.cas.ipc.bw.jaxb.ButtonListEx;
import com.comitfs.cas.ipc.bw.jaxb.ButtonSheetResponseEx;
import com.comitfs.cas.ipc.bw.jaxb.StandardErrorResponse;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;

public class ButtonInformation {

    private final Logger LOG = Logger.getLogger(ButtonInformation.class);

    private final IPCSite ipcSite;

    public ButtonInformation(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
    }

    public List<ButtonEx> fetchButtonInformation(String loginName, String buttonType) {
        try {
            IPCUser ipcUser = IPCDataService.getInstance().getUserById(loginName,ipcSite.getId());
            String apiVersion = ipcSite.getApiVersion();

            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.FETCH_BUTTONINFO.toString(), null, ipcSite.isSecure());

            //based on search criteria construct the resourceUrl
           
            if(ipcUser == null) {
            	 String prefix = JiveGlobals.getProperty("plugin.cas.ipc"+ipcSite.getId()+".prefix.to.ofuser.as.unigy.profile","");
     			if(!prefix.isEmpty()) {
     				loginName = prefix+loginName;
     			}
                resourceUrl = resourceUrl + loginName;
            }else {
            	resourceUrl = resourceUrl + ipcUser.getUserId();
            	
            }

            if (buttonType != null && !buttonType.isEmpty()) {
                resourceUrl = resourceUrl + IPCRESTAPI.FIND_BUTTONS_BY + buttonType;
            }

            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));


            if (respEntity == null) {
                LOG.error("Response Entity for FetchButtonInformation  is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error("FetchButtonInformation error response " + formattedResponseXML);
                return null;

            }
            if (respEntity instanceof ButtonSheetResponseEx) {
                ButtonSheetResponseEx buttonSheetResponseExJaxbBean = (ButtonSheetResponseEx) respEntity;

                LOG.debug("ButtonSheet fetched with ResponseDesc: " + buttonSheetResponseExJaxbBean.getReason().getReasonDescription());

                ButtonListEx buttonListEx = buttonSheetResponseExJaxbBean.getButtonListEx();

                if (buttonListEx != null) {
                    List<ButtonEx> buttonExJaxbBean = buttonListEx.getButtonEx();
                    return buttonExJaxbBean;
                }
            }
            return null;
        } catch (Exception e) {
            LOG.error("Exception fetching button info: ", e);
            return null;
        }
    }
}