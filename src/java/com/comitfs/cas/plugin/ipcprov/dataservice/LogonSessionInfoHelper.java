package com.comitfs.cas.plugin.ipcprov.dataservice;

import com.comitfs.cas.ipc.bw.jaxb.LogonSessionData;
import com.comitfs.cas.ipc.bw.jaxb.LogonSessionDataList;
import com.comitfs.cas.ipc.bw.jaxb.LogonSessionDataResponse;
import com.comitfs.cas.ipc.bw.jaxb.StandardErrorResponse;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static com.comitfs.cas.plugin.ipcprov.util.IPCProvConstants.PLUGIN_IPC_LOG_VERBOSE;

public class LogonSessionInfoHelper {
    public static final String DATA_SUCCESSFUL = "Fetch Logon Session Data Successful";
    private final Logger log = Logger.getLogger(LogonSessionInfoHelper.class);
    private final IPCSite ipcSite;
    private String forLog;

    public LogonSessionInfoHelper(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
    }


    private LogonSessionData invokeLogonSessionAPI(String logonSessionURL, String forLog) {
        SessionManager sessionManager = null;
        String ipcAuthToken = null;
        String httpAuth = null;
        try {
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            httpAuth = IPCUtil.generateHttpAuthorization(webAPIUser.getUserId(), webAPIUser.getPassword());
            sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            ipcAuthToken = sessionManager.getIpcProvAuthToken();

            log.debug(forLog + "fetch logon session info uri: " + logonSessionURL);
            Response response = IPCProvHttpClient.invokeHTTPGET(logonSessionURL, ipcAuthToken, ipcSite.getApiVersion(), ipcSite.isSecure());

            if (response == null) {
                log.debug(forLog + "LogonSession Info response is null");
                return null;
            }

            String apiRes = response.readEntity(String.class);

            boolean verbose = JiveGlobals.getBooleanProperty(String.format(PLUGIN_IPC_LOG_VERBOSE, ipcSite.getId()), false);
            if (verbose) {
                log.debug(forLog + "response of LogonSession Info : " + apiRes);
            }

            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));
            if (respEntity == null) {
                log.error(forLog + "Response Entity for LogonSession  is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                log.error(forLog + "logonSession error response " + apiRes);
                return null;
            } else if (respEntity instanceof LogonSessionDataResponse) {
                LogonSessionDataResponse lsdr = (LogonSessionDataResponse) respEntity;
                String reasonDescription = lsdr.getReason().getReasonDescription();
                if (!reasonDescription.equalsIgnoreCase(DATA_SUCCESSFUL)) {
                    log.warn(forLog + "Logon session information API failed " + reasonDescription);
                    return null;
                }

                LogonSessionDataList lsdList = lsdr.getLogonSessionDataList().getValue();
                List<LogonSessionData> logonSessionDataList = lsdList.getLogonSessionData();
                if (logonSessionDataList.size() > 1) {
                    log.warn(forLog + " logonSession response has more list than expected: " + logonSessionDataList.size());
                }

                return logonSessionDataList.get(0);
            }
        } catch (Exception e) {
            log.error(forLog + "Error getting Logon Session information ", e);
        } finally {
            if (sessionManager != null) {
                sessionManager.deleteSession(httpAuth, ipcAuthToken, ipcSite.getApiVersion());
            }
        }
        return null;
    }

    public LogonSessionData getLogonSessionInfo(String device) {
        String forLog = String.format("[%s] ", device);
        try {
            String params = "";
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            if (StringUtils.isNumeric(device)) {
                params = "deviceId=" + device;
            } else {
                params = "deviceUUID=\"" + device + "\"";
            }
            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.LOGONSESSION_INFO.toString(), URLEncoder.encode(params, StandardCharsets.UTF_8.toString()), ipcSite.isSecure());
            log.debug(forLog + " fetch logon session info uri: " + resourceUrl);

            return invokeLogonSessionAPI(resourceUrl, forLog);
        } catch (Exception e) {
            log.error(forLog + "error fetching logon session info for device: " + device, e);
        }
        return null;
    }

    public Integer updateUserWithLogonSession(String ofUserId, Integer ipcId, String authToken) {
        Integer deviceId = 0;
        SessionManager sessionManager = null;
        String ipcAuthToken = authToken;
        String httpAuth = null;
        try {
            this.forLog = String.format("[%s][%s] ", ipcSite.getName(), ofUserId);
            if (ipcId == null || ipcId == 0) {
                IPCUser ipcUser = IPCDataService.getInstance().getUserById(ofUserId,ipcSite.getId());
                if (ipcUser == null) {
                    log.warn(forLog + "user not found to fetch logon session information");
                    return deviceId;
                }
                ipcId = ipcUser.getIPCId();
            }
            //after creating session, invoke httpsput
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            if (ipcAuthToken == null) {
                WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
                httpAuth = IPCUtil.generateHttpAuthorization(webAPIUser.getUserId(), webAPIUser.getPassword());
                sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
                ipcAuthToken = sessionManager.getIpcProvAuthToken();
            }

            String params = "userId=" + ipcId;

            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.LOGONSESSION_INFO.toString(), URLEncoder.encode(params, StandardCharsets.UTF_8.toString()), ipcSite.isSecure());
            log.debug(forLog + " fetch logon session info uri: " + resourceUrl);
            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, ipcSite.getApiVersion(), ipcSite.isSecure());

            if (response == null) {
                log.debug(forLog + "LogonSession Info response is null: " + response);
                return deviceId;
            }

            boolean verbose = JiveGlobals.getBooleanProperty(String.format(PLUGIN_IPC_LOG_VERBOSE, ipcSite.getId()), false);

            String apiRes = response.readEntity(String.class);

            if (verbose) {
                log.debug(forLog + "response of LogonSession Info : " + apiRes);
            }

            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));
            if (respEntity == null) {
                log.error(forLog + "Response Entity for LogonSession  is null.");
                return deviceId;
            } else if (respEntity instanceof StandardErrorResponse) {
                log.error(forLog + "logonSession error response " + apiRes);
                return deviceId;
            } else if (respEntity instanceof LogonSessionDataResponse) {
                LogonSessionDataResponse lsdr = (LogonSessionDataResponse) respEntity;
                String reasonDescription = lsdr.getReason().getReasonDescription();
                if (!reasonDescription.equalsIgnoreCase(DATA_SUCCESSFUL)) {
                    log.warn(forLog + "Logon session information API failed " + reasonDescription);
                    return deviceId;
                }

                LogonSessionDataList lsdList = lsdr.getLogonSessionDataList().getValue();
                List<LogonSessionData> logonSessionDataList = lsdList.getLogonSessionData();
                if (logonSessionDataList.size() > 1) {
                    log.warn(forLog + " logonSession response has more list than expected: " + logonSessionDataList.size());
                    return deviceId;
                }

                LogonSessionData lsd = logonSessionDataList.get(0);
                deviceId = lsd.getDeviceId();
            }
        } catch (Exception e) {
            log.error(forLog + "Error getting Logon Session information ", e);
        } finally {
            if (sessionManager != null) {
                sessionManager.deleteSession(httpAuth, ipcAuthToken, ipcSite.getApiVersion());
            }
        }
        return deviceId;
    }
}
