package com.comitfs.cas.plugin.ipcprov.dataservice;

import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.PersonalContactsBean;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.POCContactInfo;
import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import static com.comitfs.cas.plugin.ipcprov.util.IPCProvConstants.PLUGIN_IPC_LOG_VERBOSE;

public class EnterpriseDirectory {
    private final IPCSite ipcSite;
    private final List<EnterpriseContact> enterpriseContactList;
    private final Logger LOG = Logger.getLogger(EnterpriseDirectory.class);
    private final String forLog;
    private final List<PersonalContactsBean> epContacts;
    private final int totalCountToFetch;
    private final int startPageNum;
    private long numPagesInIPC = 1;
    private long totalRecsAvailable;

    public EnterpriseDirectory(IPCSite ipcSite, int count, int startPageNum) {
        this.ipcSite = ipcSite;
        this.enterpriseContactList = new ArrayList<EnterpriseContact>();
        this.epContacts = new ArrayList<>();
        this.totalCountToFetch = count;
        this.startPageNum = startPageNum;
        this.forLog = String.format("[%s] ", ipcSite.getName());
    }

    public List<PersonalContactsBean> fetchEnterpriseDirectory(int pageNumber, int count) {
        try {
            int numRecsPerPage = 400;

            if (count <= 400) {
                numRecsPerPage = count;
            }

            String resourceURI = "/svc/bw/data/enterprise-contact?filtertype=page&pagenum=" + pageNumber + "&numrecsperpage=" + numRecsPerPage;
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            String resourceUrl = IPCUtil.formURI(bwPoolConnection, resourceURI, null, ipcSite.isSecure());


            LOG.debug(forLog + "uri for enterprise contacts: " + resourceUrl);
            String apiVersion = ipcSite.getApiVersion();

            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            if (response == null) {
                LOG.debug(forLog + "Enterprise contacts response is null");
                return null;
            }

            String strResp = response.readEntity(String.class);

            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(strResp));


            if (respEntity == null) {
                LOG.error(forLog + "Response Entity for EnterpriseDirectory  is null");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error(forLog + "EnterpriseDirectory error response " + formattedResponseXML);
            } else if (respEntity instanceof EnterpriseContactResponse) {
                EnterpriseContactResponse enterpriseContactResp = (EnterpriseContactResponse) respEntity;
                EnterpriseContactList enterpriseContacts = enterpriseContactResp.getEnterpriseContactList();
                addToList(enterpriseContacts);

                if ((pageNumber == 1 || startPageNum == pageNumber) && count > 400) {
                    float numPages = IPCUtil.calculatePagesToRequestPerCount(enterpriseContactResp.getNumberOfRecsAvailable(), count);
                    if (numPages > 0) {
                        numPagesInIPC = Math.round(numPages);
                    }
                    this.totalRecsAvailable = enterpriseContactResp.getNumberOfRecsAvailable();
                } else if (pageNumber == 1 || startPageNum == pageNumber) {
                    float numPages = IPCUtil.calculatePagesToRequest(enterpriseContactResp.getNumberOfRecsAvailable());
                    if (numPages > 0) {
                        numPagesInIPC = Math.round(numPages);
                    }
                    this.totalRecsAvailable = enterpriseContactResp.getNumberOfRecsAvailable();
                }

                if (count > 400) {
                    count = count - numRecsPerPage;
                }

                if (pageNumber == numPagesInIPC || enterpriseContactList.size() == totalCountToFetch) {
                    retrieveDirectoryInfo();
                    return epContacts;
                } else {
                    LOG.debug("Current pgnum: " + pageNumber + "   Remaining count: " + count);
                    return fetchEnterpriseDirectory(++pageNumber, count);
                }
            }
        } catch (Exception e) {
            LOG.error(forLog + "Error EnterpriseDirectory: ", e);
        }
        return epContacts;
    }

    private void retrieveDirectoryInfo() {
        try {
            for (EnterpriseContact epContact : enterpriseContactList) {

                ArrayList<POCContactInfo> pocIDContactInfo = fetchEPContactByCID(String.valueOf(epContact.getContactId()));

                PersonalContactsBean econtact = new PersonalContactsBean();
                econtact.setInfo(pocIDContactInfo);
                econtact.setCompany(epContact.getCompany().getValue());
                econtact.setContactType(null != epContact.getType().getValue().value() ? epContact.getType().getValue().value() : "");
                econtact.setFirstName(epContact.getFirstName().getValue());
                econtact.setLastName(epContact.getLastName().getValue());
                econtact.setLocale(epContact.getLocale().getValue().value());
                econtact.setMailingAddress(epContact.getMailingAddress().getValue().value());
                econtact.setPersonalContactId(String.valueOf(epContact.getContactId()));
                econtact.setPresenceUri(epContact.getPresenceURI().getValue());

                if (!epContacts.contains(econtact)) {
                    epContacts.add(econtact);
                }
            }
        } catch (Exception e) {
            LOG.error(forLog + "Error fetching personal contacts: ", e);
        }
    }

    private void addToList(EnterpriseContactList enterpriseContacts) {
        try {
            enterpriseContactList.addAll(enterpriseContacts.getEnterpriseContact());
        } catch (Exception e) {
            LOG.error(forLog + "Error adding enterprise contacts: ", e);
        }
    }

    public ArrayList<POCContactInfo> fetchEPContactByCID(String contactID) {
        ArrayList<POCContactInfo> contactInfo = null;
        try {
            String resourceURI = "/svc/bw/data/enterprise-contact/" + contactID + "/poc";
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            String resourceUrl = IPCUtil.formURI(bwPoolConnection, resourceURI, null, ipcSite.isSecure());

            long siteId = ipcSite.getId();
            boolean verbose = JiveGlobals.getBooleanProperty(String.format(PLUGIN_IPC_LOG_VERBOSE, siteId), false);
            if (verbose) {
                LOG.debug(forLog + "uri for Enterprise Directory Contact using CID: " + resourceUrl);
            }

            String apiVersion = ipcSite.getApiVersion();

            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            if (response == null) {
                LOG.debug(forLog + "Personal contact with POCID: " + contactID + "   response is null");
            }

            String strResp = response.readEntity(String.class);

            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(strResp));


            if (respEntity == null) {
                LOG.error(forLog + "Response Entity for Enterprise Directory Contact using CID " + contactID + " is null");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error(forLog + "Enterprise Directory Contact using CID error response " + formattedResponseXML);
            } else if (respEntity instanceof EnterprisePointOfContactResponse) {
                EnterprisePointOfContactResponse epContactResponse = (EnterprisePointOfContactResponse) respEntity;
                EnterprisePointOfContactList epocList = epContactResponse.getEnterprisePointOfContactList();

                List<EnterprisePointOfContact> ePOCDataList = epocList.getEnterprisePointOfContact();

                contactInfo = new ArrayList<>();

                for (EnterprisePointOfContact personalPointOfContact : ePOCDataList) {
                    POCContactInfo epocContactInfo = new POCContactInfo();
                    epocContactInfo.setData(personalPointOfContact.getData().getValue());
                    epocContactInfo.setPocType(personalPointOfContact.getPOCType().getValue().value());
                    epocContactInfo.setIsPreferred(String.valueOf(personalPointOfContact.isIsPreferred()));
                    epocContactInfo.setMediaType(personalPointOfContact.getMediaType().getValue().value());
                    epocContactInfo.setShortDescriptor(personalPointOfContact.getShortDescriptor().getValue());

                    contactInfo.add(epocContactInfo);
                }
            }
        } catch (Exception e) {
            LOG.error(forLog + "Error fetching contact info with ID: " + contactID, e);
        }
        return contactInfo;
    }

    public long getTotalRecsAvailable() {
        return totalRecsAvailable;
    }

    public int getStartPageNum() {
        return startPageNum;
    }
}
