package com.comitfs.cas.plugin.ipcprov.dataservice;

import com.comitfs.cas.ipc.bw.jaxb.UserPreferencesType;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.DeviceInventory;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.DeviceInfo;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;
import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;
import org.jivesoftware.util.cache.Cache;
import org.jivesoftware.util.cache.CacheFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

public class IPCDataService {
    public static final IPCDataService IPC_DATA_SERVICE = new IPCDataService();
    private static final String IPC_USER_SITE_MAP = "ipcprovusersitemap";
    private static final String IPC_USER_MAP = "ipcprovusermap";
//    private final Cache<String, IPCUser> ipcUserMap;
    private final Map<String, IPCUser> ipcUserMap;
    private final Map<Long, ArrayList<IPCUser>> ipcUserSiteMap = new HashMap<Long, ArrayList<IPCUser>>();
    int threadPoolSize = JiveGlobals.getIntProperty("plugin.ipcprov.threadpool.size", 100);
    Logger logger = Logger.getLogger(this.getClass());
    ExecutorService service = Executors.newFixedThreadPool(threadPoolSize);
    private Cache<Long, ArrayList<IPCUser>> ipcUserSiteMapCache;
    private List<IPCUser> ipcImportUsers = new ArrayList<IPCUser>();
    private String httpAuth;

    private IPCDataService() {
        super();
        CacheFactory.setMaxLifetimeProperty(IPC_USER_SITE_MAP, -1l);
        CacheFactory.setMaxSizeProperty(IPC_USER_SITE_MAP, -1l);
        ipcUserSiteMapCache = CacheFactory.createCache(IPC_USER_SITE_MAP);
        
        CacheFactory.setMaxLifetimeProperty(IPC_USER_MAP, -1l);
        CacheFactory.setMaxSizeProperty(IPC_USER_MAP, -1l);
        ipcUserMap = CacheFactory.createCache(IPC_USER_MAP);
 //       ipcUserMap = new HashMap();
    }

    public static IPCDataService getInstance() {
        return IPC_DATA_SERVICE;
    }


    public List<IPCUser> getIpcImportUsers() {
        return ipcImportUsers;
    }

    public void setIpcImportUsers(List<IPCUser> ipcImportUsers) {
        this.ipcImportUsers = ipcImportUsers;
    }

    public IPCUser getUserById(String userId, long siteId) {
    	logger.debug("IPC user Cache size "+ ipcUserMap.size());
        return ipcUserMap.get(userId.toLowerCase()+"#"+siteId);
    }

    public Map<Long, List<String>> getEnterpriseContactsForAllSites() {
        logger.debug("Started fetching enterprise contacts for all sites");

        List<IPCSite> ipcSites = IPCSiteManager.getInstance().getIpcSites();
        Map<Long, List<String>> siteUsersMap = new HashMap<Long, List<String>>();

        ipcSites.forEach(ipcSite -> {
            logger.debug("Fetching enterprise contacts for site " + ipcSite.getId());
            try {
                EnterpriseContacts enterpriseContacts = new EnterpriseContacts(ipcSite);
                List<String> userIds = enterpriseContacts.fetchEnterpriseContacts(1);
                logger.debug("Completed fetching enterprise contacts for site " + ipcSite.getId() + " contact count " + userIds.size());
                siteUsersMap.put(ipcSite.getId(), userIds);
            } catch (Throwable e) {
                logger.error("Exception while fecthing Enterpries contacts for site " + ipcSite.getId(), e);
            }
        });

        return siteUsersMap;

    }

    public void getUserPreferences(Map<Long, List<String>> userSiteMap) {
        logger.debug("Clearing existing users " + LocalDateTime.now());
        ipcUserMap.clear();
        ipcUserSiteMap.clear();
        logger.debug("Loading user preference started " + LocalDateTime.now());
        List<Future<Boolean>> userPreferenceFutureList = new ArrayList<Future<Boolean>>();
        userSiteMap.entrySet().stream()
                .forEach(e -> {
                    Long siteId = e.getKey();
                    List<String> userIds = e.getValue();
                    SessionManager sessionManager = this.createSession(siteId);
                    try {
                        logger.debug("Loading user preference started  for site " + siteId);
                        userIds.forEach(userId -> {
                            Future<Boolean> future = service.submit(new LoadUserPreferenceThread(userId, siteId, sessionManager));
                            userPreferenceFutureList.add(future);
                        });
                    } catch (Throwable ue) {
                        logger.error("Exception while fecthing userPreference fro site " + siteId, ue);
                    }
                    userPreferenceFutureList.forEach(future -> {
                        try {
                            future.get();
                        } catch (Throwable ex) {
                            logger.error("Exception while future process", ex);
                        }
                    });
                    logger.info("ipcUsersSiteMap size"+ipcUserSiteMap.get(siteId).size());
                    ipcUserSiteMapCache.put(siteId, ipcUserSiteMap.get(siteId));
                    //updateCache();
                    logger.debug("Loading user preference completed for site " + siteId + "ipcUsers in cache "+ipcUserSiteMapCache.get(siteId).size());
                    this.deleteSession(siteId, sessionManager);
                });

        logger.debug("Loading user preference completed " + LocalDateTime.now());
        JiveGlobals.setProperty("plugin.ipcprov.loading.preferences.done", String.valueOf(true));
    }

    private SessionManager createSession(Long siteId) {
        IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
        SessionManager sessionManager = null;
        try {
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            httpAuth = IPCUtil.generateHttpAuthorization(webAPIUser.getUserId(), webAPIUser.getPassword());
            logger.debug("[Get preferences - ] Session created for and auth Token generated for site " + siteId + " -- " + sessionManager.getIpcProvAuthToken());

        } catch (Exception e) {
            logger.error("Excption while authToken while load user preferences", e);
        }
        return sessionManager;
    }

    private void deleteSession(Long siteId, SessionManager sessionManager) {
        try {
            if (sessionManager != null) {
                IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
                sessionManager.deleteSession(httpAuth, sessionManager.getIpcProvAuthToken(), ipcSite.getApiVersion());
                logger.debug("[Get preferences - ]Session deleted for site " + siteId);
            }

        } catch (Exception e) {
            logger.error("Excption while deleting session load user preferences", e);
        }
    }


    private void updateCache() {
        ipcUserSiteMap.entrySet().stream()
                .forEach(e -> {
                    Long siteId = e.getKey();
                    ArrayList<IPCUser> userIds = e.getValue();
                    ipcUserSiteMapCache.put(siteId, userIds);
                });
    }

    public void updateLocalMap() {
    	logger.info("Updating local map");
        ipcUserSiteMapCache.entrySet().stream()
                .forEach(e -> {
                    Long siteId = e.getKey();
                    ArrayList<IPCUser> userIds = e.getValue();
                    ipcUserSiteMap.put(siteId, userIds);
                });
        logger.info("Updating local map done");
    }

    public void getUserPreference(String userId, IPCSite ipcSite) {

        try {
            UserInformation userInformation = new UserInformation(ipcSite);
            UserPreferencesType userPreferencesType = userInformation.fetchUserPreferences(userId);
            if(userPreferencesType != null) {
            	LogonSessionInfoHelper logonSessionInfoHelper = new LogonSessionInfoHelper(ipcSite);
            	Integer deviceId = logonSessionInfoHelper.updateUserWithLogonSession(userId, userPreferencesType.getId(), null);
            	DeviceInfo deviceInfo = null;
            	if (deviceId != null && deviceId != 0) {
            		DeviceInventory deviceInventory = new DeviceInventory(ipcSite);
            		deviceInfo = deviceInventory.getDeviceModel(deviceId, null);
            	}
            	IPCUser ipcUser = getIpcUser(userId, userPreferencesType, deviceInfo,ipcSite.getId());
            	logger.debug("Ipc User generated for user " + userId + " " + ipcUser);
            	this.addIpcUserToMap(ipcUser, ipcSite.getId(), userId);
            	//updateCache();
            	ipcUserSiteMapCache.put(ipcSite.getId(), ipcUserSiteMap.get(ipcSite.getId()));
            }
        } catch (Throwable e) {
            logger.error("Exception while fetching user Preference " + userId, e);
        }

    }

    private void addIpcUserToMap(IPCUser ipcUser, long siteId, String userId) {
        try {
            ArrayList<IPCUser> ipcUsers = this.getIpcUserSiteMap(siteId);
            logger.debug("addIpcUserToMap IPCUser Cache " + ipcUsers.size());
            int itemIndex = IntStream.range(0, ipcUsers.size())
                    .filter(i -> userId.equals(ipcUsers.get(i).getUserId()))
                    .findFirst()
                    .orElse(-1);
            if (itemIndex == -1) {
                this.getIpcUserSiteMap(siteId).add(ipcUser);
            } else {
                ipcUsers.set(itemIndex, ipcUser);
            }

        } catch (Throwable e) {
            logger.error("Exception while loading user Preferences " + userId, e);
        }
    }

    public IPCUser getIpcUser(String userId, UserPreferencesType userPreferencesType, DeviceInfo deviceInfo,long siteId) {
        IPCUser ipcUser = new IPCUser();
        try {
        	logger.info("getIpcUser usermap size "+getIpcUserMap().size());
            if (userPreferencesType != null) {
                ipcUser.setUserId(userId);
                ipcUser.setIPCId(userPreferencesType.getId());
                ipcUser.setPersonalExtn(userPreferencesType.getPersonalExtensionAOR() != null ? userPreferencesType.getPersonalExtensionAOR().getValue() : "");
                if (userPreferencesType.getTurretAudioPreferences() != null) {
                    ipcUser.setTurretRecordingEnabled(userPreferencesType.getTurretAudioPreferences().getRecordingEnabled().getValue());
                    ipcUser.setTurretrecordMixProfileId(userPreferencesType.getTurretAudioPreferences().getRecordMixProfileId() != null ? userPreferencesType.getTurretAudioPreferences().getRecordMixProfileId().getValue() : null);
                    ipcUser.setTurretrecordMixProfileName(userPreferencesType.getTurretAudioPreferences().getRecordMixProfileName() != null ? userPreferencesType.getTurretAudioPreferences().getRecordMixProfileName().getValue() : null);
                    ipcUser.setTurretRecordingProtocol(userPreferencesType.getTurretAudioPreferences().getRecordProtocol() != null ? userPreferencesType.getTurretAudioPreferences().getRecordProtocol().toString() : null);
                }
                if (userPreferencesType.getPulseAudioPreferences() != null) {
                    ipcUser.setPulseRecordingEnabled(userPreferencesType.getPulseAudioPreferences().getRecordingEnabled().getValue());
                    ipcUser.setPulserecordMixProfileId(userPreferencesType.getPulseAudioPreferences().getRecordMixProfileId() != null ? userPreferencesType.getPulseAudioPreferences().getRecordMixProfileId().getValue() : null);
                    ipcUser.setPulserecordMixProfileName(userPreferencesType.getPulseAudioPreferences().getRecordMixProfileName() != null ? userPreferencesType.getPulseAudioPreferences().getRecordMixProfileName().getValue() : null);
                    ipcUser.setPulseRecordingProtocol(userPreferencesType.getPulseAudioPreferences().getRecordProtocol() != null ? userPreferencesType.getPulseAudioPreferences().getRecordProtocol().toString() : null);
                }
                if (userPreferencesType.getIQMaxTouchAudioPreferences() != null) {
                    ipcUser.setIqMaxTouchRecordingEnabled(userPreferencesType.getIQMaxTouchAudioPreferences().isRecordingEnabled());
                    ipcUser.setIqMaxTouchrecordMixProfileId(userPreferencesType.getIQMaxTouchAudioPreferences().getRecordMixProfileId() != null ? userPreferencesType.getIQMaxTouchAudioPreferences().getRecordMixProfileId() : null);
                    ipcUser.setIqMaxTouchrecordMixProfileName(userPreferencesType.getIQMaxTouchAudioPreferences().getRecordMixProfileName() != null ? userPreferencesType.getIQMaxTouchAudioPreferences().getRecordMixProfileName() : null);
                    ipcUser.setIqMaxTouchRecordingProtocol(userPreferencesType.getIQMaxTouchAudioPreferences().getRecordProtocol() != null ? userPreferencesType.getIQMaxTouchAudioPreferences().getRecordProtocol().toString() : null);
                }
                if (deviceInfo != null) {
                    ipcUser.setDeviceModel(deviceInfo.getDeviceModel());
                }
                this.getIpcUserMap().put(userId.toLowerCase()+"#"+siteId, ipcUser);
                return ipcUser;

            }
        } catch (Throwable e) {
            logger.error("Exception while creating Ipc user ", e);
        }
        return null;
    }

    public Map<String, IPCUser> getIpcUserMap() {
        return ipcUserMap;
    }

    public ArrayList<IPCUser> getIpcUserSiteMap(long siteId) {
    	
    	logger.debug("IPC user site map =="+siteId+"==" );
        if (!ipcUserSiteMap.containsKey(siteId)) {
        	logger.debug("IPC user site map no users for =="+siteId+"==" );
            ipcUserSiteMap.put(siteId, new ArrayList<IPCUser>());
        }
        logger.debug("IPC user site map =="+siteId+"=="+ipcUserSiteMap.get(siteId).size() );
        return ipcUserSiteMap.get(siteId);
    }
    
    public  Map<Long, ArrayList<IPCUser>> getIpcUserSiteMap() {
    	
        return ipcUserSiteMap;
    }
	public Cache<Long, ArrayList<IPCUser>> getIpcUserSiteMapCache() {
        return ipcUserSiteMapCache;
    }

    public void setIpcUserSiteMapCache(Cache<Long, ArrayList<IPCUser>> ipcUserSiteMapCache) {
        this.ipcUserSiteMapCache = ipcUserSiteMapCache;
    }


}