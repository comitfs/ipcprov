package com.comitfs.cas.plugin.ipcprov.dataservice;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.PersonalContactsBean;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.POCContactInfo;
import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import static com.comitfs.cas.plugin.ipcprov.util.IPCProvConstants.PLUGIN_IPC_LOG_VERBOSE;

public class PersonalContactsInformation {
    public static final String INTERNAL_SERVER_ERROR = "Internal server error";
    private final IPCSite ipcSite;
    private final List<PersonalContact> personaContactList;
    private final Logger LOG = Logger.getLogger(PersonalContactsInformation.class);
    private final String forLog;
    private final List<String> personalContactsIdList;
    private final List<PersonalContactsBean> personalContacts;
    private long numPagesInIPC = 1;


    public PersonalContactsInformation(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.personaContactList = new ArrayList<PersonalContact>();
        this.personalContactsIdList = new ArrayList<String>();
        this.personalContacts = new ArrayList<>();
        this.forLog = String.format("[%s] ", ipcSite.getName());
    }

    public List<PersonalContactsBean> fetchPersonalContacts(String loginName, int pageNumber) {
        try {
            String resourceURI = "/svc/bw/data/user/" + loginName + "/personal-contact?filtertype=page&pagenum=";
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String resourceUrl = IPCUtil.formURI(bwPoolConnection,
                    resourceURI, String.valueOf(pageNumber), ipcSite.isSecure());

            LOG.debug(forLog + "uri for Personal Contacts: " + resourceUrl);
            String apiVersion = ipcSite.getApiVersion();

            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            LOG.info(forLog + " Personal contacts API res: " + response);

            String apiRes = response.readEntity(String.class);
            boolean verbose = JiveGlobals.getBooleanProperty(String.format(PLUGIN_IPC_LOG_VERBOSE, ipcSite.getId()), false);
            if (verbose) {
                LOG.debug(forLog + " Personal contacts response: " + apiRes);
            }

            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                LOG.error(forLog + "Response Entity for PersonalContacts  is null");
                return null;
            }

            if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error(forLog + "fetchPersonalContacts error response " + formattedResponseXML);
            } else if (respEntity instanceof PersonalContactResponse) {
                PersonalContactResponse personalContactResponse = (PersonalContactResponse) respEntity;
                PersonalContactList pcs = personalContactResponse.getPersonalContactList();
                addToList(pcs);

                if (pageNumber == 1) {
                    float numPages = IPCUtil.calculatePagesToRequest(personalContactResponse.getNumberOfRecsAvailable());
                    if (numPages > 0) {
                        numPagesInIPC = Math.round(numPages);
                    }
                }

                if (pageNumber == numPagesInIPC) {
                    retrieveContacts(loginName);
                    return personalContacts;
                } else {
                    return fetchPersonalContacts(loginName, ++pageNumber);
                }
            }
        } catch (Exception e) {
            LOG.error(forLog + "Error fetching personal contacts: ", e);
        }
        return personalContacts;
    }

    private void addToList(PersonalContactList personalContacts) {
        try {
            personaContactList.addAll(personalContacts.getPersonalContact());
        } catch (Exception e) {
            LOG.error(forLog + "Error adding personal contacts: ", e);
        }
    }

    private void retrieveContacts(String loginName) {
        try {
            for (PersonalContact pContact : personaContactList) {

                ArrayList<POCContactInfo> pocIDContactInfo = fetchPersonalContactByPOCID(loginName, String.valueOf(pContact.getPersonalContactId()));

                PersonalContactsBean contact = new PersonalContactsBean();
                contact.setInfo(pocIDContactInfo);
                contact.setCompany(pContact.getCompany().getValue());
                contact.setContactType(null != pContact.getType().value() ? pContact.getType().value() : "");
                contact.setFirstName(pContact.getFirstName().getValue());
                contact.setLastName(pContact.getLastName().getValue());
                contact.setLocale(pContact.getLocale().getValue().value());
                contact.setMailingAddress(pContact.getMailingAddress().getValue().value());
                contact.setPersonalContactId(String.valueOf(pContact.getPersonalContactId()));
                contact.setPresenceUri(pContact.getPresenceURI().getValue());

                if (!personalContacts.contains(contact)) {
                    personalContacts.add(contact);
                }
            }
        } catch (Exception e) {
            LOG.error(forLog + "Error fetching personal contacts: ", e);
        }
    }

    public ArrayList<POCContactInfo> fetchPersonalContactByPOCID(String loginName, String pocID) {
        ArrayList<POCContactInfo> contactInfo = null;
        try {
            String resourceURI = "/svc/bw/data/user/" + loginName + "/personal-contact/" + pocID + "/poc";
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String resourceUrl = IPCUtil.formURI(bwPoolConnection,
                    resourceURI, null, ipcSite.isSecure());

            LOG.debug(forLog + "uri for Personal Contacts using POCID: " + resourceUrl);
            String apiVersion = ipcSite.getApiVersion();

            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            long siteId = ipcSite.getId();

            LOG.info(forLog + "Personal Contacts using POCID response: " + response);

            String apiRes = response.readEntity(String.class);

            boolean verbose = JiveGlobals.getBooleanProperty(String.format(PLUGIN_IPC_LOG_VERBOSE, siteId), false);
            if (verbose) {
                LOG.debug(forLog + "Response of Personal Contact with POCID: : " + pocID + apiRes);
            }

            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                LOG.error(forLog + "Response Entity for PersonalContact with POCID: : " + pocID + " is null");
                return null;
            }
            if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error(forLog + "PersonalContact error response " + formattedResponseXML);
            } else if (respEntity instanceof PersonalPointOfContactResponse) {
                PersonalPointOfContactResponse personalContactResponse = (PersonalPointOfContactResponse) respEntity;
                PersonalPointOfContactList pocList = personalContactResponse.getPersonalPointOfContactList();

                List<PersonalPointOfContact> pPOCList = pocList.getPersonalPointOfContact();

                contactInfo = new ArrayList<>();

                for (PersonalPointOfContact personalPointOfContact : pPOCList) {
                    POCContactInfo pocContactInfo = new POCContactInfo();
                    pocContactInfo.setData(personalPointOfContact.getData().getValue());
                    pocContactInfo.setPocType(personalPointOfContact.getPOCType().getValue().value());
                    pocContactInfo.setIsPreferred(String.valueOf(personalPointOfContact.isIsPreferred()));
                    pocContactInfo.setMediaType(personalPointOfContact.getMediaType().getValue().value());
                    pocContactInfo.setShortDescriptor(personalPointOfContact.getShortDescriptor().getValue());

                    contactInfo.add(pocContactInfo);
                }
            }
        } catch (Exception e) {
            LOG.error(forLog + "Error fetching personal contacts: ", e);
        }
        return contactInfo;
    }
}
