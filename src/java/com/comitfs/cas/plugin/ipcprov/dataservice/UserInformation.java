package com.comitfs.cas.plugin.ipcprov.dataservice;


import java.io.StringReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;

import com.comitfs.cas.ipc.bw.jaxb.FetchUserSpeakerChannelResponse;
import com.comitfs.cas.ipc.bw.jaxb.Locale;
import com.comitfs.cas.ipc.bw.jaxb.StandardErrorResponse;
import com.comitfs.cas.ipc.bw.jaxb.User;
import com.comitfs.cas.ipc.bw.jaxb.UserAccountStatus;
import com.comitfs.cas.ipc.bw.jaxb.UserPreferencesResponse;
import com.comitfs.cas.ipc.bw.jaxb.UserPreferencesType;
import com.comitfs.cas.ipc.bw.jaxb.UserResponse;
import com.comitfs.cas.ipc.bw.jaxb.UserSpeakerChannel;
import com.comitfs.cas.ipc.bw.jaxb.UserSpeakerChannelList;
import com.comitfs.cas.plugin.ipcprov.beans.SpeakerChannelAttributes;
import com.comitfs.cas.plugin.ipcprov.beans.UserAttributes;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;

public class UserInformation {

    private final Logger LOG = Logger.getLogger(UserInformation.class);
    private final IPCSite ipcSite;
    private String authToken = null;
    private LocalDateTime authTokenTime;
    private SessionManager sessionManager = null;

    private String httpAuth = null;

    public UserInformation(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.createSession();
    }

    public UserInformation(IPCSite ipcSite, String authToken) {
        this.ipcSite = ipcSite;
        this.authToken = authToken;

    }

    public UserAttributes fetchUserInformation(String loginName) {

        LOG.debug("fetching login information for : " + loginName);

        IPCUser ipcUser = IPCDataService.getInstance().getUserById(loginName,ipcSite.getId());

        String apiVersion = ipcSite.getApiVersion();

        try {
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String resourceUrl = IPCUtil.formURI(bwPoolConnection,
                    IPCRESTAPI.FETCH_USERINFO.toString(), null, ipcSite.isSecure());

            Response userInfoResposne = IPCProvHttpClient.invokeHTTPGET(resourceUrl + ipcUser.getIPCId(), ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = userInfoResposne.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object userRespEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            UserAttributes userAttributes = new UserAttributes();

            if (userRespEntity instanceof UserResponse) {
                UserResponse userResponse = (UserResponse) userRespEntity;

                User user = userResponse.getUser();
                userAttributes.setLoginName(user.getLoginName().getValue());
                userAttributes.setFirstName(user.getFirstName().getValue());
                userAttributes.setLastName(user.getLastName().getValue());
                userAttributes.setTitle(user.getTitle().getValue());
                userAttributes.setUserCDIID(user.getUserCDIId());
                UserAccountStatus accountStatus = user.getAccountStatus();
                userAttributes.setAccountActive(accountStatus.value());
            }

            final String uri = IPCUtil.formURI(bwPoolConnection,
                    IPCRESTAPI.DATA_API.toString(), IPCRESTAPI.USER_PREF_WITH_LOGIN_NAME + ipcUser.getUserId(), ipcSite.isSecure());
            final Response userPreferencesResponse = IPCProvHttpClient.invokeHTTPGET(uri, ipcAuthToken, ipcSite.getApiVersion(), ipcSite.isSecure());
            String apiUserPrefRes = userPreferencesResponse.readEntity(String.class);
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiUserPrefRes));

            if (respEntity instanceof UserPreferencesResponse) {
                UserPreferencesResponse response = (UserPreferencesResponse) respEntity;
                UserPreferencesType userPreferencesType = response.getUserPreferences();

                userAttributes.setEmail(userPreferencesType.getEmailAddress().getValue());
                userAttributes.setDisplayDateFormat(userPreferencesType.getDateDisplayFormat().value());
                userAttributes.setTimeDisplayFormat(userPreferencesType.getTimeDisplayFormat().value());

                JAXBElement<Locale> locale = userPreferencesType.getLocale();
                userAttributes.setLocale(locale.getValue().value());
            }

            return userAttributes;
        } catch (Exception e) {
            LOG.error("Error fetching UserInformation: ", e);
        }
        return null;
    }

    public UserAttributes fetchUser(String loginName) {

        LOG.debug("fetching login information for : " + loginName);

        IPCUser ipcUser = IPCDataService.getInstance().getUserById(loginName,ipcSite.getId());

        String apiVersion = ipcSite.getApiVersion();

        try {
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String resourceUrl = IPCUtil.formURI(bwPoolConnection,
                    IPCRESTAPI.FETCH_USERINFO.toString(), null, ipcSite.isSecure());

            Response userInfoResposne = IPCProvHttpClient.invokeHTTPGET(resourceUrl + ipcUser.getIPCId(), ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = userInfoResposne.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object userRespEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            UserAttributes userAttributes = new UserAttributes();

            if (userRespEntity instanceof UserResponse) {
                UserResponse userResponse = (UserResponse) userRespEntity;

                User user = userResponse.getUser();
                userAttributes.setLoginName(user.getLoginName().getValue());
                userAttributes.setFirstName(user.getFirstName().getValue());
                userAttributes.setLastName(user.getLastName().getValue());
                userAttributes.setTitle(user.getTitle().getValue());
                userAttributes.setUserCDIID(user.getUserCDIId());
                UserAccountStatus accountStatus = user.getAccountStatus();
                userAttributes.setAccountActive(accountStatus.value());
            }


            return userAttributes;
        } catch (Exception e) {
            LOG.error("Error fetching UserInformation: ", e);
        }
        return null;
    }
    
    public SpeakerChannelAttributes fetchSpeakerChannels(String loginName) {

        LOG.debug("fetching speaker channels for : " + loginName);

        IPCUser ipcUser = IPCDataService.getInstance().getUserById(loginName,ipcSite.getId());

        String apiVersion = ipcSite.getApiVersion();

        try {
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String resourceUrl = IPCUtil.formURI(bwPoolConnection,
                    IPCRESTAPI.FETCH_SPEAKERS.toString(), null, ipcSite.isSecure());

            Response userInfoResposne = IPCProvHttpClient.invokeHTTPGET(resourceUrl + ipcUser.getIPCId(), ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = userInfoResposne.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object userRespEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            SpeakerChannelAttributes speakerchannelAttributes = new SpeakerChannelAttributes();
            if (userRespEntity == null) {
            	LOG.error("Response Entity for SpeakerDataHelper is null.User: " + ipcUser.getUserId());
                return null;
            }

            if (userRespEntity instanceof StandardErrorResponse) {
            	LOG.error("FetchSpeaker error response: " + apiRes);
            }
            else if (userRespEntity instanceof FetchUserSpeakerChannelResponse) {
            	  FetchUserSpeakerChannelResponse responseEx = (FetchUserSpeakerChannelResponse) userRespEntity;

                  if (null == responseEx.getUserSpeakerChannelList()) {
                      LOG.info("FetchUserSpeakerChannelResponse is empty. No speakers created for user: " + ipcUser.getUserId());
                      return null;
                  }
                  
                  UserSpeakerChannelList speakerChannelList = responseEx.getUserSpeakerChannelList();

                  ArrayList<UserSpeakerChannel> speakerList = (ArrayList) speakerChannelList.getUserSpeakerChannel();
                  speakerchannelAttributes.setSpeakerList(speakerList);
                  speakerchannelAttributes.setIpcId(ipcUser.getIPCId());
                
            }

            return speakerchannelAttributes;
        } catch (Exception e) {
            LOG.error("Error fetching UserInformation: ", e);
        }
        return null;
    }


    public UserPreferencesType fetchUserPreferences(String loginName) {
        try {

            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            final String uri = IPCUtil.formURI(bwPoolConnection,
                    IPCRESTAPI.DATA_API.toString(), IPCRESTAPI.USER_PREF_WITH_LOGIN_NAME + loginName, ipcSite.isSecure());
            final Response userPreferencesResponse = IPCProvHttpClient.invokeHTTPGET(uri, authToken, ipcSite.getApiVersion(), ipcSite.isSecure());

            String apiRes = userPreferencesResponse.readEntity(String.class);
            //LOG.info("apiResponse in fetchUserPreferences "+apiRes);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity instanceof UserPreferencesResponse) {
                UserPreferencesResponse response = (UserPreferencesResponse) respEntity;
                UserPreferencesType userPreferencesType = response.getUserPreferences();
                return userPreferencesType;
            }

        } catch (Exception e) {
            LOG.error("Exception in fetchUserPreferences(): ", e);
        } finally {
            this.deleteSession();
        }
        return null;

    }

    public void createSession() {
        try {
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            httpAuth = IPCUtil.generateHttpAuthorization(webAPIUser.getUserId(), webAPIUser.getPassword());
            authToken = sessionManager.getIpcProvAuthToken();
            authTokenTime = LocalDateTime.now();

        } catch (Exception e) {
            LOG.error("Excption while authToken ", e);
        }

    }

    public void deleteSession() {
        try {
            if (sessionManager != null) {
                sessionManager.deleteSession(httpAuth, authToken, ipcSite.getApiVersion());
            }

        } catch (Exception e) {
            LOG.error("Excption while authToken ", e);
        }

    }
}
