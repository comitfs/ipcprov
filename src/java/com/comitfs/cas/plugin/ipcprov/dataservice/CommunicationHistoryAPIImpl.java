package com.comitfs.cas.plugin.ipcprov.dataservice;

import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.CommunicationHistoryBean;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.*;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jivesoftware.util.JiveGlobals;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;

import static com.comitfs.cas.plugin.ipcprov.util.IPCProvConstants.PLUGIN_IPC_LOG_VERBOSE;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class CommunicationHistoryAPIImpl {
    public static final String REQ_FAILURE = "REQ_FAILURE";
    public static final String DATEPAGE_FILTER = "datepage";
    public static final String DATEPAGELASTMODIFIED_FILTER = "datepagelastmodified";
    private final Logger log = Logger.getLogger(getClass());
    private final String NO_DATA_FOR_SEARCH_CRITERIA = "Failed to get Communication History info/ no data found";
    private final IPCSite ipcSite;

    public CommunicationHistoryAPIImpl(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
    }

    public Object getCallHistory(String filtertype, String loginname, String longStarTime, String longEndTime, String timeZone, String pageNum, String deviceId) {
        SessionManager sessionManager = null;
        String ipcAuthToken = null;
        String httpAuth = null;
        try {
            if (null == ipcSite) {
                return toReasonDataRestBean(REQ_FAILURE, "Invalid loginname/User does not exist in loaded sited");
            }

            final String forLog = String.format("[%s][%s]", ipcSite.getName(), "CDR");

            IPCUser user = IPCDataService.getInstance().getUserById(loginname,ipcSite.getId());
            if (null == user) {
                return toReasonDataRestBean(REQ_FAILURE, "user not found in site: " + ipcSite.getName());
            }

            if (StringUtils.isNotBlank(filtertype)) {
                if (!filtertype.equalsIgnoreCase(DATEPAGE_FILTER) && !filtertype.equalsIgnoreCase(DATEPAGELASTMODIFIED_FILTER)) {
                    return toReasonDataRestBean(REQ_FAILURE, "invalid filtertype. Valid values are '" + DATEPAGE_FILTER + "' or '" + DATEPAGELASTMODIFIED_FILTER + "'");
                }
            } else {
                filtertype = DATEPAGE_FILTER;
            }

            if (!isValidLong(longStarTime)) {
                return toReasonDataRestBean(REQ_FAILURE, "invalid starttime. It must be valid milliseconds of requested DateTime");
            }

            if (!isValidTimeZone(timeZone)) {
                return toReasonDataRestBean(REQ_FAILURE, "invalid timezone");
            }

            String reqParams = "filtertype=" + filtertype + "&loginname=" + loginname + "&starttime=" + longStarTime + "&timezone=" + timeZone + "&timeformat=absolute";

            if (StringUtils.isNotBlank(longEndTime)) {
                if (isValidLong(longEndTime)) {
                    reqParams = reqParams + "&endtime=" + longEndTime;
                } else {
                    return toReasonDataRestBean(REQ_FAILURE, "invalid endtime");
                }
            }

            if (StringUtils.isNotBlank(pageNum)) {
                if (isValidLong(pageNum)) {
                    reqParams = reqParams + "&pagenum=" + pageNum;
                } else {
                    return toReasonDataRestBean(REQ_FAILURE, "invalid pagenum");
                }
            }

            if (StringUtils.isNotBlank(deviceId)) {
                if (isValidLong(deviceId)) {
                    reqParams = reqParams + "&deviceid=" + deviceId;
                } else {
                    return toReasonDataRestBean(REQ_FAILURE, "invalid deviceid");
                }
            }
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            httpAuth = IPCUtil.generateHttpAuthorization(webAPIUser.getUserId(), webAPIUser.getPassword());
            sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            ipcAuthToken = sessionManager.getIpcProvAuthToken();
            String commHistUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.COMMUNICATION_HISTORY.toString(), reqParams, ipcSite.isSecure());
            boolean verbose = JiveGlobals.getBooleanProperty(String.format(PLUGIN_IPC_LOG_VERBOSE, ipcSite.getId()), false);
            if (verbose) {
                log.info(forLog + " cas-call-history url: " + commHistUrl);
            }

            String apiVer = ipcSite.getApiVersion();
            Response response = IPCProvHttpClient.invokeHTTPGET(commHistUrl, ipcAuthToken, apiVer, ipcSite.isSecure());

            Object respEntity = null;

            if (null != response) {
                String apiRes = response.readEntity(String.class);
                JAXBContext jaxbContext = UtilHelper.getJAXBContext();
                respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));
            }
            if (respEntity == null) {
                log.error(forLog + "Response Entity for Communication history  is null.");
                return toReasonDataRestBean(REQ_FAILURE, "no response received");
            }

            if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error(forLog + "CommunicationHistory error response " + formattedResponseXML);
                return toReasonDataRestBean(REQ_FAILURE, formattedResponseXML);
            }

            if (respEntity instanceof CommunicationHistoryResponse) {
                CommunicationHistoryResponse commHistoryRes = (CommunicationHistoryResponse) respEntity;
                String reasonDescription = commHistoryRes.getReason().getReasonDescription();
                if (reasonDescription.equalsIgnoreCase(NO_DATA_FOR_SEARCH_CRITERIA)) {
                    return toReasonDataRestBean(REQ_FAILURE, reasonDescription);
                }

                CommunicationHistoryList cdrList = commHistoryRes.getCommunicationHistoryList();
                return toCommunicationHistoryBeanList(cdrList);
            }
        } catch (Exception e) {
            log.error("Error fetching communication history : " + loginname, e);
        } finally {
            if (sessionManager != null) {
                sessionManager.deleteSession(httpAuth, ipcAuthToken, ipcSite.getApiVersion());
            }
        }
        return null;
    }

    private ResponseDataBean toReasonDataRestBean(String errorCode, String errorDesc) {
        ResponseDataBean responseDataBean = new ResponseDataBean();
        responseDataBean.setReasonCode(errorCode);
        responseDataBean.setReasonDescription(errorDesc);
        return responseDataBean;
    }

    private List<CommunicationHistoryBean> toCommunicationHistoryBeanList(CommunicationHistoryList cdrList) {
        List<CommunicationHistoryBean> chbList = new ArrayList<>();
        for (CommunicationHistory cdr : cdrList.getCommunicationHistory()) {
            try {
                CommunicationHistoryBean chbean = new CommunicationHistoryBean();
                chbean.setId(cdr.getId());
                chbean.setLastModified(cdr.getLastModified().toString());

                int userId = cdr.getUserId();
                chbean.setUserName("");
                chbean.setUserId(userId);
                chbean.setDeviceIdId(cdr.getDeviceIdId());
                chbean.setButtonNumber(cdr.getButtonNumber());
                int resourceAORId = cdr.getResourceAORId();
                chbean.setLineNumber("");
                chbean.setAppearance(cdr.getRolloverAppearance());
                chbean.setResourceAORId(resourceAORId);
                chbean.setDeviceChannel(cdr.getDeviceChannel().getValue());
                chbean.setDeviceChannelType(cdr.getDeviceChannelType().value());
                chbean.setDestination(cdr.getDestination().getValue());
                chbean.setEventType(cdr.getEventType().getValue());
                chbean.setStartTime(cdr.getStartTime().getValue().toString());
                chbean.setRingTime(cdr.getRingTime());
                chbean.setDuration(cdr.getDuration());
                chbean.setPttDuration(cdr.getPttDuration());
                chbean.setCliName(cdr.getCLIName().getValue());
                chbean.setCliNumber(cdr.getCLINumber().getValue());
                chbean.setReasonForDisconnect(cdr.getReasonForDisconnect().getValue());
                chbean.setCallUsage(cdr.getCallUsage().getValue().value());
                chbean.setAnsweringPartyName(cdr.getAnsweringPartyName().getValue());
                chbean.setPersonalPointOfContactId(cdr.getPersonalPointOfContactId());
                chbean.setParentUserCDIId(cdr.getParentUserCDIId());
                chbean.setPointOfContactId(cdr.getPointOfContactId());
                chbean.setTrunkId(cdr.getTrunkId());
                chbean.setTrunkBChannel(cdr.getTrunkBChannel());
                chbean.setE164Destination(cdr.getE164Destination().getValue());
                chbean.setRoutedDestination(cdr.getRoutedDestination().getValue());
                chbean.setEnterpriseCallId(cdr.getEnterpriseCallId().getValue());
                chbList.add(chbean);
            } catch (Exception e) {
                log.error("Error converting call-history response to bean list ", e);
            }
        }
        return chbList;
    }

    private boolean isValidTimeZone(String timezone) {
        boolean valid = false;
        try {
            if (StringUtils.isNotBlank(timezone) && null != TimeZone.fromValue(timezone.toUpperCase())) {
                valid = true;
            }
        } catch (Exception e) {
        }
        return valid;
    }

    private boolean isValidLong(String starttime) {
        boolean valid = false;
        try {
            if (StringUtils.isNotBlank(starttime) && Long.parseLong(starttime) > 0) {
                valid = true;
            }
        } catch (NumberFormatException e) {
        }
        return valid;
    }
}