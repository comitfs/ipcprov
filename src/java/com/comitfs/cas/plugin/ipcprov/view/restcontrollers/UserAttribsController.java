package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;


import com.comitfs.cas.plugin.ipcprov.beans.UserAttributes;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.UpdateEndUserAttributeHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.UserInformation;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.UserInfoRestBean;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ipcprov/api/userinfo")
public class UserAttribsController {
    private final Logger LOG = Logger.getLogger(UserAttribsController.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response userInfo(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName) {

        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {
                //fetch user info based on the username/id passed and serialize into JSON for response
                UserInformation userInformation = new UserInformation(ipcSite);
                UserAttributes user = userInformation.fetchUserInformation(loginName);

                LOG.debug("user.getFirstName(): " + user.getFirstName());

                UserInfoRestBean userInfo = new UserInfoRestBean();
                userInfo.setUserAttributes(user);
                return Response.status(Response.Status.OK)
                        .entity(userInfo).build();
            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
            }
        } catch (Exception e) {
            LOG.debug("Exception in UserAttribsController: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response userInfo(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName, UserInfoRestBean userInfo) {


        ResponseDataBean responseDataBean;
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {

                //call the handler
                UpdateEndUserAttributeHandler handler = new UpdateEndUserAttributeHandler(ipcSite);

                //TODO validate input data
                responseDataBean = handler.handle(loginName, userInfo.getUserAttributes());
            } else {
                responseDataBean = new ResponseDataBean();
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
            }
        } catch (Exception e) {
            LOG.error("Error in updating UserAttributes: ", e);
            responseDataBean = new ResponseDataBean();
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }

}