package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.beans.EnterpriseDirectoryResponseBean;
import com.comitfs.cas.plugin.ipcprov.beans.PersonalContactsBean;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.EnterpriseDirectory;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("ipcprov/api/enterprisedirectory")
public class EnterpriseDirectoryContacts {
    private final Logger LOG = Logger.getLogger(EnterpriseDirectoryContacts.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response enterpriseDirectory(@QueryParam("siteid") long siteId
            , @QueryParam("loginname") String loginName, @QueryParam("count") String countNo
            , @QueryParam("pageNum") String startPage) {
        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            int count = StringUtils.isEmpty(countNo) ? 400 : Integer.parseInt(countNo);

            int startPageNum = StringUtils.isEmpty(startPage) ? 1 : Integer.parseInt(startPage);

            if (ipcSite != null) {

                EnterpriseDirectory enterpriseDirectory = new EnterpriseDirectory(ipcSite, count, startPageNum);

                ArrayList<PersonalContactsBean> epContacts = (ArrayList) enterpriseDirectory.fetchEnterpriseDirectory(startPageNum, count);
                EnterpriseDirectoryResponseBean responseBean = new EnterpriseDirectoryResponseBean();
                responseBean.setEnterpriseContacts(epContacts);
                responseBean.setTotalEnterpriseContactRecs(enterpriseDirectory.getTotalRecsAvailable());
                responseBean.setCount(epContacts.size());
                responseBean.setPageNum(enterpriseDirectory.getStartPageNum());
                return Response.status(Response.Status.OK)
                        .entity(responseBean).build();


            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid ComponentName/Component does not exist");
            }

        } catch (Exception e) {
            LOG.error("Error in casbw: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();

        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();

    }


}
