package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.TraderFeaturesBean;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.UpdateTraderFeaturesHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.UserInformation;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.TraderFeaturesRestBean;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

@Path("ipcprov/api/traderinfo")
public class TraderFeaturesController {
    private final Logger LOG = Logger.getLogger(TraderFeaturesController.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response traderInfo(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName) {


        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {
                UserInformation userInformation = new UserInformation(ipcSite);
                UserPreferencesType userPreferencesTypeBean = userInformation.fetchUserPreferences(loginName);

                TraderFeaturesBean traderFeatures = new TraderFeaturesBean();

                traderFeatures.setPersonalExtn(userPreferencesTypeBean.getPersonalExtensionAOR() != null ? userPreferencesTypeBean.getPersonalExtensionAOR().getValue() : "");

                //TODO-personal extn override
                //traderFeatures.setPersonalExtnOverride();

                traderFeatures.setIntercomExtn(userPreferencesTypeBean.getIntercomExtension() != null ? userPreferencesTypeBean.getIntercomExtension().getValue() : "");


                JAXBElement<UserCDIAutoHold> autoHoldJaxbBeaEle = userPreferencesTypeBean.getAutoHold();
                UserCDIAutoHold autoHold = autoHoldJaxbBeaEle.getValue();
                if (autoHold != null)
                    traderFeatures.setAutoSelectHold(autoHold.value());

                traderFeatures.setLanguage(userPreferencesTypeBean.getLanguage() != null ? userPreferencesTypeBean.getLanguage().value() : "");
                traderFeatures.setAllowPrivacyToggle(String.valueOf(userPreferencesTypeBean.getTogglePrivacyAllowed() != null ? userPreferencesTypeBean.getTogglePrivacyAllowed().getValue() : ""));
                traderFeatures.setAlternateHSTimeout(String.valueOf(userPreferencesTypeBean.getHandsetSelectTimeout() != null ? userPreferencesTypeBean.getHandsetSelectTimeout().getValue() : ""));
                traderFeatures.setBlockingToneOnSpeakerMute(String.valueOf(userPreferencesTypeBean.getBlockingTone() != null ? userPreferencesTypeBean.getBlockingTone().getValue() : ""));

                JAXBElement<UserCDIHandsetSelectMode> hsSeletModebean = userPreferencesTypeBean.getHandsetSelectMode();
                if (hsSeletModebean != null) {
                    UserCDIHandsetSelectMode hsSelectMode = hsSeletModebean.getValue();
                    if (hsSelectMode != null) {
                        traderFeatures.setHandsetSelectMode(hsSelectMode.value());
                    }
                }

                JAXBElement<UserCDIHandsetMuteOption> hsMuteOptionJaxb = userPreferencesTypeBean.getHandsetMuteOption();
                if (hsMuteOptionJaxb != null) {
                    UserCDIHandsetMuteOption hsMuteOption = hsMuteOptionJaxb.getValue();
                    if (hsMuteOption != null)
                        traderFeatures.setHandSetMuteOption(hsMuteOption.value());
                }

                JAXBElement<UserCDIHandsetBtn> hsBtnJaxb = userPreferencesTypeBean.getHandsetBtn();
                if (hsBtnJaxb != null && hsBtnJaxb.getValue() != null) {
                    traderFeatures.setHandSetButtonActions(hsBtnJaxb.getValue().value());
                }

                traderFeatures.setFloatAllIncomingCalls(String.valueOf(userPreferencesTypeBean.getFloatAllIncoming() != null ? userPreferencesTypeBean.getFloatAllIncoming().getValue() : ""));
                traderFeatures.setFloatCallsOnHold(String.valueOf(userPreferencesTypeBean.getHoldQueue() != null ? userPreferencesTypeBean.getHoldQueue().getValue() : ""));
                traderFeatures.setForceTalkBackMute(String.valueOf(userPreferencesTypeBean.getForcedTalkbackMute() != null ? userPreferencesTypeBean.getForcedTalkbackMute().getValue() : ""));

                TraderFeaturesRestBean restBean = new TraderFeaturesRestBean();
                restBean.setLoginName(loginName);
                restBean.setTraderFeatures(traderFeatures);

                return Response.status(Response.Status.OK)
                        .entity(restBean).build();
            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");

            }

        } catch (Exception e) {
            LOG.error("Error in TraderFeaturesController: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response traderInfo(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName, TraderFeaturesRestBean traderFeaturesRestBeans) {

        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {


                //call the trader features update handler
                UpdateTraderFeaturesHandler handler = new UpdateTraderFeaturesHandler(ipcSite);
                responseDataBean = handler.handle(loginName, traderFeaturesRestBeans.getTraderFeatures());
            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");

            }
        } catch (Exception e) {
            LOG.error("Error in updating TraderFeatures: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }

}
