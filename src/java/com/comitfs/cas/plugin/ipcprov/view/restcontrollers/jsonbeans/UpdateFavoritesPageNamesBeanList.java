package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


import java.util.List;

public class UpdateFavoritesPageNamesBeanList {

    private List<UpdateFavPageNamesBean> updateFavPageNamesBeanList;

    public List<UpdateFavPageNamesBean> getUpdateFavPageNamesBeanList() {
        return updateFavPageNamesBeanList;
    }

    public void setUpdateFavPageNamesBeanList(List<UpdateFavPageNamesBean> updateFavPageNamesBeanList) {
        this.updateFavPageNamesBeanList = updateFavPageNamesBeanList;
    }
}
