package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import org.apache.log4j.Logger;

import com.comitfs.cas.plugin.ipcprov.beans.UserPreferences;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

@Path("ipcprov/api/userpreference")
public class FetchUserPreferences {
    private final Logger log = Logger.getLogger(FetchUserPreferences.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response userPreference(@QueryParam("siteid") Long siteId, @DefaultValue("false") @QueryParam("reload") Boolean reload, @QueryParam("loginname") String loginName) {

        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite = null;
            if (siteId == null || siteId == 0) {
                ipcSite = IPCSiteManager.getInstance().getIpcSites().get(0);
            } else {
                ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
            }


            if (ipcSite != null) {
                siteId = ipcSite.getId();
                if (reload) {
                    Map<Long, List<String>> siteUsers = IPCDataService.getInstance().getEnterpriseContactsForAllSites();
                    IPCDataService.getInstance().getUserPreferences(siteUsers);
                }

                if (loginName != null) {
                    IPCDataService.getInstance().getUserPreference(loginName, ipcSite);
                    IPCUser ipcUser = IPCDataService.getInstance().getIpcUserMap().get(loginName+"#"+siteId);
                    return Response.status(Response.Status.OK)
                            .entity(ipcUser).build();
                }
                List<IPCUser> ipcUsers = IPCDataService.getInstance().getIpcUserSiteMapCache().get(siteId);
                UserPreferences userPreferences = new UserPreferences();
                userPreferences.setIpcUsers(ipcUsers);
                return Response.status(Response.Status.OK)
                        .entity(userPreferences).build();


                //TODO: TBD: user should be able to fetch buttons info based on other params?

            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User/BW does not exist");
            }

        } catch (Throwable e) {
            log.error("Error in casbw: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }

}
