package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;


import com.comitfs.cas.plugin.ipcprov.beans.AssignRemoveUserRoleBean;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.AssignRemoveUserRoleHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ipcprov/api/assign-remove-roles")
public class AssignRemoveRoleRestController {
    private final Logger LOG = Logger.getLogger(AssignRemoveRoleRestController.class);


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignRemoveRoles(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName, AssignRemoveUserRoleBean roleManagementBean) {

        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {
                LOG.debug("ipcSite not null in AssignRemoveRoleRestController");
                //read the json input data and invoke the handler
                AssignRemoveUserRoleHandler roleManagementHandler = new AssignRemoveUserRoleHandler(ipcSite);
                responseDataBean = roleManagementHandler.handle(loginName, roleManagementBean);
            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid Request");
            }
        } catch (Exception e) {
            LOG.error("Error in Role Management: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription("Internal Server Error " + e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }
}
