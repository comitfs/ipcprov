package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.comitfs.cas.plugin.ipcprov.beans.CommunicationHistoryBean;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.CommunicationHistoryAPIImpl;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

@Path("ipcprov/api/ipc-call-history")
public class CommunicationHistoryController {
    private final Logger log = Logger.getLogger(CommunicationHistoryController.class);



    @GET
	@Produces(MediaType.APPLICATION_JSON)
    public Response ipcCallHistory(@QueryParam("siteid") long siteId,@QueryParam("loginname") String loginName,
    		@QueryParam("filtertype") String filtertype,@QueryParam("endtime") String endtime,
    		@QueryParam("starttime") String starttime,@QueryParam("timezone") String timezone,
    		@QueryParam("pagenum") String pagenum,@QueryParam("deviceid") String deviceid) {

    	ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
        	IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            CommunicationHistoryAPIImpl cdrImpl = new CommunicationHistoryAPIImpl(ipcSite);
            Object callHistory = cdrImpl.getCallHistory(filtertype, loginName, starttime, endtime, timezone, pagenum, deviceid);

            if (null == callHistory) {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("no response");
            }

            if (callHistory instanceof ResponseDataBean) {
            	 return Response.status(Response.Status.OK)
                         .entity(callHistory).build();
            }

            if (callHistory instanceof List<?>) {
                List<CommunicationHistoryBean> cdrList = (List<CommunicationHistoryBean>) callHistory;
                return Response.status(Response.Status.OK)
                        .entity(cdrList).build();
            }
        } catch (Exception e) {
            log.error("Error fetching IPC Communication-History ", e);
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }


}
