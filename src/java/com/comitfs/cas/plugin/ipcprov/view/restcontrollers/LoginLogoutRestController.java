package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.beans.ForceLogInLogOutBean;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.LoginForceLogoutHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ipcprov/api/loginLogoutDevice")
public class LoginLogoutRestController {

    private final Logger LOG = Logger.getLogger(LoginLogoutRestController.class);


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response loginLogoutDevice(@QueryParam("siteid") Long siteId, @QueryParam("loginname") String loginName, ForceLogInLogOutBean loginlogoutInfo) {
        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite;
            if (siteId == null || siteId == 0) {
                ipcSite = IPCSiteManager.getInstance().getIpcSites().get(0);
            } else {
                ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
            }

            if (ipcSite != null) {
                LoginForceLogoutHandler handler = new LoginForceLogoutHandler(ipcSite);
                responseDataBean = handler.handle(loginName, loginlogoutInfo);
            } else {
                responseDataBean = new ResponseDataBean();
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
            }
        } catch (Exception e) {
            LOG.error("Error in doPost() of LoginLogoutRestController: ", e);
            responseDataBean = new ResponseDataBean();
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
        }
        return Response.status(Response.Status.OK).entity(responseDataBean).build();
    }

}
