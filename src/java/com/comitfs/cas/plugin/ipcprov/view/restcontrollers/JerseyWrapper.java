package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.jivesoftware.admin.AuthCheckFilter;
import org.jivesoftware.util.JiveGlobals;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The Class JerseyWrapper.
 */
public class JerseyWrapper extends ServletContainer {

    /**
     * The Constant CUSTOM_AUTH_PROPERTY_NAME
     */
    private static final String CUSTOM_AUTH_PROPERTY_NAME = "plugin.ipcprov.customAuthFilter";

    /**
     * The Constant REST_AUTH_TYPE
     */
    private static final String REST_AUTH_TYPE = "plugin.ipcprov.httpAuth";

    /**
     * The Constant AUTHFILTER.
     */
    private static final String AUTHFILTER = "com.comitfs.cas.plugin.ipcprov.AuthFilter";

    /**
     * The Constant CORSFILTER.
     */
    private static final String CORSFILTER = "com.comitfs.cas.plugin.ipcprov.CORSFilter";

    /**
     * The Constant CONTAINER_REQUEST_FILTERS.
     */
    private static final String CONTAINER_REQUEST_FILTERS = "com.sun.jersey.spi.container.ContainerRequestFilters";

    /**
     * The Constant CONTAINER_RESPONSE_FILTERS.
     */
    private static final String CONTAINER_RESPONSE_FILTERS = "com.sun.jersey.spi.container.ContainerResponseFilters";

    /**
     * The Constant GZIP_FILTER.
     */
    private static final String GZIP_FILTER = "com.sun.jersey.api.container.filter.GZIPContentEncodingFilter";

    /**
     * The Constant RESOURCE_CONFIG_CLASS_KEY.
     */
    private static final String RESOURCE_CONFIG_CLASS_KEY = "com.sun.jersey.config.property.resourceConfigClass";

    /**
     * The Constant RESOURCE_CONFIG_CLASS.
     */
    private static final String RESOURCE_CONFIG_CLASS = "com.sun.jersey.api.core.PackagesResourceConfig";

    /**
     * The Constant SCAN_PACKAGE_DEFAULT.
     */
    private static final String SCAN_PACKAGE_DEFAULT = JerseyWrapper.class.getPackage().getName();

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The Constant SERVLET_URL.
     */
    private static final String SERVLET_URL = "ipcprov/*";
    /**
     * The Constant JERSEY_LOGGER.
     */
    private final static Logger JERSEY_LOGGER = Logger.getLogger("com.sun.jersey");
    /**
     * The config.
     */
    private static final Map<String, Object> config;
    /**
     * The prc.
     */
    private static final ResourceConfig prc;
    private static String loadingStatusMessage = null;

    static {
        JERSEY_LOGGER.setLevel(Level.SEVERE);
        config = new HashMap<String, Object>();
        config.put(RESOURCE_CONFIG_CLASS_KEY, RESOURCE_CONFIG_CLASS);
        config.put("com.sun.jersey.api.json.POJOMappingFeature", true);
        config.put(CONTAINER_REQUEST_FILTERS, AUTHFILTER);
        prc = new ResourceConfig().packages(SCAN_PACKAGE_DEFAULT);
        prc.addProperties(config);
//        prc.getProperties().put(CONTAINER_RESPONSE_FILTERS, CORSFILTER);
        // prc.getProperties().put(CONTAINER_RESPONSE_FILTERS, GZIP_FILTER);
        //  loadAuthenticationFilter();

        prc.register(AddLineToButtonSheetController.class);
        prc.register(AddRemoveUserFromGroupController.class);
        prc.register(AssignRemoveRoleRestController.class);
        prc.register(AssignRevokeUserLicenseController.class);
        prc.register(AudioPreferencesController.class);
        prc.register(ChangePasswordRestController.class);
        prc.register(CommunicationHistoryController.class);
        prc.register(CreateEndUserAcctController.class);
        prc.register(EnterpriseDirectoryContacts.class);
        prc.register(FetchAuthDomainInfoController.class);
        prc.register(FetchFixedButtonsRestController.class);
        prc.register(FetchRolesRestController.class);
        prc.register(FetchTurretPagesRestController.class);
        prc.register(FetchUserGroupsRestController.class);
        prc.register(GetButtonInfoController.class);
        prc.register(LoginLogoutRestController.class);
        prc.register(PersonalContactsController.class);
        prc.register(TraderFeaturesController.class);
        prc.register(UpdateFavoritePageNamesController.class);
        prc.register(UpdateFixedButtonController.class);
        prc.register(UserAttribsController.class);
        prc.register(FetchUserPreferences.class);
        prc.register(FetchIPCSites.class);
        prc.register(FetchLogonSessionInfo.class);
        prc.register(FetchRecorderMixProfiles.class);
        prc.register(UpdateUserAudioPreference.class);
        prc.register(LicenseInformationRestController.class);
        prc.register(TurretInventoryController.class);
    }

    /**
     * Instantiates a new jersey wrapper.
     */
    public JerseyWrapper() {
        super(prc);
    }

    public static String tryLoadingAuthenticationFilter(String customAuthFilterClassName) {

        try {
            if (customAuthFilterClassName != null) {
                Class.forName(customAuthFilterClassName, false, JerseyWrapper.class.getClassLoader());
                loadingStatusMessage = null;
            }
        } catch (ClassNotFoundException e) {
            loadingStatusMessage = "No custom auth filter found for IPCProv plugin with name " + customAuthFilterClassName;
        }

        if (customAuthFilterClassName == null || customAuthFilterClassName.isEmpty())
            loadingStatusMessage = "Classname field can't be empty!";
        return loadingStatusMessage;
    }

    public static String loadAuthenticationFilter() {

        // Check if custom AuthFilter is available
        String customAuthFilterClassName = JiveGlobals.getProperty(CUSTOM_AUTH_PROPERTY_NAME);
        String restAuthType = JiveGlobals.getProperty(REST_AUTH_TYPE);
        String pickedAuthFilter = AUTHFILTER;

        try {
            if (customAuthFilterClassName != null && "custom".equals(restAuthType)) {
                Class.forName(customAuthFilterClassName, false, JerseyWrapper.class.getClassLoader());
                pickedAuthFilter = customAuthFilterClassName;
                loadingStatusMessage = null;
            }
        } catch (ClassNotFoundException e) {
            loadingStatusMessage = "No custom auth filter found for IPCProv plugin! " + customAuthFilterClassName + " " + restAuthType;
        }

        // prc.getProperties().put(CONTAINER_REQUEST_FILTERS, GZIP_FILTER);
        prc.getProperties().put(CONTAINER_REQUEST_FILTERS, pickedAuthFilter);
        return loadingStatusMessage;
    }

    /*
     * Returns the loading status message.
     *
     * @return the loading status message.
     */
    public static String getLoadingStatusMessage() {
        return loadingStatusMessage;
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
     */
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        // loadAuthenticationFilter();
        super.init(servletConfig);
        // Exclude this servlet from requering the user to login
        AuthCheckFilter.addExclude(SERVLET_URL);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.sun.jersey.spi.container.servlet.ServletContainer#destroy()
     */
    @Override
    public void destroy() {
        super.destroy();
        // Release the excluded URL
        AuthCheckFilter.removeExclude(SERVLET_URL);
    }

}
