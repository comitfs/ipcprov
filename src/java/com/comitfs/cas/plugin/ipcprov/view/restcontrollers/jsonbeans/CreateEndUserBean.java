package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

public class CreateEndUserBean {
    private String loginName;
    private String password;
    private String passwordChangeMode;
    private String dateDisplayFormat;
    private String timeDisplayFormat;
    private String isTemporary;
    private String authType;
    private String firstName;
    private String lastName;
    private String locale;
    private String title;
    private String email;
    private String locationId;
    private String accountPolicyId;
    private String authDomainId;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordChangeMode() {
        return passwordChangeMode;
    }

    public void setPasswordChangeMode(String passwordChangeMode) {
        this.passwordChangeMode = passwordChangeMode;
    }

    public String getDateDisplayFormat() {
        return dateDisplayFormat;
    }

    public void setDateDisplayFormat(String dateDisplayFormat) {
        this.dateDisplayFormat = dateDisplayFormat;
    }

    public String getTimeDisplayFormat() {
        return timeDisplayFormat;
    }

    public void setTimeDisplayFormat(String timeDisplayFormat) {
        this.timeDisplayFormat = timeDisplayFormat;
    }

    public String getIsTemporary() {
        return isTemporary;
    }

    public void setIsTemporary(String isTemporary) {
        this.isTemporary = isTemporary;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getAccountPolicyId() {
        return accountPolicyId;
    }

    public void setAccountPolicyId(String accountPolicyId) {
        this.accountPolicyId = accountPolicyId;
    }

    public String getAuthDomainId() {
        return authDomainId;
    }

    public void setAuthDomainId(String authDomainId) {
        this.authDomainId = authDomainId;
    }
}
