package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResourceAORLineBean {

    private String resourceAORId;
    private String descriptor;
    private String appearance;
    private String type;
    private String resourceAOR;

    public String getResourceAORId() {
        return resourceAORId;
    }

    public void setResourceAORId(String resourceAORId) {
        this.resourceAORId = resourceAORId;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResourceAOR() {
        return resourceAOR;
    }

    public void setResourceAOR(String resourceAOR) {
        this.resourceAOR = resourceAOR;
    }
}
