package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.FetchAuthDomains;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.AuthDomainInfo;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.AuthDomainRespBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("ipcprov/api/authdomaininfo")
public class FetchAuthDomainInfoController {
    private final Logger log = Logger.getLogger(FetchTurretPagesRestController.class);


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response authDomainInfo(@QueryParam("siteid") long siteId) {

        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {

            //componentName. ex:ipc1
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
            if (ipcSite == null) {
                log.debug("Fetch Auth Domain Info - Component null for siteId " + siteId);
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("IPC Component not exist");
            }


            if (ipcSite != null) {
                FetchAuthDomains fetchAuthDomains = new FetchAuthDomains(ipcSite);
                ArrayList<AuthDomainInfo> authInfoList = fetchAuthDomains.fetchAuthDomains();
                AuthDomainRespBean responseBean = new AuthDomainRespBean();
                responseBean.setAuthDomains(authInfoList);
            }

        } catch (Exception e) {
            log.error("Error in casbw: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }


}
