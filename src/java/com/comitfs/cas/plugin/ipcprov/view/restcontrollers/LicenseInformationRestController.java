package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.ipc.bw.jaxb.LicenseFeatureData;
import com.comitfs.cas.plugin.ipcprov.beans.LicenseFeatureDataBean;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.LicenseInformationHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.LicenseInfoRestBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("ipcprov/api/licenseInformation")
public class LicenseInformationRestController {
    private final Logger log = Logger.getLogger(LicenseInformationRestController.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response licenseInformation(@QueryParam("siteid") Long siteId, @QueryParam("loginname") String loginName, @QueryParam("filter") String queryFilter) {
        try {
            IPCSite ipcSite;
            if (siteId == null || siteId == 0) {
                ipcSite = IPCSiteManager.getInstance().getIpcSites().get(0);
            } else {
                ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
            }

            if (null == ipcSite) {
                ResponseDataBean responseDataBean = new ResponseDataBean();
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
                return Response.status(Response.Status.OK).entity(responseDataBean).build();
            }

            LicenseInformationHandler licenseInformationHandler = new LicenseInformationHandler(ipcSite);
            List<LicenseFeatureDataBean> licenseFeatureData = licenseInformationHandler.fetchLicenseInfo(loginName, queryFilter, 1);
            LicenseInfoRestBean restBean = new LicenseInfoRestBean(loginName, licenseFeatureData);
            return Response.status(Response.Status.OK).entity(restBean).build();
        } catch (Exception e) {
            log.error("Error fetching license information of user: " + loginName, e);
            ResponseDataBean responseDataBean = new ResponseDataBean();
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseDataBean).build();
        }
    }
}
