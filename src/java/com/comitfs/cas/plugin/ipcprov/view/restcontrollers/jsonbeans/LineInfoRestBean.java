package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

import java.io.Serializable;
import java.util.List;

import com.comitfs.cas.plugin.ipcprov.beans.LineInfoBean;

public class LineInfoRestBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<LineInfoBean> lineResourceInformation;
    private ResponseDataBean reasonData;

    public ResponseDataBean getReasonData() {
        return reasonData;
    }

    public void setReasonData(ResponseDataBean reasonData) {
        this.reasonData = reasonData;
    }

    public List<LineInfoBean> getLineResourceInformation() {
        return lineResourceInformation;
    }

    public void setLineResourceInformation(List<LineInfoBean> lineResourceInformation) {
        this.lineResourceInformation = lineResourceInformation;
    }
}


