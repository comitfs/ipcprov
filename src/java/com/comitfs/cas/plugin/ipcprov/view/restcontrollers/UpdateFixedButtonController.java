package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;


import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.UpdateFixedButtonHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.FixedButtonBeanList;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

@Path("ipcprov/api/update-fixed-buttons")
public class UpdateFixedButtonController {
    private final Logger LOG = Logger.getLogger(UpdateFixedButtonController.class);
 

    @POST
	@Produces(MediaType.APPLICATION_JSON)
    public Response updateFixedButtons(@QueryParam("siteid") long siteId,@QueryParam("loginname") String loginName,FixedButtonBeanList fixedButtonBeanList) {
        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
        	IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {

                UpdateFixedButtonHandler updateFixedButtonHandler = new UpdateFixedButtonHandler(ipcSite);
                responseDataBean = updateFixedButtonHandler.handle(loginName, fixedButtonBeanList);

            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid Request");
            }
        } catch (Exception e) {
            LOG.error("Error in creating end user account: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription("Internal Server Error " + e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }
}
