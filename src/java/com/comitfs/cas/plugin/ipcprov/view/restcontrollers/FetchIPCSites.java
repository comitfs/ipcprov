package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.comitfs.cas.plugin.ipcprov.beans.IPCSiteBean;
import com.comitfs.cas.plugin.ipcprov.beans.IPCSiteList;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

@Path("ipcprov/api/ipcsites")
public class FetchIPCSites {
	private final Logger log = Logger.getLogger(FetchIPCSites.class);


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response ipcSites() {
		ResponseDataBean responseDataBean = new ResponseDataBean();
		try {
			List<IPCSite> ipcSites= new ArrayList<IPCSite>(IPCSiteManager.getInstance().getIpcSiteMap().values()); 
			if (ipcSites != null) {
				IPCSiteList ipcSiteResponse = new IPCSiteList();
				ipcSites.forEach(ipcSite ->{
					IPCSiteBean ipcSiteBean = new IPCSiteBean();
					ipcSiteBean.setSiteId(ipcSite.getId());
					ipcSiteBean.setSiteName(ipcSite.getName());
					ipcSiteResponse.getIpcSiteBeans().add(ipcSiteBean);
				});
				return Response.status(Response.Status.OK)
						.entity(ipcSiteResponse).build();
			}

		} catch (Throwable e) {
			log.error("Error in casbw: ", e);
			responseDataBean.setReasonCode("REQ_FAILURE");
			responseDataBean.setReasonDescription(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(responseDataBean).build();
		}
		return Response.status(Response.Status.OK)
				.entity(responseDataBean).build();
	}

		
}
