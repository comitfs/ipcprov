package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.comitfs.cas.plugin.ipcprov.beans.RecorderProfile;
import com.comitfs.cas.plugin.ipcprov.beans.RecorderProfiles;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.FetchRecorderProfiles;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

@Path("ipcprov/api/recoderprofiles")
public class FetchRecorderMixProfiles {


	private final Logger log = Logger.getLogger(FetchRecorderMixProfiles.class);


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response recoderProfiles(@QueryParam("siteid") Long siteId) {

		ResponseDataBean responseDataBean = new ResponseDataBean();
		try {
			IPCSite ipcSite = null;
			if(siteId == null || siteId == 0) {
				ipcSite = IPCSiteManager.getInstance().getIpcSites().get(0);
			}else {
				ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
			}


			if (ipcSite != null) {

				FetchRecorderProfiles fectFetchRecorderProfiles = new FetchRecorderProfiles(ipcSite);
				List<RecorderProfile> recorderProfileList =  fectFetchRecorderProfiles.fetchRecorderMixProfiles();
				RecorderProfiles recorderProfiles = new RecorderProfiles();
				recorderProfiles.setRecorderProfiles(recorderProfileList);
				return Response.status(Response.Status.OK)
						.entity(recorderProfileList).build();


				//TODO: TBD: user should be able to fetch buttons info based on other params?

			} else {
				responseDataBean.setReasonCode("REQ_FAILURE");
				responseDataBean.setReasonDescription("Invalid login name/User/BW does not exist");
			}

		} catch (Throwable e) {
			log.error("Error in casbw: ", e);
			responseDataBean.setReasonCode("REQ_FAILURE");
			responseDataBean.setReasonDescription(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(responseDataBean).build();
		}
		return Response.status(Response.Status.OK)
				.entity(responseDataBean).build();
	}

}

