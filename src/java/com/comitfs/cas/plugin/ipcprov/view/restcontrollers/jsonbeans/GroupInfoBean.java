package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

import java.util.List;

public class GroupInfoBean {
    private List<UserGroupInfo> userGroups;

    public List<UserGroupInfo> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<UserGroupInfo> userGroups) {
        this.userGroups = userGroups;
    }
}
