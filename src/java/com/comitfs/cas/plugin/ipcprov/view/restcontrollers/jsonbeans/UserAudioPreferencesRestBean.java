package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


import java.io.Serializable;

import com.comitfs.cas.plugin.ipcprov.beans.UserAudioPreferencesBean;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserAudioPreferencesRestBean implements Serializable{

    private UserAudioPreferencesBean audioPreferences;

    public UserAudioPreferencesBean getAudioPreferences() {
        return audioPreferences;
    }

    public void setAudioPreferences(UserAudioPreferencesBean audioPreferences) {
        this.audioPreferences = audioPreferences;
    }
}
