package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.beans.AddRemoveUsrFromGroupBean;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.AddRemoveUserFromEndUserGroup;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ipcprov/api/add-remove-user-group")
public class AddRemoveUserFromGroupController {
    private final Logger LOG = Logger.getLogger(AddRemoveUserFromGroupController.class);


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addRemoveUserGroup(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName, AddRemoveUsrFromGroupBean addRemoveUsrFromGroupBean) {

        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
            if (ipcSite != null) {

                AddRemoveUserFromEndUserGroup addRemoveUserFromEndUserGroup = new AddRemoveUserFromEndUserGroup(ipcSite);
                responseDataBean = addRemoveUserFromEndUserGroup.handle(loginName, addRemoveUsrFromGroupBean);


            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid User");
            }
        } catch (Exception e) {
            LOG.error("Error in add-remove user group membership: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription("Internal Server Error " + e.getMessage());

        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }
}
