package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;


import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.UpdateUserAudioPreferencesHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.UserAudioPreferencesRestBean;
import org.apache.log4j.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ipcprov/api/audiopreferences")
public class AudioPreferencesController {
    private final Logger LOG = Logger.getLogger(AudioPreferencesController.class);


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response audioPreferences(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName, UserAudioPreferencesRestBean audioPreferencesInfo) {

        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {

                //read the json input data and invoke the handler
                UpdateUserAudioPreferencesHandler handler = new UpdateUserAudioPreferencesHandler(ipcSite);

                responseDataBean = handler.handle(loginName, audioPreferencesInfo.getAudioPreferences());

            } else {
                responseDataBean = new ResponseDataBean();
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
            }

        } catch (Exception e) {
            LOG.error("Error in updating UserAudioPreferences: ", e);
            responseDataBean = new ResponseDataBean();
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }


}
