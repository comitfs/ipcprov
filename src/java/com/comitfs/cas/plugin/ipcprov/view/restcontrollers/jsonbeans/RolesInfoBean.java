package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

import java.util.List;

public class RolesInfoBean {
    private List<RoleInfo> roleInfo;

    public List<RoleInfo> getRoleInfo() {
        return roleInfo;
    }

    public void setRoleInfo(List<RoleInfo> roleInfo) {
        this.roleInfo = roleInfo;
    }
}
