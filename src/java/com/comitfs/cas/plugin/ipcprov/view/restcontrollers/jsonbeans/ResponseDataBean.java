package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


public class ResponseDataBean {

    private String reasonCode;
    private String reasonDescription;

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }
}
