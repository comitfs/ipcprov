package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


public class UpdateFavPageNamesBean {

    private String id;
    private String parentUserMercuryId;
    private String pageNumber;
    private String pageName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentUserMercuryId() {
        return parentUserMercuryId;
    }

    public void setParentUserMercuryId(String parentUserMercuryId) {
        this.parentUserMercuryId = parentUserMercuryId;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }
}
