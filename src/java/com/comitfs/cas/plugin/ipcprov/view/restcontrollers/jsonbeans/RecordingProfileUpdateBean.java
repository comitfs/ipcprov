package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class RecordingProfileUpdateBean {

	private String deviceModel;
	private Boolean recording;
	private int recordingProfile;
}
