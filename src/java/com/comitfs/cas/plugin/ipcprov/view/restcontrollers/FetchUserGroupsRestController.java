package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.FetchUserGroups;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.GroupInfoBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.UserGroupInfo;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("ipcprov/api/usergroupinfo")
public class FetchUserGroupsRestController {
    private final Logger log = Logger.getLogger(FetchUserGroupsRestController.class);


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response userGroupInfo(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName) {

        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);


            if (ipcSite == null) {
                log.debug("Fetch Role Info - Component null for ipcSite " + ipcSite);
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("IPC Component not exist");
            }


            if (ipcSite != null) {
                FetchUserGroups fetchUserGroups = new FetchUserGroups(ipcSite);

                ArrayList<UserGroupInfo> userGroupsList = fetchUserGroups.fetchUserGroups();


                GroupInfoBean groupInfoBean = new GroupInfoBean();
                groupInfoBean.setUserGroups(userGroupsList);

                return Response.status(Response.Status.OK)
                        .entity(groupInfoBean).build();

            }

        } catch (Exception e) {
            log.error("Error in casbw: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }


}
