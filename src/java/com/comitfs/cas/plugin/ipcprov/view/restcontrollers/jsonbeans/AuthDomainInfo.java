package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

public class AuthDomainInfo {
    private String authDomainId;
    private String authDomainName;

    public String getAuthDomainId() {
        return authDomainId;
    }

    public void setAuthDomainId(String authDomainId) {
        this.authDomainId = authDomainId;
    }

    public String getAuthDomainName() {
        return authDomainName;
    }

    public void setAuthDomainName(String authDomainName) {
        this.authDomainName = authDomainName;
    }
}
