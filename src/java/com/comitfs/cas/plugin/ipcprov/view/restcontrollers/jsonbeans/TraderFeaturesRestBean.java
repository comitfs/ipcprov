package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


import com.comitfs.cas.plugin.ipcprov.beans.TraderFeaturesBean;

public class TraderFeaturesRestBean {

    private String loginName;
    private TraderFeaturesBean traderFeatures;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public TraderFeaturesBean getTraderFeatures() {
        return traderFeatures;
    }

    public void setTraderFeatures(TraderFeaturesBean traderFeatures) {
        this.traderFeatures = traderFeatures;
    }
}
