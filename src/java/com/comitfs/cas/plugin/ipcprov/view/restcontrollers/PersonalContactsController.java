package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.beans.PersonalContactsBean;
import com.comitfs.cas.plugin.ipcprov.beans.PersonalContactsResponseBean;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.PersonalContactsInformation;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("ipcprov/api/personalcontacts")
public class PersonalContactsController {
    private final Logger LOG = Logger.getLogger(PersonalContactsController.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response personalContacts(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName) {
        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {
                PersonalContactsInformation personalContacts = new PersonalContactsInformation(ipcSite);

                ArrayList<PersonalContactsBean> pContacts = (ArrayList) personalContacts.fetchPersonalContacts(loginName, 1);
                PersonalContactsResponseBean responseBean = new PersonalContactsResponseBean();
                responseBean.setPersonalContacts(pContacts);

                return Response.status(Response.Status.OK)
                        .entity(responseBean).build();
            } else {
                LOG.debug("Fetch Fixed Buttons - Component null for loginName " + loginName);
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
            }

        } catch (Exception e) {
            LOG.error("Error in casbw: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }

}
