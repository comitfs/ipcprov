package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


import com.comitfs.cas.ipc.bw.jaxb.PersonalContactLocale;
import com.comitfs.cas.ipc.bw.jaxb.UserDateDisplayFormat;
import com.comitfs.cas.ipc.bw.jaxb.UserTimeDisplayFormat;

public class UserSpecificDataFormatsRestBean {

    private UserTimeDisplayFormat[] timeDisplayFormat;
    private UserDateDisplayFormat[] dateDisplayFormat;
    private PersonalContactLocale[] locale;

    public UserSpecificDataFormatsRestBean(String version) {
        this.timeDisplayFormat = UserTimeDisplayFormat.values();
        this.dateDisplayFormat = UserDateDisplayFormat.values();
        this.locale = PersonalContactLocale.values();

    }

    public PersonalContactLocale[] getLocale() {
        return locale;
    }

    public void setLocale(PersonalContactLocale[] locale) {
        this.locale = locale;
    }

    public UserTimeDisplayFormat[] getTimeDisplayFormat() {
        return timeDisplayFormat;
    }

    public void setTimeDisplayFormat(UserTimeDisplayFormat[] timeDisplayFormat) {
        this.timeDisplayFormat = timeDisplayFormat;
    }

    public UserDateDisplayFormat[] getDateDisplayFormat() {
        return dateDisplayFormat;
    }

    public void setDateDisplayFormat(UserDateDisplayFormat[] dateDisplayFormat) {
        this.dateDisplayFormat = dateDisplayFormat;
    }

}
