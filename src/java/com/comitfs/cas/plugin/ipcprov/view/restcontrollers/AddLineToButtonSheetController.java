package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;


import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.AddLineToButtonSheetHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.LineResourceInformation;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.AddLineToButtonRestBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.LineInfoRestBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import com.fasterxml.jackson.databind.ObjectMapper;
@Path("ipcprov/api/lineinfo")
public class AddLineToButtonSheetController  {
    private final Logger log = Logger.getLogger(AddLineToButtonSheetController.class);
    
    @GET
	@Produces(MediaType.APPLICATION_JSON)
    public Response lineinfo(@QueryParam("siteid") long siteId,@QueryParam("loginname") String loginName,@QueryParam("linetype") String lineType) {
        
    	ResponseDataBean responseDataBean = new ResponseDataBean();
        LineInfoRestBean lineInfo = new LineInfoRestBean();
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
            log.debug("line type "+lineType);
            if (ipcSite != null) {
                //fetch the lines info from Unigy
                LineResourceInformation fetchLineResourceInfo = new LineResourceInformation(ipcSite);
                lineInfo = fetchLineResourceInfo.fetchLineInformation(1, lineType);
            } else {
            	responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
                lineInfo.setReasonData(responseDataBean);
            }
        } catch (Exception e) {
            log.error("Error in AddLineToButtonSheetController: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
       
        return Response.status(Response.Status.OK)
                .entity(lineInfo).build();
    }

    @POST
	@Produces(MediaType.APPLICATION_JSON)
    public Response lineinfo(@QueryParam("siteid") long siteId,@QueryParam("loginname") String loginName,AddLineToButtonRestBean lineInfoRestBean) {
    	ResponseDataBean finalResponse = new ResponseDataBean();

        try {
        	 IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {

                AddLineToButtonSheetHandler handler = new AddLineToButtonSheetHandler(ipcSite);
                finalResponse = handler.handle(loginName, lineInfoRestBean);

            } else {
                //this is incase of request is raised directly to the plugin by-passing REST plugin. So need to check for user validation.
                log.debug("invalid loginname in AddLineInfoCOntroller");
                finalResponse.setReasonCode("REQ_FAILURE");
                finalResponse.setReasonDescription("Invalid login name/User does not exist");
            }
            ObjectMapper Obj = new ObjectMapper();
            Obj.writeValueAsString(finalResponse);
        } catch (Exception e) {
            log.error("Error in AddLineToButtonSheetController: ", e);
            finalResponse.setReasonCode("REQ_FAILURE");
            finalResponse.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(finalResponse).build();
        }
        return Response.status(Response.Status.OK)
                .entity(finalResponse).build();
    }


}
