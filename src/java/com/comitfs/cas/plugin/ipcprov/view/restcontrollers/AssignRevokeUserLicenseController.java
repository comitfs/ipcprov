package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.beans.AssignRevokeLicenseBean;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.AssignRevokeUserLicense;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ipcprov/api/assign-remove-user-license")
public class AssignRevokeUserLicenseController {
    private final Logger LOG = Logger.getLogger(AssignRevokeUserLicenseController.class);


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignRemoveUserLicense(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName, AssignRevokeLicenseBean assignRevokeLicenseBean) {

        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
            if (ipcSite != null) {

                AssignRevokeUserLicense assignRevokeUserLicense = new AssignRevokeUserLicense(ipcSite);
                responseDataBean = assignRevokeUserLicense.handleResponse(loginName, assignRevokeLicenseBean);
            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid User");
            }
        } catch (Exception e) {
            LOG.error("Error in add-remove user group membership: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription("Internal Server Error " + e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }
}
