package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

import java.util.List;

public class AuthDomainRespBean {
    private List authDomains;

    public List getAuthDomains() {
        return authDomains;
    }

    public void setAuthDomains(List authDomains) {
        this.authDomains = authDomains;
    }
}
