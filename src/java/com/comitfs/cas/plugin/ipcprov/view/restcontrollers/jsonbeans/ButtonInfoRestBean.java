package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


import java.util.List;

import com.comitfs.cas.plugin.ipcprov.beans.ButtonSettingsBean;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ButtonInfoRestBean {

    private String loginName;
    private List<ButtonSettingsBean> buttons;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public List<ButtonSettingsBean> getButtons() {
        return buttons;
    }

    public void setButtons(List<ButtonSettingsBean> buttons) {
        this.buttons = buttons;
    }
}
