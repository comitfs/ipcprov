package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


public class FixedButtonBean {

    private String id;
    private String buttonId;
    private String buttonLabel;
    private String buttonNumber;
    private String userMercuryId;
    private String slotPosition;
    private String buttonType;
    private String userCDIId;

    public String getButtonId() {
        return buttonId;
    }

    public void setButtonId(String buttonId) {
        this.buttonId = buttonId;
    }

    public String getButtonLabel() {
        return buttonLabel;
    }

    public void setButtonLabel(String buttonLabel) {
        this.buttonLabel = buttonLabel;
    }

    public String getButtonNumber() {
        return buttonNumber;
    }

    public void setButtonNumber(String buttonNumber) {
        this.buttonNumber = buttonNumber;
    }

    public String getUserMercuryId() {
        return userMercuryId;
    }

    public void setUserMercuryId(String userMercuryId) {
        this.userMercuryId = userMercuryId;
    }

    public String getSlotPosition() {
        return slotPosition;
    }

    public void setSlotPosition(String slotPosition) {
        this.slotPosition = slotPosition;
    }

    public String getButtonType() {
        return buttonType;
    }

    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserCDIId() {
        return userCDIId;
    }

    public void setUserCDIId(String userCDIId) {
        this.userCDIId = userCDIId;
    }
}
