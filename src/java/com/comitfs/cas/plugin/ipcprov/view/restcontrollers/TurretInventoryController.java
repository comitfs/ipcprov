package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.beans.TurretInventoryRespBean;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.DeviceInventory;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ipcprov/api/turret-inventory")
public class TurretInventoryController {
    private final Logger log = Logger.getLogger(TurretInventoryController.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response turretPageInfo(@NotNull @QueryParam("siteid") long siteId, @DefaultValue("0") @QueryParam("pagenum") long pageNum) {
        ResponseDataBean responseDataBean = new ResponseDataBean();

        IPCSite ipcSite = null;
        try {
            ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite == null) {
                log.debug("Turret inventory - invalid site ");
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid siteid");
                return Response.status(Response.Status.OK).entity(responseDataBean).build();
            }

            DeviceInventory di = new DeviceInventory(ipcSite);
            if (pageNum == 0) {
                pageNum = 1;
            }
            TurretInventoryRespBean turretInventoryRespBean = di.turretInventory(pageNum);
            int feedback = turretInventoryRespBean.getFeedback();

            if (feedback == -1) {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription(turretInventoryRespBean.getRespDesc());
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseDataBean).build();
            } else if (feedback == 0) {
                responseDataBean.setReasonCode("REQ_SUCCESS");
                responseDataBean.setReasonDescription(turretInventoryRespBean.getRespDesc());
                return Response.status(Response.Status.OK).entity(responseDataBean).build();
            } else {
                return Response.status(Response.Status.OK).entity(turretInventoryRespBean).build();
            }
        } catch (Exception e) {
            log.error("error fetching turret inventory from site: " + (null != ipcSite ? ipcSite.getName() : ""), e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseDataBean).build();
        }
    }
}
