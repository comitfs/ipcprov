package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

import java.util.List;

public class TurretPagesBeans {

    private List<TurretPageBean> turretPages;

    public List<TurretPageBean> getTurretPages() {
        return turretPages;
    }

    public void setTurretPages(List<TurretPageBean> turretPages) {
        this.turretPages = turretPages;
    }
}
