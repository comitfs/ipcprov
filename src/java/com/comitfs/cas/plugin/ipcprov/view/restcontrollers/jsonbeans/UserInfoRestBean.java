package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

import com.comitfs.cas.plugin.ipcprov.beans.UserAttributes;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfoRestBean {

    private UserAttributes userAttributes;

    public UserAttributes getUserAttributes() {
        return userAttributes;
    }

    public void setUserAttributes(UserAttributes userAttributes) {
        this.userAttributes = userAttributes;
    }
}
