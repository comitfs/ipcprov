package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.ButtonSettingsBean;
import com.comitfs.cas.plugin.ipcprov.beans.ResourceAORLineBean;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.UpdateEndUserButtonSettingsHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.ButtonInformation;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ButtonInfoRestBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.jivesoftware.util.Log;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Path("ipcprov/api/buttoninfo")
public class GetButtonInfoController {
    private final Logger LOG = Logger.getLogger(GetButtonInfoController.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buttonInfo(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName, @QueryParam("buttontype") String buttonType) {

        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);


            if (ipcSite != null) {
                //TODO: TBD: user should be able to fetch buttons info based on other params?

                List<ButtonEx> buttons = getButtonInformation(loginName, buttonType, ipcSite);
                List<ButtonSettingsBean> buttonSettingsBeanList = parseButtonInfo(buttons);

                ButtonInfoRestBean resp = new ButtonInfoRestBean();
                resp.setLoginName(loginName);
                resp.setButtons(buttonSettingsBeanList);
                return Response.status(Response.Status.OK)
                        .entity(resp).build();
            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
            }

        } catch (Exception e) {
            LOG.error("Error in casbw: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response buttonInfo(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName, ButtonInfoRestBean buttonInfoRestBean) {

        ResponseDataBean responseDataBean = new ResponseDataBean();
        ObjectMapper Obj = new ObjectMapper();
        String jsonStr = null;
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);


            if (ipcSite != null) {
                List<ButtonSettingsBean> buttonBeans = buttonInfoRestBean.getButtons();

                UpdateEndUserButtonSettingsHandler handler = new UpdateEndUserButtonSettingsHandler(ipcSite);
                responseDataBean = handler.handle(loginName, buttonBeans);


            } else {
                //this is incase of request is raised directly to the plugin by-passing REST plugin. So need to check for user validation.
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
            }
            jsonStr = Obj.writeValueAsString(responseDataBean);
        } catch (Exception e) {
            LOG.error("Error in CAS-BW ButtonInfoController:  ", e);
            responseDataBean = new ResponseDataBean();
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(jsonStr).build();
        }
        return Response.status(Response.Status.OK)
                .entity(jsonStr).build();
    }

    public List<ButtonEx> getButtonInformation(String loginName, String buttonType, IPCSite ipcSite) {
        ButtonInformation buttonInformation = new ButtonInformation(ipcSite);
        return buttonInformation.fetchButtonInformation(loginName, buttonType);
    }


    private List<ButtonSettingsBean> parseButtonInfo(List<ButtonEx> buttonExList) {
        ArrayList<ButtonSettingsBean> buttonSettingsBeanList = null;
        try {
            if (buttonExList != null) {
                buttonSettingsBeanList = new ArrayList<ButtonSettingsBean>();

                LOG.debug("##button list size fetched: " + buttonExList.size());

                for (ButtonEx buttonEx : buttonExList) {
                    ButtonButtonType buttonType = buttonEx.getButtonType().getValue();
                    
                    ButtonSettingsBean buttonSettingsBean = new ButtonSettingsBean();
                    buttonSettingsBean.setButtonNumber(buttonEx.getButtonNumber());
                    buttonSettingsBean.setButtonId(String.valueOf(buttonEx.getId()));
                    buttonSettingsBean.setButtonType(buttonType.value());
                    buttonSettingsBean.setButtonColor(buttonEx.getButtonColor());

                    if (buttonType == ButtonButtonType.INVALID_BUTTON_TYPE) {
                    	buttonSettingsBeanList.add(buttonSettingsBean);
                        continue;
                    }

                    buttonSettingsBean.setButtonLabel(buttonEx.getButtonLabel() != null ? buttonEx.getButtonLabel().getValue() : null);
                    buttonSettingsBean.setLockedForProgrammingAtCDI(buttonEx.getLockedForProgrammingAtTheTurret() != null ? String.valueOf(buttonEx.getLockedForProgrammingAtTheTurret().getValue()) : null);

                    switch (buttonType) {
                        case RESOURCE:
                            parseResourceButtonData(buttonEx, buttonSettingsBean);
                            break;
                        case SPEED_DIAL:
                        case HUNT_AND_SPEED_DIAL:
                        	parseSpeedDialButtonData(buttonEx, buttonSettingsBean);
                        	break;
                        case RESOURCE_AND_SPEED_DIAL:
                            parseResourceAndSpeedDialButtonData(buttonEx, buttonSettingsBean);
                            break;
                        case ICM:
                            buttonSettingsBean.setIcmNumberToDial(buttonEx.getDestination().getValue());
                            break;
                        case POINT_OF_CONTACT:
                        	parsePointOfContactButtonData(buttonEx, buttonSettingsBean);
                        	 break;
                        case MWI:
                        	parseResourceButtonData(buttonEx, buttonSettingsBean);
                        	buttonSettingsBean.setDestinationOrNumberToDial(buttonEx.getDestination().getValue());
                            break;
                        case ONE_BUTTON_DIVERT:
                        	parseOneButtonDivertData(buttonEx,buttonSettingsBean);
                        	break;
                        case KEY_SEQUENCE:
                        	parseKeySequenceButtonData(buttonEx,buttonSettingsBean);
                        	break;
                        case ONE_BUTTON_ICM_DIVERT:
                        	parseOneButtonICMButtonData(buttonEx,buttonSettingsBean);
                        	break;
                        case SIMPLEX_CONFERENCE:
                        	parseSimpleConferenceButtonData(buttonEx,buttonSettingsBean);
                        	break;
                        case DUPLEX_CONFERENCE:
                        	parseDuplexConferenceButtonData(buttonEx,buttonSettingsBean);
                        	break;
                        case PERSONAL_POINT_OF_CONTACT:
                        	parsePersonalPointOfContactButtonData(buttonEx,buttonSettingsBean);
                        	break;
                    }
                    buttonSettingsBeanList.add(buttonSettingsBean);
                }
            }
        } catch (Exception e) {
            LOG.error("Exception parsing button info: ", e);
            return null;
        }
        return buttonSettingsBeanList;
    }

    private void parseSpeedDialButtonData(ButtonEx buttonEx, ButtonSettingsBean buttonSettingsBean) {
        buttonSettingsBean.setSpeedDialType(buttonEx.getSpeedDialType().getValue().value());
        JAXBElement<String> destination = buttonEx.getDestination();
        buttonSettingsBean.setDestinationOrNumberToDial(destination.getValue());
    }
    private void parseResourceAndSpeedDialButtonData(ButtonEx buttonEx,ButtonSettingsBean buttonSettingsBean)
    {
    	  buttonSettingsBean.setSpeedDialType(buttonEx.getSpeedDialType().getValue().value());
          JAXBElement<String> destination = buttonEx.getDestination();
          buttonSettingsBean.setDestinationOrNumberToDial(destination.getValue());
          JAXBElement<ButtonIncomingActionCLI> buttonIncomingActionCLI = buttonEx.getDisplayIncomingCLI();
          buttonSettingsBean.setDisplayIncomingCLI(buttonIncomingActionCLI.getValue().value());

          JAXBElement<ButtonIncomingActionFloat> buttonIncomingActionFloat = buttonEx.getIncomingActionFloat();
          buttonSettingsBean.setIncomingActionFloat(buttonIncomingActionFloat.getValue().value());

          JAXBElement<ButtonIncomingActionPriority> buttonIncomingActionPriority = buttonEx.getIncomingActionPriority();
          buttonSettingsBean.setIncomingActionPriority(buttonIncomingActionPriority.getValue().value());

          JAXBElement<ButtonIncomingActionRings> buttonIncomingActionRings = buttonEx.getIncomingActionRings();
          buttonSettingsBean.setIncomingActionRings(buttonIncomingActionRings.getValue().value());
          
          buttonSettingsBean.setAutoSignal(String.valueOf(buttonEx.getAutoSignal().getValue()));
          buttonSettingsBean.setRingtone(String.valueOf(buttonEx.getRingTone().getValue()));
          buttonSettingsBean.setDisplayInCallHistory(String.valueOf(buttonEx.getDisplayInCallHistory().getValue()));
          
          final List<ButtonResourceAOR> lines = buttonEx.getLine();
          for (ButtonResourceAOR buttonLine : lines) {
              ResourceAORLineBean line = new ResourceAORLineBean();
              line.setResourceAORId(String.valueOf(buttonLine.getResourceAORId()));
              line.setAppearance(String.valueOf(buttonLine.getAppearance().getValue()));

              String lineAOR = buttonLine.getResourceAOR().getValue();

              ResourceAORType lineType = buttonLine.getType().getValue();
              line.setType(lineType.value());

              line.setResourceAOR(lineAOR);
              buttonSettingsBean.setLine(line);
          }
    }
    private void parseResourceButtonData(ButtonEx buttonEx, ButtonSettingsBean buttonSettingsBean) {
        buttonSettingsBean.setSpeedDialType(buttonEx.getSpeedDialType().getValue().value());
        buttonSettingsBean.setAutoSignal(String.valueOf(buttonEx.getAutoSignal().getValue()));
        buttonSettingsBean.setRingtone(String.valueOf(buttonEx.getRingTone().getValue()));
        buttonSettingsBean.setDisplayInCallHistory(String.valueOf(buttonEx.getDisplayInCallHistory().getValue()));

        JAXBElement<ButtonIncomingActionCLI> buttonIncomingActionCLI = buttonEx.getDisplayIncomingCLI();
        if(buttonIncomingActionCLI != null) {
        	buttonSettingsBean.setDisplayIncomingCLI(buttonIncomingActionCLI.getValue() != null ? buttonIncomingActionCLI.getValue().value() : "");
        }

        JAXBElement<ButtonIncomingActionFloat> buttonIncomingActionFloat = buttonEx.getIncomingActionFloat();
        if(buttonIncomingActionFloat != null) {
        	buttonSettingsBean.setIncomingActionFloat(buttonIncomingActionFloat.getValue() != null ? buttonIncomingActionFloat.getValue().value(): "");
        }

        JAXBElement<ButtonIncomingActionPriority> buttonIncomingActionPriority = buttonEx.getIncomingActionPriority();
        if(buttonIncomingActionPriority != null) {
        	buttonSettingsBean.setIncomingActionPriority(buttonIncomingActionPriority.getValue() != null ?buttonIncomingActionPriority.getValue().value():"");
        }

        JAXBElement<ButtonIncomingActionRings> buttonIncomingActionRings = buttonEx.getIncomingActionRings();
        if(buttonIncomingActionRings != null) {
        	buttonSettingsBean.setIncomingActionRings(buttonIncomingActionRings.getValue() != null ? buttonIncomingActionRings.getValue().value() : "");
        }

        final List<ButtonResourceAOR> lines = buttonEx.getLine();
        for (ButtonResourceAOR buttonLine : lines) {
            ResourceAORLineBean line = new ResourceAORLineBean();
            line.setResourceAORId(String.valueOf(buttonLine.getResourceAORId()));
            line.setAppearance(String.valueOf(buttonLine.getAppearance().getValue()));

            String lineAOR = buttonLine.getResourceAOR().getValue();

            ResourceAORType lineType = buttonLine.getType().getValue();
            line.setType(lineType.value());

            line.setResourceAOR(lineAOR);
            buttonSettingsBean.setLine(line);
        }
    }
    
    private void parsePointOfContactButtonData(ButtonEx buttonEx, ButtonSettingsBean buttonSettingsBean) {
    	if(buttonEx.getPointOfContactId()!=null)
    	{
    		buttonSettingsBean.setPointOfContactId(BigInteger.valueOf(buttonEx.getPointOfContactId().getValue()));
    	}
    	if(buttonEx.getPersonalPointOfContactId()!=null)
    	{
    		buttonSettingsBean.setPersonalPointOfContactId(String.valueOf(buttonEx.getPersonalPointOfContactId().getValue()));
    	}
    }
    private void parsePersonalPointOfContactButtonData(ButtonEx buttonEx, ButtonSettingsBean buttonSettingsBean) {
    	if(buttonEx.getPointOfContactId()!=null)
    	{
    		buttonSettingsBean.setPointOfContactId(BigInteger.valueOf(buttonEx.getPointOfContactId().getValue()));
    	}
    	if(buttonEx.getPersonalPointOfContactId()!=null)
    	{
    		buttonSettingsBean.setPersonalPointOfContactId(String.valueOf(buttonEx.getPersonalPointOfContactId().getValue()));
    	}
}
    private void parseOneButtonDivertData(ButtonEx buttonEx, ButtonSettingsBean buttonSettingsBean) {
       
        buttonSettingsBean.setGetOneButtonDivertReason(String.valueOf(buttonEx.getDivertReason().getValue()));
        buttonSettingsBean.setOneButtonDivertDestination(String.valueOf(buttonEx.getDivertOnImmorBusytoExtension().getValue()));
        JAXBElement<ButtonIncomingActionCLI> buttonIncomingActionCLI = buttonEx.getDisplayIncomingCLI();
        buttonSettingsBean.setDisplayIncomingCLI(buttonIncomingActionCLI.getValue().value());
        final List<ButtonResourceAOR> lines = buttonEx.getLine();
        for (ButtonResourceAOR buttonLine : lines) {
            ResourceAORLineBean line = new ResourceAORLineBean();
            line.setResourceAORId(String.valueOf(buttonLine.getResourceAORId()));
            line.setAppearance(String.valueOf(buttonLine.getAppearance().getValue()));

            String lineAOR = buttonLine.getResourceAOR().getValue();
            ResourceAORType lineType = buttonLine.getType().getValue();
            line.setType(lineType.value());
            line.setResourceAOR(lineAOR);
            buttonSettingsBean.setLine(line);
        }
    }
    
    private void parseKeySequenceButtonData(ButtonEx buttonEx, ButtonSettingsBean buttonSettingsBean) {
        List<KeyCodesInformation> keyCodeInfo = buttonEx.getKeyCodeInfo();
        List<String> keyCodes = new ArrayList<String>();
        for(KeyCodesInformation info : keyCodeInfo)
        {
        	keyCodes.add(String.valueOf(info.getValue()));
        	//buttonSettingsBean.setKeyCodeName(String.valueOf(info.getName()));
        }
        buttonSettingsBean.setKeyCodeValue(keyCodes);
    }
    
    private void parseOneButtonICMButtonData(ButtonEx buttonEx, ButtonSettingsBean buttonSettingsBean) {
    	  buttonSettingsBean.setGetOneButtonDivertReason(String.valueOf(buttonEx.getDivertReason().getValue()));
          buttonSettingsBean.setOneButtonDivertDestination(String.valueOf(String.valueOf(buttonEx.getDivertOnImmorBusytoExtension().getValue())));
          JAXBElement<ButtonIncomingActionCLI> buttonIncomingActionCLI = buttonEx.getDisplayIncomingCLI();
          buttonSettingsBean.setDisplayIncomingCLI(buttonIncomingActionCLI.getValue().value());
    }
    
    private void parseSimpleConferenceButtonData(ButtonEx buttonEx, ButtonSettingsBean buttonSettingsBean) {
    	buttonSettingsBean.setSpeedDialType(buttonEx.getSpeedDialType().getValue().value());
    	buttonSettingsBean.setDisplayInCallHistory(String.valueOf(buttonEx.getDisplayInCallHistory().getValue()));

        JAXBElement<ButtonIncomingActionCLI> buttonIncomingActionCLI = buttonEx.getDisplayIncomingCLI();
        buttonSettingsBean.setDisplayIncomingCLI(buttonIncomingActionCLI.getValue().value());

        JAXBElement<ButtonIncomingActionFloat> buttonIncomingActionFloat = buttonEx.getIncomingActionFloat();
        buttonSettingsBean.setIncomingActionFloat(buttonIncomingActionFloat.getValue().value());

        JAXBElement<ButtonIncomingActionPriority> buttonIncomingActionPriority = buttonEx.getIncomingActionPriority();
        buttonSettingsBean.setIncomingActionPriority(buttonIncomingActionPriority.getValue().value());

        JAXBElement<ButtonIncomingActionRings> buttonIncomingActionRings = buttonEx.getIncomingActionRings();
        buttonSettingsBean.setIncomingActionRings(buttonIncomingActionRings.getValue().value());
        
        final List<ButtonResourceAOR> lines = buttonEx.getLine();
        List<ResourceAORLineBean> resourceAORBeanList = new ArrayList<ResourceAORLineBean>();
        for (ButtonResourceAOR buttonLine : lines) {
            ResourceAORLineBean line = new ResourceAORLineBean();
            line.setResourceAORId(String.valueOf(buttonLine.getResourceAORId()));
            line.setAppearance(String.valueOf(buttonLine.getAppearance().getValue()));

            String lineAOR = buttonLine.getResourceAOR().getValue();
            ResourceAORType lineType = buttonLine.getType().getValue();
            line.setType(lineType.value());
            line.setResourceAOR(lineAOR);
            resourceAORBeanList.add(line);
        }
        buttonSettingsBean.setResourceAORBeanList(resourceAORBeanList);
  }
    
    private void parseDuplexConferenceButtonData(ButtonEx buttonEx, ButtonSettingsBean buttonSettingsBean) {
    	buttonSettingsBean.setDisplayInCallHistory(String.valueOf(buttonEx.getDisplayInCallHistory().getValue()));

        JAXBElement<ButtonIncomingActionCLI> buttonIncomingActionCLI = buttonEx.getDisplayIncomingCLI();
        buttonSettingsBean.setDisplayIncomingCLI(buttonIncomingActionCLI.getValue().value());

        JAXBElement<ButtonIncomingActionFloat> buttonIncomingActionFloat = buttonEx.getIncomingActionFloat();
        buttonSettingsBean.setIncomingActionFloat(buttonIncomingActionFloat.getValue().value());

        JAXBElement<ButtonIncomingActionPriority> buttonIncomingActionPriority = buttonEx.getIncomingActionPriority();
        buttonSettingsBean.setIncomingActionPriority(buttonIncomingActionPriority.getValue().value());

        JAXBElement<ButtonIncomingActionRings> buttonIncomingActionRings = buttonEx.getIncomingActionRings();
        buttonSettingsBean.setIncomingActionRings(buttonIncomingActionRings.getValue().value());
        
        final List<ButtonResourceAOR> lines = buttonEx.getLine();
        List<ResourceAORLineBean> resourceAORBeanList = new ArrayList<ResourceAORLineBean>();
        for (ButtonResourceAOR buttonLine : lines) {
            ResourceAORLineBean line = new ResourceAORLineBean();
            line.setResourceAORId(String.valueOf(buttonLine.getResourceAORId()));
            line.setAppearance(String.valueOf(buttonLine.getAppearance().getValue()));

            String lineAOR = buttonLine.getResourceAOR().getValue();
            ResourceAORType lineType = buttonLine.getType().getValue();
            line.setType(lineType.value());
            line.setResourceAOR(lineAOR);
            resourceAORBeanList.add(line);
        }
        buttonSettingsBean.setResourceAORBeanList(resourceAORBeanList);
  }
}
