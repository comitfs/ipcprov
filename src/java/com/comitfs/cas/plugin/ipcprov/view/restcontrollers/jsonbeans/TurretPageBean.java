package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


public class TurretPageBean {
    protected Integer id;
    protected Integer parentUserMercuryId;
    protected Integer pageNumber;
    protected String pageName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentUserMercuryId() {
        return parentUserMercuryId;
    }

    public void setParentUserMercuryId(Integer parentUserMercuryId) {
        this.parentUserMercuryId = parentUserMercuryId;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }
}
