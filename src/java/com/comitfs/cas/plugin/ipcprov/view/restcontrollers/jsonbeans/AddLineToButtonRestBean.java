package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


public class AddLineToButtonRestBean {
    private String buttonId;
    private String buttonType;
    private String lineId;
    private String lineType;
    private String resourceAOR;
    private String appearance;

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getButtonId() {
        return buttonId;
    }

    public void setButtonId(String buttonId) {
        this.buttonId = buttonId;
    }

    public String getButtonType() {
        return buttonType;
    }

    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getResourceAOR() {
        return resourceAOR;
    }

    public void setResourceAOR(String resourceAOR) {
        this.resourceAOR = resourceAOR;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }
}
