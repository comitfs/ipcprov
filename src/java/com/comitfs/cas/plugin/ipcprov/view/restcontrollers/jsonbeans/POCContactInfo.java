package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

public class POCContactInfo {
    private String pocType;
    private String data;
    private String isPreferred;
    private String mediaType;
    private String shortDescriptor;

    public String getPocType() {
        return pocType;
    }

    public void setPocType(String pocType) {
        this.pocType = pocType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getIsPreferred() {
        return isPreferred;
    }

    public void setIsPreferred(String isPreferred) {
        this.isPreferred = isPreferred;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getShortDescriptor() {
        return shortDescriptor;
    }

    public void setShortDescriptor(String shortDescriptor) {
        this.shortDescriptor = shortDescriptor;
    }
}
