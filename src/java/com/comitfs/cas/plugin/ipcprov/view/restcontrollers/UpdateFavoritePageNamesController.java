package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;


import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.UpdateFavoritePageNamesHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.UpdateFavoritesPageNamesBeanList;
import org.apache.log4j.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ipcprov/api/update-favorite-page-names")
public class UpdateFavoritePageNamesController {
    private final Logger LOG = Logger.getLogger(UpdateFavoritePageNamesController.class);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateFavoritePageNames(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName, UpdateFavoritesPageNamesBeanList updateFavoritesPageNamesBeanList) {

        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {
                LOG.debug("component not null in UpdateFavoritePageNamesController");
                UpdateFavoritePageNamesHandler updateFavoritePageNamesHandler = new UpdateFavoritePageNamesHandler(ipcSite);
                responseDataBean = updateFavoritePageNamesHandler.handle(loginName, updateFavoritesPageNamesBeanList);
            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("User does not exist");
            }
        } catch (Exception e) {
            LOG.error("Error updating favorite page names: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription("Internal Server Error " + e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }
}
