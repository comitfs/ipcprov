package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.UpdateAudioRecordingPreferencesHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.RecordingProfileUpdateBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ipcprov/api/updateaudiorecording")
public class UpdateUserAudioPreference {
    private final Logger LOG = Logger.getLogger(UpdateUserAudioPreference.class);


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateAudioRecording(@QueryParam("siteid") Long siteId, @QueryParam("loginname") String loginName, RecordingProfileUpdateBean recordingProfileUpdateBean) {

        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            IPCSite ipcSite = null;
            if (siteId == null || siteId == 0) {
                ipcSite = IPCSiteManager.getInstance().getIpcSites().get(0);
            } else {
                ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
            }

            if (ipcSite != null && recordingProfileUpdateBean != null && recordingProfileUpdateBean.getDeviceModel() != null) {

                //read the json input data and invoke the handler
                UpdateAudioRecordingPreferencesHandler handler = new UpdateAudioRecordingPreferencesHandler(ipcSite);
                String deviceType = recordingProfileUpdateBean.getDeviceModel();
                if (recordingProfileUpdateBean.getDeviceModel() != null) {
                    if (recordingProfileUpdateBean.getDeviceModel().equalsIgnoreCase("IQ/MAX Edge") || recordingProfileUpdateBean.getDeviceModel().equalsIgnoreCase("IQMAX")) {
                        deviceType = "Turret";
                    }
                }
                if (recordingProfileUpdateBean.getRecording()) {
                    responseDataBean = handler.handle(loginName, recordingProfileUpdateBean.getRecordingProfile(), deviceType, "IP");
                } else {
                    //recordingProfile is hardcoded to 1 as the server BW is failing if we send 0/empty
                    responseDataBean = handler.handle(loginName, 1, deviceType, "None");
                }

            } else {
                responseDataBean = new ResponseDataBean();
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
            }

        } catch (Exception e) {
            LOG.error("Error in updating UserAudioPreferences: ", e);
            responseDataBean = new ResponseDataBean();
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }
}
