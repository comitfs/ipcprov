package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;


import com.comitfs.cas.ipc.bw.jaxb.ReasonCodes;
import com.comitfs.cas.plugin.ipcprov.IPCProvPlugin;
import com.comitfs.cas.plugin.ipcprov.beans.UserAttributes;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.CreateEndUserHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.jivesoftware.openfire.PrivateStorage;
import org.jivesoftware.openfire.XMPPServer;
import org.xmpp.packet.JID;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Iterator;

@Path("ipcprov/api/create-end-user")
public class CreateEndUserAcctController {
    private final Logger log = Logger.getLogger(CreateEndUserAcctController.class);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response CreateEndUser(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName, UserAttributes userAttributes) {

        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {

            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {
                log.debug("ipcSite not null in CreateEndUserAcctController");
                CreateEndUserHandler createEndUserHandler = new CreateEndUserHandler(ipcSite);
                responseDataBean = createEndUserHandler.handle(userAttributes);
                if (responseDataBean.getReasonCode() == ReasonCodes.REQ_SUCCESS.name()) {
                    addUserTSC(userAttributes.getLoginName(), new JID(IPCProvPlugin.IPC_PLUGIN_NAME + siteId + "." + IPCSiteManager.getInstance().getDomain()).toString(), ipcSite.getName());
                    //Adding user to map
                    if (userAttributes.getLoginName() != null) {
                        IPCDataService.getInstance().getUserPreference(userAttributes.getLoginName(), ipcSite);
                    }
                }


            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid Request");
            }
        } catch (Exception e) {
            log.error("Error in creating end user account: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription("Internal Server Error " + e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }

    public void addUserTSC(String userName, String tscJID, String site) {
        log.info(String.format("Site Association User: %s Site: %s", userName, site));
        PrivateStorage privateStorage = XMPPServer.getInstance().getPrivateStorage();
        try {
            Document document = DocumentHelper.parseText("<openlink xmlns=\"http://xmpp.org/protocol/openlink:01:00:00#tsc\"></openlink>");

            if (document != null) {
                Element searchElement = document.getRootElement();
                Element olElement = privateStorage.get(userName, searchElement);

                boolean foundTSC = false;

                for (Iterator i = olElement.elementIterator("tsc"); i.hasNext(); ) {
                    Element tsc = (Element) i.next();
                    log.info("tsc from privatestorage is : " + tsc.getText());
                    if (tscJID.equals(tsc.getText())) {
                        log.info("User already associated for TSC: " + userName);
                        foundTSC = true;
                        break;
                    }
                }
                if (foundTSC) {
                    return;
                }

                //previous tsc being replaced with new tsc, now updating to add one more tsc tag to the same row ie no more replacing but adding extra
                log.info(String.format("Association not found for User: %s Site: %s associating now", userName, site));

                olElement = privateStorage.get(userName, searchElement);
                Element tsc = olElement.addElement("tsc");
                tsc.setText(tscJID);
                tsc.addAttribute("name", site);
                privateStorage.add(userName, olElement);
            }
        } catch (Exception e) {
            log.error("Exception in associating user to site", e);
        }
    }
}
