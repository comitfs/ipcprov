package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

import com.comitfs.cas.plugin.ipcprov.beans.LicenseFeatureDataBean;

@Getter
@Setter
@NoArgsConstructor
public class LicenseInfoRestBean implements Serializable {
    private String loginName;
    private List<LicenseFeatureDataBean> licenseFeatureDataList;

    public LicenseInfoRestBean(String loginName, List<LicenseFeatureDataBean> licenseFeatureDataList) {
        this.loginName = loginName;
        this.licenseFeatureDataList = licenseFeatureDataList;
    }
}
