package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;


import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.comitfs.cas.plugin.ipcprov.beans.ChangeUserAccountPasswordBean;
import com.comitfs.cas.plugin.ipcprov.beans.UserAttributes;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.ChangeEndUserAcctPasswordHandler;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.UserInformation;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

@Path("ipcprov/api/changeuserpwd")
public class ChangePasswordRestController {

    private final Logger LOG = Logger.getLogger(ChangePasswordRestController.class);

    @GET
	@Produces(MediaType.APPLICATION_JSON)
    public Response changeUserPwd(@QueryParam("siteid") long siteId,@QueryParam("loginname") String loginName) {



            ResponseDataBean responseDataBean = new ResponseDataBean();
            ChangeUserAccountPasswordBean changeUsrPwdBean = new ChangeUserAccountPasswordBean();
            try {

            	IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
                if (ipcSite != null) {
                    UserInformation userInformation = new UserInformation(ipcSite);
                    UserAttributes user = userInformation.fetchUserInformation(loginName);

                    changeUsrPwdBean.setPasswordChangeMode(user.getPasswordChangeMode());
                } else {
                    responseDataBean.setReasonCode("REQ_FAILURE");
                    responseDataBean.setReasonDescription("Invalid login name/User does not exist");
                }
            } catch (Exception e) {
                LOG.error("Exception in ChangePasswordRestController: ", e);
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription(e.getMessage());
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(responseDataBean).build();
            }
            return Response.status(Response.Status.OK)
                    .entity(changeUsrPwdBean).build();
        }


    @POST
	@Produces(MediaType.APPLICATION_JSON)
    public Response changeUserPwd(@QueryParam("siteid") long siteId,@QueryParam("loginname") String loginName,ChangeUserAccountPasswordBean changeUsrPwdBean) {

        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {

        	IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite != null) {
                LOG.debug("component not null in CHangePwdController");
                //read the json input data and invoke the handler
                ChangeEndUserAcctPasswordHandler handler = new ChangeEndUserAcctPasswordHandler(ipcSite);
                responseDataBean = handler.handle(loginName, changeUsrPwdBean);
            } else {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
            }
        } catch (Exception e) {
            LOG.error("Error in updating User Password: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(changeUsrPwdBean).build();
    }

}
