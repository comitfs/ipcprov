package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.FetchRoles;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.RoleInfo;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.RolesInfoBean;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("ipcprov/api/roleinfo")
public class FetchRolesRestController {
    private final Logger log = Logger.getLogger(FetchRolesRestController.class);


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response roleInfo(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName) {

        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite == null) {
                log.debug("Fetch Role Info - Component null for ipcSite " + ipcSite);
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("IPC Component not exist");
            }


            if (ipcSite != null) {
                FetchRoles fetchRoles = new FetchRoles(ipcSite);

                ArrayList<RoleInfo> rolesList = fetchRoles.fetchRoles();

                RolesInfoBean rolesListResp = new RolesInfoBean();
                rolesListResp.setRoleInfo(rolesList);
                return Response.status(Response.Status.OK)
                        .entity(rolesListResp).build();

            }

        } catch (Exception e) {
            log.error("Error in casbw: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }

}
