package com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans;


import com.comitfs.cas.ipc.bw.jaxb.*;

public class TraderSpecificDataFormatsRestBean {

    private AutoSelectHoldType[] autoSelectHold;
    private UserCDILanguage[] language;
    private UserCDIHandsetSelectMode[] handsetSelectMode;
    private UserCDIHandsetMuteOption[] handsetCallMuteOption;
    private UserCDIHandsetBtn[] handsetButtonActions;

    public TraderSpecificDataFormatsRestBean() {
        this.autoSelectHold = AutoSelectHoldType.values();
        this.language = UserCDILanguage.values();
        this.handsetSelectMode = UserCDIHandsetSelectMode.values();
        this.handsetCallMuteOption = UserCDIHandsetMuteOption.values();
        this.handsetButtonActions = UserCDIHandsetBtn.values();
    }

    public AutoSelectHoldType[] getAutoSelectHold() {
        return autoSelectHold;
    }

    public void setAutoSelectHold(AutoSelectHoldType[] autoSelectHold) {
        this.autoSelectHold = autoSelectHold;
    }

    public UserCDILanguage[] getLanguage() {
        return language;
    }

    public void setLanguage(UserCDILanguage[] language) {
        this.language = language;
    }

    public UserCDIHandsetSelectMode[] getHandsetSelectMode() {
        return handsetSelectMode;
    }

    public void setHandsetSelectMode(UserCDIHandsetSelectMode[] handsetSelectMode) {
        this.handsetSelectMode = handsetSelectMode;
    }

    public UserCDIHandsetMuteOption[] getHandsetCallMuteOption() {
        return handsetCallMuteOption;
    }

    public void setHandsetCallMuteOption(UserCDIHandsetMuteOption[] handsetCallMuteOption) {
        this.handsetCallMuteOption = handsetCallMuteOption;
    }

    public UserCDIHandsetBtn[] getHandsetButtonActions() {
        return handsetButtonActions;
    }

    public void setHandsetButtonActions(UserCDIHandsetBtn[] handsetButtonActions) {
        this.handsetButtonActions = handsetButtonActions;
    }
}
