package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.FetchFixedButtons;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.FixedButtonBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.FixedButtonBeanList;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("ipcprov/api/fixedbuttonsinfo")
public class FetchFixedButtonsRestController {
    private final Logger log = Logger.getLogger(FetchFixedButtonsRestController.class);


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response fixedButtonsInfo(@QueryParam("siteid") long siteId, @QueryParam("loginname") String loginName) {
        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);

            if (ipcSite == null) {
                log.debug("Fetch Fixed Buttons - Component null for loginName " + loginName);
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("Invalid login name/User does not exist");
            }


            if (ipcSite != null) {
                FetchFixedButtons fetchFixedButtons = new FetchFixedButtons(ipcSite);

                ArrayList<FixedButtonBean> fixedButtons = fetchFixedButtons.fetchFixedButtons(loginName);

                FixedButtonBeanList fixedButtonBeanList = new FixedButtonBeanList();
                fixedButtonBeanList.setFixedButtonBeans(fixedButtons);
                return Response.status(Response.Status.OK)
                        .entity(fixedButtonBeanList).build();
            }

        } catch (Exception e) {
            log.error("Error in casbw: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(responseDataBean).build();
        }
        return Response.status(Response.Status.OK)
                .entity(responseDataBean).build();
    }


}
