package com.comitfs.cas.plugin.ipcprov.view.restcontrollers;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.comitfs.cas.ipc.bw.jaxb.LogonSessionData;
import com.comitfs.cas.plugin.ipcprov.beans.DeviceLoginStatusBean;
import com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov.DeviceInventory;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.core.IPCSiteManager;
import com.comitfs.cas.plugin.ipcprov.dataservice.LogonSessionInfoHelper;
import com.comitfs.cas.plugin.ipcprov.util.DeviceInfo;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

@Path("ipcprov/api/logonsession")
public class FetchLogonSessionInfo {
	private final Logger log = Logger.getLogger(this.getClass());
	
    @Path("/query")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response queryDeviceLoginStatus(@NotNull @QueryParam("siteid") long siteId, @NotNull @QueryParam("device") String device) {
        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
            if (ipcSite == null) {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("no site found with id: " + siteId);
                return Response.status(Response.Status.BAD_REQUEST).entity(responseDataBean).build();
            }

            LogonSessionData logonSessionData = new LogonSessionInfoHelper(ipcSite).getLogonSessionInfo(device);
            if (null == logonSessionData) {
                responseDataBean.setReasonCode("REQ_FAILURE");
                responseDataBean.setReasonDescription("error fetching logon session information for device: " + device);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseDataBean).build();
            }

            DeviceLoginStatusBean deviceLoginStatusBean = new DeviceLoginStatusBean();
            deviceLoginStatusBean.setDeviceId(logonSessionData.getDeviceId());
            deviceLoginStatusBean.setLoginName(logonSessionData.getLoginName().getValue());
            deviceLoginStatusBean.setDeviceIpAddr(logonSessionData.getIPAddress().getValue());
            deviceLoginStatusBean.setMac(logonSessionData.getDeviceUUID().getValue());
            return Response.status(Response.Status.OK).entity(deviceLoginStatusBean).build();
        } catch (Throwable e) {
            log.error("Error in casbw: ", e);
            responseDataBean.setReasonCode("REQ_FAILURE");
            responseDataBean.setReasonDescription(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseDataBean).build();
        }
    }

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLogonSessionInfo(@NotNull @QueryParam("siteid") long siteId,@NotNull @QueryParam("loginname") String loginName,@QueryParam("deviceid") Integer deviceId) {
		ResponseDataBean responseDataBean = new ResponseDataBean();
		try {
			IPCSite ipcSite = IPCSiteManager.getInstance().getIpcSiteMap().get(siteId);
			DeviceInfo deviceInfo = new DeviceInfo();

			if (ipcSite != null && loginName != null) {
				LogonSessionInfoHelper logonSessionInfoHelper = new LogonSessionInfoHelper(ipcSite);
				if(deviceId == null || deviceId == 0) {
					deviceId = logonSessionInfoHelper.updateUserWithLogonSession(loginName,null,null);
				}
				DeviceInventory deviceInventory = new DeviceInventory(ipcSite);
				deviceInfo = deviceInventory.getDeviceModel(deviceId,null);
				return Response.status(Response.Status.OK)
						.entity(deviceInfo).build();


				//TODO: TBD: user should be able to fetch buttons info based on other params?

			} else {
				responseDataBean.setReasonCode("REQ_FAILURE");
				responseDataBean.setReasonDescription("Invalid login name/User does not exist/siteid is missing");
			}

		} catch (Throwable e) {
			log.error("Error in casbw: ", e);
			responseDataBean.setReasonCode("REQ_FAILURE");
			responseDataBean.setReasonDescription(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(responseDataBean).build();
		}
		return Response.status(Response.Status.OK)
				.entity(responseDataBean).build();
	}

}
