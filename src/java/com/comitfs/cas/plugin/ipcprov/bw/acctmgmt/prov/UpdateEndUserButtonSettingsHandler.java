package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.ButtonSettingsBean;
import com.comitfs.cas.plugin.ipcprov.beans.ButtonTypeBean;
import com.comitfs.cas.plugin.ipcprov.beans.ResourceAORBean;
import com.comitfs.cas.plugin.ipcprov.beans.ResourceAORLineBean;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


public class UpdateEndUserButtonSettingsHandler {
    public static final String REQ_FAILURE = "REQ_FAILURE";
    public static final String REQ_FAILURE_DESC = "Invalid input :";
    public static final String FOR_BUTTONID = " for ButtonId : ";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(UpdateEndUserButtonSettingsHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    public UpdateEndUserButtonSettingsHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String casUserName, List<ButtonSettingsBean> buttonSettingsBeanList) throws Exception {
        try {
            IPCUser ipcUser = IPCDataService.getInstance().getUserById(casUserName,ipcSite.getId());

            //validate button settings,and send error response if any invalid value is present in input
            ResponseDataBean reasonData = validateButtonSettings(ipcSite, buttonSettingsBeanList);

            if (reasonData == null) {
                reasonData = new ResponseDataBean();

                UpdateEndUserButton updateEndUserButton = new UpdateEndUserButton();

                UserIdentifier userIdentifier = new UserIdentifier();
                if (ipcUser.getIPCId() != null) {
                    userIdentifier.setUserId(BigInteger.valueOf(ipcUser.getIPCId()));
                } else if (ipcUser.getUserId() != null) {
                    userIdentifier.setLoginName(ipcUser.getUserId());
                }

                updateEndUserButton.setUserIdentifier(userIdentifier);

                ButtonInfoList buttonInfoList = new ButtonInfoList();
                ArrayList<ButtonInformation> buttonsList = (ArrayList<ButtonInformation>) buttonInfoList.getButton();

                for (ButtonSettingsBean buttonSettingsBean : buttonSettingsBeanList) {

                    if (buttonSettingsBean.getButtonType() != null && !buttonSettingsBean.getButtonType().isEmpty()) {
                        String buttonTypeValue = buttonSettingsBean.getButtonType();
                        ButtonTypeBean buttonTypeBean = null;
                        ButtonInformation buttonInformation = null;

                        buttonTypeBean = ButtonTypeBean.fromValue(buttonTypeValue);

                        switch (buttonTypeBean) {

                            case INVALID_BUTTON_TYPE: {
                                log.debug("received invalidate button request for buttonid: " + buttonSettingsBean.getButtonId());
                                buttonInformation = invalidateButton(buttonSettingsBean);
                                break;
                            }
                            case RESOURCE: {
                                buttonInformation = updateResourceButton(buttonSettingsBean);
                                break;
                            }
                            case SPEEDDIAL: {
                                buttonInformation = updateSpeedDialButton(buttonSettingsBean);
                                break;
                            }
                            case INTERCOM: {
                                buttonInformation = updateIntercomButton(buttonSettingsBean);
                                break;
                            }
                            case RESOURCE_AND_SPEEDDIAL: {
                                //TODO: Versioning. Currently this method supports only BW 2.1
                                buttonInformation = updateResourceAndSpeedDial(buttonSettingsBean);
                                break;
                            }
                            case HUNT_AND_SPEEDDIAL: {
                                //TODO: Versioning. Currently this method supports only BW 2.1
                                buttonInformation = updateHuntAndSpeedDialButton(buttonSettingsBean);
                                break;
                            }
                            case MESSAGE_WAITING_INDICATOR: {
                                //TODO: Versioning. Currently this method supports only BW 2.1
                                buttonInformation = updateMWIButton(buttonSettingsBean);
                                break;
                            }
                            case POINT_OF_CONTACT:{
                            	//TODO: Directory type should be dynamic
                            	buttonInformation = updatePointOfContactButton(buttonSettingsBean);
                            	break;
                            }
                            case ONE_BUTTON_DIVERT:{
                            	buttonInformation = updateOneDivertButton(buttonSettingsBean);
                                break;
                            }
                            case KEY_SEQUENCE:{
                            	buttonInformation = updateKeySequenceButton(buttonSettingsBean);
                                break;
                            }
                            case ONE_BUTTON_ICM_DIVERT:{
                            	buttonInformation = updateOneButtonICMDivertButton(buttonSettingsBean);
                            	break;
                            }
                            case SIMPLEX_CONFERENCE:{
                            	buttonInformation = updateSimplexConferenceButton(buttonSettingsBean);
                            	break;
                            }
                            case DUPLEX_CONFERENCE:{
                            	buttonInformation = updateDuplexConferenceButton(buttonSettingsBean);
                            	break;
                            }
                            case PERSONAL_POINT_OF_CONTACT:{
                            	buttonInformation = updatePersonalPointOfContactButton(buttonSettingsBean);
                            	break;
                            }
                        }
                        if (buttonInformation != null) {
                            buttonsList.add(buttonInformation);
                        }
                    }
                }

                updateEndUserButton.setButtonList(buttonInfoList);
                ReasonData finalResponse = invokeUpdateButtonSettings(updateEndUserButton);
                reasonData.setReasonCode(finalResponse.getReasonCode().value());
                reasonData.setReasonDescription(finalResponse.getReasonDescription());
                return reasonData;
            } else if (reasonData != null && reasonData.getReasonCode().equals(REQ_FAILURE)) {
                log.debug("invalid input data, returning req_failure");
                return reasonData;
            }
            return null;
        } catch (Exception e) {
            log.info("Exception in UpdateUserButtonSettings handle()", e);
            return null;
        }
    }

    public ButtonInformation invalidateButton(ButtonSettingsBean buttonSettingsBean) {
        log.debug("Going to invalidate a button");

        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }
            return buttonInformation;
        } catch (Exception e) {
            log.error("Exception in setting Update Resource ButtonSettings: ", e);
            return null;
        }
    }

    public ButtonInformation updateResourceButton(ButtonSettingsBean buttonSettingsBean) {
        log.debug("Going to set and invoke Update Resource Button");

        try {
            ButtonInformation buttonInformation = new ButtonInformation();

            buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));

            ButtonDetails buttonDetails = new ButtonDetails();

            ResourceButtonInformation resourceButtonInformation = new ResourceButtonInformation();

            if (buttonSettingsBean.getIncomingActionRings() != null) {
                resourceButtonInformation.setIncomingActionRings(ButtonIncomingActionRings.fromValue(buttonSettingsBean.getIncomingActionRings()));
            }
            if (buttonSettingsBean.getIncomingActionPriority() != null) {
                resourceButtonInformation.setIncomingActionPriority(ButtonIncomingActionPriority.fromValue(buttonSettingsBean.getIncomingActionPriority()));
            }
            if (buttonSettingsBean.getIncomingActionFloat() != null) {
                resourceButtonInformation.setIncomingActionFloat(ButtonIncomingActionFloat.fromValue(buttonSettingsBean.getIncomingActionFloat()));
            }

            if (buttonSettingsBean.getDisplayIncomingCLI() != null) {
                resourceButtonInformation.setDisplayIncomingCLI(ButtonIncomingActionCLI.fromValue(buttonSettingsBean.getDisplayIncomingCLI()));
            }

            if (buttonSettingsBean.getDisplayInCallHistory() != null) {
                resourceButtonInformation.setDisplayInCallHistory(Boolean.valueOf(buttonSettingsBean.getDisplayInCallHistory()));
            }

            if (buttonSettingsBean.getRingtone() != null && !buttonSettingsBean.getRingtone().isEmpty()) {
                resourceButtonInformation.setRingTone(Integer.valueOf(buttonSettingsBean.getRingtone()));
            }
            

            ResourceAORInformation resourceAORInformation = new ResourceAORInformation();
            ResourceAORLineBean line = buttonSettingsBean.getLine();
            if (line != null) {
                if (line.getAppearance() != null && !line.getAppearance().isEmpty()) {
                    resourceAORInformation.setLineAppearance(Integer.valueOf(line.getAppearance()));
                }
                if (line.getResourceAORId() != null && !line.getResourceAORId().isEmpty()) {
                    resourceAORInformation.setResourceAORId(BigInteger.valueOf(Long.valueOf(line.getResourceAORId())));
                }
                resourceButtonInformation.setResourceAORInfo(resourceAORInformation);
            }

            if (buttonSettingsBean.getAutoSignal() != null && !buttonSettingsBean.getAutoSignal().isEmpty()) {
                resourceButtonInformation.setAutoSignal(Boolean.valueOf(buttonSettingsBean.getAutoSignal()));
            }

            if (buttonSettingsBean.getSpeedDialType() != null && !buttonSettingsBean.getSpeedDialType().isEmpty()) {
                if (ButtonIcon.fromValue(buttonSettingsBean.getSpeedDialType()) != null) {
                    resourceButtonInformation.setSpeedDialType(ButtonIcon.fromValue(buttonSettingsBean.getSpeedDialType()));
                }
            }
            buttonDetails.setResourceButtonInfo(resourceButtonInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.error("Exception in setting Update Resource ButtonSettings: ", e);
            return null;
        }
    }

    private ButtonInformation updateResourceAndSpeedDial(ButtonSettingsBean buttonSettingsBean) {
        log.debug("Going to set and invoke Update Resource and Resource SpeedDial Button");
        try {
            ResourceSpeedDialButtonInformation resourceSpeedDialButtonInformation = new ResourceSpeedDialButtonInformation();
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }
            ButtonDetails buttonDetails = new ButtonDetails();

            if (buttonSettingsBean.getIncomingActionRings() != null && !buttonSettingsBean.getIncomingActionRings().isEmpty()) {
                if (ButtonIncomingActionRings.fromValue(buttonSettingsBean.getIncomingActionRings()) != null) {
                    resourceSpeedDialButtonInformation.setIncomingActionRings(ButtonIncomingActionRings.fromValue(buttonSettingsBean.getIncomingActionRings()));
                }
            }

            if (buttonSettingsBean.getIncomingActionPriority() != null && !buttonSettingsBean.getIncomingActionPriority().isEmpty()) {
                String incomingAxnPriority = buttonSettingsBean.getIncomingActionPriority();
                if (ButtonIncomingActionPriority.fromValue(incomingAxnPriority) != null) {
                    resourceSpeedDialButtonInformation.setIncomingActionPriority(ButtonIncomingActionPriority.fromValue(incomingAxnPriority));
                }
            }

            if (buttonSettingsBean.getIncomingActionFloat() != null && !buttonSettingsBean.getIncomingActionFloat().isEmpty()) {
                String incomingAxnFloat = buttonSettingsBean.getIncomingActionFloat();
                if (ButtonIncomingActionFloat.fromValue(incomingAxnFloat) != null) {
                    resourceSpeedDialButtonInformation.setIncomingActionFloat(ButtonIncomingActionFloat.fromValue(incomingAxnFloat));
                }
            }

            if (buttonSettingsBean.getDisplayIncomingCLI() != null && !buttonSettingsBean.getDisplayIncomingCLI().isEmpty()) {
                String displayIncomingCLI = buttonSettingsBean.getDisplayIncomingCLI();
                if (ButtonIncomingActionCLI.fromValue(displayIncomingCLI) != null) {
                    resourceSpeedDialButtonInformation.setDisplayIncomingCLI(ButtonIncomingActionCLI.fromValue(displayIncomingCLI));
                }
            }

            if (buttonSettingsBean.getDisplayInCallHistory() != null && !buttonSettingsBean.getDisplayInCallHistory().isEmpty()) {
                resourceSpeedDialButtonInformation.setDisplayInCallHistory(Boolean.valueOf(buttonSettingsBean.getDisplayInCallHistory().toLowerCase()));
            }

            if (buttonSettingsBean.getRingtone() != null && !buttonSettingsBean.getRingtone().isEmpty()) {
                resourceSpeedDialButtonInformation.setRingTone(Integer.valueOf(buttonSettingsBean.getRingtone()));
            }

            ResourceAORInformation resourceAORInformation = new ResourceAORInformation();
            ResourceAORLineBean line = buttonSettingsBean.getLine();
            if (line != null) {
                if (line.getAppearance() != null && !line.getAppearance().isEmpty()) {
                    resourceAORInformation.setLineAppearance(Integer.valueOf(line.getAppearance()));
                }
                if (line.getResourceAORId() != null && !line.getResourceAORId().isEmpty()) {
                    resourceAORInformation.setResourceAORId(BigInteger.valueOf(Long.valueOf(line.getResourceAORId())));
                }
                resourceSpeedDialButtonInformation.setResourceAORInfo(resourceAORInformation);
            }

            SpeedDialButtonInformation speedDialButtonInformation = new SpeedDialButtonInformation();

            if (buttonSettingsBean.getSpeedDialType() != null && !buttonSettingsBean.getSpeedDialType().isEmpty()) {
                String speedDialType = buttonSettingsBean.getSpeedDialType();
                if (ButtonIcon.fromValue(speedDialType) != null) {
                    speedDialButtonInformation.setSpeedDialType(ButtonIcon.fromValue(speedDialType));
                }
            }
            if (buttonSettingsBean.getDestinationOrNumberToDial() != null && !buttonSettingsBean.getDestinationOrNumberToDial().isEmpty()) {
                speedDialButtonInformation.setDestinationOrNumberToDial(buttonSettingsBean.getDestinationOrNumberToDial());
            }

            resourceSpeedDialButtonInformation.setSpeedDialInfo(speedDialButtonInformation);
            buttonDetails.setResourceSpeedDialButtonInfo(resourceSpeedDialButtonInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.error("Exception setting Update Resource and SpeedDial button settings handle(): ", e);
            return null;
        }
    }

    public ButtonInformation updateHuntAndSpeedDialButton(ButtonSettingsBean buttonSettingsBean) {
        log.debug("Going to set and invoke Update Hunt and SpeedDial Button");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }

            ButtonDetails buttonDetails = new ButtonDetails();
            SpeedDialButtonInformation speedDialButtonInformation = new SpeedDialButtonInformation();

            if (buttonSettingsBean.getDestinationOrNumberToDial() != null && !buttonSettingsBean.getDestinationOrNumberToDial().isEmpty()) {
                speedDialButtonInformation.setDestinationOrNumberToDial(buttonSettingsBean.getDestinationOrNumberToDial());
            }

            if (buttonSettingsBean.getSpeedDialType() != null ) {
                String speedDialType = buttonSettingsBean.getSpeedDialType();
                if (ButtonIcon.fromValue(speedDialType) != null) {
                    speedDialButtonInformation.setSpeedDialType(ButtonIcon.fromValue(speedDialType));
                }
            }

            buttonDetails.setHuntSpeedDialButtonInfo(speedDialButtonInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.error("Exception setting Hunt and SpeedDial Button settings: ", e);
            return null;
        }
    }

    public ButtonInformation updateSpeedDialButton(ButtonSettingsBean buttonSettingsBean) {

        log.debug("Going to set Update SpeedDial Button");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }

            ButtonDetails buttonDetails = new ButtonDetails();
            SpeedDialButtonInformation speedDialButtonInformation = new SpeedDialButtonInformation();
            if (buttonSettingsBean.getDestinationOrNumberToDial() != null && !buttonSettingsBean.getDestinationOrNumberToDial().isEmpty()) {
                speedDialButtonInformation.setDestinationOrNumberToDial(buttonSettingsBean.getDestinationOrNumberToDial());
            }
            
            speedDialButtonInformation.setSpeedDialType(ButtonIcon.fromValue(buttonSettingsBean.getSpeedDialType()));
            buttonDetails.setSpeedDialButtonInfo(speedDialButtonInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.error("Exception setting SpeedDail button: ", e);
            return null;
        }
    }

    private ButtonInformation updateMWIButton(ButtonSettingsBean buttonSettingsBean) {
        log.info("Going to set and invoke Update MWI Button");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }
            ButtonDetails buttonDetails = new ButtonDetails();
            MWIButtonInformation mwiButtonInformation = new MWIButtonInformation();
            if (buttonSettingsBean.getDestinationOrNumberToDial() != null && !buttonSettingsBean.getDestinationOrNumberToDial().isEmpty()) {
                mwiButtonInformation.setNumberToDial(buttonSettingsBean.getDestinationOrNumberToDial());
            }

            ResourceAORInformation resourceAORInformation = new ResourceAORInformation();
            ResourceAORLineBean line = buttonSettingsBean.getLine();
            if (line != null) {
                if (line.getAppearance() != null && !line.getAppearance().isEmpty()) {
                    resourceAORInformation.setLineAppearance(Integer.valueOf(line.getAppearance()));
                }
                if (line.getResourceAORId() != null && !line.getResourceAORId().isEmpty()) {
                    resourceAORInformation.setResourceAORId(BigInteger.valueOf(Long.valueOf(line.getResourceAORId())));
                }
                mwiButtonInformation.setResourceAORInfo(resourceAORInformation);
            }

            buttonDetails.setMWIButtonInfo(mwiButtonInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.info("Exception updating MWI Button:  ", e);
            return null;
        }
    }
    
    
    private ButtonInformation updateOneDivertButton(ButtonSettingsBean buttonSettingsBean) {
        log.info("Going to set and invoke Update OneDivert Button");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }
            ButtonDetails buttonDetails = new ButtonDetails();
            OneButtonDivertAndResourceInformation oneDivertButtonInformation = new OneButtonDivertAndResourceInformation();
            if (buttonSettingsBean.getGetOneButtonDivertReason() != null && !buttonSettingsBean.getGetOneButtonDivertReason().isEmpty()) {
            	if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("none"))
            		oneDivertButtonInformation.setDivertReason(ButtonDivertReason.NONE);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("IMMEDIATE"))
            		oneDivertButtonInformation.setDivertReason(ButtonDivertReason.IMMEDIATE);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("ON_BUSY"))
            		oneDivertButtonInformation.setDivertReason(ButtonDivertReason.ON_BUSY);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("ON_BUSY_OR_RNA"))
            		oneDivertButtonInformation.setDivertReason(ButtonDivertReason.ON_BUSY_OR_RNA);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("ON_RNA"))
            		oneDivertButtonInformation.setDivertReason(ButtonDivertReason.ON_RNA);
            }
            
            if (buttonSettingsBean.getOneButtonDivertDestination() != null && !buttonSettingsBean.getOneButtonDivertDestination().isEmpty()) {
            	oneDivertButtonInformation.setDivertDestination(buttonSettingsBean.getOneButtonDivertDestination());
            }

            ResourceAORInformation resourceAORInformation = new ResourceAORInformation();
            ResourceAORLineBean line = buttonSettingsBean.getLine();
            if (line != null) {
                if (line.getAppearance() != null && !line.getAppearance().isEmpty()) {
                    resourceAORInformation.setLineAppearance(Integer.valueOf(line.getAppearance()));
                }
                if (line.getResourceAORId() != null && !line.getResourceAORId().isEmpty()) {
                    resourceAORInformation.setResourceAORId(BigInteger.valueOf(Long.valueOf(line.getResourceAORId())));
                }
                oneDivertButtonInformation.setResourceAORInfo(resourceAORInformation);
            }

            buttonDetails.setOneButtonDivertInfo(oneDivertButtonInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.info("Exception updating OneDivert Button:  ", e);
            return null;
        }
    }
    
    private ButtonInformation updateKeySequenceButton(ButtonSettingsBean buttonSettingsBean) {
        log.info("Going to set and invoke Update KeySequence Button");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }
            ButtonDetails buttonDetails = new ButtonDetails();
            FunctionButtonInformation functionButtonInformation = new FunctionButtonInformation();
            if(buttonSettingsBean.getKeyCodeValue()!=null && !buttonSettingsBean.getKeyCodeValue().isEmpty() )
            {
	            KeyCodeValueList keyCodeValueList = new KeyCodeValueList();
	            List<String> keyCodes = buttonSettingsBean.getKeyCodeValue();
	            for(String keyCode : keyCodes)
	            {
	            	keyCodeValueList.getKeyCodeValue().add(BigInteger.valueOf(Long.valueOf(keyCode)));
	            }
	            functionButtonInformation.setKeyCodeValueList(keyCodeValueList);
            }
            buttonDetails.setFunctionButtonInfo(functionButtonInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.info("Exception updating KeySequence Button:  ", e);
            return null;
        }
    }
    
    private ButtonInformation updatePointOfContactButton(ButtonSettingsBean buttonSettingsBean) {
        log.info("Going to set and invoke Update Point Of Contact Button");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }
            ButtonDetails buttonDetails = new ButtonDetails();
            ContactButtonInformation pointOfContactInformation = new ContactButtonInformation();
            if (buttonSettingsBean.getPointOfContactId() != null ) {
            	log.info("pointofcontact id "+buttonSettingsBean.getPointOfContactId());
            	pointOfContactInformation.setPointOfContactId(buttonSettingsBean.getPointOfContactId());
            	pointOfContactInformation.setDirectoryType(DirectoryTypes.ENTERPRISE);
            }
            buttonDetails.setContactButtonInfo(pointOfContactInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.info("Exception updating Update Point Of Contact Button:  ", e);
            return null;
        }
    }

    public ButtonInformation updateOneButtonDivertButton(ButtonSettingsBean buttonSettingsBean) {
        log.debug("Going to set and invoke Update OneButtonDivert");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }
            ButtonDetails buttonDetails = new ButtonDetails();
            OneButtonDivertAndResourceInformation oneButtonDivertAndResourceInformation = new OneButtonDivertAndResourceInformation();
            ResourceAORInformation resourceAORInformation = new ResourceAORInformation();
            ResourceAORLineBean line = buttonSettingsBean.getLine();
            if (line != null) {
                if (line.getAppearance() != null && !line.getAppearance().isEmpty()) {
                    resourceAORInformation.setLineAppearance(Integer.valueOf(line.getAppearance()));
                }
                if (line.getResourceAORId() != null && !line.getResourceAORId().isEmpty()) {
                    resourceAORInformation.setResourceAORId(BigInteger.valueOf(Long.valueOf(line.getResourceAORId())));
                }
                oneButtonDivertAndResourceInformation.setResourceAORInfo(resourceAORInformation);
            }

            if (buttonSettingsBean.getOneButtonDivertDestination() != null && !buttonSettingsBean.getOneButtonDivertDestination().isEmpty()) {
                oneButtonDivertAndResourceInformation.setDivertDestination(buttonSettingsBean.getOneButtonDivertDestination());
            }
            if (buttonSettingsBean.getGetOneButtonDivertReason() != null && !buttonSettingsBean.getGetOneButtonDivertReason().isEmpty()) {
            	if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("none"))
            		oneButtonDivertAndResourceInformation.setDivertReason(ButtonDivertReason.NONE);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("IMMEDIATE"))
            		oneButtonDivertAndResourceInformation.setDivertReason(ButtonDivertReason.IMMEDIATE);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("ON_BUSY"))
            		oneButtonDivertAndResourceInformation.setDivertReason(ButtonDivertReason.ON_BUSY);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("ON_BUSY_OR_RNA"))
            		oneButtonDivertAndResourceInformation.setDivertReason(ButtonDivertReason.ON_BUSY_OR_RNA);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("ON_RNA"))
            		oneButtonDivertAndResourceInformation.setDivertReason(ButtonDivertReason.ON_RNA);
            }
            

            buttonDetails.setOneButtonDivertInfo(oneButtonDivertAndResourceInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.error("Exception setting  OneButtonDivert settings: ", e);
            return null;
        }
    }

    public ButtonInformation updateOneButtonICMDivertButton(ButtonSettingsBean buttonSettingsBean) {
        log.debug("Going to set and invoke UpdateOneButtonICMDiverButton");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                log.debug("not null bbi");
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }

            ButtonDetails buttonDetails = new ButtonDetails();
            OneButtonDivertInformation oneButtonDivertInformation = new OneButtonDivertInformation();
            if (buttonSettingsBean.getDestinationOrNumberToDial() != null && !buttonSettingsBean.getDestinationOrNumberToDial().isEmpty()) {
                oneButtonDivertInformation.setDivertDestination(buttonSettingsBean.getDestinationOrNumberToDial());
            }
            if (buttonSettingsBean.getGetOneButtonDivertReason() != null && !buttonSettingsBean.getGetOneButtonDivertReason().isEmpty()) {
            	if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("none"))
            		oneButtonDivertInformation.setDivertReason(ButtonDivertReason.NONE);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("IMMEDIATE"))
            		oneButtonDivertInformation.setDivertReason(ButtonDivertReason.IMMEDIATE);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("ON_BUSY"))
            		oneButtonDivertInformation.setDivertReason(ButtonDivertReason.ON_BUSY);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("ON_BUSY_OR_RNA"))
            		oneButtonDivertInformation.setDivertReason(ButtonDivertReason.ON_BUSY_OR_RNA);
            	else if(buttonSettingsBean.getGetOneButtonDivertReason().equalsIgnoreCase("ON_RNA"))
            		oneButtonDivertInformation.setDivertReason(ButtonDivertReason.ON_RNA);
            }
            

            buttonDetails.setOneButtonICMDivertInfo(oneButtonDivertInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;

        } catch (Exception e) {
            log.error("Exception setting OneButtonICMDivert button: ", e);
            return null;
        }
    }

    public ButtonInformation updateSimplexConferenceButton(ButtonSettingsBean buttonSettingsBean) {
        log.debug("Going to set and invoke updateSimplexConferenceButton");

        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                log.debug("not null bbi");
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }

            ButtonDetails buttonDetails = new ButtonDetails();
            ConferenceButtonInformation conferenceButtonInformation = new ConferenceButtonInformation();
            if (buttonSettingsBean.getResourceAORBeanList() != null) {
                LineList lineList = new LineList();

                for (ResourceAORLineBean resourceAORBean : buttonSettingsBean.getResourceAORBeanList()) {
                    ResourceAORInformation resourceAORInformation = new ResourceAORInformation();
                    resourceAORInformation.setResourceAORId(BigInteger.valueOf(Long.valueOf(resourceAORBean.getResourceAORId())));
                    resourceAORInformation.setLineAppearance(Integer.valueOf(resourceAORBean.getAppearance()));
                    lineList.getLineResource().add(resourceAORInformation);
                }
                conferenceButtonInformation.setLineList(lineList);
            }

            if (buttonSettingsBean.getSpeedDialTypeForSimplexConference() != null && !buttonSettingsBean.getSpeedDialTypeForSimplexConference().isEmpty()) {
                String speedDialType = buttonSettingsBean.getSpeedDialTypeForSimplexConference();
                if (ButtonIcon.valueOf(speedDialType) != null) {
                    conferenceButtonInformation.setSpeedDialType(ButtonIcon.valueOf(speedDialType));
                }
            }

            buttonDetails.setSimplexConferenceButtonInfo(conferenceButtonInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.error("Exception setting SinplexConferenceButton: ", e);
            return null;
        }
    }

    public ButtonInformation updateDuplexConferenceButton(ButtonSettingsBean buttonSettingsBean) {
        log.debug("Going to invoke UpdateDuplexCongerenceButton");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                log.debug("not null bbi");
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }

            ButtonDetails buttonDetails = new ButtonDetails();
            if (buttonSettingsBean.getResourceAORBeanList() != null) {
                LineList lineList = new LineList();
                for (ResourceAORLineBean resourceAORBean : buttonSettingsBean.getResourceAORBeanList()) {
                    ResourceAORInformation resourceAORInformation = new ResourceAORInformation();
                    resourceAORInformation.setResourceAORId(BigInteger.valueOf(Long.valueOf(resourceAORBean.getResourceAORId())));
                    resourceAORInformation.setLineAppearance(Integer.valueOf(resourceAORBean.getAppearance()));
                    lineList.getLineResource().add(resourceAORInformation);
                }
                buttonDetails.setDuplexConferenceButtonInfo(lineList);
            }

            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;

        } catch (Exception e) {
            log.error("Exception setting DuplexConferenceButton ", e);
            return null;
        }
    }

    public ButtonInformation updateIntercomButton(ButtonSettingsBean buttonSettingsBean) {
        log.debug("Going to invoke UpdateIntercomButton");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }

            ButtonDetails buttonDetails = new ButtonDetails();
            if (buttonSettingsBean.getIcmNumberToDial() != null && !buttonSettingsBean.getIcmNumberToDial().isEmpty()) {
                buttonDetails.setICMNumberToDial(buttonSettingsBean.getIcmNumberToDial());
            }
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.error("Exception setting Intercom Button: ", e);
            return null;
        }
    }
    
    private ButtonInformation updatePersonalPointOfContactButton(ButtonSettingsBean buttonSettingsBean) {
        log.info("Going to set and invoke Update Point Of Contact Button");
        try {
            ButtonInformation buttonInformation = new ButtonInformation();
            if (buttonSettingsBean.getButtonId() != null) {
                buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(buttonSettingsBean));
            }
            ButtonDetails buttonDetails = new ButtonDetails();
            ContactButtonInformation pointOfContactInformation = new ContactButtonInformation();
            if (buttonSettingsBean.getPointOfContactId() != null ) {
            	log.info("pointofcontact id "+buttonSettingsBean.getPointOfContactId());
            	pointOfContactInformation.setPointOfContactId(buttonSettingsBean.getPointOfContactId());
            	pointOfContactInformation.setDirectoryType(DirectoryTypes.PERSONAL);
            }
            buttonDetails.setContactButtonInfo(pointOfContactInformation);
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.info("Exception updating Update Point Of Contact Button:  ", e);
            return null;
        }
    }

    public ReasonData invokeUpdateButtonSettings(UpdateEndUserButton updateEndUserButtonFinalObject) {
        try {
            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            //after creating session, invoke httpsput and send the update trader features request
            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());
            String apiVersion = ipcSite.getApiVersion();

            String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.BUTTON_SETTINGS.toString());
            log.debug("created resourceurl: " + resourceUrl);

            Response response = IPCProvHttpClient.httpsPut(resourceUrl,
                    httpAuth, ipcAuthToken, apiVersion, updateEndUserButtonFinalObject);
            
            log.info("final Object "+UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(updateEndUserButtonFinalObject)));

            log.debug("Update Button Settings Response: " + response);
            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for UpdateButtonSettings is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("UpdateButtonSettings error response " + formattedResponseXML);
                return errorResp.getReason();
            } else if (respEntity instanceof UpdateEndUserButtonResponse) {
                UpdateEndUserButtonResponse updateButtonSettingsResponse = (UpdateEndUserButtonResponse) respEntity;
                String updateButtonSettingsRespString = UtilHelper.marshalJAXBObjectForResponse(updateButtonSettingsResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(updateButtonSettingsRespString);
                log.debug("UpdateButtonSettings response " + formattedResponseXML);
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);
                return updateButtonSettingsResponse.getReason();
            }
        } catch (Exception e) {
            log.error("Exception Update Button Settings:  ", e);
            return null;
        }
        return null;
    }

    private ButtonBasicInformation setButtonBasicInformationForUpdate(ButtonSettingsBean buttonSettingsBean) {
        log.debug("In setButtonBasicInformationForUpdate");
        try {
            ButtonBasicInformation buttonBasicInformation = new ButtonBasicInformation();
            ButtonIdentifier buttonIdentifier = new ButtonIdentifier();
            buttonIdentifier.setButtonId(BigInteger.valueOf(Long.valueOf(buttonSettingsBean.getButtonId())));
            buttonBasicInformation.setButtonIdentifier(buttonIdentifier);
            buttonBasicInformation.setButtonType(ButtonButtonType.fromValue(buttonSettingsBean.getButtonType()));
            if(buttonSettingsBean.getButtonColor()!=null)
            {
            	buttonBasicInformation.setButtonColor(buttonSettingsBean.getButtonColor());
            }
            if (buttonSettingsBean.getButtonLabel() != null && !buttonSettingsBean.getButtonLabel().isEmpty()) {
                log.debug("!@#buttonLabel is " + buttonSettingsBean.getButtonLabel());
                buttonBasicInformation.setButtonLabel(buttonSettingsBean.getButtonLabel());
            }
            if (buttonSettingsBean.getButtonType().equalsIgnoreCase("InvalidButtonType"))
            {
            	buttonBasicInformation.setButtonLabel("");
            }
            if (buttonSettingsBean.getLockedForProgrammingAtCDI() != null && !buttonSettingsBean.getLockedForProgrammingAtCDI().isEmpty()) {
                buttonBasicInformation.setLockedForProgrammingAtCDI(Boolean.valueOf(buttonSettingsBean.getLockedForProgrammingAtCDI()));
            }
            return buttonBasicInformation;
        } catch (Exception e) {
            log.error("Exception setting ButtonBasicInformation for update: ", e);
            return null;
        }
    }

    public ResponseDataBean sendReasonDataErrResponse(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(REQ_FAILURE_DESC + errorField);
        return responseDataBean;
    }

    public ResponseDataBean validateButtonSettings(IPCSite ipcSite, List<ButtonSettingsBean> buttonSettingsBeanList) {
        ResponseDataBean responseDataBean = new ResponseDataBean();
        String buttonId = null;
        for (ButtonSettingsBean buttonSettingsBean : buttonSettingsBeanList) {
            if (StringUtils.isNotEmpty(buttonSettingsBean.getButtonId())) {
                try {
                    Integer.parseInt(buttonSettingsBean.getButtonId());
                    buttonId = buttonSettingsBean.getButtonId();
                } catch (Exception e) {
                    return sendReasonDataErrResponse(responseDataBean, "ButtonId:" + buttonSettingsBean.getButtonId());
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "ButtonId is mandatory");
            }
            if (StringUtils.isNotEmpty(buttonSettingsBean.getButtonType())) {
                if (ButtonButtonType.fromValue(buttonSettingsBean.getButtonType()) == null) {
                    return sendReasonDataErrResponse(responseDataBean, "ButtonType" + FOR_BUTTONID + buttonId);
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "ButtonType is mandatory" + FOR_BUTTONID + buttonId);
            }

            if (buttonSettingsBean.getButtonType().equals("Resource")) {
                if (StringUtils.isNotEmpty(buttonSettingsBean.getIncomingActionRings())) {
                    if (ButtonIncomingActionRings.fromValue(buttonSettingsBean.getIncomingActionRings()) == null) {
                        return sendReasonDataErrResponse(responseDataBean, "IncomingActionRings" + FOR_BUTTONID + buttonId);
                    }
                }

                if (StringUtils.isNotEmpty(buttonSettingsBean.getIncomingActionPriority())) {
                    if (ButtonIncomingActionPriority.fromValue(buttonSettingsBean.getIncomingActionPriority()) == null) {
                        return sendReasonDataErrResponse(responseDataBean, "IncomingActionPriority" + FOR_BUTTONID + buttonId);
                    }
                }

                if (StringUtils.isNotEmpty(buttonSettingsBean.getIncomingActionFloat())) {
                    if (ButtonIncomingActionFloat.fromValue(buttonSettingsBean.getIncomingActionFloat()) == null) {
                        return sendReasonDataErrResponse(responseDataBean, "IncomingActionFloat" + FOR_BUTTONID + buttonId);
                    }
                }

                if (buttonSettingsBean.getDisplayIncomingCLI() != null && !buttonSettingsBean.getDisplayIncomingCLI().isEmpty()) {
                    if (ButtonIncomingActionCLI.fromValue(buttonSettingsBean.getDisplayIncomingCLI()) == null) {
                        return sendReasonDataErrResponse(responseDataBean, "IncomingActionCLI" + FOR_BUTTONID + buttonId);
                    }
                }

                if (buttonSettingsBean.getRingtone() != null && !buttonSettingsBean.getRingtone().isEmpty()) {
                    try {
                        Integer.parseInt(buttonSettingsBean.getRingtone());
                    } catch (Exception e) {
                        log.error("Invalid ringtone: ", e);
                        return sendReasonDataErrResponse(responseDataBean, "Ringtone" + FOR_BUTTONID + buttonId);
                    }
                }

                if (buttonSettingsBean.getSpeedDialType() != null && !buttonSettingsBean.getSpeedDialType().isEmpty()) {
                    if (ButtonIcon.fromValue(buttonSettingsBean.getSpeedDialType()) == null) {
                        return sendReasonDataErrResponse(responseDataBean, "SpeedDialType" + FOR_BUTTONID + buttonId);
                    }
                }

                if (buttonSettingsBean.getLine() != null) {
                    ResourceAORLineBean lineBean = buttonSettingsBean.getLine();

                    if (lineBean.getResourceAORId() != null) {
                        try {
                            Integer.parseInt(lineBean.getResourceAORId());
                        } catch (Exception e) {
                            log.error("Invalid ResourceAORId: ", e);
                            return sendReasonDataErrResponse(responseDataBean, "ResourceAORId" + FOR_BUTTONID + buttonId);
                        }
                    } else {
                        return sendReasonDataErrResponse(responseDataBean, "ResourceAORId is mandatory" + FOR_BUTTONID + buttonId);
                    }
                    if (lineBean.getAppearance() != null) {
                        try {
                            Integer.parseInt(lineBean.getAppearance());
                        } catch (Exception e) {
                            log.error("Invalid Appearance: ", e);
                            return sendReasonDataErrResponse(responseDataBean, "Appearance" + FOR_BUTTONID + buttonId);
                        }
                    } else {
                        return sendReasonDataErrResponse(responseDataBean, "Line Appearance is mandatory" + FOR_BUTTONID + buttonId);
                    }
                }

            }
            else if (buttonSettingsBean.getButtonType().equals("SpeedDial") || buttonSettingsBean.getButtonType().equals("HuntAndSpeedDial")) {
                if (buttonSettingsBean.getSpeedDialType() != null && !buttonSettingsBean.getSpeedDialType().equalsIgnoreCase("")) {
                    if (ButtonIcon.fromValue(buttonSettingsBean.getSpeedDialType()) == null) {
                        return sendReasonDataErrResponse(responseDataBean, "SpeedDialType" + FOR_BUTTONID + buttonId);
                    }
                } else {
                    return sendReasonDataErrResponse(responseDataBean, "SpeedDialType is mandatory" + FOR_BUTTONID + buttonId);
                }
               
            }

            else if (buttonSettingsBean.getButtonType().equals("ICM")) {
                if (buttonSettingsBean.getIcmNumberToDial() == null || buttonSettingsBean.getIcmNumberToDial().isEmpty()) {
                    return sendReasonDataErrResponse(responseDataBean, "Intercom Number To Dial is mandatory" + FOR_BUTTONID + buttonId);
                } else if (buttonSettingsBean.getIcmNumberToDial() != null) {
                    try {
                        Integer.parseInt(buttonSettingsBean.getIcmNumberToDial());
                    } catch (Exception e) {
                        return sendReasonDataErrResponse(responseDataBean, "Intercom Number To Dial" + FOR_BUTTONID + buttonId);
                    }
                }
            }
            
            else if(buttonSettingsBean.getButtonType().equals("ResourceAndSpeedDial")){
            	
            	if (buttonSettingsBean.getSpeedDialType() != null && !buttonSettingsBean.getSpeedDialType().equalsIgnoreCase("")) {
                    if (ButtonIcon.fromValue(buttonSettingsBean.getSpeedDialType()) == null) {
                        return sendReasonDataErrResponse(responseDataBean, "SpeedDialType" + FOR_BUTTONID + buttonId);
                    }
                } else {
                    return sendReasonDataErrResponse(responseDataBean, "SpeedDialType is mandatory" + FOR_BUTTONID + buttonId);
                }
            	if(buttonSettingsBean.getDestinationOrNumberToDial() == null  || buttonSettingsBean.getDestinationOrNumberToDial().isEmpty())
            	{
            		 return sendReasonDataErrResponse(responseDataBean, "Destination Number To Dial is mandatory" + FOR_BUTTONID + buttonId);
            	}
            	 if (buttonSettingsBean.getLine() != null) {
                     ResourceAORLineBean lineBean = buttonSettingsBean.getLine();

                     if (lineBean.getResourceAORId() != null) {
                         try {
                             Integer.parseInt(lineBean.getResourceAORId());
                         } catch (Exception e) {
                             log.error("Invalid ResourceAORId: ", e);
                             return sendReasonDataErrResponse(responseDataBean, "ResourceAORId" + FOR_BUTTONID + buttonId);
                         }
                     } else {
                         return sendReasonDataErrResponse(responseDataBean, "ResourceAORId is mandatory" + FOR_BUTTONID + buttonId);
                     }
                     if (lineBean.getAppearance() != null) {
                         try {
                             Integer.parseInt(lineBean.getAppearance());
                         } catch (Exception e) {
                             log.error("Invalid Appearance: ", e);
                             return sendReasonDataErrResponse(responseDataBean, "Appearance" + FOR_BUTTONID + buttonId);
                         }
                     } else {
                         return sendReasonDataErrResponse(responseDataBean, "Line Appearance is mandatory" + FOR_BUTTONID + buttonId);
                     }
                 }
            }
            
            else if(buttonSettingsBean.getButtonType().equals("PointOfContact")) {
            	if(buttonSettingsBean.getPointOfContactId() == null)
            	{
            		 return sendReasonDataErrResponse(responseDataBean, "PointOfContact Id is mandatory" + FOR_BUTTONID + buttonId);
            	}
            }
            else if(buttonSettingsBean.getButtonType().equals("KeySequence")) {
            	if(buttonSettingsBean.getKeyCodeValue()==null || buttonSettingsBean.getKeyCodeValue().size()==0)
            	{
            		 return sendReasonDataErrResponse(responseDataBean, "KeyCode values are mandatory" + FOR_BUTTONID + buttonId);
            	}
            }
            
            else if(buttonSettingsBean.getButtonType().equals("OneButtonDivert")){
            	if(buttonSettingsBean.getOneButtonDivertDestination() == null || buttonSettingsBean.getOneButtonDivertDestination().isEmpty())
            	{
            		 return sendReasonDataErrResponse(responseDataBean, "OneButtonDivert Destination is mandatory" + FOR_BUTTONID + buttonId);
            	}
            	if(buttonSettingsBean.getGetOneButtonDivertReason() == null || buttonSettingsBean.getGetOneButtonDivertReason().isEmpty())
            	{
            		 return sendReasonDataErrResponse(responseDataBean, "OneButtonDivert Reason is mandatory" + FOR_BUTTONID + buttonId);
            	}
            	if (buttonSettingsBean.getLine() != null) {
                    ResourceAORLineBean lineBean = buttonSettingsBean.getLine();

                    if (lineBean.getResourceAORId() != null) {
                        try {
                            Integer.parseInt(lineBean.getResourceAORId());
                        } catch (Exception e) {
                            log.error("Invalid ResourceAORId: ", e);
                            return sendReasonDataErrResponse(responseDataBean, "ResourceAORId" + FOR_BUTTONID + buttonId);
                        }
                    } else {
                        return sendReasonDataErrResponse(responseDataBean, "ResourceAORId is mandatory" + FOR_BUTTONID + buttonId);
                    }
                    if (lineBean.getAppearance() != null) {
                        try {
                            Integer.parseInt(lineBean.getAppearance());
                        } catch (Exception e) {
                            log.error("Invalid Appearance: ", e);
                            return sendReasonDataErrResponse(responseDataBean, "Appearance" + FOR_BUTTONID + buttonId);
                        }
                    } else {
                        return sendReasonDataErrResponse(responseDataBean, "Line Appearance is mandatory" + FOR_BUTTONID + buttonId);
                    }
                }

            }
            else if(buttonSettingsBean.getButtonType().equals("MWI")) {
            	if(buttonSettingsBean.getDestinationOrNumberToDial() == null  || buttonSettingsBean.getDestinationOrNumberToDial().isEmpty())
            	{
            		 return sendReasonDataErrResponse(responseDataBean, "Destination Number To Dial is mandatory" + FOR_BUTTONID + buttonId);
            	}
            	if (buttonSettingsBean.getLine() != null) {
                    ResourceAORLineBean lineBean = buttonSettingsBean.getLine();

                    if (lineBean.getResourceAORId() != null) {
                        try {
                            Integer.parseInt(lineBean.getResourceAORId());
                        } catch (Exception e) {
                            log.error("Invalid ResourceAORId: ", e);
                            return sendReasonDataErrResponse(responseDataBean, "ResourceAORId" + FOR_BUTTONID + buttonId);
                        }
                    } else {
                        return sendReasonDataErrResponse(responseDataBean, "ResourceAORId is mandatory" + FOR_BUTTONID + buttonId);
                    }
                    if (lineBean.getAppearance() != null) {
                        try {
                            Integer.parseInt(lineBean.getAppearance());
                        } catch (Exception e) {
                            log.error("Invalid Appearance: ", e);
                            return sendReasonDataErrResponse(responseDataBean, "Appearance" + FOR_BUTTONID + buttonId);
                        }
                    } else {
                        return sendReasonDataErrResponse(responseDataBean, "Line Appearance is mandatory" + FOR_BUTTONID + buttonId);
                    }
                }
            }

        }
        return null;
    }
}
