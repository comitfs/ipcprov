package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.AddRemoveUsrFromGroupBean;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;

public class AddRemoveUserFromEndUserGroup {

    public static final String INTERNAL_SERVER_ERROR = "Internal server error";
    public static final String REQ_FAILURE = "REQ_FAILURE";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(AddRemoveUserFromEndUserGroup.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    public AddRemoveUserFromEndUserGroup(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String casUserName, AddRemoveUsrFromGroupBean addRemoveUsrFromGroupBean) {
        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            log.debug("handling AddRemoveUserFromEndUserGroup for UserName: " + casUserName);

            IPCUser ipcUser = IPCDataService.getInstance().getUserById(casUserName,ipcSite.getId());
            if (ipcUser == null) {
                IPCDataService.getInstance().getUserPreference(casUserName, ipcSite);
                ipcUser = IPCDataService.getInstance().getUserById(casUserName, ipcSite.getId());
            }
            String apiVer = ipcSite.getApiVersion();

            UserIdentifier userIdentifier = new UserIdentifier();
            //set the ipcuser's unigue id to be updated with attributes
            if (null != ipcUser && ipcUser.getIPCId() != null) {
                userIdentifier.setUserId(BigInteger.valueOf(ipcUser.getIPCId()));
            } else if (casUserName != null) {
                userIdentifier.setLoginName(casUserName);
            }

            GroupMembership groupMembership = new GroupMembership();
            groupMembership.setUserIdentifier(userIdentifier);
            GroupIdentifier groupIdentifier = new GroupIdentifier();

            if (addRemoveUsrFromGroupBean.getGroupId() != null && !addRemoveUsrFromGroupBean.getGroupId().isEmpty()) {
                groupIdentifier.setGroupId(BigInteger.valueOf(Integer.valueOf(addRemoveUsrFromGroupBean.getGroupId())));
            }
            if (addRemoveUsrFromGroupBean.getGroupName() != null && !addRemoveUsrFromGroupBean.getGroupName().isEmpty()) {
                groupIdentifier.setGroupName(addRemoveUsrFromGroupBean.getGroupName());
            }
            groupMembership.setGroupIdentifier(groupIdentifier);

            if (addRemoveUsrFromGroupBean.getRetainConfiguration() != null && addRemoveUsrFromGroupBean.getRetainConfiguration().isEmpty()) {
                groupMembership.setRetainConfiguration(Boolean.valueOf(addRemoveUsrFromGroupBean.getRetainConfiguration()));
            }

            String addRemoveUsrRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(groupMembership));
            log.debug(String.format("Group Membership request string:\n" + addRemoveUsrRequestString));
            //TODO: acctmgmt usrname & pwd shd be retrieved from ipc site settings table based on the user Id passed as input

            //create session with the above retrieved acct mgmt usr/pwd from ipcsite table
            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            //after creating session, invoke httpsput
            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());

            String resourceUrl = null;
            if (addRemoveUsrFromGroupBean.getOperationType().equalsIgnoreCase("add")) {
                resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.ADDUSR_ENDUSRGROUP.toString());
            } else if (addRemoveUsrFromGroupBean.getOperationType().equalsIgnoreCase("remove")) {
                resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.REMOVEUSR_ENDUSRGROUP.toString());
            }
            log.debug("created resourceurl for add/remove user from group: " + resourceUrl);

            Response response = IPCProvHttpClient.httpsPut(resourceUrl,
                    httpAuth, ipcAuthToken, apiVer, groupMembership);

            log.debug("Add/Remove GroupMembershipRequest Response: " + response);

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for Add/Remove GroupMembershipRequest is null.");
                return toErrorResponseDataBean(responseDataBean, "Add/Remove GroupMembershipRequest response is null");
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("Add/Remove GroupMembershipRequest error response " + formattedResponseXML);
                log.error("Create User error response " + formattedResponseXML);
                responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
            } else if (respEntity instanceof GroupMembershipResponse) {
                GroupMembershipResponse groupMembershipResponse = (GroupMembershipResponse) respEntity;
                String groupMembershipRespString = UtilHelper.marshalJAXBObjectForResponse(groupMembershipResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(groupMembershipRespString);
                log.debug("GroupMembershipResponse response " + formattedResponseXML);

                ReasonData reasonData = groupMembershipResponse.getReason();
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVer);
                responseDataBean.setReasonCode(reasonData.getReasonCode().value());
                responseDataBean.setReasonDescription(reasonData.getReasonDescription());

            }
        } catch (Exception e) {
            log.error("Exception adding/removing user in End User Group : ", e);
            return toErrorResponseDataBean(responseDataBean, INTERNAL_SERVER_ERROR);
        }
        return responseDataBean;
    }

    private ResponseDataBean toErrorResponseDataBean(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(errorField);
        return responseDataBean;
    }
}
