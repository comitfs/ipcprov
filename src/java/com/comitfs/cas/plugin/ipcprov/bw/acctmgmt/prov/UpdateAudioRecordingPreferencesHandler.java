package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;

import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.dataservice.UserInformation;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;

public class UpdateAudioRecordingPreferencesHandler {
    public static final String REQ_FAILURE = "REQ_FAILURE";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(UpdateUserAudioPreferencesHandler.class);

    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    private String deviceType;

    public UpdateAudioRecordingPreferencesHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String casUserName, int recordingProfile, String deviceType, String recordingProtocol) {
        try {
            log.debug("handling UpdateAudioRecordingPreferencesHandler");

            IPCUser ipcUser = IPCDataService.getInstance().getUserById(casUserName,ipcSite.getId());
            String apiVersion = ipcSite.getApiVersion();

            ResponseDataBean responseDataBean = new ResponseDataBean();
            UpdateAudioPreferences updateAudioPreferences = new UpdateAudioPreferences();
            UserIdentifier userIdentifier = new UserIdentifier();
            userIdentifier.setUserId(BigInteger.valueOf(ipcUser.getIPCId()));

            updateAudioPreferences.setUserIdentifier(userIdentifier);
            if (UserAudioDeviceType.fromValue(deviceType) != null) {
                updateAudioPreferences.setDeviceType(UserAudioDeviceType.fromValue(deviceType));
            }

            UserAudioPreferences audioPreferences = new UserAudioPreferences();
            if (deviceType == null) {
                responseDataBean.setReasonCode(REQ_FAILURE);
                responseDataBean.setReasonDescription("DeviceType is mandatory");
                return responseDataBean;
            }

            UserInformation userInformation = new UserInformation(ipcSite);
            UserPreferencesType userPreferencesType = userInformation.fetchUserPreferences(casUserName);

            if (deviceType.equals("Turret")) {
                TurretAudioFeatures turretAudioFeatures = new TurretAudioFeatures();
                TurretAudioPreferences turretAudioPreferences = userPreferencesType.getTurretAudioPreferences();


                turretAudioFeatures.setCallRinger(turretAudioPreferences.getCallRinger());
                turretAudioFeatures.setRingerVolume(turretAudioPreferences.getRingerVolume().getValue());
                turretAudioFeatures.setHandsetReceiveVolume(turretAudioPreferences.getHfmRxVolume().getValue());
                turretAudioFeatures.setHandsetTransmitVolume(turretAudioPreferences.getHdstTxVolume().getValue());
                turretAudioFeatures.setHandsetSideToneAttenuation(turretAudioPreferences.getHdstSideToneAtten().getValue());
                String hsReceiveNoiseReduceMode = turretAudioPreferences.getHdstRxNoiseReduceMode().getValue().value();
                if (NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode) != null) {
                    turretAudioFeatures.setHandsetReceiveNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode));
                }
                String hsTxNoiseReduceMode = turretAudioPreferences.getHdstTxNoiseReduceMode().getValue().value();
                if (NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode) != null) {
                    turretAudioFeatures.setHandsetTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode));
                }
                String hsRxEqualization = turretAudioPreferences.getHdstRxEqualization().getValue().value();
                if (ReceiverEqualizationTypes.fromValue(hsRxEqualization) != null) {
                    turretAudioFeatures.setHandsetReceiveEqualization(ReceiverEqualizationTypes.fromValue(hsRxEqualization));
                }
                turretAudioFeatures.setIsHandsetWideband(turretAudioPreferences.getHdstIsWideband().getValue());
                turretAudioFeatures.setSpeakerMasterRxVolume1(turretAudioPreferences.getSpkrMasterRxVolume1().getValue());
                turretAudioFeatures.setSpeakerTransmitVolume(turretAudioPreferences.getSpkrTxVolume().getValue());
                String spkrTxNoiseReduceMode = turretAudioPreferences.getSpkrTxNoiseReduceMode().getValue().value();
                if (NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode) != null) {
                    turretAudioFeatures.setSpeakerTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode));
                }
                String spkrRxEqualization = turretAudioPreferences.getSpkrRxEqualization().getValue().value();
                if (ReceiverEqualizationTypes.fromValue(spkrRxEqualization) != null) {
                    turretAudioFeatures.setSpeakerReceiveEqualization(ReceiverEqualizationTypes.fromValue(spkrRxEqualization));
                }

                turretAudioFeatures.setSpeakerSummingMode(turretAudioPreferences.getSpkrSummingMode().getValue());
                String hfmRxEqualization = turretAudioPreferences.getHfmRxEqualization().getValue().value();
                if (ReceiverEqualizationTypes.fromValue(hfmRxEqualization) != null) {
                    turretAudioFeatures.setHFMReceiveEqualization(ReceiverEqualizationTypes.fromValue(hfmRxEqualization));
                }
                turretAudioFeatures.setHFMReceiveVolume(turretAudioPreferences.getHfmRxVolume().getValue());
                String hfmTxAutoGainControlMode = turretAudioPreferences.getHfmTxAGCMode().getValue().value();
                if (NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode) != null) {
                    turretAudioFeatures.setHFMTransmitAutoGainControlMode(NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode));
                }
                String hfmTxNoiseReduceMode = turretAudioPreferences.getHdstTxNoiseReduceMode().getValue().value();
                if (NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode) != null) {
                    turretAudioFeatures.setHFMTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode));
                }
                turretAudioFeatures.setHFMTransmitVolume(turretAudioPreferences.getHfmTxVolume().getValue());
                turretAudioFeatures.setIPRecordOutputGain(turretAudioPreferences.getIpRecordOutputGain().getValue());
                turretAudioFeatures.setSpeakerIPRecordMicrophoneMixGain(turretAudioPreferences.getSpkrIPRecordMicMixGain().getValue());
                turretAudioFeatures.setHandsetIPRecordMicrophoneMixGain(turretAudioPreferences.getHdstIPRecordMicMixGain().getValue());
                turretAudioFeatures.setRecordMixProfiles(String.valueOf(recordingProfile));
                String recordProtocolValue = recordingProtocol;
                if (RecordingProtocolTypes.fromValue(recordProtocolValue) != null) {
                    turretAudioFeatures.setRecordProtocol(RecordingProtocolTypes.fromValue(recordProtocolValue));
                }
                turretAudioFeatures.setAcousticFeedbackReduction(turretAudioPreferences.getAcousticFeedbackReduction().getValue());
                String replayPlayBackOutputValue = turretAudioPreferences.getReplayPlaybackOutput().getValue().value();
                if (OutputModuleTypes.fromValue(replayPlayBackOutputValue) != null) {
                    turretAudioFeatures.setReplayPlaybackOutput(OutputModuleTypes.fromValue(replayPlayBackOutputValue));
                }

                turretAudioFeatures.setHighPriorityRing(turretAudioPreferences.getHighPriRing());
                turretAudioFeatures.setLowPriorityRing(turretAudioPreferences.getLowPriRing());
                turretAudioFeatures.setSpeakerMasterRxVolume2(turretAudioPreferences.getSpkrMasterRxVolume2().getValue());
                turretAudioFeatures.setSpeakerMasterRxVolume3(turretAudioPreferences.getSpkrMasterRxVolume3().getValue());
                String lineInMixerOutput = turretAudioPreferences.getLineInMixerOutput().getValue().value();
                if (OutputModuleTypes.fromValue(lineInMixerOutput) != null) {
                    turretAudioFeatures.setLineInMixerOutput(OutputModuleTypes.fromValue(lineInMixerOutput));
                }
                audioPreferences.setTurretAudioFeatures(turretAudioFeatures);
            } else if (deviceType.equals("Pulse")) {
                PulseAudioFeatures pulseAudioFeatures = new PulseAudioFeatures();
                PulseAudioPreferences pulseAudioPreferences = userPreferencesType.getPulseAudioPreferences();

                pulseAudioFeatures.setCallRinger(pulseAudioPreferences.getCallRinger());

                pulseAudioFeatures.setRingerVolume(pulseAudioPreferences.getRingerVolume().getValue());

                pulseAudioFeatures.setHandsetReceiveVolume(pulseAudioPreferences.getHdstRxVolume().getValue());

                pulseAudioFeatures.setHandsetTransmitVolume(pulseAudioPreferences.getHdstTxVolume().getValue());

                pulseAudioFeatures.setHandsetSideToneAttenuation(pulseAudioPreferences.getHdstSideToneAtten().getValue());

                String hsReceiveNoiseReduceMode = pulseAudioPreferences.getHdstRxNoiseReduceMode().getValue().value();
                if (NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode) != null) {
                    pulseAudioFeatures.setHandsetReceiveNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode));
                }

                String hsTxNoiseReduceMode = pulseAudioPreferences.getHdstTxNoiseReduceMode().getValue().value();
                if (NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode) != null) {
                    pulseAudioFeatures.setHandsetTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode));
                }

                String hsRxEqualization = pulseAudioPreferences.getHdstRxEqualization().getValue().value();
                if (ReceiverEqualizationTypes.fromValue(hsRxEqualization) != null) {
                    pulseAudioFeatures.setHandsetReceiveEqualization(ReceiverEqualizationTypes.fromValue(hsRxEqualization));
                }

                pulseAudioFeatures.setIsHandsetWideband(pulseAudioPreferences.getHdstIsWideband().getValue());

                pulseAudioFeatures.setSpeakerMasterRxVolume1(pulseAudioPreferences.getSpkrMasterRxVolume1().getValue());

                pulseAudioFeatures.setSpeakerTransmitVolume(pulseAudioPreferences.getSpkrTxVolume().getValue());

                String spkrTxNoiseReduceMode = pulseAudioPreferences.getSpkrTxNoiseReduceMode().getValue().value();
                if (NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode) != null) {
                    pulseAudioFeatures.setSpeakerTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode));
                }

                String spkrRxEqualization = pulseAudioPreferences.getSpkrRxEqualization().getValue().value();
                if (ReceiverEqualizationTypes.fromValue(spkrRxEqualization) != null) {
                    pulseAudioFeatures.setSpeakerReceiveEqualization(ReceiverEqualizationTypes.fromValue(spkrRxEqualization));
                }

                pulseAudioFeatures.setSpeakerSummingMode(pulseAudioPreferences.getSpkrSummingMode().getValue());

                String hfmRxEqualization = pulseAudioPreferences.getHfmRxEqualization().getValue().value();
                if (ReceiverEqualizationTypes.fromValue(hfmRxEqualization) != null) {
                    pulseAudioFeatures.setHFMReceiveEqualization(ReceiverEqualizationTypes.fromValue(hfmRxEqualization));
                }

                pulseAudioFeatures.setHFMReceiveVolume(pulseAudioPreferences.getHfmRxVolume().getValue());

                String hfmTxAutoGainControlMode = pulseAudioPreferences.getHfmTxAGCMode().getValue().value();
                if (NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode) != null) {
                    pulseAudioFeatures.setHFMTransmitAutoGainControlMode(NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode));
                }


                String hfmTxNoiseReduceMode = pulseAudioPreferences.getHfmTxNoiseReduceMode().getValue().value();
                if (NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode) != null) {
                    pulseAudioFeatures.setHFMTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode));
                }

                pulseAudioFeatures.setHFMTransmitVolume(pulseAudioPreferences.getHfmTxVolume().getValue());

                pulseAudioFeatures.setIPRecordOutputGain(pulseAudioPreferences.getIpRecordOutputGain().getValue());

                pulseAudioFeatures.setSpeakerIPRecordMicrophoneMixGain(pulseAudioPreferences.getSpkrIPRecordMicMixGain().getValue());

                pulseAudioFeatures.setHandsetIPRecordMicrophoneMixGain(pulseAudioPreferences.getHdstIPRecordMicMixGain().getValue());

                pulseAudioFeatures.setRecordMixProfiles(String.valueOf(recordingProfile));

                String recordProtocolValue = recordingProtocol;
                if (RecordingProtocolTypes.fromValue(recordProtocolValue) != null) {
                    pulseAudioFeatures.setRecordProtocol(RecordingProtocolTypes.fromValue(recordProtocolValue));
                }

                pulseAudioFeatures.setAcousticFeedbackReduction(pulseAudioPreferences.getAcousticFeedbackReduction().getValue());
                String replayPlayBackOutputValue = pulseAudioPreferences.getReplayPlaybackOutput().getValue().value();
                if (OutputModuleTypes.fromValue(replayPlayBackOutputValue) != null) {
                    pulseAudioFeatures.setReplayPlaybackOutput(OutputModuleTypes.fromValue(replayPlayBackOutputValue));
                }

                String speakerLatching = pulseAudioPreferences.getSpeakerLatching().getValue().value();
                if (SpeakerLatchingTypes.fromValue(speakerLatching) != null) {
                    pulseAudioFeatures.setSpeakerLatching(SpeakerLatchingTypes.fromValue(speakerLatching));
                }

                //HFMTransitionDevice
                if (pulseAudioPreferences.getHfmTransitionDevice() != null) {
                    String hfmTransitionDevice = pulseAudioPreferences.getHfmTransitionDevice().getValue().value();
                    if (HFMTransitionDeviceTypes.fromValue(hfmTransitionDevice) != null) {
                        pulseAudioFeatures.setHFMTransitionDevice(HFMTransitionDeviceTypes.fromValue(hfmTransitionDevice));
                    }
                }

                //SpeakerAudioOutput
                if (pulseAudioPreferences.getSpkrAudioOutSelect() != null) {
                    String speakerAudioOutput = pulseAudioPreferences.getSpkrAudioOutSelect().getValue().value();
                    if (SpeakerAudioOutputTypes.fromValue(speakerAudioOutput) != null) {
                        pulseAudioFeatures.setSpeakerAudioOutput(SpeakerAudioOutputTypes.fromValue(speakerAudioOutput));
                    }
                }

                //USBReceiveVolume
                if (pulseAudioPreferences.getUsbRcvVolume() != null) {
                    pulseAudioFeatures.setUSBReceiveVolume(pulseAudioPreferences.getUsbRcvVolume().getValue());
                }

                //USBTransmitVolume
                if (pulseAudioPreferences.getUsbXmtVolume() != null) {
                    pulseAudioFeatures.setUSBTransmitVolume(pulseAudioPreferences.getUsbXmtVolume().getValue());
                }

                //ICMAutoAnswer
                if (pulseAudioPreferences.getIcmAutoAnswer() != null) {
                    pulseAudioFeatures.setICMAutoAnswer(pulseAudioPreferences.getIcmAutoAnswer().getValue());
                }

                //ICMStartOnHfm
//	                if (pulseAudioPreferences.get != null && !pulseAudioFeaturesBean.getIcmStartOnHfm().isEmpty()) {
//	                    pulseAudioFeatures.setICMStartOnHfm(Boolean.valueOf(pulseAudioFeaturesBean.getIcmStartOnHfm()));
//	                }

                //DisplayNameNumber
                if (pulseAudioPreferences.getDisplayNameNumber() != null && pulseAudioPreferences.getDisplayNameNumber().getValue() != null) {
                    String displayNameNumber = pulseAudioPreferences.getDisplayNameNumber().getValue().value();
                    if (DisplayNameNumberTypes.fromValue(displayNameNumber) != null) {
                        pulseAudioFeatures.setDisplayNameNumber(DisplayNameNumberTypes.fromValue(displayNameNumber));
                    }
                }
                audioPreferences.setPulseAudioFeatures(pulseAudioFeatures);
            } else if (deviceType.equals("IQ_MAX_TOUCH")) {
                log.debug("deviceType is IQ_MAX_TOUCH in handler");

                IQMAXTouchAudioFeatures iqMaxTouchAudioFeatures = new IQMAXTouchAudioFeatures();

                IQMaxTouchAudioPreferences iqMaxTouchAudioPreferences = userPreferencesType.getIQMaxTouchAudioPreferences();


                iqMaxTouchAudioFeatures.setCallRinger(iqMaxTouchAudioPreferences.getCallRinger());
                iqMaxTouchAudioFeatures.setRingerVolume(iqMaxTouchAudioPreferences.getRingerVolume());
                iqMaxTouchAudioFeatures.setHandsetReceiveVolume(iqMaxTouchAudioPreferences.getHdstRxVolume());
                iqMaxTouchAudioFeatures.setHandsetTransmitVolume(iqMaxTouchAudioPreferences.getHdstTxVolume());
                iqMaxTouchAudioFeatures.setHandsetSideToneAttenuation(iqMaxTouchAudioPreferences.getHdstSideToneAtten());
                String hsReceiveNoiseReduceMode = iqMaxTouchAudioPreferences.getHdstRxNoiseReduceMode().value();
                if (NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode) != null) {
                    iqMaxTouchAudioFeatures.setHandsetReceiveNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode));
                }
                String hsTxNoiseReduceMode = iqMaxTouchAudioPreferences.getHdstTxNoiseReduceMode().value();
                if (NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode) != null) {
                    iqMaxTouchAudioFeatures.setHandsetTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode));
                }
                String hsRxEqualization = iqMaxTouchAudioPreferences.getHdstRxEqualization().value();
                if (ReceiverEqualizationTypes.fromValue(hsRxEqualization) != null) {
                    iqMaxTouchAudioFeatures.setHandsetReceiveEqualization(ReceiverEqualizationTypes.fromValue(hsRxEqualization));
                }
                iqMaxTouchAudioFeatures.setIsHandsetWideband(Boolean.FALSE);
                iqMaxTouchAudioFeatures.setSpeakerMasterRxVolume1(iqMaxTouchAudioPreferences.getSpkrMasterRxVolume());
                iqMaxTouchAudioFeatures.setSpeakerTransmitVolume(iqMaxTouchAudioPreferences.getSpkrTxVolume());
                String spkrTxNoiseReduceMode = iqMaxTouchAudioPreferences.getSpkrTxNoiseReduceMode().value();
                if (NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode) != null) {
                    iqMaxTouchAudioFeatures.setSpeakerTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode));
                }
                String spkrRxEqualization = iqMaxTouchAudioPreferences.getSpkrRxEqualization().value();
                if (ReceiverEqualizationTypes.fromValue(spkrRxEqualization) != null) {
                    iqMaxTouchAudioFeatures.setSpeakerReceiveEqualization(ReceiverEqualizationTypes.fromValue(spkrRxEqualization));
                }

                iqMaxTouchAudioFeatures.setSpeakerSummingMode(Boolean.FALSE);
                String hfmRxEqualization = iqMaxTouchAudioPreferences.getHdstRxEqualization().value();
                if (ReceiverEqualizationTypes.fromValue(hfmRxEqualization) != null) {
                    iqMaxTouchAudioFeatures.setHFMReceiveEqualization(ReceiverEqualizationTypes.fromValue(hfmRxEqualization));
                }
                iqMaxTouchAudioFeatures.setHFMReceiveVolume(iqMaxTouchAudioPreferences.getHfmRxVolume());
                String hfmTxAutoGainControlMode = iqMaxTouchAudioPreferences.getHfmTxAGCMode().value();
                if (NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode) != null) {
                    iqMaxTouchAudioFeatures.setHFMTransmitAutoGainControlMode(NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode));
                }
                String hfmTxNoiseReduceMode = iqMaxTouchAudioPreferences.getHfmTxNoiseReduceMode().value();
                if (NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode) != null) {
                    iqMaxTouchAudioFeatures.setHFMTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode));
                }
                iqMaxTouchAudioFeatures.setHFMTransmitVolume(iqMaxTouchAudioPreferences.getHfmTxVolume());
                iqMaxTouchAudioFeatures.setIPRecordOutputGain(iqMaxTouchAudioPreferences.getIPRecordOutputGain());
                iqMaxTouchAudioFeatures.setSpeakerIPRecordMicrophoneMixGain(iqMaxTouchAudioPreferences.getSpkrIPRecordMicMixGain());
                iqMaxTouchAudioFeatures.setHandsetIPRecordMicrophoneMixGain(iqMaxTouchAudioPreferences.getHdstIPRecordMicMixGain());
                iqMaxTouchAudioFeatures.setRecordMixProfiles(String.valueOf(recordingProfile));
                String recordProtocolValue = recordingProtocol;
                if (RecordingProtocolTypes.fromValue(recordProtocolValue) != null) {
                    iqMaxTouchAudioFeatures.setRecordProtocol(RecordingProtocolTypes.fromValue(recordProtocolValue));
                }
                iqMaxTouchAudioFeatures.setAcousticFeedbackReduction(Boolean.TRUE);

                String replayPlayBackOutputValue = iqMaxTouchAudioPreferences.getReplayPlaybackOutput() != null ? iqMaxTouchAudioPreferences.getReplayPlaybackOutput().value() : "Hfm";
                if (OutputModuleTypes.fromValue(replayPlayBackOutputValue) != null) {
                    iqMaxTouchAudioFeatures.setReplayPlaybackOutput(OutputModuleTypes.fromValue(replayPlayBackOutputValue));
                }
                iqMaxTouchAudioFeatures.setHighPriorityRing(iqMaxTouchAudioPreferences.getHighPriRing());
                iqMaxTouchAudioFeatures.setLowPriorityRing(iqMaxTouchAudioPreferences.getLowPriRing());
                iqMaxTouchAudioFeatures.setMasterReceiveVolume(iqMaxTouchAudioPreferences.getSpkrMasterRxVolume());
                iqMaxTouchAudioFeatures.setSpeakerSlideToLatchEnabled(Boolean.TRUE);
                iqMaxTouchAudioFeatures.setSpeakerSoloModeAttenuation(iqMaxTouchAudioPreferences.getSpkrIPRecordMicMixGain());

                audioPreferences.setIQMAXTouchAudioFeatures(iqMaxTouchAudioFeatures);
            }

            updateAudioPreferences.setUserAudioPreferences(audioPreferences);
            String updateUserAudioPrefsRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(updateAudioPreferences));
            log.debug(String.format("update user audio preferences:" + updateUserAudioPrefsRequestString));

            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            //after creating session, invoke httpsput and send the update user audio request
            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());
            String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.AUDIO_PREFERENCES.toString());
            log.debug("created resourceurl: " + resourceUrl);
            Response response = IPCProvHttpClient.httpsPut(resourceUrl, null, ipcAuthToken, apiVersion, updateAudioPreferences);

            log.debug("Update End User Audio Preferences Response: " + response);

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for UpdateEndUserAudioPreferences is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("UpdateEndUserAudioPreferences error response " + formattedResponseXML);
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
                responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                return responseDataBean;
            } else if (respEntity instanceof UpdateAudioPreferencesResponse) {
                UpdateAudioPreferencesResponse updateResponse = (UpdateAudioPreferencesResponse) respEntity;
                String updateRespString = UtilHelper.marshalJAXBObjectForResponse(updateResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(updateRespString);
                log.debug("UpdateEndUserAudioPreferences response " + formattedResponseXML);
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);
                responseDataBean.setReasonCode(updateResponse.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(updateResponse.getReason().getReasonDescription());
                return responseDataBean;
            }
            return null;
        } catch (Exception e) {
            log.error("Exception updating user audio preferences : ", e);
            return null;
        }
    }

}
