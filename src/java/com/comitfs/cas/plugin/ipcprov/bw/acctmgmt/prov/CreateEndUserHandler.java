package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.UserAttributes;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;

public class CreateEndUserHandler {

    public static final String INTERNAL_SERVER_ERROR = "Internal server error";
    public static final String REQ_FAILURE = "REQ_FAILURE";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(CreateEndUserHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    public CreateEndUserHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(UserAttributes userAttributes) {
        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            String apiVersion = ipcSite.getApiVersion();
            CreateUser createUser = new CreateUser();
            UserInformation userInformation = new UserInformation();

            if (userAttributes.getLoginName() != null && !userAttributes.getLoginName().isEmpty()) {
                userInformation.setLoginName(userAttributes.getLoginName());
            }
            if (userAttributes.getPassword() != null && !userAttributes.getPassword().isEmpty()) {
                userInformation.setPassword(userAttributes.getPassword());
            }
            if (userAttributes.getPasswordChangeMode() != null && !userAttributes.getPasswordChangeMode().isEmpty()) {
                userInformation.setPasswordChangeMode(Boolean.valueOf(userAttributes.getPasswordChangeMode()));
            }
            if (userAttributes.getDisplayDateFormat() != null && !userAttributes.getDisplayDateFormat().isEmpty() &&
                    !userAttributes.getDisplayDateFormat().equals("none")) {
                String dateDisplayFormat = userAttributes.getDisplayDateFormat();
                if (UserDateDisplayFormat.fromValue(dateDisplayFormat) != null) {
                    userInformation.setDateDisplayFormat(UserDateDisplayFormat.fromValue(dateDisplayFormat));
                }
            }
            if (userAttributes.getTimeDisplayFormat() != null && !userAttributes.getTimeDisplayFormat().isEmpty() &&
                    !userAttributes.getTimeDisplayFormat().equals("none")) {
                String timeDisplayFormat = userAttributes.getTimeDisplayFormat();
                if (UserTimeDisplayFormat.fromValue(timeDisplayFormat) != null) {
                    userInformation.setTimeDisplayFormat(UserTimeDisplayFormat.fromValue(timeDisplayFormat));
                }
            }
            if (userAttributes.getIsTemporary() != null && !userAttributes.getIsTemporary().isEmpty()) {
                userInformation.setIsTemporary(Boolean.valueOf(userAttributes.getIsTemporary()));
            }
            if (userAttributes.getAuthType() != null && !userAttributes.getAuthType().isEmpty()) {
                String authType = userAttributes.getAuthType();
                if (UserAuthType.fromValue(authType) != null) {
                    userInformation.setAuthType(UserAuthType.fromValue(authType));
                }
            }
            if (userAttributes.getFirstName() != null && !userAttributes.getFirstName().isEmpty()) {
                userInformation.setFirstName(userAttributes.getFirstName());
            }
            if (userAttributes.getLastName() != null && !userAttributes.getLastName().isEmpty()) {
                userInformation.setLastName(userAttributes.getLastName());
            }
            if (userAttributes.getLocale() != null && !userAttributes.getLocale().isEmpty()) {
                String locale = userAttributes.getLocale();
                if (PersonalContactLocale.fromValue(locale) != null) {
                    userInformation.setLocale(PersonalContactLocale.fromValue(locale));
                }
            }
            if (userAttributes.getTitle() != null && !userAttributes.getTitle().isEmpty()) {
                userInformation.setTitle(userAttributes.getTitle());
            }
            if (userAttributes.getEmail() != null && !userAttributes.getEmail().isEmpty()) {
                userInformation.setEmail(userAttributes.getEmail());
            }
            if (userAttributes.getLocationId() != null && !userAttributes.getLocationId().isEmpty()) {
                userInformation.setLocationId(Integer.valueOf(userAttributes.getLocationId()));
            }
            if (userAttributes.getAccountPolicyId() != null && !userAttributes.getAccountPolicyId().isEmpty()) {
                userInformation.setAccountPolicyId(Integer.valueOf(userAttributes.getAccountPolicyId()));
            }

            if (userAttributes.getAuthDomainId() != null && !userAttributes.getAuthDomainId().isEmpty()) {
                userInformation.setAuthDomainId(Integer.valueOf(userAttributes.getAuthDomainId()));
            }

            createUser.setUserInfo(userInformation);

            String createUserRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(createUser));
            log.debug(String.format("create end user  string:" + createUserRequestString));

            //TODO: acctmgmt usrname & pwd shd be retrieved from ipc site settings table based on the user Id passed as input

            //create session with the above retrieved acct mgmt usr/pwd from ipcsite table
            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            //after creating session, invoke httpsput
            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());

            String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.CREATE_USR.toString());
            log.debug("created resourceurl: " + resourceUrl);

            Response response = IPCProvHttpClient.httpsPost(resourceUrl,
                    httpAuth, ipcAuthToken, apiVersion, createUser);

            log.debug("Create User Response: " + response);

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for CreateUser is null.");
                return toErrorResponseDataBean(responseDataBean, "CreateEndUserAccount response is null");
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("Create User error response " + formattedResponseXML);
                responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
            } else if (respEntity instanceof CreateUserResponse) {
                CreateUserResponse createUserResponse = (CreateUserResponse) respEntity;
                String createUserRespString = UtilHelper.marshalJAXBObjectForResponse(createUserResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(createUserRespString);
                log.debug("CreateUser response " + formattedResponseXML);

                ReasonData reasonData = createUserResponse.getReason();
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);
                responseDataBean.setReasonCode(reasonData.getReasonCode().value());
                responseDataBean.setReasonDescription(reasonData.getReasonDescription());

            }
        } catch (Exception e) {
            log.debug("Exception in CreateEndUser handle(): ", e);
            return toErrorResponseDataBean(responseDataBean, INTERNAL_SERVER_ERROR);
        }
        return responseDataBean;
    }

    private ResponseDataBean toErrorResponseDataBean(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(errorField);
        return responseDataBean;
    }
}
