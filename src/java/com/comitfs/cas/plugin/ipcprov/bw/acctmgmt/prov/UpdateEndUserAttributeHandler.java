package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.UserAttributes;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;


public class UpdateEndUserAttributeHandler {
    public static final String REQ_FAILURE = "REQ_FAILURE";
    public static final String REQ_FAILURE_DESC = "Invalid input ";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(UpdateEndUserAttributeHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;
    private String displayDateFormat;
    private String timeDisplayFormat;

    public UpdateEndUserAttributeHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String casUserName, UserAttributes userAttributes) throws Exception {

        IPCUser ipcUser = IPCDataService.getInstance().getUserById(casUserName,ipcSite.getId());
        String apiVersion = ipcSite.getApiVersion();

        //for response
        ResponseDataBean responseDataBean = new ResponseDataBean();

        String userId = ipcUser.getUserId();
        Integer ipcId = ipcUser.getIPCId();
        log.debug("userId : " + userId + " ipcId: " + ipcId);

        UpdateUser updateUser = new UpdateUser();

        try {
            UpdateUserInformation updateUserAttribs = new UpdateUserInformation();
            if (ipcId != null) {
                updateUserAttribs.setUserId(BigInteger.valueOf(ipcId));
            } else if (userId != null) {
                updateUserAttribs.setLoginName(userId);
            }

            if (userAttributes.getLoginName() != null && !userAttributes.getLoginName().isEmpty()) {
                updateUserAttribs.setLoginName(userAttributes.getLoginName());
            }

            if (userAttributes.getFirstName() != null && !userAttributes.getFirstName().isEmpty()) {
                updateUserAttribs.setFirstName(userAttributes.getFirstName());
            }

            if (userAttributes.getLastName() != null && !userAttributes.getLastName().isEmpty()) {
                updateUserAttribs.setLastName(userAttributes.getLastName());

            }

            if (userAttributes.getTitle() != null && !userAttributes.getTitle().isEmpty()) {
                updateUserAttribs.setTitle(userAttributes.getTitle());
            }

            if (userAttributes.getEmail() != null && !userAttributes.getEmail().isEmpty()) {
                updateUserAttribs.setEmail(userAttributes.getEmail());
            }
            if (userAttributes.getLocale() != null && !userAttributes.getLocale().isEmpty() &&
                    !userAttributes.getLocale().equals("none")) {
                String locale = userAttributes.getLocale();
                if (PersonalContactLocale.fromValue(locale) == null) {
                    responseDataBean.setReasonCode(REQ_FAILURE);
                    responseDataBean.setReasonDescription(REQ_FAILURE_DESC + "Locale");
                    return responseDataBean;
                } else if (PersonalContactLocale.fromValue(userAttributes.getLocale()) != null) {
                    updateUserAttribs.setLocale(PersonalContactLocale.fromValue(userAttributes.getLocale()));
                }
            }

            if (userAttributes.getDisplayDateFormat() != null && !userAttributes.getDisplayDateFormat().isEmpty() &&
                    !userAttributes.getDisplayDateFormat().equals("none")) {
                displayDateFormat = userAttributes.getDisplayDateFormat();
                if (UserDateDisplayFormat.fromValue(displayDateFormat) == null) {
                    responseDataBean.setReasonCode(REQ_FAILURE);
                    responseDataBean.setReasonDescription(REQ_FAILURE_DESC + "DateDisplayFormat");
                    return responseDataBean;
                }
                if (UserDateDisplayFormat.fromValue(displayDateFormat) != null) {
                    updateUserAttribs.setDateDisplayFormat(UserDateDisplayFormat.fromValue(displayDateFormat));
                }
            }
            if (userAttributes.getTimeDisplayFormat() != null && !userAttributes.getTimeDisplayFormat().isEmpty() &&
                    !userAttributes.getTimeDisplayFormat().equals("none")) {
                timeDisplayFormat = userAttributes.getTimeDisplayFormat();
                if (UserTimeDisplayFormat.fromValue(timeDisplayFormat) == null) {
                    responseDataBean.setReasonCode(REQ_FAILURE);
                    responseDataBean.setReasonDescription(REQ_FAILURE_DESC + "TimeDisplayFormat");
                    return responseDataBean;
                }
                if (UserTimeDisplayFormat.fromValue(timeDisplayFormat) != null) {
                    updateUserAttribs.setTimeDisplayFormat(UserTimeDisplayFormat.fromValue(timeDisplayFormat));
                }
            }

            if (userAttributes.getAccountActive() != null && !userAttributes.getAccountActive().isEmpty()) {
                String accountActive = userAttributes.getAccountActive();
                log.debug("accountActive value in update user attributes handler: " + accountActive);
                if (accountActive.equalsIgnoreCase("True") || accountActive.equalsIgnoreCase("False")) {
                    log.debug("Account active status being set to : " + userAttributes.getAccountActive() + " for userId : " + casUserName);
                    updateUserAttribs.setAccountActive(Boolean.valueOf(userAttributes.getAccountActive()));
                }
            }
            updateUser.setUpdateUserInfo(updateUserAttribs);

            String updateUserAttribsRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(updateUser));
            log.debug(String.format("update user attribs:" + updateUserAttribsRequestString));


            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();
            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());

            String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.USR_ACCT_MGT.toString());
            log.debug("created resourceurl: " + resourceUrl);

            Response response = IPCProvHttpClient.httpsPut(resourceUrl, httpAuth, ipcAuthToken, apiVersion, updateUser);

            log.debug("Update End User Attributes Response: " + response);

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for UpdateEndUserAttributes is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("UpdateEndUserAttributes error response " + formattedResponseXML);
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
                responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                return responseDataBean;
            } else if (respEntity instanceof UpdateUserResponse) {
                UpdateUserResponse updateUserResponse = (UpdateUserResponse) respEntity;
                String updateUserRespString = UtilHelper.marshalJAXBObjectForResponse(updateUserResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(updateUserRespString);
                log.debug("UpdateEndUserAttributes response " + formattedResponseXML);
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);
                responseDataBean.setReasonCode(updateUserResponse.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(updateUserResponse.getReason().getReasonDescription());
                return responseDataBean;
            }
        } catch (Exception ex) {
            log.error("Failed to update user information - Exception Encountered - ", ex);
            return null;
        }
        return null;
    }
}
