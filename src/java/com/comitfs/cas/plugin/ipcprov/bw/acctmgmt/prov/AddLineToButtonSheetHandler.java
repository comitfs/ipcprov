package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.AddLineToButtonRestBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.List;

public class AddLineToButtonSheetHandler {
    public static final String REQ_FAILURE = "REQ_FAILURE";
    public static final String REQ_FAILURE_DESC = "Invalid input :";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(AddLineToButtonSheetHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    public AddLineToButtonSheetHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String casUserName, AddLineToButtonRestBean lineInfo) {

        try {
            log.debug("handling AddLineToButtonSheetHandler");

            IPCUser ipcUser = IPCDataService.getInstance().getUserById(casUserName, ipcSite.getId());

            //validate input and send error response if any invalid value is present in input
            ResponseDataBean reasonData = validateInputParams(lineInfo);

            if (reasonData == null) {
                reasonData = new ResponseDataBean();

                UpdateEndUserButton updateEndUserButton = new UpdateEndUserButton();

                //1.set useridentifier- userid/loginname
                UserIdentifier userIdentifier = new UserIdentifier();
                if (ipcUser.getIPCId() != null) {
                    userIdentifier.setUserId(BigInteger.valueOf(ipcUser.getIPCId()));
                } else if (ipcUser.getUserId() != null) {
                    userIdentifier.setLoginName(ipcUser.getUserId());
                }

                updateEndUserButton.setUserIdentifier(userIdentifier);

                //2. set buttoninfolist object(contains 1.buttonbasicinfo, 2.buttondetails)
                ButtonInfoList buttonInfoList = new ButtonInfoList();
                List<ButtonInformation> buttonsList = buttonInfoList.getButton();
                ButtonInformation buttonInformation = null;

                buttonInformation = addLineAndUpdateEndUserButton(lineInfo);
                if (buttonInformation != null) {
                    buttonsList.add(buttonInformation);
                }

                updateEndUserButton.setButtonList(buttonInfoList);

                ReasonData finalResponse = invokeUpdateButtonSettings(updateEndUserButton);
                reasonData.setReasonCode(finalResponse.getReasonCode().value());

                String finalReasonDesc = reasonData.getReasonCode().equals("REQ_SUCCESS") ? "Line Assignment Successful" : reasonData.getReasonCode();
                reasonData.setReasonDescription(finalReasonDesc);
                return reasonData;
            } else {
                log.debug("invalid input data, returning req_failure");
                return reasonData;
            }
        } catch (Exception e) {
            log.error("Error in handle() of AddLineToButtonSheetHandler: ", e);
        }
        return null;
    }

    public ResponseDataBean validateInputParams(AddLineToButtonRestBean lineInfo) {
        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            if (lineInfo.getButtonId() != null) {
                try {
                    Integer.parseInt(lineInfo.getButtonId());
                } catch (Exception e) {
                    log.error("Error in handle() of AddLineToButtonSheetHandler: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "ButtonId :" + lineInfo.getButtonId());
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "ButtonId is mandatory");
            }

            if (lineInfo.getButtonType() != null) {
                if (ButtonButtonType.fromValue(lineInfo.getButtonType()) == null) {
                    return sendReasonDataErrResponse(responseDataBean, "Button type");
                }
                if (lineInfo.getButtonType().equals("ICM") || lineInfo.getButtonType().equals("SpeedDial")) {
                    log.debug("Add line to unassignable Button types: ICM/SpeedDial");
                    return sendReasonDataErrResponse(responseDataBean, "Line Assignment cannot be completed.");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "ButtonType is mandatory");
            }

            if (lineInfo.getLineId() != null) {
                try {
                    Integer.parseInt(lineInfo.getLineId());
                } catch (Exception e) {
                    log.error("Error in handle() of AddLineToButtonSheetHandler, invalid line id : ", e);
                    return sendReasonDataErrResponse(responseDataBean, "LineId :" + lineInfo.getLineId());

                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "Line ID is mandatory");
            }

            if (lineInfo.getAppearance() != null) {
                try {
                    Integer.parseInt(lineInfo.getAppearance());
                } catch (Exception e) {
                    log.error("Error in handle() of AddLineToButtonSheetHandler, invalid appearance id : ", e);
                    return sendReasonDataErrResponse(responseDataBean, "Appearance :" + lineInfo.getAppearance());
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "Line Appearance is mandatory");
            }

            if (lineInfo.getResourceAOR() == null) {
                return sendReasonDataErrResponse(responseDataBean, "ResourceAOR is mandatory ");
            }
        } catch (Exception e) {
            log.error("Error while validating input parameters in AddLineToButtonSheetHandler: ", e);
        }
        return null;
    }

    public ResponseDataBean sendReasonDataErrResponse(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(REQ_FAILURE_DESC + errorField);
        return responseDataBean;
    }


    public ButtonInformation addLineAndUpdateEndUserButton(AddLineToButtonRestBean lineInfo) {

        log.debug("Going to set and invoke Update Resource Button");

        try {
            ButtonInformation buttonInformation = new ButtonInformation();

            buttonInformation.setButtonBasicInfo(setButtonBasicInformationForUpdate(lineInfo));

            ButtonDetails buttonDetails = new ButtonDetails();

            ResourceButtonInformation resourceButtonInformation = new ResourceButtonInformation();

            if (lineInfo.getButtonType().equalsIgnoreCase("InvalidButtonType")) {
                log.debug("input Button type is InvalidButtonType, set default button values for Resource button while adding Line");
                resourceButtonInformation.setIncomingActionRings(ButtonIncomingActionRings.REPEAT);
                resourceButtonInformation.setIncomingActionPriority(ButtonIncomingActionPriority.LOW);
                resourceButtonInformation.setIncomingActionFloat(ButtonIncomingActionFloat.NO_FLOAT);
                resourceButtonInformation.setDisplayIncomingCLI(ButtonIncomingActionCLI.CLI);
                resourceButtonInformation.setDisplayInCallHistory(true);
                resourceButtonInformation.setRingTone(0);
                resourceButtonInformation.setAutoSignal(false);
                resourceButtonInformation.setSpeedDialType(ButtonIcon.NONE);
            }

            ResourceAORInformation resourceAORInformation = new ResourceAORInformation();
            resourceAORInformation.setLineAppearance(Integer.valueOf(lineInfo.getAppearance()));
            resourceAORInformation.setResourceAORId(BigInteger.valueOf(Long.valueOf(lineInfo.getLineId())));

            resourceButtonInformation.setResourceAORInfo(resourceAORInformation);
            buttonDetails.setResourceButtonInfo(resourceButtonInformation);

            log.debug("done setting ResourceButtonInfo in ButtonDetails obj");
            buttonInformation.setButtonDetails(buttonDetails);
            return buttonInformation;
        } catch (Exception e) {
            log.error("Exception in setting Update Resource ButtonSettings: ", e);
            return null;
        }
    }

    private ButtonBasicInformation setButtonBasicInformationForUpdate(AddLineToButtonRestBean lineInfo) {
        log.debug("In setButtonBasicInformationForUpdate of AddLineToButtonSheetHandler");
        try {
            ButtonBasicInformation buttonBasicInformation = new ButtonBasicInformation();
            ButtonIdentifier buttonIdentifier = new ButtonIdentifier();
            buttonIdentifier.setButtonId(BigInteger.valueOf(Long.valueOf(lineInfo.getButtonId())));

            if (lineInfo.getButtonType().equals("InvalidButtonType")) {
                log.debug("setting buttonlabel when add line to invalidbuttontype: " + lineInfo.getResourceAOR());
                String defaultLabel = lineInfo.getResourceAOR() + "-" + lineInfo.getAppearance();
                buttonBasicInformation.setButtonLabel(defaultLabel);
            }

            buttonBasicInformation.setButtonIdentifier(buttonIdentifier);
            buttonBasicInformation.setButtonType(ButtonButtonType.RESOURCE);
            log.debug("done setting button id and button type as Resource to add a line to button");
            return buttonBasicInformation;
        } catch (Exception e) {
            log.error("Exception setting ButtonBasicInformation for update: ", e);
            return null;
        }
    }

    public ReasonData invokeUpdateButtonSettings(UpdateEndUserButton updateEndUserButtonFinalObject) {
        try {
            String updateButtonSettingsRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(updateEndUserButtonFinalObject));

            log.debug(String.format("add line to button - update button setting request string:\n" + updateButtonSettingsRequestString));


            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();

            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());
            String apiVersion = ipcSite.getApiVersion();

            String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.BUTTON_SETTINGS.toString());
            log.debug("created resourceurl: " + resourceUrl);

            Response response = IPCProvHttpClient.httpsPut(resourceUrl,
                    httpAuth, ipcAuthToken, apiVersion, updateEndUserButtonFinalObject);

            log.debug("Update Button Settings Response: " + response);
            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for UpdateButtonSettings is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("UpdateButtonSettings error response " + formattedResponseXML);
                return errorResp.getReason();
            } else if (respEntity instanceof UpdateEndUserButtonResponse) {
                UpdateEndUserButtonResponse updateButtonSettingsResponse = (UpdateEndUserButtonResponse) respEntity;
                String updateButtonSettingsRespString = UtilHelper.marshalJAXBObjectForResponse(updateButtonSettingsResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(updateButtonSettingsRespString);
                log.debug("UpdateButtonSettings response " + formattedResponseXML);

                //delete session
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);

                //return formattedResponseXML;
                return updateButtonSettingsResponse.getReason();
            }
        } catch (Exception e) {
            log.error("Exception Update Button Settings:  ", e);
            return null;
        }
        return null;
    }
}