package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;

import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.DeviceDataBean;
import com.comitfs.cas.plugin.ipcprov.beans.TurretInventoryRespBean;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.*;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import java.io.StringReader;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DeviceInventory {
    private final Logger log = Logger.getLogger(DeviceInventory.class);
    private final IPCSite ipcSite;
    private final String forLog;

    public DeviceInventory(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.forLog = String.format("[%s] ", ipcSite.getName());
    }

    public DeviceInfo getDeviceModel(int deviceId, String authToken) {
        String httpAuth = null;
        SessionManager sessionManager = null;
        String ipcAuthToken = authToken;
        try {
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            if (authToken == null) {
                WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
                httpAuth = IPCUtil.generateHttpAuthorization(webAPIUser.getUserId(), webAPIUser.getPassword());
                sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
                ipcAuthToken = sessionManager.getIpcProvAuthToken();
            }

            String apiVersion = ipcSite.getApiVersion();

            String encodedDeviceRoleParam = URLEncoder.encode("deviceId=" + deviceId, StandardCharsets.UTF_8.toString());
            String url = IPCRESTAPI.DEVICE_INVENTORY + "querystr=" + encodedDeviceRoleParam;

            String resourceUrl = IPCUtil.formURI(bwPoolConnection, url, null, ipcSite.isSecure());

            log.debug(forLog + "uri for device inventory info " + resourceUrl);

            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());
            String apiRes = response.readEntity(String.class);

            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error(forLog + "Response Entity for DeviceInventory  is null.");
                return null;
            }
            if (respEntity instanceof StandardErrorResponse) {
                log.error(forLog + "DeviceInventory error response " + apiRes);
                return null;
            }
            DeviceInventoryResponse deviceInventoryResp = (DeviceInventoryResponse) respEntity;

            DeviceDataList deviceDataList = deviceInventoryResp.getDeviceDataList();

            DeviceData deviceData = deviceDataList.getDeviceData().get(0);
            DeviceInfo deviceInfo = new DeviceInfo();
            if (deviceData != null) {
                deviceInfo.setDeviceModel(deviceData.getModel() != null ? deviceData.getModel().getValue() : "");
                deviceInfo.setDeviceIpAddr(deviceData.getIPAddress());
                deviceInfo.setDeviceId(deviceId);
            }
            return deviceInfo;
        } catch (Exception e) {
            log.error(forLog + "Error getting DeviceData ", e);
        } finally {
            if (sessionManager != null) {
                sessionManager.deleteSession(httpAuth, ipcAuthToken, ipcSite.getApiVersion());
            }
        }
        return null;
    }

    public TurretInventoryRespBean turretInventory(long pageNum) {
        String httpAuth = null;
        SessionManager sessionManager = null;
        String ipcAuthToken = null;

        try {
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            httpAuth = IPCUtil.generateHttpAuthorization(webAPIUser.getUserId(), webAPIUser.getPassword());
            sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String apiVersion = ipcSite.getApiVersion();

            String encodedDeviceRoleParam = URLEncoder.encode("deviceRole in [\"Turret\", \"Pulse\", \"UDA\", \"IQ_MAX_TOUCH\"]", StandardCharsets.UTF_8.toString());
            String url = IPCRESTAPI.DEVICE_INVENTORY + "pagenum=" + pageNum + "&querystr=" + encodedDeviceRoleParam;
            String inventoryUri = IPCUtil.formURI(bwPoolConnection, url, null, ipcSite.isSecure());

            log.debug(forLog + "uri for CCM inventory info " + inventoryUri);

            Response response = IPCProvHttpClient.invokeHTTPGET(inventoryUri, ipcAuthToken, apiVersion, ipcSite.isSecure());
            String apiRes = response.readEntity(String.class);

            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));
            if (respEntity == null) {
                log.error(forLog + "Response Entity for IPC turret inventory  is null");
                return new TurretInventoryRespBean(-1, "Internal error response", 0, 0, null);
            }

            if (respEntity instanceof StandardErrorResponse) {
                log.error(forLog + "Unigy turret inventory error response " + apiRes);
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;

                return new TurretInventoryRespBean(0, errorResp.getReason().getReasonDescription(), 0, 0, null);
            }

            if (respEntity instanceof DeviceInventoryResponse) {
                DeviceInventoryResponse deviceInventoryResp = (DeviceInventoryResponse) respEntity;
                Integer numPagesAvailable = deviceInventoryResp.getNumPagesAvailable();

                if (null == numPagesAvailable) {
                    return new TurretInventoryRespBean(0, deviceInventoryResp.getReason().getReasonDescription(), 0, 0, null);
                }

                final Integer respPageNum = deviceInventoryResp.getPageNum();

                DeviceDataList deviceDataList = deviceInventoryResp.getDeviceDataList();

                ArrayList<DeviceData> deviceDataArrayList = (ArrayList<DeviceData>) deviceDataList.getDeviceData();

                log.debug(forLog + "turret device list retrieved: " + deviceDataArrayList.size());
                final Set<DeviceDataBean> dataBeanSet = toDeviceDataBean(deviceDataArrayList);
                return new TurretInventoryRespBean(1, "Success", respPageNum, numPagesAvailable, dataBeanSet);
            }
        } catch (Exception e) {
            log.error("error retrieving turret inventory", e);
        } finally {
            if (sessionManager != null) {
                sessionManager.deleteSession(httpAuth, ipcAuthToken, ipcSite.getApiVersion());
            }
        }

        return new TurretInventoryRespBean(-1, "Internal server error", 0, 0, null);
    }

    private Set<DeviceDataBean> toDeviceDataBean(List<DeviceData> deviceDataList) {
        Set<DeviceDataBean> dataBeanSet = new HashSet<>();
        for (DeviceData deviceData : deviceDataList) {
            String mac = deviceData.getDeviceUUID();
            String model = deviceData.getModel().getValue();
            String ip = deviceData.getIPAddress();
            int zoneId = deviceData.getParentZoneId();
            String zoneName = deviceData.getParentZoneName();
            String ccmVIP = deviceData.getCcmVIP();

            String loggedOnTraderName = null;
            String loggedOnTraderState = null;

            String role = deviceData.getDeviceRole();
            if (role.equalsIgnoreCase("UDA")) {
                loggedOnTraderName = deviceData.getDeviceUDA().getLoggedOnTraderName();
                loggedOnTraderState = deviceData.getDeviceUDA().getLoggedOnTraderState();
            } else if (role.equalsIgnoreCase("Turret")) {
                loggedOnTraderName = deviceData.getDeviceTurret().getLoggedOnTraderName();
                loggedOnTraderState = deviceData.getDeviceTurret().getLoggedOnTraderState();
            } else if (role.equalsIgnoreCase("IQ_MAX_TOUCH")) {
                loggedOnTraderName = deviceData.getDeviceIQMaxTouch().getLoggedOnTraderName();
                loggedOnTraderState = deviceData.getDeviceIQMaxTouch().getLoggedOnTraderState();
            }

            String floor = "";
            String turretSiteCode = "";
            String location = "";
            String timezone = "";
            JAXBElement<DunkinLocation> dunkinLocation = deviceData.getDunkinLocation();
            if (null != dunkinLocation && null != dunkinLocation.getValue()) {
                floor = dunkinLocation.getValue().getFloor().getValue();
                turretSiteCode = dunkinLocation.getValue().getIpcSiteCode();
                location = dunkinLocation.getValue().getName();
                timezone = dunkinLocation.getValue().getTimeZone().getValue();
            }
            String deployed = deviceData.getDeployedDate().getValue().toString();
            DeviceDataBean ddb = new DeviceDataBean(mac, model, role, ip, loggedOnTraderName, loggedOnTraderState, zoneId, zoneName, ccmVIP, floor, turretSiteCode, location, timezone, deployed);
            dataBeanSet.add(ddb);
        }
        return dataBeanSet;
    }

}