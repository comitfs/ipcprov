package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.*;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;

public class UpdateUserAudioPreferencesHandler {

    public static final String REQ_FAILURE = "REQ_FAILURE";
    public static final String REQ_FAILURE_DESC = "Invalid input :";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(UpdateUserAudioPreferencesHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    private String deviceType;

    public UpdateUserAudioPreferencesHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String casUserName, UserAudioPreferencesBean userAudioPreferencesBean) {
        try {
            log.debug("handling UpdateUserAudioPreferencesHandler");

            IPCUser ipcUser = IPCDataService.getInstance().getUserById(casUserName,ipcSite.getId());
            String apiVersion = ipcSite.getApiVersion();

            ResponseDataBean responseDataBean = validateAudioPreferencesInputData(userAudioPreferencesBean);
            if (responseDataBean != null && responseDataBean.getReasonCode().equals(REQ_FAILURE)) {
                return responseDataBean;
            }

            responseDataBean = new ResponseDataBean();
            UpdateAudioPreferences updateAudioPreferences = new UpdateAudioPreferences();
            UserIdentifier userIdentifier = new UserIdentifier();
            //set the ipcuser's unigue id to be updated with attributes
            if (ipcUser.getIPCId() != null) {
                userIdentifier.setUserId(BigInteger.valueOf(ipcUser.getIPCId()));
            } else if (ipcUser.getUserId() != null) {
                userIdentifier.setLoginName(ipcUser.getUserId());
            }

            updateAudioPreferences.setUserIdentifier(userIdentifier);
            if (StringUtils.isNotEmpty(userAudioPreferencesBean.getDeviceType())) {
                String deviceType = userAudioPreferencesBean.getDeviceType();
                if (UserAudioDeviceType.fromValue(deviceType) != null) {
                    updateAudioPreferences.setDeviceType(UserAudioDeviceType.fromValue(deviceType));
                }
            }

            UserAudioPreferences audioPreferences = new UserAudioPreferences();
            this.deviceType = userAudioPreferencesBean.getDeviceType();
            if (deviceType == null) {
                responseDataBean.setReasonCode(REQ_FAILURE);
                responseDataBean.setReasonDescription("DeviceType is mandatory");
                return responseDataBean;
            }

            if (deviceType.equals("Turret")) {
                TurretAudioFeatures turretAudioFeatures = new TurretAudioFeatures();
                GeneralAudioPreferencesBean generalAudioPreferencesBean = userAudioPreferencesBean.getGeneralAudioPreferences();

                turretAudioFeatures.setCallRinger(Integer.parseInt(generalAudioPreferencesBean.getCallRinger()));
                turretAudioFeatures.setRingerVolume(Integer.parseInt(generalAudioPreferencesBean.getRingerVolume()));
                turretAudioFeatures.setHandsetReceiveVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsetReceiveVolume()));
                turretAudioFeatures.setHandsetTransmitVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsetTransmitVolume()));
                turretAudioFeatures.setHandsetSideToneAttenuation(Integer.parseInt(generalAudioPreferencesBean.getHandsetSideToneAttenuation()));
                String hsReceiveNoiseReduceMode = generalAudioPreferencesBean.getHandsetRxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode) != null) {
                    turretAudioFeatures.setHandsetReceiveNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode));
                }
                String hsTxNoiseReduceMode = generalAudioPreferencesBean.getHandsetTxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode) != null) {
                    turretAudioFeatures.setHandsetTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode));
                }
                String hsRxEqualization = generalAudioPreferencesBean.getHandsetRxEqualization();
                if (ReceiverEqualizationTypes.fromValue(hsRxEqualization) != null) {
                    turretAudioFeatures.setHandsetReceiveEqualization(ReceiverEqualizationTypes.fromValue(hsRxEqualization));
                }
                turretAudioFeatures.setIsHandsetWideband(Boolean.valueOf(generalAudioPreferencesBean.getIsHSWideBand()));
                turretAudioFeatures.setSpeakerMasterRxVolume1(Integer.parseInt(generalAudioPreferencesBean.getSpeakerMasterRxVolume1()));
                turretAudioFeatures.setSpeakerTransmitVolume(Integer.parseInt(generalAudioPreferencesBean.getSpeakerTransmitVolume()));
                String spkrTxNoiseReduceMode = generalAudioPreferencesBean.getSpeakerTxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode) != null) {
                    turretAudioFeatures.setSpeakerTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode));
                }
                String spkrRxEqualization = generalAudioPreferencesBean.getSpeakerRxEqualization();
                if (ReceiverEqualizationTypes.fromValue(spkrRxEqualization) != null) {
                    turretAudioFeatures.setSpeakerReceiveEqualization(ReceiverEqualizationTypes.fromValue(spkrRxEqualization));
                }

                turretAudioFeatures.setSpeakerSummingMode(Boolean.valueOf(generalAudioPreferencesBean.getSpeakerSummingMode()));
                String hfmRxEqualization = generalAudioPreferencesBean.getHandsFreeRxEqualization();
                if (ReceiverEqualizationTypes.fromValue(hfmRxEqualization) != null) {
                    turretAudioFeatures.setHFMReceiveEqualization(ReceiverEqualizationTypes.fromValue(hfmRxEqualization));
                }
                turretAudioFeatures.setHFMReceiveVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsFreeMRxVolume()));
                String hfmTxAutoGainControlMode = generalAudioPreferencesBean.getHfmTxAutoGainControlMode();
                if (NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode) != null) {
                    turretAudioFeatures.setHFMTransmitAutoGainControlMode(NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode));
                }
                String hfmTxNoiseReduceMode = generalAudioPreferencesBean.getHfmTxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode) != null) {
                    turretAudioFeatures.setHFMTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode));
                }
                turretAudioFeatures.setHFMTransmitVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsFreeMTransmitVolume()));
                turretAudioFeatures.setIPRecordOutputGain(Integer.parseInt(generalAudioPreferencesBean.getIpRecordOutputGain()));
                turretAudioFeatures.setSpeakerIPRecordMicrophoneMixGain(Integer.parseInt(generalAudioPreferencesBean.getSpeakerIPRecordMicMixGain()));
                turretAudioFeatures.setHandsetIPRecordMicrophoneMixGain(Integer.parseInt(generalAudioPreferencesBean.getHandsetIPRecordMicMixGain()));
                turretAudioFeatures.setRecordMixProfiles(generalAudioPreferencesBean.getRecordMixProfiles());
                String recordProtocolValue = generalAudioPreferencesBean.getRecordProtocol();
                if (RecordingProtocolTypes.fromValue(recordProtocolValue) != null) {
                    turretAudioFeatures.setRecordProtocol(RecordingProtocolTypes.fromValue(recordProtocolValue));
                }
                turretAudioFeatures.setAcousticFeedbackReduction(Boolean.valueOf(generalAudioPreferencesBean.getAccousticFeedbackReduction()));
                String replayPlayBackOutputValue = generalAudioPreferencesBean.getReplayPlaybackOutput();
                if (OutputModuleTypes.fromValue(replayPlayBackOutputValue) != null) {
                    turretAudioFeatures.setReplayPlaybackOutput(OutputModuleTypes.fromValue(replayPlayBackOutputValue));
                }

                TurretAudioFeaturesBean turretAudioFeaturesBean = userAudioPreferencesBean.getTurretAudioFeatures();
                turretAudioFeatures.setHighPriorityRing(Integer.parseInt(turretAudioFeaturesBean.getHighPriorityRing()));
                turretAudioFeatures.setLowPriorityRing(Integer.parseInt(turretAudioFeaturesBean.getLowPriorityRing()));
                turretAudioFeatures.setSpeakerMasterRxVolume2(Integer.parseInt(turretAudioFeaturesBean.getSpeakerMasterRxVolume2()));
                turretAudioFeatures.setSpeakerMasterRxVolume3(Integer.parseInt(turretAudioFeaturesBean.getSpeakerMasterRxVolume3()));
                String lineInMixerOutput = turretAudioFeaturesBean.getLineInMixerOutput();
                if (OutputModuleTypes.fromValue(lineInMixerOutput) != null) {
                    turretAudioFeatures.setLineInMixerOutput(OutputModuleTypes.fromValue(lineInMixerOutput));
                }
                audioPreferences.setTurretAudioFeatures(turretAudioFeatures);
            } else if (deviceType.equals("Pulse")) {
                PulseAudioFeatures pulseAudioFeatures = new PulseAudioFeatures();

                GeneralAudioPreferencesBean generalAudioPreferencesBean = userAudioPreferencesBean.getGeneralAudioPreferences();

                pulseAudioFeatures.setCallRinger(Integer.parseInt(generalAudioPreferencesBean.getCallRinger()));

                pulseAudioFeatures.setRingerVolume(Integer.parseInt(generalAudioPreferencesBean.getRingerVolume()));

                pulseAudioFeatures.setHandsetReceiveVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsetReceiveVolume()));

                pulseAudioFeatures.setHandsetTransmitVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsetTransmitVolume()));

                pulseAudioFeatures.setHandsetSideToneAttenuation(Integer.parseInt(generalAudioPreferencesBean.getHandsetSideToneAttenuation()));

                String hsReceiveNoiseReduceMode = generalAudioPreferencesBean.getHandsetRxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode) != null) {
                    pulseAudioFeatures.setHandsetReceiveNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode));
                }

                String hsTxNoiseReduceMode = generalAudioPreferencesBean.getHandsetTxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode) != null) {
                    pulseAudioFeatures.setHandsetTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode));
                }

                String hsRxEqualization = generalAudioPreferencesBean.getHandsetRxEqualization();
                if (ReceiverEqualizationTypes.fromValue(hsRxEqualization) != null) {
                    pulseAudioFeatures.setHandsetReceiveEqualization(ReceiverEqualizationTypes.fromValue(hsRxEqualization));
                }

                pulseAudioFeatures.setIsHandsetWideband(Boolean.valueOf(generalAudioPreferencesBean.getIsHSWideBand()));

                pulseAudioFeatures.setSpeakerMasterRxVolume1(Integer.parseInt(generalAudioPreferencesBean.getSpeakerMasterRxVolume1()));

                pulseAudioFeatures.setSpeakerTransmitVolume(Integer.parseInt(generalAudioPreferencesBean.getSpeakerTransmitVolume()));

                String spkrTxNoiseReduceMode = generalAudioPreferencesBean.getSpeakerTxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode) != null) {
                    pulseAudioFeatures.setSpeakerTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode));
                }

                String spkrRxEqualization = generalAudioPreferencesBean.getSpeakerRxEqualization();
                if (ReceiverEqualizationTypes.fromValue(spkrRxEqualization) != null) {
                    pulseAudioFeatures.setSpeakerReceiveEqualization(ReceiverEqualizationTypes.fromValue(spkrRxEqualization));
                }

                pulseAudioFeatures.setSpeakerSummingMode(Boolean.valueOf(generalAudioPreferencesBean.getSpeakerSummingMode()));

                String hfmRxEqualization = generalAudioPreferencesBean.getHandsFreeRxEqualization();
                if (ReceiverEqualizationTypes.fromValue(hfmRxEqualization) != null) {
                    pulseAudioFeatures.setHFMReceiveEqualization(ReceiverEqualizationTypes.fromValue(hfmRxEqualization));
                }

                pulseAudioFeatures.setHFMReceiveVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsFreeMRxVolume()));

                String hfmTxAutoGainControlMode = generalAudioPreferencesBean.getHfmTxAutoGainControlMode();
                if (NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode) != null) {
                    pulseAudioFeatures.setHFMTransmitAutoGainControlMode(NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode));
                }


                String hfmTxNoiseReduceMode = generalAudioPreferencesBean.getHfmTxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode) != null) {
                    pulseAudioFeatures.setHFMTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode));
                }

                pulseAudioFeatures.setHFMTransmitVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsFreeMTransmitVolume()));

                pulseAudioFeatures.setIPRecordOutputGain(Integer.parseInt(generalAudioPreferencesBean.getIpRecordOutputGain()));

                pulseAudioFeatures.setSpeakerIPRecordMicrophoneMixGain(Integer.parseInt(generalAudioPreferencesBean.getSpeakerIPRecordMicMixGain()));

                pulseAudioFeatures.setHandsetIPRecordMicrophoneMixGain(Integer.parseInt(generalAudioPreferencesBean.getHandsetIPRecordMicMixGain()));

                pulseAudioFeatures.setRecordMixProfiles(generalAudioPreferencesBean.getRecordMixProfiles());

                String recordProtocolValue = generalAudioPreferencesBean.getRecordProtocol();
                if (RecordingProtocolTypes.fromValue(recordProtocolValue) != null) {
                    pulseAudioFeatures.setRecordProtocol(RecordingProtocolTypes.fromValue(recordProtocolValue));
                }

                pulseAudioFeatures.setAcousticFeedbackReduction(Boolean.valueOf(generalAudioPreferencesBean.getAccousticFeedbackReduction()));
                String replayPlayBackOutputValue = generalAudioPreferencesBean.getReplayPlaybackOutput();
                if (OutputModuleTypes.fromValue(replayPlayBackOutputValue) != null) {
                    pulseAudioFeatures.setReplayPlaybackOutput(OutputModuleTypes.fromValue(replayPlayBackOutputValue));
                }

                PulseAudioFeaturesBean pulseAudioFeaturesBean = userAudioPreferencesBean.getPulseAudioFeatures();
                if (pulseAudioFeaturesBean.getSpeakerLatching() != null && !pulseAudioFeaturesBean.getSpeakerLatching().isEmpty()) {
                    String speakerLatching = pulseAudioFeaturesBean.getSpeakerLatching();
                    if (SpeakerLatchingTypes.fromValue(speakerLatching) != null) {
                        pulseAudioFeatures.setSpeakerLatching(SpeakerLatchingTypes.fromValue(speakerLatching));
                    }
                }

                //HFMTransitionDevice
                if (pulseAudioFeaturesBean.getHfmTransitionDevice() != null && !pulseAudioFeaturesBean.getHfmTransitionDevice().isEmpty()) {
                    String hfmTransitionDevice = pulseAudioFeaturesBean.getHfmTransitionDevice();
                    if (HFMTransitionDeviceTypes.fromValue(hfmTransitionDevice) != null) {
                        pulseAudioFeatures.setHFMTransitionDevice(HFMTransitionDeviceTypes.fromValue(hfmTransitionDevice));
                    }
                }

                //SpeakerAudioOutput
                if (pulseAudioFeaturesBean.getSpeakerAudioOutput() != null && !pulseAudioFeaturesBean.getSpeakerAudioOutput().isEmpty()) {
                    String speakerAudioOutput = pulseAudioFeaturesBean.getSpeakerAudioOutput();
                    if (SpeakerAudioOutputTypes.fromValue(speakerAudioOutput) != null) {
                        pulseAudioFeatures.setSpeakerAudioOutput(SpeakerAudioOutputTypes.fromValue(speakerAudioOutput));
                    }
                }

                //USBReceiveVolume
                if (pulseAudioFeaturesBean.getUsbReceiveVolume() != null && !pulseAudioFeaturesBean.getUsbReceiveVolume().isEmpty()) {
                    pulseAudioFeatures.setUSBReceiveVolume(Integer.parseInt(pulseAudioFeaturesBean.getUsbReceiveVolume()));
                }

                //USBTransmitVolume
                if (pulseAudioFeaturesBean.getUsbTransmitVolume() != null && !pulseAudioFeaturesBean.getUsbTransmitVolume().isEmpty()) {
                    pulseAudioFeatures.setUSBTransmitVolume(Integer.parseInt(pulseAudioFeaturesBean.getUsbTransmitVolume()));
                }

                //ICMAutoAnswer
                if (pulseAudioFeaturesBean.getIcmAutoAnswer() != null && !pulseAudioFeaturesBean.getIcmAutoAnswer().isEmpty()) {
                    pulseAudioFeatures.setICMAutoAnswer(Boolean.valueOf(pulseAudioFeaturesBean.getIcmAutoAnswer()));
                }

                //ICMStartOnHfm
                if (pulseAudioFeaturesBean.getIcmStartOnHfm() != null && !pulseAudioFeaturesBean.getIcmStartOnHfm().isEmpty()) {
                    pulseAudioFeatures.setICMStartOnHfm(Boolean.valueOf(pulseAudioFeaturesBean.getIcmStartOnHfm()));
                }

                //DisplayNameNumber
                if (pulseAudioFeaturesBean.getDisplayNameNumber() != null && !pulseAudioFeaturesBean.getDisplayNameNumber().isEmpty()) {
                    String displayNameNumber = pulseAudioFeaturesBean.getDisplayNameNumber();
                    if (DisplayNameNumberTypes.fromValue(displayNameNumber) != null) {
                        pulseAudioFeatures.setDisplayNameNumber(DisplayNameNumberTypes.fromValue(displayNameNumber));
                    }
                }
                audioPreferences.setPulseAudioFeatures(pulseAudioFeatures);
            } else if (deviceType.equals("IQ_MAX_TOUCH")) {
                log.debug("deviceType is IQ_MAX_TOUCH in handler");

                IQMAXTouchAudioFeatures iqMaxTouchAudioFeatures = new IQMAXTouchAudioFeatures();

                GeneralAudioPreferencesBean generalAudioPreferencesBean = userAudioPreferencesBean.getGeneralAudioPreferences();


                iqMaxTouchAudioFeatures.setCallRinger(Integer.parseInt(generalAudioPreferencesBean.getCallRinger()));
                iqMaxTouchAudioFeatures.setRingerVolume(Integer.parseInt(generalAudioPreferencesBean.getRingerVolume()));
                iqMaxTouchAudioFeatures.setHandsetReceiveVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsetReceiveVolume()));
                iqMaxTouchAudioFeatures.setHandsetTransmitVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsetTransmitVolume()));
                iqMaxTouchAudioFeatures.setHandsetSideToneAttenuation(Integer.parseInt(generalAudioPreferencesBean.getHandsetSideToneAttenuation()));
                String hsReceiveNoiseReduceMode = generalAudioPreferencesBean.getHandsetRxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode) != null) {
                    iqMaxTouchAudioFeatures.setHandsetReceiveNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsReceiveNoiseReduceMode));
                }
                String hsTxNoiseReduceMode = generalAudioPreferencesBean.getHandsetTxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode) != null) {
                    iqMaxTouchAudioFeatures.setHandsetTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hsTxNoiseReduceMode));
                }
                String hsRxEqualization = generalAudioPreferencesBean.getHandsetRxEqualization();
                if (ReceiverEqualizationTypes.fromValue(hsRxEqualization) != null) {
                    iqMaxTouchAudioFeatures.setHandsetReceiveEqualization(ReceiverEqualizationTypes.fromValue(hsRxEqualization));
                }
                iqMaxTouchAudioFeatures.setIsHandsetWideband(Boolean.valueOf(generalAudioPreferencesBean.getIsHSWideBand()));
                iqMaxTouchAudioFeatures.setSpeakerMasterRxVolume1(Integer.parseInt(generalAudioPreferencesBean.getSpeakerMasterRxVolume1()));
                iqMaxTouchAudioFeatures.setSpeakerTransmitVolume(Integer.parseInt(generalAudioPreferencesBean.getSpeakerTransmitVolume()));
                String spkrTxNoiseReduceMode = generalAudioPreferencesBean.getSpeakerTxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode) != null) {
                    iqMaxTouchAudioFeatures.setSpeakerTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(spkrTxNoiseReduceMode));
                }
                String spkrRxEqualization = generalAudioPreferencesBean.getSpeakerRxEqualization();
                if (ReceiverEqualizationTypes.fromValue(spkrRxEqualization) != null) {
                    iqMaxTouchAudioFeatures.setSpeakerReceiveEqualization(ReceiverEqualizationTypes.fromValue(spkrRxEqualization));
                }

                iqMaxTouchAudioFeatures.setSpeakerSummingMode(Boolean.valueOf(generalAudioPreferencesBean.getSpeakerSummingMode()));
                String hfmRxEqualization = generalAudioPreferencesBean.getHandsFreeRxEqualization();
                if (ReceiverEqualizationTypes.fromValue(hfmRxEqualization) != null) {
                    iqMaxTouchAudioFeatures.setHFMReceiveEqualization(ReceiverEqualizationTypes.fromValue(hfmRxEqualization));
                }
                iqMaxTouchAudioFeatures.setHFMReceiveVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsFreeMRxVolume()));
                String hfmTxAutoGainControlMode = generalAudioPreferencesBean.getHfmTxAutoGainControlMode();
                if (NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode) != null) {
                    iqMaxTouchAudioFeatures.setHFMTransmitAutoGainControlMode(NoiseReduceModeTypes.fromValue(hfmTxAutoGainControlMode));
                }
                String hfmTxNoiseReduceMode = generalAudioPreferencesBean.getHfmTxNoiseReduceMode();
                if (NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode) != null) {
                    iqMaxTouchAudioFeatures.setHFMTransmitNoiseReduceMode(NoiseReduceModeTypes.fromValue(hfmTxNoiseReduceMode));
                }
                iqMaxTouchAudioFeatures.setHFMTransmitVolume(Integer.parseInt(generalAudioPreferencesBean.getHandsFreeMTransmitVolume()));
                iqMaxTouchAudioFeatures.setIPRecordOutputGain(Integer.parseInt(generalAudioPreferencesBean.getIpRecordOutputGain()));
                iqMaxTouchAudioFeatures.setSpeakerIPRecordMicrophoneMixGain(Integer.parseInt(generalAudioPreferencesBean.getSpeakerIPRecordMicMixGain()));
                iqMaxTouchAudioFeatures.setHandsetIPRecordMicrophoneMixGain(Integer.parseInt(generalAudioPreferencesBean.getHandsetIPRecordMicMixGain()));
                iqMaxTouchAudioFeatures.setRecordMixProfiles(generalAudioPreferencesBean.getRecordMixProfiles());
                String recordProtocolValue = generalAudioPreferencesBean.getRecordProtocol();
                if (RecordingProtocolTypes.fromValue(recordProtocolValue) != null) {
                    iqMaxTouchAudioFeatures.setRecordProtocol(RecordingProtocolTypes.fromValue(recordProtocolValue));
                }
                iqMaxTouchAudioFeatures.setAcousticFeedbackReduction(Boolean.valueOf(generalAudioPreferencesBean.getAccousticFeedbackReduction()));

                String replayPlayBackOutputValue = generalAudioPreferencesBean.getReplayPlaybackOutput();
                if (OutputModuleTypes.fromValue(replayPlayBackOutputValue) != null) {
                    iqMaxTouchAudioFeatures.setReplayPlaybackOutput(OutputModuleTypes.fromValue(replayPlayBackOutputValue));
                }

                IQMAXTouchAudioFeaturesBean iqMAXTouchAudioFeaturesBean = userAudioPreferencesBean.getIqMAXTouchAudioFeatures();

                if (iqMAXTouchAudioFeaturesBean.getHighPriorityRing() != null && !iqMAXTouchAudioFeaturesBean.getHighPriorityRing().isEmpty()) {
                    iqMaxTouchAudioFeatures.setHighPriorityRing(Integer.parseInt(iqMAXTouchAudioFeaturesBean.getHighPriorityRing()));
                }
                if (iqMAXTouchAudioFeaturesBean.getLowPriorityRing() != null && !iqMAXTouchAudioFeaturesBean.getLowPriorityRing().isEmpty()) {
                    iqMaxTouchAudioFeatures.setLowPriorityRing(Integer.parseInt(iqMAXTouchAudioFeaturesBean.getLowPriorityRing()));
                }
                if (iqMAXTouchAudioFeaturesBean.getMasterReceiveVolume() != null && !iqMAXTouchAudioFeaturesBean.getMasterReceiveVolume().isEmpty()) {
                    iqMaxTouchAudioFeatures.setMasterReceiveVolume(Integer.parseInt(iqMAXTouchAudioFeaturesBean.getMasterReceiveVolume()));
                }
                if (iqMAXTouchAudioFeaturesBean.getSpeakerSlideToLatchEnabled() != null && !iqMAXTouchAudioFeaturesBean.getSpeakerSlideToLatchEnabled().isEmpty()) {
                    iqMaxTouchAudioFeatures.setSpeakerSlideToLatchEnabled(Boolean.valueOf(iqMAXTouchAudioFeaturesBean.getSpeakerSlideToLatchEnabled()));
                }
                if (iqMAXTouchAudioFeaturesBean.getSpeakerSoloModeAttenuation() != null && !iqMAXTouchAudioFeaturesBean.getSpeakerSoloModeAttenuation().isEmpty()) {
                    iqMaxTouchAudioFeatures.setSpeakerSoloModeAttenuation(Integer.parseInt(iqMAXTouchAudioFeaturesBean.getSpeakerSoloModeAttenuation()));
                }

                audioPreferences.setIQMAXTouchAudioFeatures(iqMaxTouchAudioFeatures);
            }

            updateAudioPreferences.setUserAudioPreferences(audioPreferences);
            String updateUserAudioPrefsRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(updateAudioPreferences));
            log.debug(String.format("update user audio preferences:" + updateUserAudioPrefsRequestString));

            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            //after creating session, invoke httpsput and send the update user audio request
            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());
            String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.AUDIO_PREFERENCES.toString());
            log.debug("created resourceurl: " + resourceUrl);
            Response response = IPCProvHttpClient.httpsPut(resourceUrl, null, ipcAuthToken, apiVersion, updateAudioPreferences);

            log.debug("Update End User Audio Preferences Response: " + response);

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for UpdateEndUserAudioPreferences is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("UpdateEndUserAudioPreferences error response " + formattedResponseXML);
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
                responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                return responseDataBean;
            } else if (respEntity instanceof UpdateAudioPreferencesResponse) {
                UpdateAudioPreferencesResponse updateResponse = (UpdateAudioPreferencesResponse) respEntity;
                String updateRespString = UtilHelper.marshalJAXBObjectForResponse(updateResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(updateRespString);
                log.debug("UpdateEndUserAudioPreferences response " + formattedResponseXML);
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);
                responseDataBean.setReasonCode(updateResponse.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(updateResponse.getReason().getReasonDescription());
                return responseDataBean;
            }
            return null;
        } catch (Exception e) {
            log.error("Exception updating user audio preferences : ", e);
            return null;
        }
    }

    public ResponseDataBean validateAudioPreferencesInputData(UserAudioPreferencesBean userAudioPreferencesBean) {
        ResponseDataBean responseDataBean = new ResponseDataBean();

        deviceType = userAudioPreferencesBean.getDeviceType();

        if (StringUtils.isNotEmpty(deviceType)) {
            if (UserAudioDeviceType.fromValue(deviceType) == null) {
                return sendReasonDataErrResponse(responseDataBean, " Device Type");
            }
        } else if (deviceType == null || deviceType.isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Device Type is mandatory");
        }

        GeneralAudioPreferencesBean generalAudioPreferencesBean = userAudioPreferencesBean.getGeneralAudioPreferences();
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getCallRinger())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getCallRinger());
            } catch (Exception e) {
                log.error("Invalid ringtone: ", e);
                return sendReasonDataErrResponse(responseDataBean, "Call Ringer");
            }
        } else if (generalAudioPreferencesBean.getCallRinger() == null || generalAudioPreferencesBean.getCallRinger().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Call Ringer is mandatory");
        }

        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getRingerVolume())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getRingerVolume());
            } catch (Exception e) {
                log.error("Invalid ringer volume: ", e);
                return sendReasonDataErrResponse(responseDataBean, "Ringer Volume");
            }
        } else if (generalAudioPreferencesBean.getRingerVolume() == null || generalAudioPreferencesBean.getRingerVolume().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Ringer Volume is mandatory");
        }

        //handsetreceivevolume
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHandsetReceiveVolume())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getHandsetReceiveVolume());
            } catch (Exception e) {
                log.error("Invalid handset receive volume: ", e);
                return sendReasonDataErrResponse(responseDataBean, "Handset Receive Volume");
            }
        } else if (generalAudioPreferencesBean.getHandsetReceiveVolume() == null || generalAudioPreferencesBean.getHandsetReceiveVolume().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Handset Receive Volume is mandatory");
        }

        //handsettransmitvolume
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHandsetTransmitVolume())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getHandsetTransmitVolume());
            } catch (Exception e) {
                log.error("Invalid handset transmit volume: ", e);
                return sendReasonDataErrResponse(responseDataBean, "Handset Transmit Volume");
            }
        } else if (generalAudioPreferencesBean.getHandsetTransmitVolume() == null || generalAudioPreferencesBean.getHandsetTransmitVolume().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Handset Transmit Volume is mandatory");
        }

        //handsetSideToneAttenuation
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHandsetSideToneAttenuation())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getHandsetSideToneAttenuation());
            } catch (Exception e) {
                log.error("Invalid handsetSideToneAttenuation: ", e);
                return sendReasonDataErrResponse(responseDataBean, "Handset Side Tone Attenuation");
            }
        } else if (generalAudioPreferencesBean.getHandsetSideToneAttenuation() == null || generalAudioPreferencesBean.getHandsetSideToneAttenuation().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Handset Side Tone Attenuation is mandatory");
        }

        // handsetRxNoiseReduceMode
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHandsetRxNoiseReduceMode())) {
            if (NoiseReduceModeTypes.fromValue(generalAudioPreferencesBean.getHandsetRxNoiseReduceMode()) == null) {
                return sendReasonDataErrResponse(responseDataBean, "Handset Receiver Noise Reduce Mode");
            }
        } else if (generalAudioPreferencesBean.getHandsetRxNoiseReduceMode() == null || generalAudioPreferencesBean.getHandsetRxNoiseReduceMode().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Handset Receiver Noise Reduce Mode is mandatory");
        }

        //handsetTxNoiseReduceMode
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHandsetTxNoiseReduceMode())) {
            if (NoiseReduceModeTypes.fromValue(generalAudioPreferencesBean.getHandsetTxNoiseReduceMode()) == null) {
                return sendReasonDataErrResponse(responseDataBean, "Handset Transmit Noise Reduce Mode");
            }
        } else if (generalAudioPreferencesBean.getHandsetTxNoiseReduceMode() == null || generalAudioPreferencesBean.getHandsetTxNoiseReduceMode().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Handset Transmit Noise Reduce Mode is mandatory");
        }

        //handsetRxEqualization
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHandsetRxEqualization())) {
            if (ReceiverEqualizationTypes.fromValue(generalAudioPreferencesBean.getHandsetRxEqualization()) == null) {
                return sendReasonDataErrResponse(responseDataBean, "Handset Receiver Equalization");
            }
        } else if (generalAudioPreferencesBean.getHandsetRxEqualization() == null || generalAudioPreferencesBean.getHandsetRxEqualization().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Handset Receiver Equalization is mandatory");
        }

        //isHSWideBand
        log.debug("value of isHSWideBand: " + generalAudioPreferencesBean.getIsHSWideBand());
        if (!StringUtils.isNotEmpty(generalAudioPreferencesBean.getIsHSWideBand())) {
            return sendReasonDataErrResponse(responseDataBean, "Handset Wideband is mandatory");
        }

        //speakerMasterRxVolume1
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getSpeakerMasterRxVolume1())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getSpeakerMasterRxVolume1());
            } catch (Exception e) {
                log.error("Invalid SpeakerMasterRxVolume1: ", e);
                return sendReasonDataErrResponse(responseDataBean, "Speaker Master Receiver Volume1");
            }
        } else if (generalAudioPreferencesBean.getSpeakerMasterRxVolume1() == null || generalAudioPreferencesBean.getSpeakerMasterRxVolume1().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Speaker Master Receiver Volume1 is mandatory");
        }

        //speakerTxNoiseReduceMode
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getSpeakerTxNoiseReduceMode())) {
            if (NoiseReduceModeTypes.fromValue(generalAudioPreferencesBean.getSpeakerTxNoiseReduceMode()) == null) {
                return sendReasonDataErrResponse(responseDataBean, "Speaker Transmit Noise Reduce Mode");
            }
        } else if (generalAudioPreferencesBean.getSpeakerTxNoiseReduceMode() == null || generalAudioPreferencesBean.getSpeakerTxNoiseReduceMode().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Speaker Transmit Noise Reduce Mode is mandatory");
        }

        //speakerTransmitVolume
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getSpeakerTransmitVolume())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getSpeakerTransmitVolume());
            } catch (Exception e) {
                log.error("Invalid SpeakerTransmitVolume: ", e);
                return sendReasonDataErrResponse(responseDataBean, "Speaker Transmit Volume");
            }
        } else if (generalAudioPreferencesBean.getSpeakerTransmitVolume() == null || generalAudioPreferencesBean.getSpeakerTransmitVolume().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Speaker Transmit Volume is mandatory");
        }

        //speakerRxEqualization
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getSpeakerRxEqualization())) {
            if (ReceiverEqualizationTypes.fromValue(generalAudioPreferencesBean.getSpeakerRxEqualization()) == null) {
                return sendReasonDataErrResponse(responseDataBean, "Speaker Receiver Equalization");
            }
        } else if (generalAudioPreferencesBean.getSpeakerRxEqualization() == null || generalAudioPreferencesBean.getSpeakerRxEqualization().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Speaker Receiver Equalization is mandatory");
        }

        //speakerSummingMode
        if (!StringUtils.isNotEmpty(generalAudioPreferencesBean.getSpeakerSummingMode())) {
            return sendReasonDataErrResponse(responseDataBean, "Speaker Summing Mode is mandatory");
        }

        //handsFreeRxEqualization
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHandsFreeRxEqualization())) {
            if (ReceiverEqualizationTypes.fromValue(generalAudioPreferencesBean.getHandsFreeRxEqualization()) == null) {
                return sendReasonDataErrResponse(responseDataBean, "HandsFree Receiver Equalization");
            }
        } else if (generalAudioPreferencesBean.getHandsFreeRxEqualization() == null || generalAudioPreferencesBean.getHandsFreeRxEqualization().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "HandsFree Receiver Equalization is mandatory");
        }

        //handsFreeMRxVolume
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHandsFreeMRxVolume())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getHandsFreeMRxVolume());
            } catch (Exception e) {
                log.error("Invalid HandsFreeMRxVolume: ", e);
                return sendReasonDataErrResponse(responseDataBean, "HandsFreeMReceiver Volume");
            }
        } else if (generalAudioPreferencesBean.getHandsFreeMRxVolume() == null || generalAudioPreferencesBean.getHandsFreeMRxVolume().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "HandsFreeMReceiver Volume is mandatory");
        }

        //hfmTxAutoGainControlMode
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHfmTxAutoGainControlMode())) {
            if (NoiseReduceModeTypes.fromValue(generalAudioPreferencesBean.getHfmTxAutoGainControlMode()) == null) {
                return sendReasonDataErrResponse(responseDataBean, "HFM Transmit Auto Gain Control Mode");
            }
        } else if (generalAudioPreferencesBean.getHfmTxAutoGainControlMode() == null || generalAudioPreferencesBean.getHfmTxAutoGainControlMode().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "HFM Transmit Auto Gain Control Mode is mandatory");
        }

        //hfmTxNoiseReduceMode
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHfmTxNoiseReduceMode())) {
            if (NoiseReduceModeTypes.fromValue(generalAudioPreferencesBean.getHfmTxNoiseReduceMode()) == null) {
                return sendReasonDataErrResponse(responseDataBean, "HFM Transmit Noise Reduce Mode");
            }
        } else if (generalAudioPreferencesBean.getHfmTxNoiseReduceMode() == null || generalAudioPreferencesBean.getHfmTxNoiseReduceMode().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "HFM Transmit Noise Reduce Mode is mandatory");
        }

        //handsFreeMTransmitVolume
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHandsFreeMTransmitVolume())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getHandsFreeMTransmitVolume());
            } catch (Exception e) {
                log.error("Invalid HandsFreeMTransmit: ", e);
                return sendReasonDataErrResponse(responseDataBean, "HandsFreeMTransmit Volume");
            }
        } else if (generalAudioPreferencesBean.getHandsFreeMTransmitVolume() == null || generalAudioPreferencesBean.getHandsFreeMTransmitVolume().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "HandsFreeMTransmit Volume is mandatory");
        }

        //ipRecordOutputGain
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getIpRecordOutputGain())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getIpRecordOutputGain());
            } catch (Exception e) {
                log.error("Invalid IPRecordOutputGain: ", e);
                return sendReasonDataErrResponse(responseDataBean, "IPRecordOutputGain Volume");
            }
        } else if (generalAudioPreferencesBean.getIpRecordOutputGain() == null || generalAudioPreferencesBean.getIpRecordOutputGain().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "IPRecordOutputGain is mandatory");
        }

        //speakerIPRecordMicMixGain
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getSpeakerIPRecordMicMixGain())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getSpeakerIPRecordMicMixGain());
            } catch (Exception e) {
                log.error("Invalid SpeakerIPRecordMicMixGain: ", e);
                return sendReasonDataErrResponse(responseDataBean, "SpeakerIPRecordMicMixGain");
            }
        } else if (generalAudioPreferencesBean.getSpeakerIPRecordMicMixGain() == null || generalAudioPreferencesBean.getSpeakerIPRecordMicMixGain().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "SpeakerIPRecordMicMixGain is mandatory");
        }

        //handsetIPRecordMicMixGain
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getHandsetIPRecordMicMixGain())) {
            try {
                Integer.parseInt(generalAudioPreferencesBean.getHandsetIPRecordMicMixGain());
            } catch (Exception e) {
                log.error("Invalid HandsetIPRecordMicMixGain: ", e);
                return sendReasonDataErrResponse(responseDataBean, "HandsetIPRecordMicMixGain");
            }
        } else if (generalAudioPreferencesBean.getHandsetIPRecordMicMixGain() == null || generalAudioPreferencesBean.getHandsetIPRecordMicMixGain().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "HandsetIPRecordMicMixGain is mandatory");
        }

        //recordProtocol
        if (StringUtils.isNotEmpty(generalAudioPreferencesBean.getRecordProtocol())) {
            if (RecordingProtocolTypes.fromValue(generalAudioPreferencesBean.getRecordProtocol()) == null) {
                return sendReasonDataErrResponse(responseDataBean, "Record Protocol");
            }
        } else if (generalAudioPreferencesBean.getRecordProtocol() == null || generalAudioPreferencesBean.getRecordProtocol().isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "Record Protocol is mandatory");
        }

        //accousticFeedbackReduction
        if (!StringUtils.isNotEmpty(generalAudioPreferencesBean.getAccousticFeedbackReduction())) {
            return sendReasonDataErrResponse(responseDataBean, "AccousticFeedbackReduction is mandatory");
        }

        //replayPlaybackOutput
        String replayPlaybackOutput = generalAudioPreferencesBean.getReplayPlaybackOutput();
        if (null == replayPlaybackOutput || replayPlaybackOutput.isEmpty()) {
            return sendReasonDataErrResponse(responseDataBean, "ReplayPlaybackOutput is mandatory");
        }

        OutputModuleTypes outputModuleTypes = null;
        try {
            outputModuleTypes = OutputModuleTypes.fromValue(replayPlaybackOutput);
        } catch (Exception e) {
        }

        if (outputModuleTypes == null) {
            return sendReasonDataErrResponse(responseDataBean, "ReplayPlaybackOutput");
        }

        TurretAudioFeaturesBean turretAudioFeaturesBean = userAudioPreferencesBean.getTurretAudioFeatures();

        if (turretAudioFeaturesBean != null) {
            //highPriorityRing
            if (StringUtils.isNotEmpty(turretAudioFeaturesBean.getHighPriorityRing())) {
                try {
                    Integer.parseInt(turretAudioFeaturesBean.getHighPriorityRing());
                } catch (Exception e) {
                    log.error("Invalid HighPriorityRing: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "HighPriorityRing");
                }
            } else if (turretAudioFeaturesBean.getHighPriorityRing() == null || turretAudioFeaturesBean.getHighPriorityRing().isEmpty()) {
                return sendReasonDataErrResponse(responseDataBean, "HighPriorityRing is mandatory");
            }
            //lowPriorityRing
            if (StringUtils.isNotEmpty(turretAudioFeaturesBean.getLowPriorityRing())) {
                try {
                    Integer.parseInt(turretAudioFeaturesBean.getLowPriorityRing());
                } catch (Exception e) {
                    log.error("Invalid LowPriorityRing: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "LowPriorityRing");
                }
            } else if (turretAudioFeaturesBean.getLowPriorityRing() == null || turretAudioFeaturesBean.getLowPriorityRing().isEmpty()) {
                return sendReasonDataErrResponse(responseDataBean, "LowPriorityRing is mandatory");
            }
            //speakerMasterRxVolume2
            if (StringUtils.isNotEmpty(turretAudioFeaturesBean.getSpeakerMasterRxVolume2())) {
                try {
                    Integer.parseInt(turretAudioFeaturesBean.getSpeakerMasterRxVolume2());
                } catch (Exception e) {
                    log.error("Invalid SpeakerMasterRxVolume2: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "SpeakerMasterRxVolume2");
                }
            } else if (turretAudioFeaturesBean.getSpeakerMasterRxVolume2() == null || turretAudioFeaturesBean.getSpeakerMasterRxVolume2().isEmpty()) {
                return sendReasonDataErrResponse(responseDataBean, "SpeakerMasterRxVolume2 is mandatory");
            }
            //speakerMasterRxVolume3
            if (StringUtils.isNotEmpty(turretAudioFeaturesBean.getSpeakerMasterRxVolume3())) {
                try {
                    Integer.parseInt(turretAudioFeaturesBean.getSpeakerMasterRxVolume3());
                } catch (Exception e) {
                    log.error("Invalid SpeakerMasterRxVolume3: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "SpeakerMasterRxVolume3");
                }
            } else if (turretAudioFeaturesBean.getSpeakerMasterRxVolume3() == null || turretAudioFeaturesBean.getSpeakerMasterRxVolume3().isEmpty()) {
                return sendReasonDataErrResponse(responseDataBean, "SpeakerMasterRxVolume3 is mandatory");
            }

            //lineInMixerOutput
            if (StringUtils.isNotEmpty(turretAudioFeaturesBean.getLineInMixerOutput())) {
                if (OutputModuleTypes.fromValue(turretAudioFeaturesBean.getLineInMixerOutput()) == null) {
                    return sendReasonDataErrResponse(responseDataBean, "LineInMixerOutput");
                }
            } else if (turretAudioFeaturesBean.getLineInMixerOutput() == null || turretAudioFeaturesBean.getLineInMixerOutput().isEmpty()) {
                return sendReasonDataErrResponse(responseDataBean, "LineInMixerOutput");
            }
        } else if (deviceType.equals("Turret") && turretAudioFeaturesBean == null) {
            return sendReasonDataErrResponse(responseDataBean, "TurretAudioFeatures data is mandatory");
        }

        /*
        IQ_MAX_TOUCH Features
         */
        IQMAXTouchAudioFeaturesBean iqmaxTouchAudioFeaturesBean = userAudioPreferencesBean.getIqMAXTouchAudioFeatures();
        if (iqmaxTouchAudioFeaturesBean != null) {
            if (StringUtils.isNotEmpty(iqmaxTouchAudioFeaturesBean.getHighPriorityRing())) {
                try {
                    Integer.parseInt(iqmaxTouchAudioFeaturesBean.getHighPriorityRing());
                } catch (Exception e) {
                    log.error("Invalid HighPriorityRing: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "HighPriorityRing");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "HighPriorityRing is Mandatory");
            }

            if (StringUtils.isNotEmpty(iqmaxTouchAudioFeaturesBean.getLowPriorityRing())) {
                try {
                    Integer.parseInt(iqmaxTouchAudioFeaturesBean.getLowPriorityRing());
                } catch (Exception e) {
                    log.error("Invalid LowPriorityRing: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "LowPriorityRing");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "LowPriorityRing is Mandatory");
            }

            if (StringUtils.isNotEmpty(iqmaxTouchAudioFeaturesBean.getMasterReceiveVolume())) {
                try {
                    Integer.parseInt(iqmaxTouchAudioFeaturesBean.getMasterReceiveVolume());
                } catch (Exception e) {
                    log.error("Invalid MasterReceiveVolume: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "MasterReceiveVolume");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "MasterReceiveVolume is Mandatory");
            }

            if (StringUtils.isNotEmpty(iqmaxTouchAudioFeaturesBean.getSpeakerSoloModeAttenuation())) {
                try {
                    Integer.parseInt(iqmaxTouchAudioFeaturesBean.getSpeakerSoloModeAttenuation());
                } catch (Exception e) {
                    log.error("Invalid SpeakerSoloModeAttenuation: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "SpeakerSoloModeAttenuation");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "SpeakerSoloModeAttenuation is Mandatory");
            }


        } else if (deviceType.equals("IQ_MAX_TOUCH") && iqmaxTouchAudioFeaturesBean == null) {
            return sendReasonDataErrResponse(responseDataBean, "IQMAXTouchAudioFeatures data is mandatory");
        }


        /*
        Pulse Audio Features
         */
        PulseAudioFeaturesBean pulseAudioFeaturesBean = userAudioPreferencesBean.getPulseAudioFeatures();
        if (pulseAudioFeaturesBean != null) {
            if (StringUtils.isNotEmpty(pulseAudioFeaturesBean.getSpeakerLatching())) {
                if (SpeakerLatchingTypes.fromValue(pulseAudioFeaturesBean.getSpeakerLatching()) == null) {
                    return sendReasonDataErrResponse(responseDataBean, "Speaker Latching Type Protocol");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "Speaker Latching Type Protocol is mandatory");
            }

            if (StringUtils.isNotEmpty(pulseAudioFeaturesBean.getHfmTransitionDevice())) {
                if (HFMTransitionDeviceTypes.fromValue(pulseAudioFeaturesBean.getHfmTransitionDevice()) == null) {
                    return sendReasonDataErrResponse(responseDataBean, "HFM Transition Device");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "HFM Transition Device is Mandatory");
            }

            if (StringUtils.isNotEmpty(pulseAudioFeaturesBean.getSpeakerAudioOutput())) {
                if (SpeakerAudioOutputTypes.fromValue(pulseAudioFeaturesBean.getSpeakerAudioOutput()) == null) {
                    return sendReasonDataErrResponse(responseDataBean, "Speaker Audio Output");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "Speaker Audio Output is Mandatory");
            }

            if (StringUtils.isNotEmpty(pulseAudioFeaturesBean.getUsbReceiveVolume())) {
                try {
                    Integer.parseInt(pulseAudioFeaturesBean.getUsbReceiveVolume());
                } catch (Exception e) {
                    log.error("Invalid UsbReceiveVolume: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "USB Receive Volume");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "USB Receive Volume is Mandatory");
            }

            if (StringUtils.isNotEmpty(pulseAudioFeaturesBean.getUsbTransmitVolume())) {
                try {
                    Integer.parseInt(pulseAudioFeaturesBean.getUsbTransmitVolume());
                } catch (Exception e) {
                    log.error("Invalid UsbTransmitVolume: ", e);
                    return sendReasonDataErrResponse(responseDataBean, "USB Transmit Volume");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "USB Transmit Volume is Mandatory");
            }


            if (StringUtils.isNotEmpty(pulseAudioFeaturesBean.getDisplayNameNumber())) {
                if (DisplayNameNumberTypes.fromValue(pulseAudioFeaturesBean.getDisplayNameNumber()) == null) {
                    return sendReasonDataErrResponse(responseDataBean, "Display Name Number Type");
                }
            } else {
                return sendReasonDataErrResponse(responseDataBean, "Display Name Number Type is Mandatory");
            }

            if (!StringUtils.isNotEmpty(pulseAudioFeaturesBean.getIcmAutoAnswer())) {
                return sendReasonDataErrResponse(responseDataBean, "ICM Auto Answer is mandatory");
            }

            if (!StringUtils.isNotEmpty(pulseAudioFeaturesBean.getIcmStartOnHfm())) {
                return sendReasonDataErrResponse(responseDataBean, "ICMStartOnHFM is mandatory");
            }

        } else if (deviceType.equals("Pulse") && pulseAudioFeaturesBean == null) {
            return sendReasonDataErrResponse(responseDataBean, "PulseAudioFeatures data is mandatory");
        }
        return null;
    }

    public ResponseDataBean sendReasonDataErrResponse(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(REQ_FAILURE_DESC + errorField);
        return responseDataBean;
    }
}
