package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;

import com.comitfs.cas.ipc.bw.jaxb.RoleInformationEx;
import com.comitfs.cas.ipc.bw.jaxb.RoleListEx;
import com.comitfs.cas.ipc.bw.jaxb.RoleResponse;
import com.comitfs.cas.ipc.bw.jaxb.StandardErrorResponse;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.RoleInfo;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.util.ArrayList;


public class FetchRoles {
    private final Logger LOG = Logger.getLogger(FetchRoles.class);

    private final IPCSite ipcSite;

    public FetchRoles(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
    }

    public ArrayList<RoleInfo> fetchRoles() {

        ArrayList<RoleInfo> roleInfoList = new ArrayList<>();
        try {
            String apiVersion = ipcSite.getApiVersion();
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();
            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.FETCH_ROLES.toString(), null, ipcSite.isSecure());

            LOG.debug(" Fetch Roles URL: " + resourceUrl);


            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));


            if (respEntity == null) {
                LOG.error("Response Entity for FetchRoles  is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error("Fetch Roles error response " + formattedResponseXML);
                return null;

            }
            if (respEntity instanceof RoleResponse) {
                RoleResponse responseExJaxbBean = (RoleResponse) respEntity;

                LOG.debug("FetchRoles fetched with ResponseDesc: " + responseExJaxbBean.getReason().getReasonDescription());

                RoleListEx roleListEx = responseExJaxbBean.getRoleList();
                ArrayList<RoleInformationEx> rolesList = (ArrayList<RoleInformationEx>) roleListEx.getRole();
                LOG.debug("Fetched Roles size: " + rolesList.size());


                for (RoleInformationEx roleData : rolesList) {
                    RoleInfo roleInfo = new RoleInfo();
                    roleInfo.setRoleId(String.valueOf(roleData.getRoleId()));
                    roleInfo.setRoleName(roleData.getRoleName().getValue());
                    roleInfo.setRoleType(roleData.getRoleType().value());
                    roleInfo.setDescription(roleData.getDescription().getValue());
                    roleInfo.setScope(roleData.getScope().value());
                    roleInfo.setValue(String.valueOf(roleData.getValue()));

                    roleInfoList.add(roleInfo);
                }
            }

            return roleInfoList;
        } catch (Exception e) {
            LOG.error("Exception fetching role info: ", e);
            return roleInfoList;
        }
    }

}
