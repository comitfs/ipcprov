package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;

import com.comitfs.cas.ipc.bw.jaxb.LicenseDataResponse;
import com.comitfs.cas.ipc.bw.jaxb.LicenseFeatureData;
import com.comitfs.cas.ipc.bw.jaxb.LicenseFeatureDataList;
import com.comitfs.cas.plugin.ipcprov.beans.LicenseFeatureDataBean;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class LicenseInformationHandler {
    private final Logger log = Logger.getLogger(LicenseInformationHandler.class);
    private final IPCSite site;
    List<LicenseFeatureDataBean> licenseFeatureDataBeanList;
    private int pageCounter;
    private int pagesToRequest;

    public LicenseInformationHandler(IPCSite site) {
        this.site = site;
        this.licenseFeatureDataBeanList = new ArrayList<LicenseFeatureDataBean>();
    }

    public List<LicenseFeatureDataBean> fetchLicenseInfo(String loginName, String queryFilter, int pageNum) {
        try {
            String apiVersion = site.getApiVersion();
            WebAPIUser webAPIUser = new WebAPIUserStub(site).getCTIWebAPIUser();
            BWPoolConnection bwPoolConnection = new BWZoneSelector(site).getActiveBWZone();
            SessionManager sessionManager = new SessionManager(webAPIUser, site, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.LICENSE_INFORMATION.toString(), null, site.isSecure());
            resourceUrl = resourceUrl + ((StringUtils.isNotBlank(queryFilter)) ?
                    queryFilter : URLEncoder.encode("loginName=\"" + loginName + "\"", StandardCharsets.UTF_8.toString()));
            resourceUrl = resourceUrl + "&pagenum=" + pageNum;

            log.info("fetch license information api url: " + resourceUrl);
            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, site.isSecure());

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("no response received for lincense information: " + loginName);
                return null;
            }

            log.info("fetch license information api response: " + apiRes);

            if (respEntity instanceof LicenseDataResponse) {
                LicenseDataResponse licenseDataResponse = (LicenseDataResponse) respEntity;
                LicenseFeatureDataList featureDataList = licenseDataResponse.getLicenseFeatureDataList();
                if (null == featureDataList) {
                    return null;
                }

                addToList(featureDataList.getLicenseFeatureData());

                if (pageCounter == 1) {
                    float numPages = licenseDataResponse.getNumPagesAvailable();
                    if (numPages > 0) {
                        pagesToRequest = Math.round(numPages);
                    }
                }

                if (pageCounter == pagesToRequest) {
                    return licenseFeatureDataBeanList;
                } else {
                    return fetchLicenseInfo(loginName, queryFilter, ++pageCounter);
                }
            }
        } catch (Exception e) {
            log.error("Error fetching license information of user: " + loginName, e);
        }
        return licenseFeatureDataBeanList;
    }

    private void addToList(List<LicenseFeatureData> licenseFeatureData) {
        licenseFeatureData.forEach(licenseFeature
                -> this.licenseFeatureDataBeanList
                .add(new LicenseFeatureDataBean(licenseFeature.getUserId(), licenseFeature.getZoneId()
                        , licenseFeature.getFeatureName(), licenseFeature.getFeatureDescription().getValue()
                        , licenseFeature.getFeatureCode(), licenseFeature.getLicenseClass().value()
                        , licenseFeature.getFeatureSource().value()))
        );
    }
}
