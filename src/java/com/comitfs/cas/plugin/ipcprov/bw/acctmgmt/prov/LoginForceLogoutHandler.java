package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;

import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.ForceLogInLogOutBean;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.List;

public class LoginForceLogoutHandler {
    public static final String OPERATION_LOGIN = "login";
    public static final String OPERATION_FORCELOGOUT = "forcelogout";

    public static final String REQ_FAILURE = "REQ_FAILURE";
    public static final String REQ_FAILURE_DESC = "Invalid input :";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(LoginForceLogoutHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    public LoginForceLogoutHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String casUserName, ForceLogInLogOutBean forceLogInLogOutBean) {

        String apiVersion = ipcSite.getApiVersion();

        ResponseDataBean responseDataBean = new ResponseDataBean();

        CDISession cdiSession = new CDISession();

        try {
            UserIdentifier userIdentifier = new UserIdentifier();

            userIdentifier.setLoginName(casUserName);

            cdiSession.setUserIdentifier(userIdentifier);

            if (forceLogInLogOutBean.getOperationType() != null) {
                if (forceLogInLogOutBean.getOperationType().equalsIgnoreCase(OPERATION_LOGIN)) {

                    LogInInformation logInInformation = new LogInInformation();

                    if (forceLogInLogOutBean.getDeviceId() != null) {

                        try {
                            Integer deviceId = Integer.parseInt(forceLogInLogOutBean.getDeviceId());
                            logInInformation.setDeviceId(BigInteger.valueOf(deviceId));
                        } catch (Exception e) {
                            log.error("Invalid deviceid: " + forceLogInLogOutBean.getDeviceId() + " ", e);
                            return sendReasonDataErrResponse(responseDataBean, "DeviceId");
                        }
                    } else {
                        List<IPCUser> ipcUsers = IPCDataService.getInstance().getIpcImportUsers();
                        log.debug("imported user devices : " + ipcUsers.size());
                        ipcUsers.forEach(user -> {
                            if (user.getUserId().equalsIgnoreCase(casUserName)) {
                                log.debug("imported user device ID : " + user.getDeviceId());
                                logInInformation.setDeviceId(BigInteger.valueOf(user.getDeviceId()));
                            }
                        });

                    }
                    cdiSession.setLogInInfo(logInInformation);
                } else if (forceLogInLogOutBean.getOperationType().equalsIgnoreCase(OPERATION_FORCELOGOUT)) {
                    //log.debug("Invoking Force Logout for user: " + ipcUser.getUserId() + ":" + ipcUser.getIPCId());

                    LogOutInformation logOutInformation = new LogOutInformation();
                    logOutInformation.setIgnoreOpenHandSetCalls(Boolean.valueOf(forceLogInLogOutBean.getIgnoreOpenHandSetCalls()));
                    cdiSession.setLogOutInfo(logOutInformation);
                } else {
                    log.error("Invalid OperationType: " + forceLogInLogOutBean.getOperationType());
                    return sendReasonDataErrResponse(responseDataBean, "OperationType");
                }
            } else {
                log.error("Invalid OperationType: " + forceLogInLogOutBean.getOperationType());
                return sendReasonDataErrResponse(responseDataBean, "OperationType");
            }

            String loginLogoutRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(cdiSession));
            log.debug(String.format("Request string:" + loginLogoutRequestString));

            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();

            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String httpAuth = IPCUtil.generateHttpAuthorization(accountManagementUser.getUserId(), accountManagementUser.getPassword());

            String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.LOGIN_LOGOUT.toString());

            Response response = IPCProvHttpClient.httpsPut(resourceUrl, httpAuth, ipcAuthToken, apiVersion, cdiSession);

            log.debug("LoginForceLogout Response: " + response);

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for Login is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("LoginForceLogout error response " + formattedResponseXML);

                responseDataBean.setReasonCode(REQ_FAILURE);
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
                return responseDataBean;

            } else if (respEntity instanceof CDISessionResponse) {
                CDISessionResponse cdiSessionResponse = (CDISessionResponse) respEntity;
                String loginRespString = UtilHelper.marshalJAXBObjectForResponse(cdiSessionResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(loginRespString);
                log.debug("LoginForceLogout response " + formattedResponseXML);

                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);
                String reasonCode = cdiSessionResponse.getReason().getReasonCode().value();
                String reasonDescription = cdiSessionResponse.getReason().getReasonDescription();
                responseDataBean.setReasonCode(reasonCode);
                responseDataBean.setReasonDescription(reasonDescription);
                return responseDataBean;
            }
        } catch (Exception e) {
            log.error("Exception in LoginForceLogout Handler: ", e);
            return null;
        }
        return null;
    }

    public ResponseDataBean sendReasonDataErrResponse(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(REQ_FAILURE_DESC + errorField);
        return responseDataBean;
    }
}
