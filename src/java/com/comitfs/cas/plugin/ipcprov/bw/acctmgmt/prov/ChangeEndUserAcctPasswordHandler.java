package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;

import com.comitfs.cas.ipc.bw.jaxb.ChangeEndUserPassword;
import com.comitfs.cas.ipc.bw.jaxb.ChangeEndUserPasswordResponse;
import com.comitfs.cas.ipc.bw.jaxb.StandardErrorResponse;
import com.comitfs.cas.ipc.bw.jaxb.UserIdentifier;
import com.comitfs.cas.plugin.ipcprov.beans.ChangeUserAccountPasswordBean;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.io.StringReader;
import java.math.BigInteger;

public class ChangeEndUserAcctPasswordHandler {

    private final static QName _ChangeEndUserPasswordPasswordChangeMode_QNAME = new QName("http://www.ipc.com/bw", "PasswordChangeMode");
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(ChangeEndUserAcctPasswordHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    public ChangeEndUserAcctPasswordHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }


    public ResponseDataBean handle(String casUserName, ChangeUserAccountPasswordBean userAccountPasswordBean) {

        try {
            IPCUser ipcUser = IPCDataService.getInstance().getUserById(casUserName,ipcSite.getId());
            String apiVersion = ipcSite.getApiVersion();

            ResponseDataBean responseDataBean = new ResponseDataBean();
            ChangeEndUserPassword changeEndUserPassword = new ChangeEndUserPassword();

            UserIdentifier userIdentifier = new UserIdentifier();
            if (ipcUser.getIPCId() != null) {
                userIdentifier.setUserId(BigInteger.valueOf(ipcUser.getIPCId()));
            } else if (ipcUser.getUserId() != null) {
                userIdentifier.setLoginName(ipcUser.getUserId());
            }
            changeEndUserPassword.setUserIdentifier(userIdentifier);

            if (userAccountPasswordBean.getNewPassword() != null) {
                changeEndUserPassword.setNewPassword(userAccountPasswordBean.getNewPassword());
            }

            if (userAccountPasswordBean.getPasswordChangeMode() != null && !userAccountPasswordBean.getPasswordChangeMode().isEmpty()) {
                boolean passwordChangeModeValue = Boolean.valueOf(userAccountPasswordBean.getPasswordChangeMode());
                changeEndUserPassword.setPasswordChangeMode(new JAXBElement<Boolean>(_ChangeEndUserPasswordPasswordChangeMode_QNAME, Boolean.class,
                        ChangeEndUserPassword.class, passwordChangeModeValue));
            }

            String changeUserPasswordRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(changeEndUserPassword));
            log.debug(String.format("change end user password string:\n" + changeUserPasswordRequestString));


            //create session with the above retrieved acct mgmt usr/pwd from ipcsite table
            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            //after creating session, invoke httpsput and send the update trader features request
            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());

            String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.CHANGE_USR_PASSWORD.toString());
            log.debug("created resourceurl: " + resourceUrl);

            Response response = IPCProvHttpClient.httpsPut(resourceUrl,
                    httpAuth, ipcAuthToken, apiVersion, changeEndUserPassword);

            log.debug("Change User Password Response: " + response);

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for ChangeUserPassword is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("Change User Password error response " + formattedResponseXML);

                responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
                return responseDataBean;
            } else if (respEntity instanceof ChangeEndUserPasswordResponse) {
                ChangeEndUserPasswordResponse pwdResponse = (ChangeEndUserPasswordResponse) respEntity;
                String changeEndUserPasswordRespString = UtilHelper.marshalJAXBObjectForResponse(pwdResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(changeEndUserPasswordRespString);
                log.debug("ChangeEndUserPassword response " + formattedResponseXML);

                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);
                responseDataBean.setReasonCode(pwdResponse.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(pwdResponse.getReason().getReasonDescription());
                return responseDataBean;
            }
        } catch (Exception e) {
            log.error("Exception  in ChangeAccountPasswordHandler: ", e);
            return null;
        }
        return null;
    }
}
