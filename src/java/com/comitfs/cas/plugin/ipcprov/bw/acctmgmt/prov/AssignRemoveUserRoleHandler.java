package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.AssignRemoveUserRoleBean;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.ArrayList;

public class AssignRemoveUserRoleHandler {

    public static final String INTERNAL_SERVER_ERROR = "Internal server error";
    public static final String REQ_FAILURE = "REQ_FAILURE";

    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(AssignRemoveUserRoleHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    public AssignRemoveUserRoleHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String casUserName, AssignRemoveUserRoleBean roleBean) {
        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            IPCUser ipcUser = IPCDataService.getInstance().getUserById(casUserName,ipcSite.getId());
            String apiVer = ipcSite.getApiVersion();

            RoleAssignment roleAssignment = new RoleAssignment();

            UserIdentifier userIdentifier = new UserIdentifier();
            //set the ipcuser's unigue id to be updated with attributes
            if (null == ipcUser || ipcUser.getIPCId() == null) {
                userIdentifier.setLoginName(casUserName);
            } else {
                userIdentifier.setUserId(BigInteger.valueOf(ipcUser.getIPCId()));
            }
            roleAssignment.setUserIdentifier(userIdentifier);


            RoleNamesList roleNamesList = new RoleNamesList();
            if (roleBean.getRoleNames() != null && roleBean.getRoleNames().size() > 0) {

                for (String role : roleBean.getRoleNames()) {
                    if (RoleNames.fromValue(role) != null) {
                        ArrayList roleNames = (ArrayList<RoleNames>) roleNamesList.getRoleName();
                        roleNames.add(RoleNames.fromValue(role));
                    }
                }
            }
            roleAssignment.setRoleNamesList(roleNamesList);
            String assignRemoveRolesRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(roleAssignment));
            log.debug(String.format("Roles Assignment request string:\n" + assignRemoveRolesRequestString));

            //TODO: acctmgmt usrname & pwd shd be retrieved from ipc site settings table based on the user Id passed as input

            //create session with the above retrieved acct mgmt usr/pwd from ipcsite table
            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();
            log.debug("ipcAuthToken in assign remove end  user roles handler after creating session: " + ipcAuthToken);

            //after creating session, invoke httpsput
            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());

            String resourceUrl = null;
            if (roleBean.getOperationType().equalsIgnoreCase("assign")) {
                resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.ASSIGN_ROLE.toString());
            } else if (roleBean.getOperationType().equalsIgnoreCase("remove")) {
                resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.REMOVE_ROLE.toString());
            }
            log.debug("created resourceurl: " + resourceUrl);

            Response response = IPCProvHttpClient.httpsPut(resourceUrl,
                    httpAuth, ipcAuthToken, apiVer, roleAssignment);

            log.debug("Role Assignment/Remove Response: " + response);


            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for Role Assignment/Remove is null.");
                return toErrorResponseDataBean(responseDataBean, "updateTrader features response is null");
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("Assign/Remove Role error response " + formattedResponseXML);
                responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
            } else if (respEntity instanceof RoleAssignmentResponse) {
                RoleAssignmentResponse roleAssignmentResponse = (RoleAssignmentResponse) respEntity;
                String roleAssignmentRespString = UtilHelper.marshalJAXBObjectForResponse(roleAssignmentResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(roleAssignmentRespString);
                log.debug("RoleAssignmentResponse response " + formattedResponseXML);

                ReasonData reasonData = roleAssignmentResponse.getReason();
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVer);
                responseDataBean.setReasonCode(reasonData.getReasonCode().value());
                responseDataBean.setReasonDescription(reasonData.getReasonDescription());
            }
        } catch (Exception e) {
            log.error("Exception assigning/removing role : ", e);
            return toErrorResponseDataBean(responseDataBean, INTERNAL_SERVER_ERROR);
        }
        return responseDataBean;
    }

    private ResponseDataBean toErrorResponseDataBean(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(errorField);
        return responseDataBean;
    }
}
