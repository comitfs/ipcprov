package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;

import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.RecorderProfile;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;


public class FetchRecorderProfiles {
    private final Logger LOG = Logger.getLogger(FetchRecorderProfiles.class);

    private final IPCSite ipcSite;

    public FetchRecorderProfiles(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
    }

    public ArrayList<RecorderProfile> fetchRecorderMixProfiles() {

        ArrayList<RecorderProfile> recorderProfileList = new ArrayList<>();
        try {
            String apiVersion = ipcSite.getApiVersion();
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();
            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.FETCH_RECORDER_PROFILES.toString(), null, ipcSite.isSecure());

            LOG.debug(" fetchRecorderMixProfiles URL: " + resourceUrl);


            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            if (apiRes != null) {
                apiRes = apiRes.replace("\n", "");
            }
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));


            if (respEntity == null) {
                LOG.error("Response Entity for fetchRecorderMixProfiles  is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error("fetchRecorderMixProfiles error response " + formattedResponseXML);
                return null;

            }
            if (respEntity instanceof RecordingProfileDataResponse) {
                RecordingProfileDataResponse responseExJaxbBean = (RecordingProfileDataResponse) respEntity;

                LOG.debug("Fetch Recorder Mix Profiles fetched with ResponseDesc: " + responseExJaxbBean.getReason().getReasonDescription());

                RecordingProfileList recorderProfilesList = responseExJaxbBean.getRecordingProfileList();
                ArrayList<RecordingProfile> recordingProfiles = (ArrayList<RecordingProfile>) recorderProfilesList.getRecordingProfile();
                LOG.debug("Fetch RecordingProfile  size: " + recordingProfiles.size());


                for (RecordingProfile recordingProfile : recordingProfiles) {
                    RecorderProfile recorderProfile = new RecorderProfile();
                    recorderProfile.setId(recordingProfile.getId());
                    recorderProfile.setName(recordingProfile.getName());
                    List<RecordMix> recordMixs = recordingProfile.getRecordMixList().getRecordMix();
                    for (RecordMix recordMix : recordMixs) {
                        recorderProfile.getRecordMixList().add(recordMix.getDescription());
                    }

                    recorderProfileList.add(recorderProfile);
                }
            }

            return recorderProfileList;
        } catch (Exception e) {
            LOG.error("Exception fetching role info: ", e);
            return recorderProfileList;
        }
    }

}
