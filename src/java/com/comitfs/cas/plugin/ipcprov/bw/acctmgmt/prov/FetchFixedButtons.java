package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;

import com.comitfs.cas.ipc.bw.jaxb.FixedButton;
import com.comitfs.cas.ipc.bw.jaxb.FixedButtonList;
import com.comitfs.cas.ipc.bw.jaxb.FixedButtonResponse;
import com.comitfs.cas.ipc.bw.jaxb.StandardErrorResponse;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.*;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.FixedButtonBean;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.util.ArrayList;

public class FetchFixedButtons {
    private final Logger LOG = Logger.getLogger(FetchFixedButtons.class);

    private final IPCSite ipcSite;

    public FetchFixedButtons(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
    }

    public ArrayList<FixedButtonBean> fetchFixedButtons(String loginName) {

        ArrayList<FixedButtonBean> fixedButtonsResp = new ArrayList<>();
        try {
            IPCUser ipcUser = IPCDataService.getInstance().getUserById(loginName,ipcSite.getId());
            String apiVersion = ipcSite.getApiVersion();
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();
            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.FETCH_FIXED_BUTTONS.toString(), null, ipcSite.isSecure());

            resourceUrl = resourceUrl + "?finduserby=userId&value=" + ipcUser.getIPCId();
            LOG.debug(" Fetch Fixed Buttons URL: " + resourceUrl);


            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));


            if (respEntity == null) {
                LOG.error("Response Entity for FetchFixedButtons  is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error("FetchFixedButtons error response " + formattedResponseXML);
                return null;

            }
            if (respEntity instanceof FixedButtonResponse) {
                FixedButtonResponse responseExJaxbBean = (FixedButtonResponse) respEntity;

                LOG.debug("FixedButtonResponse fetched with ResponseDesc: " + responseExJaxbBean.getReason().getReasonDescription());

                FixedButtonList fixedButtonList = responseExJaxbBean.getFixedButtonList();
                LOG.debug("FixedButtonList size: " + fixedButtonList.getFixedButton().size());

                if (fixedButtonList.getFixedButton() != null && fixedButtonList.getFixedButton().size() > 0) {
                    ArrayList<FixedButton> fixedButtonsList = (ArrayList<FixedButton>) fixedButtonList.getFixedButton();

                    for (FixedButton fixedButton : fixedButtonsList) {
                        FixedButtonBean fixedButtonBean = new FixedButtonBean();
                        fixedButtonBean.setId(String.valueOf(fixedButton.getId()));
                        fixedButtonBean.setButtonId(String.valueOf(fixedButton.getButtonId()));
                        fixedButtonBean.setButtonLabel(fixedButton.getButtonLabel());
                        fixedButtonBean.setButtonType(fixedButton.getButtonType());
                        fixedButtonBean.setButtonNumber(String.valueOf(fixedButton.getButtonNumber()));
                        fixedButtonBean.setSlotPosition(String.valueOf(fixedButton.getSlotPosition()));
                        fixedButtonBean.setUserMercuryId(String.valueOf(fixedButton.getUserMercuryId()));
                        fixedButtonBean.setUserCDIId(String.valueOf(fixedButton.getUserCDIId()));

                        fixedButtonsResp.add(fixedButtonBean);
                    }
                }
            }
            return fixedButtonsResp;
        } catch (Exception e) {
            LOG.error("Exception fetching fixed button info: ", e);
            return fixedButtonsResp;
        }
    }

}
