package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.TraderFeaturesBean;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;

public class UpdateTraderFeaturesHandler {
    public static final String INTERNAL_SERVER_ERROR = "Internal server error";
    public static final String REQ_FAILURE = "REQ_FAILURE";
    public static final String REQ_FAILURE_DESC = "Invalid input: ";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(UpdateTraderFeaturesHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;
    private String autoSelectHold;
    private String language;

    public UpdateTraderFeaturesHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String casUserName, TraderFeaturesBean traderFeaturesBean) {

        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            String apiVersion = ipcSite.getApiVersion();
            UpdateTraderFeatures updateTraderFeatures = new UpdateTraderFeatures();

            IPCUser ipcUser = IPCDataService.getInstance().getUserById(casUserName,ipcSite.getId());

            UserIdentifier userIdentifier = new UserIdentifier();
            if (ipcUser.getIPCId() != null) {
                userIdentifier.setUserId(BigInteger.valueOf(ipcUser.getIPCId()));
            } else if (ipcUser.getUserId() != null) {
                userIdentifier.setLoginName(ipcUser.getUserId());
            }

            updateTraderFeatures.setUserIdentifier(userIdentifier);
            TraderInformation traderInformation = new TraderInformation();
            if (traderFeaturesBean.getIntercomExtn() != null && !traderFeaturesBean.getIntercomExtn().isEmpty()) {
                traderInformation.setIntercomExtension(traderFeaturesBean.getIntercomExtn());
            }

            if (traderFeaturesBean.getPersonalExtn() != null && !traderFeaturesBean.getPersonalExtn().isEmpty()) {
                traderInformation.setPersonalExtension(traderFeaturesBean.getPersonalExtn());
            }

            if (traderFeaturesBean.getPersonalExtnOverride() != null && !traderFeaturesBean.getPersonalExtnOverride().isEmpty()) {
                traderInformation.setPersonalExtnOverride(Boolean.valueOf(traderFeaturesBean.getPersonalExtnOverride()));
            }

            updateTraderFeatures.setTraderInfo(traderInformation);
            GeneralInformation generalInformation = new GeneralInformation();
            if (traderFeaturesBean.getAutoSelectHold() != null && !traderFeaturesBean.getAutoSelectHold().isEmpty()) {
                autoSelectHold = traderFeaturesBean.getAutoSelectHold();
                if (AutoSelectHoldType.fromValue(autoSelectHold) == null) {
                    responseDataBean.setReasonCode(REQ_FAILURE);
                    responseDataBean.setReasonDescription(REQ_FAILURE_DESC + " AutoSelectHold");
                    return responseDataBean;
                } else if (AutoSelectHoldType.fromValue(autoSelectHold) != null) {
                    generalInformation.setAutoSelectHold(AutoSelectHoldType.fromValue(autoSelectHold));
                }
            }

            if (traderFeaturesBean.getLanguage() != null && !traderFeaturesBean.getLanguage().isEmpty()) {
                language = traderFeaturesBean.getLanguage();
                if (UserCDILanguage.fromValue(language) == null) {
                    responseDataBean.setReasonCode(REQ_FAILURE);
                    responseDataBean.setReasonDescription(REQ_FAILURE_DESC + " Language");
                    return responseDataBean;
                } else if (UserCDILanguage.fromValue(language) != null) {
                    generalInformation.setLanguage(UserCDILanguage.fromValue(language));
                }
            }
            updateTraderFeatures.setGeneralInfo(generalInformation);
            UsagePreferencesInformation usagePreferences = new UsagePreferencesInformation();
            if (traderFeaturesBean.getAllowPrivacyToggle() != null && !traderFeaturesBean.getAllowPrivacyToggle().isEmpty()) {
                usagePreferences.setAllowPrivacyToggle(Boolean.valueOf(traderFeaturesBean.getAllowPrivacyToggle()));
            }

            if (traderFeaturesBean.getAlternateHSTimeout() != null && !traderFeaturesBean.getAlternateHSTimeout().isEmpty()) {
                try {
                    usagePreferences.setAlternateHandsetSelectTimeout(Integer.valueOf(traderFeaturesBean.getAlternateHSTimeout()));
                } catch (Exception e) {
                    log.error("Error : invalid value for AlternateHandsetSelectTimeout", e);
                    return toErrorResponseDataBean(responseDataBean, REQ_FAILURE_DESC + "AlternateHandsetSelectTimeout");
                }
            }

            if (traderFeaturesBean.getBlockingToneOnSpeakerMute() != null && !traderFeaturesBean.getBlockingToneOnSpeakerMute().isEmpty()) {
                usagePreferences.setBlockingToneOnSpeakerMute(Boolean.valueOf(traderFeaturesBean.getBlockingToneOnSpeakerMute()));
            }

            if (traderFeaturesBean.getHandsetSelectMode() != null && !traderFeaturesBean.getHandsetSelectMode().isEmpty()) {
                String hsSelectMode = traderFeaturesBean.getHandsetSelectMode();
                if (UserCDIHandsetSelectMode.fromValue(hsSelectMode.toLowerCase()) == null) {
                    return toErrorResponseDataBean(responseDataBean, REQ_FAILURE_DESC + "HandsetSelectMode");
                } else if (UserCDIHandsetSelectMode.fromValue(hsSelectMode.toLowerCase()) != null) {
                    usagePreferences.setHandsetSelectMode(UserCDIHandsetSelectMode.fromValue(hsSelectMode));
                }
            }

            if (traderFeaturesBean.getHandSetMuteOption() != null && !traderFeaturesBean.getHandSetMuteOption().isEmpty()) {
                String hsMuteOption = traderFeaturesBean.getHandSetMuteOption();
                if (UserCDIHandsetMuteOption.fromValue(hsMuteOption) == null) {
                    return toErrorResponseDataBean(responseDataBean, REQ_FAILURE_DESC + "Handset Call Mute Option");
                }
                if (UserCDIHandsetMuteOption.fromValue(hsMuteOption) != null) {
                    usagePreferences.setHandsetCallMuteOption(UserCDIHandsetMuteOption.fromValue(hsMuteOption));
                }
            }

            if (traderFeaturesBean.getHandSetButtonActions() != null && !traderFeaturesBean.getHandSetButtonActions().isEmpty()) {
                String hsButtonActions = traderFeaturesBean.getHandSetButtonActions();
                if (UserCDIHandsetBtn.fromValue(hsButtonActions) == null) {
                    return toErrorResponseDataBean(responseDataBean, REQ_FAILURE_DESC + "Handset Button Actions");
                }
                if (UserCDIHandsetBtn.fromValue(hsButtonActions) != null) {
                    usagePreferences.setHandsetButtonActions(UserCDIHandsetBtn.fromValue(hsButtonActions));
                }
            }

            if (traderFeaturesBean.getFloatAllIncomingCalls() != null && !traderFeaturesBean.getFloatAllIncomingCalls().isEmpty()) {
                usagePreferences.setFloatAllIncomingCalls(Boolean.valueOf(traderFeaturesBean.getFloatAllIncomingCalls()));
            }

            if (traderFeaturesBean.getFloatCallsOnHold() != null && !traderFeaturesBean.getFloatCallsOnHold().isEmpty()) {
                usagePreferences.setFloatCallsOnHold(Boolean.valueOf(traderFeaturesBean.getFloatCallsOnHold()));
            }

            if (traderFeaturesBean.getForceTalkBackMute() != null && !traderFeaturesBean.getForceTalkBackMute().isEmpty()) {
                usagePreferences.setForceTalkbackMute(Boolean.valueOf(traderFeaturesBean.getForceTalkBackMute()));
            }

            String privacyDefaultsToEnable = traderFeaturesBean.getPrivacyDefaultsToEnable();
            if (StringUtils.isNotBlank(privacyDefaultsToEnable)) {
                usagePreferences.setUserPrivacyDefaultsToEnabled(Boolean.parseBoolean(privacyDefaultsToEnable));
            }

            updateTraderFeatures.setUsagePreferences(usagePreferences);


            String updateTraderFeaturesRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(updateTraderFeatures));
            log.debug(String.format("update trader features request string:" + updateTraderFeaturesRequestString));


            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());

            String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.USR_TRADER_FEATURES.toString());
            log.debug("created UpdateTraderFeaturesHandler resourceurl: " + resourceUrl);

            Response response = IPCProvHttpClient.httpsPut(resourceUrl, null, ipcAuthToken, apiVersion, updateTraderFeatures);

            log.debug("Update Trader Features Response: " + response);

            String apiRes = response.readEntity(String.class);

            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for UpdateTraderFeatures is null.");
                toErrorResponseDataBean(responseDataBean, "updateTrader features response is null");
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("UpdateTraderFeatures error response " + formattedResponseXML);
                responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
            } else if (respEntity instanceof UpdateTraderFeaturesResponse) {
                UpdateTraderFeaturesResponse updateTraderFeaturesResponse = (UpdateTraderFeaturesResponse) respEntity;
                String updateTraderFeaturesRespString = UtilHelper.marshalJAXBObjectForResponse(updateTraderFeaturesResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(updateTraderFeaturesRespString);
                log.debug("UpdateTraderFeatures response " + formattedResponseXML);

                ReasonData reasonData = updateTraderFeaturesResponse.getReason();
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);
                responseDataBean.setReasonCode(reasonData.getReasonCode().value());
                responseDataBean.setReasonDescription(reasonData.getReasonDescription());
            }
        } catch (IllegalArgumentException ie) {
            log.warn("Exception in UpdateTraderFeaturesHandler, currently Language setting only English is supported : " + ie);
            toErrorResponseDataBean(responseDataBean, "only English language supported");
        } catch (Exception e) {
            log.error("Exception in UpdateTraderFeaturesHandler: ", e);
            toErrorResponseDataBean(responseDataBean, INTERNAL_SERVER_ERROR);
        }
        return responseDataBean;
    }

    private ResponseDataBean toErrorResponseDataBean(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(errorField);
        return responseDataBean;
    }
}
