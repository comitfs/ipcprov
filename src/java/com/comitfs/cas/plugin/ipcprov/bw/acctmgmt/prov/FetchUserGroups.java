package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;

import com.comitfs.cas.ipc.bw.jaxb.StandardErrorResponse;
import com.comitfs.cas.ipc.bw.jaxb.UserGroupListType;
import com.comitfs.cas.ipc.bw.jaxb.UserGroupResponse;
import com.comitfs.cas.ipc.bw.jaxb.UserGroupType;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.UserGroupInfo;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.util.ArrayList;

public class FetchUserGroups {
    private final Logger LOG = Logger.getLogger(FetchUserGroups.class);

    private final IPCSite ipcSite;

    public FetchUserGroups(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
    }

    public ArrayList<UserGroupInfo> fetchUserGroups() {

        ArrayList<UserGroupInfo> userGroupInfoList = new ArrayList<>();
        try {
            String apiVersion = ipcSite.getApiVersion();
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();
            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.FETCH_USER_GROUPS.toString(), null, ipcSite.isSecure());

            LOG.debug(" Fetch User Groups URL: " + resourceUrl);


            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));


            if (respEntity == null) {
                LOG.error("Response Entity for FecthUserGroups  is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error("FetchUserGroups error response " + formattedResponseXML);
                return null;

            }
            if (respEntity instanceof UserGroupResponse) {
                UserGroupResponse responseExJaxbBean = (UserGroupResponse) respEntity;

                LOG.debug("FetchUserGroups ResponseDesc: " + responseExJaxbBean.getReason().getReasonDescription());

                UserGroupListType userGroupListType = responseExJaxbBean.getUserGroups();
                ArrayList<UserGroupType> userGroupTypes = (ArrayList<UserGroupType>) userGroupListType.getUserGroup();
                LOG.debug("Fetched UserGroups size: " + userGroupTypes.size());


                for (UserGroupType userGroup : userGroupTypes) {
                    UserGroupInfo groupInfo = new UserGroupInfo();
                    groupInfo.setId(String.valueOf(userGroup.getId()));
                    groupInfo.setName(userGroup.getName());
                    groupInfo.setType(userGroup.getType().value());

                    userGroupInfoList.add(groupInfo);
                }
            }

            return userGroupInfoList;
        } catch (Exception e) {
            LOG.error("Exception fetching user groups info: ", e);
        }
        return userGroupInfoList;

    }

}
