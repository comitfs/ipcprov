package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;

import com.comitfs.cas.ipc.bw.jaxb.AuthDomainDataResponse;
import com.comitfs.cas.ipc.bw.jaxb.AuthDomainInformation;
import com.comitfs.cas.ipc.bw.jaxb.AuthDomainList;
import com.comitfs.cas.ipc.bw.jaxb.StandardErrorResponse;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.util.WebAPIUserStub;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.AuthDomainInfo;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.util.ArrayList;

public class FetchAuthDomains {
    private final Logger LOG = Logger.getLogger(FetchAuthDomains.class);

    private final IPCSite ipcSite;

    public FetchAuthDomains(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
    }

    public ArrayList<AuthDomainInfo> fetchAuthDomains() {

        ArrayList<AuthDomainInfo> authDomainInfoList = new ArrayList<AuthDomainInfo>();

        try {
            String apiVersion = ipcSite.getApiVersion();

            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.FETCH_AUTH_DOMAIN.toString(), null, ipcSite.isSecure());
            LOG.debug(" Fetch AuthDomain URL: " + resourceUrl);


            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));


            if (respEntity == null) {
                LOG.error("Response Entity for FetchAuthDomain  is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error("FetchAuthDomain error response " + formattedResponseXML);
                return null;

            }
            if (respEntity instanceof AuthDomainDataResponse) {
                AuthDomainDataResponse responseExJaxbBean = (AuthDomainDataResponse) respEntity;

                LOG.debug("FetchAuthDomain fetched with ResponseDesc: " + responseExJaxbBean.getReason().getReasonDescription());

                AuthDomainList authDomainList = responseExJaxbBean.getAuthDomainList();
                ArrayList<AuthDomainInformation> authDomainInformationList = (ArrayList<AuthDomainInformation>) authDomainList.getAuthDomain();
                LOG.debug("FetchAuthDomainList size: " + authDomainList.getAuthDomain().size());

                if (authDomainInformationList != null && authDomainInformationList.size() > 0) {

                    for (AuthDomainInformation authInfo : authDomainInformationList) {
                        AuthDomainInfo authDInfo = new AuthDomainInfo();
                        authDInfo.setAuthDomainId(String.valueOf(authInfo.getId()));
                        authDInfo.setAuthDomainName(authInfo.getDomainName());

                        authDomainInfoList.add(authDInfo);
                    }
                }
            }
            return authDomainInfoList;
        } catch (Exception e) {
            LOG.error("Exception fetching auth domain info: ", e);
            return authDomainInfoList;
        }
    }


}
