package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.FavoritesPageNameListType;
import com.comitfs.cas.ipc.bw.jaxb.FavoritesPageNameType;
import com.comitfs.cas.ipc.bw.jaxb.FetchFavoritesPageNamesResponse;
import com.comitfs.cas.ipc.bw.jaxb.StandardErrorResponse;
import com.comitfs.cas.plugin.ipcprov.core.BWPoolConnection;
import com.comitfs.cas.plugin.ipcprov.core.BWZoneSelector;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.session.WebAPIUser;
import com.comitfs.cas.plugin.ipcprov.util.*;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.TurretPageBean;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.util.ArrayList;

public class FetchTurretPages {
    private final Logger LOG = Logger.getLogger(FetchTurretPages.class);

    private final IPCSite ipcSite;

    public FetchTurretPages(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
    }


    public ArrayList<TurretPageBean> fetchTurretPages(String loginName) {

        ArrayList<TurretPageBean> turretPages = new ArrayList<>();
        try {
            IPCUser ipcUser = IPCDataService.getInstance().getUserById(loginName,ipcSite.getId());
            String apiVersion = ipcSite.getApiVersion();
            BWPoolConnection bwPoolConnection = new BWZoneSelector(ipcSite).getActiveBWZone();
            WebAPIUser webAPIUser = new WebAPIUserStub(ipcSite).getCTIWebAPIUser();
            SessionManager sessionManager = new SessionManager(webAPIUser, ipcSite, bwPoolConnection);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();
            String resourceUrl = IPCUtil.formURI(bwPoolConnection, IPCRESTAPI.FAVORITE_TURRET_PAGES.toString(), null, ipcSite.isSecure());

            resourceUrl = resourceUrl + "?finduserby=userId&value=" + ipcUser.getIPCId();
            LOG.debug(" Fetch Turret Pages URL: " + resourceUrl);


            Response response = IPCProvHttpClient.invokeHTTPGET(resourceUrl, ipcAuthToken, apiVersion, ipcSite.isSecure());

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));


            if (respEntity == null) {
                LOG.error("Response Entity for FetchTurretPages  is null.");
                return null;
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                LOG.error("FetchTurretPages error response " + formattedResponseXML);
                return null;

            }
            if (respEntity instanceof FetchFavoritesPageNamesResponse) {
                FetchFavoritesPageNamesResponse responseExJaxbBean = (FetchFavoritesPageNamesResponse) respEntity;

                LOG.debug("FetchTurretPages fetched with ResponseDesc: " + responseExJaxbBean.getReason().getReasonDescription());

                FavoritesPageNameListType favoritesPageNameListType = responseExJaxbBean.getFavoritesPageNameList();
                LOG.debug("FetchTurretPages size: " + favoritesPageNameListType.getFavoritesPageName().size());

                if (favoritesPageNameListType.getFavoritesPageName() != null && favoritesPageNameListType.getFavoritesPageName().size() > 0) {
                    ArrayList<FavoritesPageNameType> favoritesPageNameTypeList = (ArrayList<FavoritesPageNameType>) favoritesPageNameListType.getFavoritesPageName();

                    for (FavoritesPageNameType favoritesPageNameType : favoritesPageNameTypeList) {
                        TurretPageBean turretPage = new TurretPageBean();
                        turretPage.setId(favoritesPageNameType.getId());
                        turretPage.setPageName(favoritesPageNameType.getPageName());
                        turretPage.setPageNumber(favoritesPageNameType.getPageNumber());
                        turretPage.setParentUserMercuryId(favoritesPageNameType.getParentUserMercuryId());
                        turretPages.add(turretPage);
                    }
                }
            }
            return turretPages;
        } catch (Exception e) {
            LOG.error("Exception fetching turret page info: ", e);
            return turretPages;
        }
    }
}
