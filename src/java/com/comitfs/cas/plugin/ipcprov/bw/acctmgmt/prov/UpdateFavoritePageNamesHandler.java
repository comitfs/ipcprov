package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.UserAttributes;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.dataservice.UserInformation;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.UpdateFavPageNamesBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.UpdateFavoritesPageNamesBeanList;

import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;

public class UpdateFavoritePageNamesHandler {
    public static final String INTERNAL_SERVER_ERROR = "Internal server error";
    public static final String REQ_FAILURE = "REQ_FAILURE";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(UpdateFavoritePageNamesHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    public UpdateFavoritePageNamesHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String loginName, UpdateFavoritesPageNamesBeanList updateFavoritesPageNamesBeanList) {
        ResponseDataBean responseDataBean = new ResponseDataBean();
        FavoritesPageNameListType favoritesPageNameListType = null;
        try {
            String apiVersion = ipcSite.getApiVersion();

            IPCUser ipcUser = IPCDataService.getInstance().getUserById(loginName,ipcSite.getId());

            //Fetch UserInformation to pull UserCDIId
            UserInformation userInformation = new UserInformation(ipcSite);
            UserAttributes userAttributes = userInformation.fetchUser(loginName);

            if (userAttributes != null) {
                ipcUser.setUserCDIId(userAttributes.getUserCDIID());
            }

            UpdateFavoritesPageNames updateFavoritesPageNames = new UpdateFavoritesPageNames();

            updateFavoritesPageNames.setUserId(ipcUser.getIPCId());
            updateFavoritesPageNames.setUserCdiId(ipcUser.getUserCDIId());

            if (updateFavoritesPageNamesBeanList.getUpdateFavPageNamesBeanList().size() > 0) {
                favoritesPageNameListType = new FavoritesPageNameListType();

                for (UpdateFavPageNamesBean favPageNamesBean : updateFavoritesPageNamesBeanList.getUpdateFavPageNamesBeanList()) {
                    FavoritesPageNameType favoritesPageNameType = new FavoritesPageNameType();

                    if (favPageNamesBean.getId() != null && !favPageNamesBean.getId().isEmpty()) {
                        favoritesPageNameType.setId(Integer.valueOf(favPageNamesBean.getId()));
                    }

                    if (favPageNamesBean.getPageName() != null && !favPageNamesBean.getPageName().isEmpty()) {
                        favoritesPageNameType.setPageName(favPageNamesBean.getPageName());
                    }

                    if (favPageNamesBean.getPageNumber() != null && !favPageNamesBean.getPageNumber().isEmpty()) {
                        favoritesPageNameType.setPageNumber(Integer.valueOf(favPageNamesBean.getPageNumber()));
                    }

                    if (favPageNamesBean.getParentUserMercuryId() != null && !favPageNamesBean.getParentUserMercuryId().isEmpty()) {
                        favoritesPageNameType.setParentUserMercuryId(Integer.valueOf(favPageNamesBean.getParentUserMercuryId()));
                    }

                    favoritesPageNameListType.getFavoritesPageName().add(favoritesPageNameType);
                }
            }

            updateFavoritesPageNames.setFavoritesPageNameList(favoritesPageNameListType);

            String updateFavTurretPagesRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(updateFavoritesPageNames));
            log.debug(String.format("updateFavoritesPageNames request:" + updateFavTurretPagesRequestString));

            //TODO: acctmgmt usrname & pwd shd be retrieved from ipc site settings table based on the user Id passed as input

            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();

            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());

            String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.UPDATE_FAVORITE_TURRET_PAGES.toString());
            log.debug("created resourceurl for update favorite turret pages: " + resourceUrl);

            Response response = IPCProvHttpClient.invokeHTTPPut(resourceUrl,
                    httpAuth, ipcAuthToken, apiVersion, updateFavoritesPageNames, ipcSite.isSecure());

            log.debug("Update Favorites Page Names Response: " + response);

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for Update Favorites Page Names is null.");
                return toErrorResponseDataBean(responseDataBean, "Update Favorites Page Names response is null");
            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("Update Favorites Page Names error response " + formattedResponseXML);
                responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
            } else if (respEntity instanceof UpdateFavoritesPageNamesResponse) {
                UpdateFavoritesPageNamesResponse updateFavoritesPageNamesResponse = (UpdateFavoritesPageNamesResponse) respEntity;
                String updateFavPageNamesRespString = UtilHelper.marshalJAXBObjectForResponse(updateFavoritesPageNamesResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(updateFavPageNamesRespString);
                log.debug("Update Favorites Page Names response " + formattedResponseXML);

                ReasonData reasonData = updateFavoritesPageNamesResponse.getReason();
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);
                responseDataBean.setReasonCode(reasonData.getReasonCode().value());
                responseDataBean.setReasonDescription(reasonData.getReasonDescription());

            }
        } catch (Exception e) {
            log.debug("Exception in UpdateFavoritePageNamesHandler : ", e);
            return toErrorResponseDataBean(responseDataBean, INTERNAL_SERVER_ERROR);
        }
        return responseDataBean;
    }

    private ResponseDataBean toErrorResponseDataBean(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(errorField);
        return responseDataBean;
    }
}
