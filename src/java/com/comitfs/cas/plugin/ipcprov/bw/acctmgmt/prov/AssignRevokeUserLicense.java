package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.AssignRevokeLicenseBean;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.ArrayList;

public class AssignRevokeUserLicense {

    public static final String INTERNAL_SERVER_ERROR = "Internal server error";
    public static final String REQ_FAILURE = "REQ_FAILURE";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(AssignRevokeUserLicense.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    public AssignRevokeUserLicense(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handleResponse(String casUserName, AssignRevokeLicenseBean licenseBean) {
        log.debug("handling AssignRevokeUserLicense - ResponseDataBean");
        ResponseDataBean responseDataBean = new ResponseDataBean();
        try {
            IPCUser ipcUser = IPCDataService.getInstance().getUserById(casUserName,ipcSite.getId());
            String apiVer = ipcSite.getApiVersion();

            LicenseManagement licenseManagement = new LicenseManagement();

            UserIdentifier user = new UserIdentifier();
            if(null == ipcUser || ipcUser.getIPCId() == null) {
            	  user.setLoginName(casUserName);
            }
            else if (ipcUser.getIPCId() != null) {
                user.setUserId(BigInteger.valueOf(ipcUser.getIPCId()));
            } else if (ipcUser.getUserId() != null) {
                user.setLoginName(ipcUser.getUserId());
            }
            licenseManagement.setUserIdentifier(user);

            if (licenseBean.getLicenseFeatureCode() != null && licenseBean.getLicenseFeatureCode().size() > 0) {
                ArrayList<String> licenseCodes = (ArrayList) licenseBean.getLicenseFeatureCode();

                for (String license : licenseCodes) {
                    if (license != null && !license.isEmpty()) {
                        if (LicenseFeatureCodeTypes.fromValue(license) != null) {
                            licenseManagement.setLicenseFeatureCode(LicenseFeatureCodeTypes.fromValue(license));
                        }
                    }
                }
            }
            String assignRevokeLicensesRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(licenseManagement));
            log.debug(String.format("AssignRevokeLicensesRequestString:\n" + assignRevokeLicensesRequestString));

            //TODO: acctmgmt usrname & pwd shd be retrieved from ipc site settings table based on the user Id passed as input
            //create session with the above retrieved acct mgmt usr/pwd from ipcsite table
            AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
            SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
            String ipcAuthToken = sessionManager.getIpcProvAuthToken();
            log.debug("ipcAuthToken in assign revoke end  user license handler after creating session: " + ipcAuthToken);

            //after creating session, invoke httpsput
            String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());

            String resourceUrl = null;
            if (licenseBean.getOperationType().equalsIgnoreCase("assign")) {
                resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.ASSIGN_LICENSE.toString());
            } else if (licenseBean.getOperationType().equalsIgnoreCase("revoke")) {
                resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.REVOKE_LICENSE.toString());
            }
            log.debug("created resourceurl for assign/revoke license: " + resourceUrl);

            Response response = IPCProvHttpClient.httpsPut(resourceUrl,
                    httpAuth, ipcAuthToken, apiVer, licenseManagement);

            log.debug("Assignment/Revoke License Response: " + response);

            String apiRes = response.readEntity(String.class);
            JAXBContext jaxbContext = UtilHelper.getJAXBContext();
            Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

            if (respEntity == null) {
                log.error("Response Entity for Assign/Revoke License is null.");
                return toErrorResponseDataBean(responseDataBean, "Assign/Revoke LicenseRequest response is null");

            } else if (respEntity instanceof StandardErrorResponse) {
                StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                log.error("Assign/Revoke License error response " + formattedResponseXML);
                responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
            } else if (respEntity instanceof LicenseManagementResponse) {
                LicenseManagementResponse licenseMgmtResponse = (LicenseManagementResponse) respEntity;
                String licenseMgmtRespString = UtilHelper.marshalJAXBObjectForResponse(licenseMgmtResponse);
                String formattedResponseXML = UtilHelper.getFormattedXMLString(licenseMgmtRespString);
                log.debug("LicenseMgmtResponse response " + formattedResponseXML);

                log.debug("going to invoke delete session");
                sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVer);
                log.debug("delete session done, returning final response");
                ReasonData reasonData = licenseMgmtResponse.getReason();
                responseDataBean.setReasonCode(reasonData.getReasonCode().value());
                responseDataBean.setReasonDescription(reasonData.getReasonDescription());
            }
        } catch (Exception e) {
            log.error("Exception in AssignRevokeLicence handler : ", e);
            return toErrorResponseDataBean(responseDataBean, INTERNAL_SERVER_ERROR);
        }
        return responseDataBean;
    }

    private ResponseDataBean toErrorResponseDataBean(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(errorField);
        return responseDataBean;
    }
}