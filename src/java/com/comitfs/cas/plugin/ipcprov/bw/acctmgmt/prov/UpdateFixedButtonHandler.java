package com.comitfs.cas.plugin.ipcprov.bw.acctmgmt.prov;


import com.comitfs.cas.ipc.bw.jaxb.*;
import com.comitfs.cas.plugin.ipcprov.beans.UserAttributes;
import com.comitfs.cas.plugin.ipcprov.core.EndUserAcctMgmtUserStub;
import com.comitfs.cas.plugin.ipcprov.core.IPCSite;
import com.comitfs.cas.plugin.ipcprov.dataservice.IPCDataService;
import com.comitfs.cas.plugin.ipcprov.dataservice.UserInformation;
import com.comitfs.cas.plugin.ipcprov.rest.IPCRESTAPI;
import com.comitfs.cas.plugin.ipcprov.session.AccountManagementUser;
import com.comitfs.cas.plugin.ipcprov.session.SessionManager;
import com.comitfs.cas.plugin.ipcprov.util.IPCProvHttpClient;
import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.comitfs.cas.plugin.ipcprov.util.IPCUtil;
import com.comitfs.cas.plugin.ipcprov.util.UtilHelper;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.FixedButtonBean;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.FixedButtonBeanList;
import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.ResponseDataBean;

import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.ArrayList;

public class UpdateFixedButtonHandler {
    public static final String INTERNAL_SERVER_ERROR = "Internal server error";
    public static final String REQ_FAILURE = "REQ_FAILURE";
    private final IPCSite ipcSite;
    private final Logger log = Logger.getLogger(UpdateFixedButtonHandler.class);
    private final EndUserAcctMgmtUserStub endUserAcctMgmtUserStub;

    public UpdateFixedButtonHandler(IPCSite ipcSite) {
        this.ipcSite = ipcSite;
        this.endUserAcctMgmtUserStub = new EndUserAcctMgmtUserStub(ipcSite);
    }

    public ResponseDataBean handle(String loginName, FixedButtonBeanList fixedButtonBeanList) {
        ResponseDataBean responseDataBean = new ResponseDataBean();

        try {
            String apiVersion = ipcSite.getApiVersion();

            IPCUser ipcUser = IPCDataService.getInstance().getUserById(loginName,ipcSite.getId());
            if (ipcUser != null) {

                //Fetch UserInformation to pull UserCDIId
                UserInformation userInformation = new UserInformation(ipcSite);
                UserAttributes userAttributes = userInformation.fetchUser(loginName);

                if (userAttributes != null) {
                    ipcUser.setUserCDIId(userAttributes.getUserCDIID());
                }

                UpdateFixedButtonRequest updateFixedButtonReq = new UpdateFixedButtonRequest();

                UserIdentifier userIdentifier = new UserIdentifier();
                userIdentifier.setUserId(BigInteger.valueOf(ipcUser.getIPCId()));

                updateFixedButtonReq.setUserIdentifier(userIdentifier);

                FixedButtonList fbList = new FixedButtonList();
                ArrayList<FixedButtonBean> fixedButtonBeans = (ArrayList<FixedButtonBean>) fixedButtonBeanList.getFixedButtonBeans();
                if (fixedButtonBeans.size() > 0) {
                    for (FixedButtonBean buttonBean : fixedButtonBeans) {

                        FixedButton fb = new FixedButton();

                        if (buttonBean.getButtonId() != null && !buttonBean.getButtonId().isEmpty()) {
                            fb.setButtonId(Integer.valueOf(buttonBean.getButtonId()));
                        }
                        if (buttonBean.getButtonLabel() != null) {
                            fb.setButtonLabel(buttonBean.getButtonLabel());
                        }
                        if ((buttonBean.getButtonNumber() != null && !buttonBean.getButtonNumber().isEmpty())) {
                            fb.setButtonNumber(Integer.valueOf(buttonBean.getButtonNumber()));
                        }
                        if (buttonBean.getButtonType() != null && !buttonBean.getButtonType().isEmpty()) {
                            fb.setButtonType(buttonBean.getButtonType());
                        }
                        if (buttonBean.getSlotPosition() != null && !buttonBean.getSlotPosition().isEmpty()) {
                            fb.setSlotPosition(Integer.valueOf(buttonBean.getSlotPosition()));
                        }

                        if (buttonBean.getUserCDIId() != null && !buttonBean.getUserCDIId().isEmpty()) {
                            fb.setUserCDIId(Integer.valueOf(buttonBean.getUserCDIId()));
                        } else {
                            fb.setUserCDIId(ipcUser.getUserCDIId());
                        }

                        fb.setUserId(ipcUser.getIPCId());

                        if (buttonBean.getUserMercuryId() != null && !buttonBean.getUserMercuryId().isEmpty()) {
                            fb.setUserMercuryId(Integer.valueOf(buttonBean.getUserMercuryId()));
                        }
                        fbList.getFixedButton().add(fb);
                    }

                }
                updateFixedButtonReq.setFixedButtonList(fbList);

                String updateFixedButtonRequestString = UtilHelper.getFormattedXMLString(UtilHelper.marshalJAXBObjectForRequest(updateFixedButtonReq));
                log.debug(String.format("Update Fixed Button Request:" + updateFixedButtonRequestString));

                //TODO: acctmgmt usrname & pwd shd be retrieved from ipc site settings table based on the user Id passed as input

                //create session with the above retrieved acct mgmt usr/pwd from ipcsite table
                AccountManagementUser accountManagementUser = endUserAcctMgmtUserStub.getAcctMgmtUser();
                SessionManager sessionManager = new SessionManager(accountManagementUser, ipcSite);
                String ipcAuthToken = sessionManager.getIpcProvAuthToken();

                //after creating session, invoke httpsput
                String httpAuth = IPCUtil.generateHttpAuthorization(ipcSite.getAcctMgmtUser(), ipcSite.getAcctMgmtPwd());

                String resourceUrl = IPCUtil.createSecureURI(ipcSite, IPCRESTAPI.UPDATE_FIXED_BUTTON.toString());
                log.debug("created resourceurl: " + resourceUrl);

                Response response = IPCProvHttpClient.httpsPut(resourceUrl,
                        httpAuth, ipcAuthToken, apiVersion, updateFixedButtonReq);

                log.debug("UpdateFixedButton User Response: " + response);

                String apiRes = response.readEntity(String.class);
                JAXBContext jaxbContext = UtilHelper.getJAXBContext();
                Object respEntity = jaxbContext.createUnmarshaller().unmarshal(new StringReader(apiRes));

                if (respEntity == null) {
                    log.error("Response Entity for UpdateFixedButton is null.");
                    return toErrorResponseDataBean(responseDataBean, "UpdateFixedButton response is null");
                } else if (respEntity instanceof StandardErrorResponse) {
                    StandardErrorResponse errorResp = (StandardErrorResponse) respEntity;
                    String errorRespString = UtilHelper.marshalJAXBObjectForResponse(errorResp);
                    String formattedResponseXML = UtilHelper.getFormattedXMLString(errorRespString);
                    log.error("UpdateFixedButton error response " + formattedResponseXML);
                    responseDataBean.setReasonCode(errorResp.getReason().getReasonCode().value());
                    responseDataBean.setReasonDescription(errorResp.getReason().getReasonDescription());
                } else if (respEntity instanceof CreateUserResponse) {
                    UpdateFixedButtonResponse updateFixedButtonResp = (UpdateFixedButtonResponse) respEntity;
                    String updateFixedButtonRespString = UtilHelper.marshalJAXBObjectForResponse(updateFixedButtonResp);
                    String formattedResponseXML = UtilHelper.getFormattedXMLString(updateFixedButtonRespString);
                    log.debug("UpdateFixedButtonResponse " + formattedResponseXML);

                    ReasonData reasonData = updateFixedButtonResp.getReason();
                    sessionManager.deleteSession(httpAuth, ipcAuthToken, apiVersion);
                    responseDataBean.setReasonCode(reasonData.getReasonCode().value());
                    responseDataBean.setReasonDescription(reasonData.getReasonDescription());

                }
            }
        } catch (Exception e) {
            log.debug("Exception in UpdateFixedButton handle(): ", e);
            return toErrorResponseDataBean(responseDataBean, INTERNAL_SERVER_ERROR);
        }
        return responseDataBean;
    }

    private ResponseDataBean toErrorResponseDataBean(ResponseDataBean responseDataBean, String errorField) {
        responseDataBean.setReasonCode(REQ_FAILURE);
        responseDataBean.setReasonDescription(errorField);
        return responseDataBean;
    }

}
