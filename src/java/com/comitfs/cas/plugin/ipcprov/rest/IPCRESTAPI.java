package com.comitfs.cas.plugin.ipcprov.rest;

public enum IPCRESTAPI {
    SESSION_API("/svc/bw/session/"),
    DATA_API("/svc/bw/data/"),
    USR_ACCT_MGT("/svc/bw/acctmgmt/user/"),
    USR_TRADER_FEATURES("/svc/bw/acctmgmt/user/trader-features"),
    CHANGE_USR_PASSWORD("/svc/bw/acctmgmt/user/password"),
    BUTTON_SETTINGS("/svc/bw/acctmgmt/user/button"),
    CREATE_USR("/svc/bw/acctmgmt/user"),
    LOGIN_LOGOUT("/svc/bw/acctmgmt/user/cdi-session"),
    ASSIGN_ROLE("/svc/bw/acctmgmt/user/role-list?operation=add"),
    REMOVE_ROLE("/svc/bw/acctmgmt/user/role-list?operation=delete"),
    ASSIGN_LICENSE("/svc/bw/acctmgmt/user/license?operation=assign"),
    REVOKE_LICENSE("/svc/bw/acctmgmt/user/license?operation=revoke"),
    ADDUSR_ENDUSRGROUP("/svc/bw/acctmgmt/user/group-membership?operation=add"),
    REMOVEUSR_ENDUSRGROUP("/svc/bw/acctmgmt/user/group-membership?operation=remove"),
    FETCH_BUTTONINFO("/svc/bw/data/user/button?finduserby=loginName&uservalue="),
    FIND_BUTTONS_BY("&findbuttonsby=type&buttonsearchvalue="),
    FETCH_USERINFO("/svc/bw/data/user?userid="),
    AUDIO_PREFERENCES("/svc/bw/acctmgmt/user/audio-preferences"),
    ENTERPRISE_CONTACTS("/svc/bw/data/enterprise-contact?filtertype=page&pagenum="),
    DEVICE_INVENTORY("/svc/bw/data/inventory/device?"),
    LOGONSESSION_INFO("/svc/bw/data/user/logonsession?timezone=EST&timeformat=absolute&querystr="),
    RESOURCEAOR_INFO("/svc/bw/data/resourceaor?filtertype=page"),
    USER_PREF_WITH_LOGIN_NAME("user/preferences?finduserby=loginName&value="),
    COMMUNICATION_HISTORY("/svc/bw/data/communication-history?"),
    UPDATE_FIXED_BUTTON("/svc/bw/acctmgmt/user/fixedbutton"),
    FAVORITE_TURRET_PAGES("/svc/bw/data/user/favoritespagenames"),
    UPDATE_FAVORITE_TURRET_PAGES("/svc/bw/acctmgmt/user/favoritespagenames"),
    FETCH_FIXED_BUTTONS("/svc/bw/data/user/fixedbutton"),
    FETCH_AUTH_DOMAIN("/svc/bw/data/authentication-domain"),
    FETCH_ROLES("/svc/bw/data/role"),
    FETCH_USER_GROUPS("/svc/bw/data/group"),
    FETCH_RECORDER_PROFILES("/svc/bw/data/recording-profile"),
    FETCH_SPEAKERS("/svc/bw/data/user/speakerchannel?finduserby=userId&value="),
    LICENSE_INFORMATION("/svc/bw/data/license?querystr=");
	
    private final String text;

    IPCRESTAPI(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
