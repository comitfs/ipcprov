package com.comitfs.cas.plugin.ipcprov.beans;


public class AddRemoveUsrFromGroupBean {

    private String operationType;
    private String groupId;
    private String groupName;
    private String retainConfiguration;

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getRetainConfiguration() {
        return retainConfiguration;
    }

    public void setRetainConfiguration(String retainConfiguration) {
        this.retainConfiguration = retainConfiguration;
    }
}
