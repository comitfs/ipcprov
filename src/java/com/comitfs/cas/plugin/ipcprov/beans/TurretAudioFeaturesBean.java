package com.comitfs.cas.plugin.ipcprov.beans;


public class TurretAudioFeaturesBean {

    private String highPriorityRing;
    private String lowPriorityRing;
    private String speakerMasterRxVolume2;
    private String speakerMasterRxVolume3;
    private String lineInMixerOutput;

    public String getHighPriorityRing() {
        return highPriorityRing;
    }

    public void setHighPriorityRing(String highPriorityRing) {
        this.highPriorityRing = highPriorityRing;
    }

    public String getLowPriorityRing() {
        return lowPriorityRing;
    }

    public void setLowPriorityRing(String lowPriorityRing) {
        this.lowPriorityRing = lowPriorityRing;
    }

    public String getSpeakerMasterRxVolume2() {
        return speakerMasterRxVolume2;
    }

    public void setSpeakerMasterRxVolume2(String speakerMasterRxVolume2) {
        this.speakerMasterRxVolume2 = speakerMasterRxVolume2;
    }

    public String getSpeakerMasterRxVolume3() {
        return speakerMasterRxVolume3;
    }

    public void setSpeakerMasterRxVolume3(String speakerMasterRxVolume3) {
        this.speakerMasterRxVolume3 = speakerMasterRxVolume3;
    }

    public String getLineInMixerOutput() {
        return lineInMixerOutput;
    }

    public void setLineInMixerOutput(String lineInMixerOutput) {
        this.lineInMixerOutput = lineInMixerOutput;
    }

}
