package com.comitfs.cas.plugin.ipcprov.beans;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
public class LicenseFeatureDataBean implements Serializable {
    protected String featureSource;
    protected String licenseClass;
    protected String featureName;
    protected String featureDescription;
    protected Integer zoneId;
    protected Integer userId;
    private String featureCode;

    public LicenseFeatureDataBean(Integer userId, Integer zoneId, String featureName, String featureDescription
            , String featureCode, String licenseClass, String featureSource) {
        this.featureSource = featureSource;
        this.licenseClass = licenseClass;
        this.featureName = featureName;
        this.featureDescription = featureDescription;
        this.zoneId = zoneId;
        this.userId = userId;
        this.featureCode = featureCode;
    }
}
