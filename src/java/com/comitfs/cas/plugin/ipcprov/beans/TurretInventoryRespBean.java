package com.comitfs.cas.plugin.ipcprov.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class TurretInventoryRespBean {
    @JsonIgnore
    int feedback;

    @JsonIgnore
    String respDesc;

    int pageNum;
    int totalPages;
    Set<DeviceDataBean> devices;

    public TurretInventoryRespBean(int feedback, String respDesc, int pageNum, int totalPages, Set<DeviceDataBean> deviceDataBeanList) {
        this.feedback = feedback;
        this.respDesc = respDesc;
        this.pageNum = pageNum;
        this.totalPages = totalPages;
        this.devices = deviceDataBeanList;
    }
}
