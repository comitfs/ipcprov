package com.comitfs.cas.plugin.ipcprov.beans;

import java.util.ArrayList;

import com.comitfs.cas.ipc.bw.jaxb.UserSpeakerChannel;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SpeakerChannelAttributes {
	 Integer ipcId;
	 ArrayList<UserSpeakerChannel> speakerList;
	public Integer getIpcId() {
		return ipcId;
	}
	public void setIpcId(Integer ipcId) {
		this.ipcId = ipcId;
	}
	public ArrayList<UserSpeakerChannel> getSpeakerList() {
		return speakerList;
	}
	public void setSpeakerList(ArrayList<UserSpeakerChannel> speakerList) {
		this.speakerList = speakerList;
	}
	 
	 

}
