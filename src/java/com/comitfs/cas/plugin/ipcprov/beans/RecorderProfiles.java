package com.comitfs.cas.plugin.ipcprov.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@XmlRootElement(name="recorderProfiles")
public class RecorderProfiles implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<RecorderProfile> recorderProfiles;
	
	public List<RecorderProfile> getRecorderProfiles() {
		if(recorderProfiles != null) {
			recorderProfiles = new ArrayList<RecorderProfile>();
		}
		return recorderProfiles;
	}
	public void setRecorderProfiles(List<RecorderProfile> recorderProfiles) {
		this.recorderProfiles = recorderProfiles;
	}
	public RecorderProfiles(List<RecorderProfile> recorderProfiles) {
		this.recorderProfiles = recorderProfiles;
	}
	
	

}
