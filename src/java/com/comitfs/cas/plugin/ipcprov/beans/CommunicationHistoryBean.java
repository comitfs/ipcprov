package com.comitfs.cas.plugin.ipcprov.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommunicationHistoryBean {
    @JsonProperty
    private int id;

    @JsonProperty
    private int deviceIdId;

    @JsonProperty
    private int buttonNumber;

    @JsonProperty
    private int duration;

    @JsonProperty
    private int pttDuration;

    @JsonProperty
    private int ringTime;

    @JsonProperty
    private int userId;

    @JsonProperty
    private int parentUserCDIId;

    @JsonProperty
    private int appearance;

    @JsonProperty
    private int trunkId;

    @JsonProperty
    private int trunkBChannel;

    @JsonProperty
    private int personalPointOfContactId;

    @JsonProperty
    private int pointOfContactId;

    @JsonProperty
    private int resourceAORId;

    @JsonProperty
    private String answeringPartyName;

    @JsonProperty
    private String cliName;

    @JsonProperty
    private String cliNumber;

    @JsonProperty
    private String lineNumber;

    @JsonProperty
    private String callUsage;

    @JsonProperty
    private String destination;

    @JsonProperty
    private String deviceChannel;

    @JsonProperty
    private String deviceChannelType;

    @JsonProperty
    private String eventType;

    @JsonProperty
    private String reasonForDisconnect;

    @JsonProperty
    private String e164Destination;

    @JsonProperty
    private String lastModified;

    @JsonProperty
    private String startTime;

    @JsonProperty
    private String routedDestination;

    @JsonProperty
    private String userName;

    @JsonProperty
    private String enterpriseCallId;
}
