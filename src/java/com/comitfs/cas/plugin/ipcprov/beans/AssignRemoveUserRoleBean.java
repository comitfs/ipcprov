package com.comitfs.cas.plugin.ipcprov.beans;


import java.util.List;

public class AssignRemoveUserRoleBean {

    private UserIdentifierBean userIdentifierBean;
    private List<String> roleNames;
    private String operationType;

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public UserIdentifierBean getUserIdentifierBean() {
        return userIdentifierBean;
    }

    public void setUserIdentifierBean(UserIdentifierBean userIdentifierBean) {
        this.userIdentifierBean = userIdentifierBean;
    }

    public List<String> getRoleNames() {
        return roleNames;
    }

    public void setRoleNames(List<String> roleNames) {
        this.roleNames = roleNames;
    }

}

