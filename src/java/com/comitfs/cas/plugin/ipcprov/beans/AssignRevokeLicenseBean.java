package com.comitfs.cas.plugin.ipcprov.beans;


import java.util.List;

public class AssignRevokeLicenseBean {

    private UserIdentifierBean userIdentifierBean;

    private List<String> licenseFeatureCode;

    private String operationType;

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public UserIdentifierBean getUserIdentifierBean() {
        return userIdentifierBean;
    }

    public void setUserIdentifierBean(UserIdentifierBean userIdentifierBean) {
        this.userIdentifierBean = userIdentifierBean;
    }

    public List<String> getLicenseFeatureCode() {
        return licenseFeatureCode;
    }

    public void setLicenseFeatureCode(List<String> licenseFeatureCode) {
        this.licenseFeatureCode = licenseFeatureCode;
    }
}
