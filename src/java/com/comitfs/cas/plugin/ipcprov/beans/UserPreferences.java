package com.comitfs.cas.plugin.ipcprov.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.comitfs.cas.plugin.ipcprov.util.IPCUser;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
@XmlRootElement(name="userpreferences")
@JsonIgnoreProperties
public class UserPreferences implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<IPCUser> ipcUsers = new ArrayList<IPCUser>();
	
	
}
