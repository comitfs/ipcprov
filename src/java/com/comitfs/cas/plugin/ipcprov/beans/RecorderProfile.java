package com.comitfs.cas.plugin.ipcprov.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.NoArgsConstructor;


@NoArgsConstructor
@XmlRootElement(name="recorderProfile")
public class RecorderProfile implements Serializable{
	
	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
		private int id;
		private String name;
		private List<String> recordMixList;
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public List<String> getRecordMixList() {
			if(recordMixList == null) {
				recordMixList = new ArrayList<String>();
			}
			return recordMixList;
		}
		public void setRecordMixList(List<String> recordMixList) {
			this.recordMixList = recordMixList;
		}
		public RecorderProfile(int id, String name, List<String> recordMixList) {
			this.id = id;
			this.name = name;
			this.recordMixList = recordMixList;
		}
	    
	    

}
