package com.comitfs.cas.plugin.ipcprov.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeviceDataBean {
    private String mac;
    private String model;
    private String role;
    private String ip;
    private String loggedOnTraderName;
    private String loggedOnTraderState;
    private int zoneId;
    private String zoneName;
    private String ccmVIP;
    private String floor;
    private String turretSiteCode;
    private String location;
    private String timezone;
    private String deployed;

    public DeviceDataBean(String mac, String model, String role, String ip, String loggedOnTraderName
            , String loggedOnTraderState, int zoneId, String zoneName, String ccmVIP, String floor
            , String turretSiteCode, String location, String timezone, String deployed) {
        this.mac = mac;
        this.model = model;
        this.role = role;
        this.ip = ip;
        this.loggedOnTraderName = loggedOnTraderName;
        this.loggedOnTraderState = loggedOnTraderState;
        this.zoneId = zoneId;
        this.zoneName = zoneName;
        this.ccmVIP = ccmVIP;
        this.floor = floor;
        this.turretSiteCode = turretSiteCode;
        this.location = location;
        this.timezone = timezone;
        this.deployed = deployed;
    }
}
