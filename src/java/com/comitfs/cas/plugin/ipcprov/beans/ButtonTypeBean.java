package com.comitfs.cas.plugin.ipcprov.beans;


public enum ButtonTypeBean {
    INVALID_BUTTON_TYPE("InvalidButtonType"),
    RESOURCE("Resource"),
    RESOURCE_AND_SPEEDDIAL("ResourceAndSpeedDial"),
    HUNT_AND_SPEEDDIAL("HuntAndSpeedDial"),
    SPEEDDIAL("SpeedDial"),
    MESSAGE_WAITING_INDICATOR("MWI"),
    ONE_BUTTON_DIVERT("OneButtonDivert"),
    ONE_BUTTON_ICM_DIVERT("OneButtonICMDivert"),
    SIMPLEX_CONFERENCE("SimplexConference"),
    DUPLEX_CONFERENCE("DuplexConference"),
    KEY_SEQUENCE("KeySequence"),
    INTERCOM("ICM"),
    POINT_OF_CONTACT("PointOfContact"),
    PERSONAL_POINT_OF_CONTACT("PersonalPointOfContact");

    private final String value;

    ButtonTypeBean(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ButtonTypeBean fromValue(String v) {
        for (ButtonTypeBean c: ButtonTypeBean.values()) {
            if (c.value.equalsIgnoreCase(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
