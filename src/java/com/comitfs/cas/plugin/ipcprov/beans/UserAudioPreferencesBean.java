package com.comitfs.cas.plugin.ipcprov.beans;


public class UserAudioPreferencesBean {

    private String deviceType;
    private GeneralAudioPreferencesBean generalAudioPreferences;
    private TurretAudioFeaturesBean turretAudioFeatures;
    private IQMAXTouchAudioFeaturesBean iqMAXTouchAudioFeatures;
    private PulseAudioFeaturesBean pulseAudioFeatures;

    public PulseAudioFeaturesBean getPulseAudioFeatures() {
        return pulseAudioFeatures;
    }

    public void setPulseAudioFeatures(PulseAudioFeaturesBean pulseAudioFeatures) {
        this.pulseAudioFeatures = pulseAudioFeatures;
    }

    public IQMAXTouchAudioFeaturesBean getIqMAXTouchAudioFeatures() {
        return iqMAXTouchAudioFeatures;
    }

    public void setIqMAXTouchAudioFeatures(IQMAXTouchAudioFeaturesBean iqMAXTouchAudioFeatures) {
        this.iqMAXTouchAudioFeatures = iqMAXTouchAudioFeatures;
    }

    public TurretAudioFeaturesBean getTurretAudioFeatures() {
        return turretAudioFeatures;
    }

    public void setTurretAudioFeatures(TurretAudioFeaturesBean turretAudioFeatures) {
        this.turretAudioFeatures = turretAudioFeatures;
    }

    public GeneralAudioPreferencesBean getGeneralAudioPreferences() {
        return generalAudioPreferences;
    }

    public void setGeneralAudioPreferences(GeneralAudioPreferencesBean generalAudioPreferences) {
        this.generalAudioPreferences = generalAudioPreferences;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
