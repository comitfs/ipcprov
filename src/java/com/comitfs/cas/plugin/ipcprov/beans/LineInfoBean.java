package com.comitfs.cas.plugin.ipcprov.beans;


import com.fasterxml.jackson.annotation.JsonInclude;

public class LineInfoBean {
    private String lineid;
    private String type;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String privateLineType;

    private String resourceAOR;
    private String lastModified;
    private String cliNameOverride;
    private String cliNumberOverride;
    private String ringNoAnswerTime;
    private String automaticEstablishment;
    private String busyAppearanceThreshold;
    private String description;
    private String descriptor;
    private String displayStatusWhenOnSpkr;
    private String diversionReason;
    private String divertOnImmorBusytoExtension;
    private String divertOnRNAtoExtension;
    private String equipped;
    private String fallBackToDynamicRouting;
    private String forkingType;
    private String handsetMute;
    private String hoot;
    private String initiateCallOnSeize;
    private String unigyInstanceId;
    private String lineIsRecorded;
    private String maxAppearances;
    private String mediaBridgeRequired;
    private String micMute;
    private String minimalSignalRingTime;
    private String multiCDItoSingleCDIDelay;
    private String oOPSResourceId;
    private String parentEnterpriseId;
    private String publishedUserInfoBase;
    private String signaling;
    private String source;
    private String uiName;


    public String getLineid() {
        return lineid;
    }

    public void setLineid(String lineid) {
        this.lineid = lineid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResourceAOR() {
        return resourceAOR;
    }

    public void setResourceAOR(String resourceAOR) {
        this.resourceAOR = resourceAOR;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getCliNameOverride() {
        return cliNameOverride;
    }

    public void setCliNameOverride(String cliNameOverride) {
        this.cliNameOverride = cliNameOverride;
    }

    public String getCliNumberOverride() {
        return cliNumberOverride;
    }

    public void setCliNumberOverride(String cliNumberOverride) {
        this.cliNumberOverride = cliNumberOverride;
    }

    public String getRingNoAnswerTime() {
        return ringNoAnswerTime;
    }

    public void setRingNoAnswerTime(String ringNoAnswerTime) {
        this.ringNoAnswerTime = ringNoAnswerTime;
    }

    public String getAutomaticEstablishment() {
        return automaticEstablishment;
    }

    public void setAutomaticEstablishment(String automaticEstablishment) {
        this.automaticEstablishment = automaticEstablishment;
    }

    public String getBusyAppearanceThreshold() {
        return busyAppearanceThreshold;
    }

    public void setBusyAppearanceThreshold(String busyAppearanceThreshold) {
        this.busyAppearanceThreshold = busyAppearanceThreshold;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public String getDisplayStatusWhenOnSpkr() {
        return displayStatusWhenOnSpkr;
    }

    public void setDisplayStatusWhenOnSpkr(String displayStatusWhenOnSpkr) {
        this.displayStatusWhenOnSpkr = displayStatusWhenOnSpkr;
    }

    public String getDiversionReason() {
        return diversionReason;
    }

    public void setDiversionReason(String diversionReason) {
        this.diversionReason = diversionReason;
    }

    public String getDivertOnImmorBusytoExtension() {
        return divertOnImmorBusytoExtension;
    }

    public void setDivertOnImmorBusytoExtension(String divertOnImmorBusytoExtension) {
        this.divertOnImmorBusytoExtension = divertOnImmorBusytoExtension;
    }

    public String getDivertOnRNAtoExtension() {
        return divertOnRNAtoExtension;
    }

    public void setDivertOnRNAtoExtension(String divertOnRNAtoExtension) {
        this.divertOnRNAtoExtension = divertOnRNAtoExtension;
    }

    public String getEquipped() {
        return equipped;
    }

    public void setEquipped(String equipped) {
        this.equipped = equipped;
    }

    public String getFallBackToDynamicRouting() {
        return fallBackToDynamicRouting;
    }

    public void setFallBackToDynamicRouting(String fallBackToDynamicRouting) {
        this.fallBackToDynamicRouting = fallBackToDynamicRouting;
    }

    public String getForkingType() {
        return forkingType;
    }

    public void setForkingType(String forkingType) {
        this.forkingType = forkingType;
    }

    public String getHandsetMute() {
        return handsetMute;
    }

    public void setHandsetMute(String handsetMute) {
        this.handsetMute = handsetMute;
    }

    public String getHoot() {
        return hoot;
    }

    public void setHoot(String hoot) {
        this.hoot = hoot;
    }

    public String getInitiateCallOnSeize() {
        return initiateCallOnSeize;
    }

    public void setInitiateCallOnSeize(String initiateCallOnSeize) {
        this.initiateCallOnSeize = initiateCallOnSeize;
    }

    public String getUnigyInstanceId() {
        return unigyInstanceId;
    }

    public void setUnigyInstanceId(String unigyInstanceId) {
        this.unigyInstanceId = unigyInstanceId;
    }

    public String getLineIsRecorded() {
        return lineIsRecorded;
    }

    public void setLineIsRecorded(String lineIsRecorded) {
        this.lineIsRecorded = lineIsRecorded;
    }

    public String getMaxAppearances() {
        return maxAppearances;
    }

    public void setMaxAppearances(String maxAppearances) {
        this.maxAppearances = maxAppearances;
    }

    public String getMediaBridgeRequired() {
        return mediaBridgeRequired;
    }

    public void setMediaBridgeRequired(String mediaBridgeRequired) {
        this.mediaBridgeRequired = mediaBridgeRequired;
    }

    public String getMicMute() {
        return micMute;
    }

    public void setMicMute(String micMute) {
        this.micMute = micMute;
    }

    public String getMinimalSignalRingTime() {
        return minimalSignalRingTime;
    }

    public void setMinimalSignalRingTime(String minimalSignalRingTime) {
        this.minimalSignalRingTime = minimalSignalRingTime;
    }

    public String getMultiCDItoSingleCDIDelay() {
        return multiCDItoSingleCDIDelay;
    }

    public void setMultiCDItoSingleCDIDelay(String multiCDItoSingleCDIDelay) {
        this.multiCDItoSingleCDIDelay = multiCDItoSingleCDIDelay;
    }

    public String getoOPSResourceId() {
        return oOPSResourceId;
    }

    public void setoOPSResourceId(String oOPSResourceId) {
        this.oOPSResourceId = oOPSResourceId;
    }

    public String getParentEnterpriseId() {
        return parentEnterpriseId;
    }

    public void setParentEnterpriseId(String parentEnterpriseId) {
        this.parentEnterpriseId = parentEnterpriseId;
    }

    public String getPublishedUserInfoBase() {
        return publishedUserInfoBase;
    }

    public void setPublishedUserInfoBase(String publishedUserInfoBase) {
        this.publishedUserInfoBase = publishedUserInfoBase;
    }

    public String getSignaling() {
        return signaling;
    }

    public void setSignaling(String signaling) {
        this.signaling = signaling;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUiName() {
        return uiName;
    }

    public void setUiName(String uiName) {
        this.uiName = uiName;
    }

    public String getPrivateLineType() {
        return privateLineType;
    }

    public void setPrivateLineType(String privateLineType) {
        this.privateLineType = privateLineType;
    }
}
