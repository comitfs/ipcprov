package com.comitfs.cas.plugin.ipcprov.beans;


import java.util.ArrayList;

import com.comitfs.cas.plugin.ipcprov.view.restcontrollers.jsonbeans.POCContactInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class PersonalContactsBean {
    private String firstName;
    private String lastName;
    //@JsonIgnore
    private String personalContactId;
    private String mailingAddress;
    private String contactType;
    private String presenceUri;
    private String locale;

    private String company;
    private ArrayList<POCContactInfo> info;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonalContactId() {
        return personalContactId;
    }

    public void setPersonalContactId(String personalContactId) {
        this.personalContactId = personalContactId;
    }

    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getPresenceUri() {
        return presenceUri;
    }

    public void setPresenceUri(String presenceUri) {
        this.presenceUri = presenceUri;
    }

    public ArrayList<POCContactInfo> getInfo() {
        return info;
    }

    public void setInfo(ArrayList<POCContactInfo> info) {
        this.info = info;
    }
}
