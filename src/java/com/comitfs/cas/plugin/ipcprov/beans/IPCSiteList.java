package com.comitfs.cas.plugin.ipcprov.beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
@XmlRootElement(name="ipcsites")
@JsonIgnoreProperties
public class IPCSiteList {
	private List<IPCSiteBean> ipcSiteBeans = new ArrayList<IPCSiteBean>();
}
