package com.comitfs.cas.plugin.ipcprov.beans;


public class IQMAXTouchAudioFeaturesBean {

    private String highPriorityRing;
    private String lowPriorityRing;
    private String masterReceiveVolume;
    private String speakerSlideToLatchEnabled;
    private String speakerSoloModeAttenuation;

    public String getSpeakerSoloModeAttenuation() {
        return speakerSoloModeAttenuation;
    }

    public void setSpeakerSoloModeAttenuation(String speakerSoloModeAttenuation) {
        this.speakerSoloModeAttenuation = speakerSoloModeAttenuation;
    }

    public String getHighPriorityRing() {
        return highPriorityRing;
    }

    public void setHighPriorityRing(String highPriorityRing) {
        this.highPriorityRing = highPriorityRing;
    }

    public String getLowPriorityRing() {
        return lowPriorityRing;
    }

    public void setLowPriorityRing(String lowPriorityRing) {
        this.lowPriorityRing = lowPriorityRing;
    }

    public String getMasterReceiveVolume() {
        return masterReceiveVolume;
    }

    public void setMasterReceiveVolume(String masterReceiveVolume) {
        this.masterReceiveVolume = masterReceiveVolume;
    }

    public String getSpeakerSlideToLatchEnabled() {
        return speakerSlideToLatchEnabled;
    }

    public void setSpeakerSlideToLatchEnabled(String speakerSlideToLatchEnabled) {
        this.speakerSlideToLatchEnabled = speakerSlideToLatchEnabled;
    }
}
