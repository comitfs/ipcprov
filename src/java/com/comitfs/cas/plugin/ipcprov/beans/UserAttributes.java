package com.comitfs.cas.plugin.ipcprov.beans;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAttributes {
    private String loginName;
    private String firstName;
    private String lastName;
    private String password;
    private String title;
    private String email;
    private String locale;
    private String displayDateFormat;
    private String timeDisplayFormat;
    private String isTemporary;
    private String authType;
    private String locationId;
    private String accountPolicyId;
    private String passwordChangeMode;
    private String accountActive;
    private String authDomainId;
    private Integer userCDIID;

    public String getAccountActive() {
        return accountActive;
    }

    public void setAccountActive(String accountActive) {
        this.accountActive = accountActive;
    }

    public String getPasswordChangeMode() {
        return passwordChangeMode;
    }

    public void setPasswordChangeMode(String passwordChangeMode) {
        this.passwordChangeMode = passwordChangeMode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDisplayDateFormat() {
        return displayDateFormat;
    }

    public void setDisplayDateFormat(String displayDateFormat) {
        this.displayDateFormat = displayDateFormat;
    }

    public String getTimeDisplayFormat() {
        return timeDisplayFormat;
    }

    public void setTimeDisplayFormat(String timeDisplayFormat) {
        this.timeDisplayFormat = timeDisplayFormat;
    }

    public String getIsTemporary() {
        return isTemporary;
    }

    public void setIsTemporary(String isTemporary) {
        this.isTemporary = isTemporary;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getAccountPolicyId() {
        return accountPolicyId;
    }

    public void setAccountPolicyId(String accountPolicyId) {
        this.accountPolicyId = accountPolicyId;
    }

    public String getAuthDomainId() {
        return authDomainId;
    }

    public void setAuthDomainId(String authDomainId) {
        this.authDomainId = authDomainId;
    }

	public Integer getUserCDIID() {
		return userCDIID;
	}

	public void setUserCDIID(Integer userCDIID) {
		this.userCDIID = userCDIID;
	}
    
}
