package com.comitfs.cas.plugin.ipcprov.beans;


import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigInteger;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ButtonSettingsBean {


    //ButtonBasicInfo
    private String buttonId;
    private int buttonNumber;
    private String buttonType;
    private String buttonLabel;
    private String lockedForProgrammingAtCDI;


    //ButtonDetails

    private String incomingActionRings;
    private String incomingActionPriority;
    private String incomingActionFloat;
    private String displayIncomingCLI;
    private String displayInCallHistory;
    private String ringtone;
    private String divertReason;
    private String uiName;

 //   @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String speedDialType;
//    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String oneButtonDivertDestination;
  //  @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String getOneButtonDivertReason;
 //   @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String icmNumberToDial;
 //   @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String directoryType;
//    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private BigInteger pointOfContactId;
    private String autoSignal;
    private String  autoHuntEnabled;

 //   @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String destinationOrNumberToDial;
//   @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String personalPointOfContactId;

 //   @JsonIgnore
    private List<ResourceAORLineBean> resourceAORBeanList;

 //   @JsonIgnore
    private String speedDialTypeForSimplexConference;
    private List<String>  keyCodeValue;
    private String buttonColor;
    private ResourceAORLineBean line;

    public ResourceAORLineBean getLine() {
        return line;
    }

    public void setLine(ResourceAORLineBean line) {
        this.line = line;
    }

    public String getUiName() {
        return uiName;
    }

    public void setUiName(String uiName) {
        this.uiName = uiName;
    }

    public String getDivertReason() {
        return divertReason;
    }

    public void setDivertReason(String divertReason) {
        this.divertReason = divertReason;
    }


    public String getAutoHuntEnabled() {
        return autoHuntEnabled;
    }

    public void setAutoHuntEnabled(String autoHuntEnabled) {
        this.autoHuntEnabled = autoHuntEnabled;
    }

    public String getPersonalPointOfContactId() {
        return personalPointOfContactId;
    }

    public void setPersonalPointOfContactId(String personalPointOfContactId) {
        this.personalPointOfContactId = personalPointOfContactId;
    }


    public List<ResourceAORLineBean> getResourceAORBeanList() {
        return resourceAORBeanList;
    }


    public String getSpeedDialTypeForSimplexConference() {
        return speedDialTypeForSimplexConference;
    }

    public void setSpeedDialTypeForSimplexConference(String speedDialTypeForSimplexConference) {
        this.speedDialTypeForSimplexConference = speedDialTypeForSimplexConference;
    }

    public void setResourceAORBeanList(List<ResourceAORLineBean> resourceAORBeanList) {
        this.resourceAORBeanList = resourceAORBeanList;
    }

    public String getDestinationOrNumberToDial() {
        return destinationOrNumberToDial;
    }

    public void setDestinationOrNumberToDial(String destinationOrNumberToDial) {
        this.destinationOrNumberToDial = destinationOrNumberToDial;
    }

    public String getAutoSignal() {
        return autoSignal;
    }

    public void setAutoSignal(String autoSignal) {
        this.autoSignal = autoSignal;
    }

    public BigInteger getPointOfContactId() {
        return pointOfContactId;
    }

    public void setPointOfContactId(BigInteger pointOfContactId) {
        this.pointOfContactId = pointOfContactId;
    }

    public String getDirectoryType() {
        return directoryType;
    }

    public void setDirectoryType(String directoryType) {
        this.directoryType = directoryType;
    }

    public String getIcmNumberToDial() {
        return icmNumberToDial;
    }

    public void setIcmNumberToDial(String icmNumberToDial) {
        this.icmNumberToDial = icmNumberToDial;
    }

    public String getOneButtonDivertDestination() {
        return oneButtonDivertDestination;
    }

    public void setOneButtonDivertDestination(String oneButtonDivertDestination) {
        this.oneButtonDivertDestination = oneButtonDivertDestination;
    }

    public String getGetOneButtonDivertReason() {
        return getOneButtonDivertReason;
    }

    public void setGetOneButtonDivertReason(String getOneButtonDivertReason) {
        this.getOneButtonDivertReason = getOneButtonDivertReason;
    }

    public String getSpeedDialType() {
        return speedDialType;
    }

    public void setSpeedDialType(String speedDialType) {
        this.speedDialType = speedDialType;
    }


    public String getRingtone() {
        return ringtone;
    }

    public void setRingtone(String ringtone) {
        this.ringtone = ringtone;
    }

    public String getDisplayInCallHistory() {
        return displayInCallHistory;
    }

    public void setDisplayInCallHistory(String displayInCallHistory) {
        this.displayInCallHistory = displayInCallHistory;
    }

    public String getDisplayIncomingCLI() {
        return displayIncomingCLI;
    }

    public void setDisplayIncomingCLI(String displayIncomingCLI) {
        this.displayIncomingCLI = displayIncomingCLI;
    }

    public String getIncomingActionFloat() {
        return incomingActionFloat;
    }

    public void setIncomingActionFloat(String incomingActionFloat) {
        this.incomingActionFloat = incomingActionFloat;
    }

    public String getIncomingActionPriority() {
        return incomingActionPriority;
    }

    public void setIncomingActionPriority(String incomingActionPriority) {
        this.incomingActionPriority = incomingActionPriority;
    }

    public String getIncomingActionRings() {
        return incomingActionRings;
    }

    public void setIncomingActionRings(String incomingActionRings) {
        this.incomingActionRings = incomingActionRings;
    }

    public String getLockedForProgrammingAtCDI() {
        return lockedForProgrammingAtCDI;
    }

    public void setLockedForProgrammingAtCDI(String lockedForProgrammingAtCDI) {
        this.lockedForProgrammingAtCDI = lockedForProgrammingAtCDI;
    }

    public String getButtonLabel() {
        return buttonLabel;
    }

    public void setButtonLabel(String buttonLabel) {
        this.buttonLabel = buttonLabel;
    }

    public String getButtonType() {
        return buttonType;
    }

    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }


    public int getButtonNumber() {
        return buttonNumber;
    }

    public void setButtonNumber(int buttonNumber) {
        this.buttonNumber = buttonNumber;
    }

    public String getButtonId() {
        return buttonId;
    }

    public void setButtonId(String buttonId) {
        this.buttonId = buttonId;
    }

	public List<String> getKeyCodeValue() {
		return keyCodeValue;
	}

	public void setKeyCodeValue(List<String> keyCodeValue) {
		this.keyCodeValue = keyCodeValue;
	}

	public String getButtonColor() {
		return buttonColor;
	}

	public void setButtonColor(String buttonColor) {
		this.buttonColor = buttonColor;
	}

	
	
	
    
    
}
