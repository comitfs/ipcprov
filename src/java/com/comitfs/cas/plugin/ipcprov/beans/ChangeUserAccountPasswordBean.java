package com.comitfs.cas.plugin.ipcprov.beans;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChangeUserAccountPasswordBean {

    private String newPassword;
    private String passwordChangeMode;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getPasswordChangeMode() {
        return passwordChangeMode;
    }

    public void setPasswordChangeMode(String passwordChangeMode) {
        this.passwordChangeMode = passwordChangeMode;
    }
}
