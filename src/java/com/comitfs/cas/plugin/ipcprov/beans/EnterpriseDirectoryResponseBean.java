package com.comitfs.cas.plugin.ipcprov.beans;

import java.util.List;

public class EnterpriseDirectoryResponseBean {
    private long totalEnterpriseContactRecs;
    private int pageNum;
    private int count;
    private List<PersonalContactsBean> enterpriseContacts;

    public long getTotalEnterpriseContactRecs() {
        return totalEnterpriseContactRecs;
    }

    public void setTotalEnterpriseContactRecs(long totalEnterpriseContactRecs) {
        this.totalEnterpriseContactRecs = totalEnterpriseContactRecs;
    }

    public List<PersonalContactsBean> getEnterpriseContacts() {
        return enterpriseContacts;
    }

    public void setEnterpriseContacts(List<PersonalContactsBean> enterpriseContacts) {
        this.enterpriseContacts = enterpriseContacts;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
