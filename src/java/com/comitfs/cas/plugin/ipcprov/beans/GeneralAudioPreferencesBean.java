package com.comitfs.cas.plugin.ipcprov.beans;


public class GeneralAudioPreferencesBean {
    private String callRinger;
    private String ringerVolume;
    private String handsetReceiveVolume;
    private String handsetTransmitVolume;
    private String handsetSideToneAttenuation;
    private String handsetRxNoiseReduceMode;
    private String handsetTxNoiseReduceMode;
    private String handsetRxEqualization;
    private String isHSWideBand;
    private String speakerMasterRxVolume1;
    private String speakerTxNoiseReduceMode;
    private String speakerTransmitVolume;
    private String speakerRxEqualization;
    private String speakerSummingMode;
    private String handsFreeRxEqualization;
    private String handsFreeMRxVolume;
    private String hfmTxAutoGainControlMode;
    private String hfmTxNoiseReduceMode;
    private String handsFreeMTransmitVolume;
    private String ipRecordOutputGain;
    private String speakerIPRecordMicMixGain;
    private String handsetIPRecordMicMixGain;
    private String recordMixProfiles;
    private String recordProtocol;
    private String accousticFeedbackReduction;
    private String replayPlaybackOutput;

    public String getAccousticFeedbackReduction() {
        return accousticFeedbackReduction;
    }

    public void setAccousticFeedbackReduction(String accousticFeedbackReduction) {
        this.accousticFeedbackReduction = accousticFeedbackReduction;
    }

    public String getHfmTxNoiseReduceMode() {
        return hfmTxNoiseReduceMode;
    }

    public void setHfmTxNoiseReduceMode(String hfmTxNoiseReduceMode) {
        this.hfmTxNoiseReduceMode = hfmTxNoiseReduceMode;
    }

    public String getHfmTxAutoGainControlMode() {
        return hfmTxAutoGainControlMode;
    }

    public void setHfmTxAutoGainControlMode(String hfmTxAutoGainControlMode) {
        this.hfmTxAutoGainControlMode = hfmTxAutoGainControlMode;
    }

    public String getHandsFreeRxEqualization() {
        return handsFreeRxEqualization;
    }

    public void setHandsFreeRxEqualization(String handsFreeRxEqualization) {
        this.handsFreeRxEqualization = handsFreeRxEqualization;
    }

    public String getSpeakerSummingMode() {
        return speakerSummingMode;
    }

    public void setSpeakerSummingMode(String speakerSummingMode) {
        this.speakerSummingMode = speakerSummingMode;
    }

    public String getSpeakerRxEqualization() {
        return speakerRxEqualization;
    }

    public void setSpeakerRxEqualization(String speakerRxEqualization) {
        this.speakerRxEqualization = speakerRxEqualization;
    }

    public String getSpeakerTxNoiseReduceMode() {
        return speakerTxNoiseReduceMode;
    }

    public void setSpeakerTxNoiseReduceMode(String speakerTxNoiseReduceMode) {
        this.speakerTxNoiseReduceMode = speakerTxNoiseReduceMode;
    }

    public String getIsHSWideBand() {
        return isHSWideBand;
    }

    public void setIsHSWideBand(String isHSWideBand) {
        this.isHSWideBand = isHSWideBand;
    }

    public String getHandsetRxEqualization() {
        return handsetRxEqualization;
    }

    public void setHandsetRxEqualization(String handsetRxEqualization) {
        this.handsetRxEqualization = handsetRxEqualization;
    }

    public String getHandsetTxNoiseReduceMode() {
        return handsetTxNoiseReduceMode;
    }

    public void setHandsetTxNoiseReduceMode(String handsetTxNoiseReduceMode) {
        this.handsetTxNoiseReduceMode = handsetTxNoiseReduceMode;
    }

    public String getHandsetRxNoiseReduceMode() {
        return handsetRxNoiseReduceMode;
    }

    public void setHandsetRxNoiseReduceMode(String handsetRxNoiseReduceMode) {
        this.handsetRxNoiseReduceMode = handsetRxNoiseReduceMode;
    }

    public String getHandsetSideToneAttenuation() {
        return handsetSideToneAttenuation;
    }

    public void setHandsetSideToneAttenuation(String handsetSideToneAttenuation) {
        this.handsetSideToneAttenuation = handsetSideToneAttenuation;
    }

    public String getCallRinger() {
        return callRinger;
    }

    public void setCallRinger(String callRinger) {
        this.callRinger = callRinger;
    }

    public String getRingerVolume() {
        return ringerVolume;
    }

    public void setRingerVolume(String ringerVolume) {
        this.ringerVolume = ringerVolume;
    }

    public String getHandsetReceiveVolume() {
        return handsetReceiveVolume;
    }

    public void setHandsetReceiveVolume(String handsetReceiveVolume) {
        this.handsetReceiveVolume = handsetReceiveVolume;
    }

    public String getHandsetTransmitVolume() {
        return handsetTransmitVolume;
    }

    public void setHandsetTransmitVolume(String handsetTransmitVolume) {
        this.handsetTransmitVolume = handsetTransmitVolume;
    }

    public String getSpeakerMasterRxVolume1() {
        return speakerMasterRxVolume1;
    }

    public void setSpeakerMasterRxVolume1(String speakerMasterRxVolume1) {
        this.speakerMasterRxVolume1 = speakerMasterRxVolume1;
    }

    public String getSpeakerTransmitVolume() {
        return speakerTransmitVolume;
    }

    public void setSpeakerTransmitVolume(String speakerTransmitVolume) {
        this.speakerTransmitVolume = speakerTransmitVolume;
    }

    public String getHandsFreeMRxVolume() {
        return handsFreeMRxVolume;
    }

    public void setHandsFreeMRxVolume(String handsFreeMRxVolume) {
        this.handsFreeMRxVolume = handsFreeMRxVolume;
    }

    public String getHandsFreeMTransmitVolume() {
        return handsFreeMTransmitVolume;
    }

    public void setHandsFreeMTransmitVolume(String handsFreeMTransmitVolume) {
        this.handsFreeMTransmitVolume = handsFreeMTransmitVolume;
    }

    public String getIpRecordOutputGain() {
        return ipRecordOutputGain;
    }

    public void setIpRecordOutputGain(String ipRecordOutputGain) {
        this.ipRecordOutputGain = ipRecordOutputGain;
    }

    public String getSpeakerIPRecordMicMixGain() {
        return speakerIPRecordMicMixGain;
    }

    public void setSpeakerIPRecordMicMixGain(String speakerIPRecordMicMixGain) {
        this.speakerIPRecordMicMixGain = speakerIPRecordMicMixGain;
    }

    public String getHandsetIPRecordMicMixGain() {
        return handsetIPRecordMicMixGain;
    }

    public void setHandsetIPRecordMicMixGain(String handsetIPRecordMicMixGain) {
        this.handsetIPRecordMicMixGain = handsetIPRecordMicMixGain;
    }

    public String getRecordMixProfiles() {
        return recordMixProfiles;
    }

    public void setRecordMixProfiles(String recordMixProfiles) {
        this.recordMixProfiles = recordMixProfiles;
    }

    public String getRecordProtocol() {
        return recordProtocol;
    }

    public void setRecordProtocol(String recordProtocol) {
        this.recordProtocol = recordProtocol;
    }

    public String getReplayPlaybackOutput() {
        return replayPlaybackOutput;
    }

    public void setReplayPlaybackOutput(String replayPlaybackOutput) {
        this.replayPlaybackOutput = replayPlaybackOutput;
    }


}
