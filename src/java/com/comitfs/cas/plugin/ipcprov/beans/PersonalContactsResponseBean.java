package com.comitfs.cas.plugin.ipcprov.beans;


import java.util.List;

import com.comitfs.cas.plugin.ipcprov.beans.PersonalContactsBean;

public class PersonalContactsResponseBean {
    private List<PersonalContactsBean> personalContacts;

    public List<PersonalContactsBean> getPersonalContacts() {
        return personalContacts;
    }

    public void setPersonalContacts(List<PersonalContactsBean> personalContacts) {
        this.personalContacts = personalContacts;
    }
}
