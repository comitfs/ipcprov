package com.comitfs.cas.plugin.ipcprov.beans;

import com.fasterxml.jackson.annotation.JsonInclude;

public class TraderFeaturesBean {
    private String personalExtn;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String personalExtnOverride;
    private String intercomExtn;
    private String autoSelectHold;
    private String language;
    private String allowPrivacyToggle;
    private String alternateHSTimeout;
    private String blockingToneOnSpeakerMute;
    private String handsetSelectMode;
    private String handSetMuteOption;
    private String handSetButtonActions;
    private String floatAllIncomingCalls;
    private String floatCallsOnHold;
    private String forceTalkBackMute;
    private String privacyDefaultsToEnable;

    public String getForceTalkBackMute() {
        return forceTalkBackMute;
    }

    public void setForceTalkBackMute(String forceTalkBackMute) {
        this.forceTalkBackMute = forceTalkBackMute;
    }

    public String getFloatCallsOnHold() {
        return floatCallsOnHold;
    }

    public void setFloatCallsOnHold(String floatCallsOnHold) {
        this.floatCallsOnHold = floatCallsOnHold;
    }

    public String getFloatAllIncomingCalls() {
        return floatAllIncomingCalls;
    }

    public void setFloatAllIncomingCalls(String floatAllIncomingCalls) {
        this.floatAllIncomingCalls = floatAllIncomingCalls;
    }

    public String getHandSetButtonActions() {
        return handSetButtonActions;
    }

    public void setHandSetButtonActions(String handSetButtonActions) {
        this.handSetButtonActions = handSetButtonActions;
    }

    public String getHandSetMuteOption() {
        return handSetMuteOption;
    }

    public void setHandSetMuteOption(String handSetMuteOption) {
        this.handSetMuteOption = handSetMuteOption;
    }

    public String getHandsetSelectMode() {
        return handsetSelectMode;
    }

    public void setHandsetSelectMode(String handsetSelectMode) {
        this.handsetSelectMode = handsetSelectMode;
    }

    public String getPersonalExtn() {
        return personalExtn;
    }

    public void setPersonalExtn(String personalExtn) {
        this.personalExtn = personalExtn;
    }

    public String getPersonalExtnOverride() {
        return personalExtnOverride;
    }

    public void setPersonalExtnOverride(String personalExtnOverride) {
        this.personalExtnOverride = personalExtnOverride;
    }

    public String getIntercomExtn() {
        return intercomExtn;
    }

    public void setIntercomExtn(String intercomExtn) {
        this.intercomExtn = intercomExtn;
    }

    public String getAutoSelectHold() {
        return autoSelectHold;
    }

    public void setAutoSelectHold(String autoSelectHold) {
        this.autoSelectHold = autoSelectHold;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAllowPrivacyToggle() {
        return allowPrivacyToggle;
    }

    public void setAllowPrivacyToggle(String allowPrivacyToggle) {
        this.allowPrivacyToggle = allowPrivacyToggle;
    }

    public String getAlternateHSTimeout() {
        return alternateHSTimeout;
    }

    public void setAlternateHSTimeout(String alternateHSTimeout) {
        this.alternateHSTimeout = alternateHSTimeout;
    }

    public String getBlockingToneOnSpeakerMute() {
        return blockingToneOnSpeakerMute;
    }

    public void setBlockingToneOnSpeakerMute(String blockingToneOnSpeakerMute) {
        this.blockingToneOnSpeakerMute = blockingToneOnSpeakerMute;
    }

    public String getPrivacyDefaultsToEnable() {
        return privacyDefaultsToEnable;
    }

    public void setPrivacyDefaultsToEnable(String privacyDefaultsToEnable) {
        this.privacyDefaultsToEnable = privacyDefaultsToEnable;
    }
}
