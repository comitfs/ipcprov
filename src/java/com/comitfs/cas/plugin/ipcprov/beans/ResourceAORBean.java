package com.comitfs.cas.plugin.ipcprov.beans;


import com.comitfs.cas.ipc.bw.jaxb.ResourceAORType;
import lombok.Getter;

@Getter
public class ResourceAORBean {

    private final String resourceAORId;
    private final ResourceAORType aorType;
    private final boolean signaling;
    private final Integer aorId;
    private final String lineAppearance;

    public ResourceAORBean(String resourceAORId, Integer aorId, String lineAppearance, ResourceAORType aorType, boolean signaling) {
        this.resourceAORId = resourceAORId;
        this.aorType = aorType;
        this.lineAppearance = lineAppearance;
        this.signaling = signaling;
        this.aorId = aorId;
    }
}
