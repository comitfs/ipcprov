package com.comitfs.cas.plugin.ipcprov.beans;


public class ForceLogInLogOutBean {

    private String operationType;
    private String deviceId;
    private String ignoreOpenHandSetCalls;


    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getIgnoreOpenHandSetCalls() {
        return ignoreOpenHandSetCalls;
    }

    public void setIgnoreOpenHandSetCalls(String ignoreOpenHandSetCalls) {
        this.ignoreOpenHandSetCalls = ignoreOpenHandSetCalls;
    }
}
