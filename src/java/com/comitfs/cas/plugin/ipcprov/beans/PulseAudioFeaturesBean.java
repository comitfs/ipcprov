package com.comitfs.cas.plugin.ipcprov.beans;


public class PulseAudioFeaturesBean {
    private String speakerLatching;
    private String hfmTransitionDevice;
    private String speakerAudioOutput;
    private String usbReceiveVolume;
    private String usbTransmitVolume;
    private String icmAutoAnswer;
    private String icmStartOnHfm;
    private String displayNameNumber;

    public String getSpeakerLatching() {
        return speakerLatching;
    }

    public void setSpeakerLatching(String speakerLatching) {
        this.speakerLatching = speakerLatching;
    }

    public String getHfmTransitionDevice() {
        return hfmTransitionDevice;
    }

    public void setHfmTransitionDevice(String hfmTransitionDevice) {
        this.hfmTransitionDevice = hfmTransitionDevice;
    }

    public String getSpeakerAudioOutput() {
        return speakerAudioOutput;
    }

    public void setSpeakerAudioOutput(String speakerAudioOutput) {
        this.speakerAudioOutput = speakerAudioOutput;
    }

    public String getUsbReceiveVolume() {
        return usbReceiveVolume;
    }

    public void setUsbReceiveVolume(String usbReceiveVolume) {
        this.usbReceiveVolume = usbReceiveVolume;
    }

    public String getUsbTransmitVolume() {
        return usbTransmitVolume;
    }

    public void setUsbTransmitVolume(String usbTransmitVolume) {
        this.usbTransmitVolume = usbTransmitVolume;
    }

    public String getIcmAutoAnswer() {
        return icmAutoAnswer;
    }

    public void setIcmAutoAnswer(String icmAutoAnswer) {
        this.icmAutoAnswer = icmAutoAnswer;
    }

    public String getIcmStartOnHfm() {
        return icmStartOnHfm;
    }

    public void setIcmStartOnHfm(String icmStartOnHfm) {
        this.icmStartOnHfm = icmStartOnHfm;
    }

    public String getDisplayNameNumber() {
        return displayNameNumber;
    }

    public void setDisplayNameNumber(String displayNameNumber) {
        this.displayNameNumber = displayNameNumber;
    }
}
