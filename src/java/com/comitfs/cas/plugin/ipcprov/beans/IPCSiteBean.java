package com.comitfs.cas.plugin.ipcprov.beans;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@XmlRootElement(name="ipcsite")
@JsonIgnoreProperties
public class IPCSiteBean {
	private long siteId;
	private String siteName;
}
