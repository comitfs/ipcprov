package com.comitfs.cas.plugin.ipcprov.beans;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class DeviceLoginStatusBean {
    private Integer deviceId;
    private String deviceIpAddr;
    private String LoginName;
    protected String mac;
}
